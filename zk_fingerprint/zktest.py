import sys
#sys.path.append("zklib")
from zklib import zklib
from socket import SOCK_STREAM, SOCK_DGRAM
print "Connecting to %s port %s" % (str(sys.argv[1]), int(sys.argv[2]))
zk = zklib.ZKLib(str(sys.argv[1]), int(sys.argv[2]))
ret = zk.connect()
print "connection:", ret
if ret:
    print "Disable Device", zk.disableDevice()

    print "ZK Version:", zk.version()
    print "OS Version:", zk.osversion()
    print "Platform:", zk.platform()
    print "Platform Version:", zk.fmVersion()
    print "Work Code:", zk.workCode()
    print "SSR:", zk.ssr()
    print "Pin Width:", zk.pinWidth()
    print "Face Function On:", zk.faceFunctionOn()
    print "Serial Number:", zk.serialNumber()
    print "Device Name:", zk.deviceName()

    data_user = zk.getUser()
    print "Get User:"
    if data_user:
        for uid in data_user:

            if data_user[uid][2] == 14:
                level = 'Admin'
            else:
                level = 'User'
            print "[UID %d]: ID: %s, Name: %s, Level: %s, Password: %s" % (
                uid, data_user[uid][0], data_user[uid][1], level, data_user[uid][3])

    # print "Clearing  Data!"
    # clean_data = zk.clearAttendance()
    # print clean_data

    print "Get Attendance:"
    attendance = zk.getAttendance()
    if attendance:
        for lattendance in attendance:
            if lattendance[1] == 0:
                state = 'sign_in'
            elif lattendance[1] == 1:
                state = 'sign_out'
            else:
                state = 'Undefined'
            print "date %s, Time %s, ID: %s, Status: %s, STAT: %s" % (
                lattendance[2].date(), lattendance[2].time(), lattendance[0], state,
                lattendance[1])
    else:
        print "Error"

    print "Get Time:", zk.getTime()
    print "Enable Device", zk.enableDevice()
    print "Disconnect:", zk.disconnect()
