# -*- coding: utf-8 -*-
###############################################################################
#
#    Think42Labs.com
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#/zk_fingerprint/controlle
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import base64
import openerp
import openerp.modules.registry
import ast

import logging
import werkzeug
from openerp.addons.auth_signup.res_users import SignupError
from openerp.addons.web.controllers.main import ensure_db

from openerp import http
from openerp.http import request
from openerp.http import Response
from openerp.addons.web.controllers.main import Home
from openerp.addons.web.controllers.main import Session
from openerp.addons.web.controllers import main
import datetime
import pytz
import xmlrpclib
import json
import psycopg2
from openerp import models, fields, _



class zk_home(Home):

    @http.route('/', type='http', auth="none")
    def index(self, s_action=None, db=None, **kw):
        return http.local_redirect('/web', query=request.params, keep_hash=True)

    @http.route('/zk_fingerprint/controller/update', type='json', auth='none')
    def update_attendance_zk(self, **kw): # update_attendance_zk
        
        if not request.uid:
            request.uid = openerp.SUPERUSER_ID
            cr, uid, context, registry = request.cr, openerp.SUPERUSER_ID, request.context, request.registry
            attendance_data = request.params['attendance_data']
            emp_data = registry.get('hr.employee')
            att_data = registry.get('hr.attendance')
            cur_date = datetime.datetime.now().date().strftime('%Y-%m-%d')
            stime = datetime.datetime.now().time().strftime('%H:%M:%S')
            cur_time = datetime.datetime.strptime(stime, '%H:%M:%S').time()
            for attendance in attendance_data:
                emp_search = emp_data.search(cr, uid,[('otherid', '=', attendance['user_id'])])
                if not emp_search:
                    
                    continue

                att_search = att_data.search(cr,uid,[('employee_id','=',emp_search[0])])
                att_bro = att_data.browse(cr,uid,att_search)
               
                if not att_bro:
                    att_res = att_data.create(cr,uid,
                                                {'name': attendance['timestamp'],
                                                'employee_id': emp_search[0],
                                                'action': 'sign_in'})
                    
                    continue
                att_id = []
                for att in att_bro:
                    att_id.append(att.id)
                att_id = sorted(att_id)
                for att in att_bro:
                    if att.id == att_id[-1]:
                        if att.name == attendance['timestamp']:
                            break
                        if att.name.split(' ')[0] == cur_date and att.action == 'sign_in':
                            att_res = att_data.create(cr,uid, {'name': attendance['timestamp'], 
                                                            'employee_id': emp_search[0], 
                                                            'action': 'sign_out'})
                           
                            break
                        if att.name.split(' ')[0] == cur_date and att.action == 'sign_out':
                            break
                        att_res = att_data.create(cr,uid, {'name': attendance['timestamp'], 
                                                            'employee_id': emp_search[0], 
                                                            'action': 'sign_in'})
                       
                att_id = []
                    

        request.params['status']=True
        return request.params


    @http.route('/zk_fingerprint/get_user/update', type='json', auth='none')
    def update_user_zk(self, **kw): # get user from device
        if not request.uid:
            request.uid = openerp.SUPERUSER_ID
            cr, uid, context, registry = request.cr, openerp.SUPERUSER_ID, request.context, request.registry
            user_data = request.params['user_data']
            zkuser_data = registry.get('zk.user')
            for user in user_data:
                zkusr_bro = zkuser_data.search(cr,uid,[('zk_user_id','=',user['user_id'])])
                #print zkusr_bro
                if not zkusr_bro:
                    #print "no data : ", user['user_id']
                    usr_res = zkuser_data.create(cr,uid,
                                                    {'name': user['uid'],
                                                    'zk_user_id': int(user['user_id']),
                                                    })
                    #print usr_res

        request.params['status']=True
        return request.params