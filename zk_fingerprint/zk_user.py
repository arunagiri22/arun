# -*- coding: utf-8 -*-


from openerp import fields, models,api


class ZKuser(models.Model):
    _name='zk.user'
    
    zk_user_id=fields.Integer('Id in Device')
    name=fields.Char('Name in Device')
    employee_id = fields.Many2one('hr.employee', 'Related employee')
    device_id = fields.Many2one('zk.fingerprint.devices', 'Biometric Device')
    
    
    #~ _sql_constraints = [
        #~ ('employee_id_uniq', 'unique (employee_id)',
         #~ 'It is not possible relate an employee with a biometric user '
         #~ 'more than once!'),
    #~ ]
#~ 
    #~ _sql_constraints = [
        #~ ('device_id_uniq', 'unique (device_id)',
         #~ 'It is not possible to create more than one '
         #~ 'with the same biometric Id'),
    #~ ]
    
    
   
class hr_employee(models.Model):
    _inherit='hr.employee'
    
    zk_id=fields.Many2one('zk.user' ,'Bio User')
    
   
    @api.multi
    @api.onchange('zk_id')
    def onchange_user_bio(self):
        for rec in self:
            rec.otherid=rec.zk_id.zk_user_id
