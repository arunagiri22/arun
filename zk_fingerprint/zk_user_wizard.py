# -*- coding: utf-8 -*-

from openerp import api, fields, models


class BiometricUser(models.TransientModel):
    _name = 'biometric.user.wizard'
    
    device_id = fields.Many2one('zk.fingerprint.devices', 'Biometric Device')
    
    
    def import_users(self, cr, uid, ids, context):
        """
        wrapper function
        """
        for biometric_import_user in self.browse(cr, uid, ids, context):
            biometric_import_user.create_users_in_openerp()

    @api.model
    def create_users_in_openerp(self):
        self.device_id.create_user()
    
