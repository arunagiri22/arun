# -*- coding: utf-8 -*-

from openerp import models, fields, api, exceptions
from zklib import zklib
from operator import itemgetter
from itertools import groupby
from datetime import datetime
from pytz import timezone, utc
import logging

_logger = logging.getLogger(__name__)


class GetAttendance(models.TransientModel):
    _name = 'zk.fingerprint.wizard.attendance'
    _description = 'Attendance wizard'

    device_id = fields.Many2one('zk.fingerprint.devices', domain=[('active', '=', True)])
    employee_id = fields.Many2one('hr.employee')
    date_from = fields.Date('From', default=fields.Date.today())
    date_to = fields.Date('To', default=fields.Date.today())
    all_data = fields.Boolean(default=True)

    @api.constrains('date_from', 'date_to')
    def check_date(self):
        for record in self:
            if record.date_from > record.date_to:
                raise ValueError('Date From should be older than Date To')

    #~ def _get_time(self,name,employee):
        #~ reason_pool = self.env['hr.action.reason']
        #~ reason = reason_pool.search([('category_id', '=', employee.category_ids[0].id),('action_type','=','sign_in')])
        #~ print "SADSADASSDASASDASDSDDSAAS",reason.total_hours,name,type(name)
        #~ return reason.total_hours <= float(str(name).split(' ')[1].split(':')[0])
    
    
    
    def _group_attendance(self, attendance):
        g_list = []
        js_list = []
        if self.all_data:
            att_list = [
                (att[0], att[1], att[2].date(), att[2].time()) for att in attendance
            ]
        elif self.employee_id:
            att_list = [
                (att[0], att[1], att[2].date(), att[2].time()) for att in attendance if
                att[0] == self.employee_id.otherid and
                fields.Date.from_string(self.date_from) <= att[2].date() <= fields.Date.from_string(
                    self.date_to)
            ]
        else:
            att_list = [
                (att[0], att[1], att[2].date(), att[2].time()) for att in attendance if
                fields.Date.from_string(self.date_from) <= att[2].date() <= fields.Date.from_string(
                    self.date_to)
            ]
        att_list.sort(key=itemgetter(0))
        att_group = groupby(att_list, itemgetter(0))
        for k, g in att_group:
            g_list.append(list(g))
        if g_list:
            for j in g_list:
                js_group = groupby(j, itemgetter(2))
                for k, g in js_group:
                    js_list.append(list(g))
        return js_list

    @api.one
    @api.depends('device', 'date_from', 'date_to')
    def get_attendance_button(self):
        for record in self:
            zk = zklib.ZKLib(record.device_id.ip_address, int(record.device_id.ip_port))
            if not zk.connect():
                raise exceptions.Warning('Error connecting to device: %s' % record.device_id.name)
            _logger.info('Device %s Connected!' % record.device_id.name)
            zk.enableDevice()
            zk.disableDevice()
            attendance = zk.getAttendance()
            sign_ins=[]
            if not attendance:
                zk.enableDevice()
                zk.disconnect()
                raise exceptions.Warning('Unable to get attendance records...!')

            _logger.info('Attendance Found...!')
            js_list = self._group_attendance(attendance)
            if js_list:
                _logger.info('Records Sorted!')
                local_tz = timezone(self.device_id.tz)
                for att in js_list:
                    employee_id = self.env['hr.employee'].search([
                        ('otherid', '=', att[0][0])
                    ])
                    if not employee_id:
                        _logger.warning("No linked employee for record: %s" % att[0][0])
                        continue
                    _logger.info("Employee: %s" % employee_id.name)
                    if len(att) == 1:
                        _logger.info("Single record: %s" % att)
                        att_ts = datetime.combine(att[0][2], att[0][3])
                        local_timestamp = local_tz.localize(att_ts, is_dst=None)
                        local_timestamp = local_timestamp.astimezone(utc)
                        timestamp = local_timestamp.replace(tzinfo=None)
                        # Check if shift employee to use device state instead
                        if employee_id.is_night_shift:
                            state = 'sign_in' if att[0][1] == 0 else 'sign_out'
                            if not self.env['hr.attendance'].search([
                                ('employee_id', '=', employee_id.id),
                                ('name', '=', fields.Datetime.to_string(timestamp)),
                                ('action', '=', state)
                            ]):
                                try:
                                    att_id = self.env['hr.attendance'].create({
                                        'name': fields.Datetime.to_string(timestamp),
                                        'employee_id': employee_id.id,
                                        'action': state,
                                        'device_id': record.device_id.id,
                                    })
                                    _logger.info("Att ID: %s" % att_id)
                                except:
                                    continue
                            continue
                        if not self.env['hr.attendance'].search([
                            ('employee_id', '=', employee_id.id),
                            ('name', '=', fields.Datetime.to_string(timestamp)),
                            ('action', '=', 'sign_in')
                        ]):
                            try:
                                att_id = self.env['hr.attendance'].create({
                                    'name': fields.Datetime.to_string(timestamp),
                                    'employee_id': employee_id.id,
                                    'action': 'sign_in',
                                    #~ 'is_absent':self._get_time(fields.Datetime.to_string(timestamp),employee_id),
                                    'device_id': record.device_id.id,
                                })
                                sign_ins.append(att_id)
                                _logger.info("New Single Sign IN ID: %s" % att_id)
                            except:
                                continue
                    else:
                        _logger.info("Multi records: %s, %s" % (len(att), att))
                        min_att = min(att, key=itemgetter(3))
                        max_att = max(att, key=itemgetter(3))
                        min_ts = datetime.combine(min_att[2], min_att[3])
                        max_ts = datetime.combine(max_att[2], max_att[3])
                        local_timestamp_min = local_tz.localize(min_ts, is_dst=None)
                        local_timestamp_min = local_timestamp_min.astimezone(utc)
                        timestamp_min = local_timestamp_min.replace(tzinfo=None)
                        local_timestamp_max = local_tz.localize(max_ts, is_dst=None)
                        local_timestamp_max = local_timestamp_max.astimezone(utc)
                        timestamp_max = local_timestamp_max.replace(tzinfo=None)
                        try:
                            if not self.env['hr.attendance'].search(['&',
                                ('employee_id', '=', employee_id.id),
                                ('name', '=', fields.Datetime.to_string(timestamp_min)),
                                ('action', '=', 'sign_in')
                            ]):
                                att_id = self.env['hr.attendance'].create({
                                    'name': fields.Datetime.to_string(timestamp_min),
                                    'employee_id': employee_id.id,
                                    'action': 'sign_in',
                                    'device_id': record.device_id.id,
                                    #~ 'is_absent':self._get_time(fields.Datetime.to_string(timestamp_min),employee_id),
                                    
                                })
                                sign_ins.append(att_id)
                                _logger.info("Sign IN ID: %s" % att_id)
                            if not self.env['hr.attendance'].search([
                                ('name', '>', fields.Datetime.to_string(timestamp_min)),
                                ('name', '<=', fields.Datetime.to_string(timestamp_max)),'&',
                                ('employee_id', '=', employee_id.id),
                                ('action', '=', 'sign_out')
                            ], limit=1, order="name DESC"):
                                att_id = self.env['hr.attendance'].create({
                                    'name': fields.Datetime.to_string(timestamp_max),
                                    'employee_id': employee_id.id,
                                    'action': 'sign_out',
                                    'device_id': record.device_id.id,
                                })
                                _logger.info("Sign OUT ID: %s" % att_id)
                            else:
                                att_obj = self.env['hr.attendance'].search([
                                    ('name', '>', fields.Datetime.to_string(timestamp_min)),
                                    ('name', '<=', fields.Datetime.to_string(timestamp_max)),'&',
                                    ('employee_id', '=', employee_id.id),
                                    ('action', '=', 'sign_out')
                                ], limit=1, order="name DESC")
                                att_obj.write({
                                    'name': fields.Datetime.to_string(timestamp_max),
                                    'device_id': record.device_id.id,
                                })
                                _logger.info("Record UPDATE ID: %s" % att_id)
                        except:
                            continue
            #~ for sign_in in sign_ins:
                #~ is_absent = self._get_time(sign_in.name,sign_in.employee_id)
                #~ print is_absent
                #~ sign_in.write({'is_absent':is_absent})
                #~ leave = sign_in._create_leaves()
                #~ if leave:
                    #~ sign_in.write({'hr_holidays_id':leave.id})
            zk.enableDevice()
            zk.disconnect()
            _logger.info('Device Disconnected: %s' % record.device_id.name)


class ClearAttendance(models.TransientModel):
    _name = 'zk.fingerprint.clear.attendance'

    confirm = fields.Boolean('Check this box to confirm')
    device_id = fields.Integer(default=lambda self: self.id)

    @api.multi
    def clear_attendance(self):
        pass

