This module to help using ZK biodevices (Fingerprint) as it's widely used for attendance purposes.

the structure of the module as below:

- ZKDevices (class)
  name
  ip address

  
- schedule (class)
- device (ZKDevices)
  schedule (cron)

- Attendance (class)
  timestamp (datetime)
  status (In/Out)
  user_id (bio id)
