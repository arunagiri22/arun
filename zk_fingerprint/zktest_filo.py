# -*- coding: utf-8 -*-
import sys
import datetime

sys.path.append("zklib")
from zklib import zklib
from socket import SOCK_STREAM, SOCK_DGRAM
from operator import itemgetter
from itertools import groupby
import pprint

pp = pprint.PrettyPrinter(indent=4)

#zk = zklib.ZKLib("192.168.160.160", 4370)
#zk = zklib.ZKLib("192.168.75.6", 4370)
#zk = zklib.ZKLib("192.168.79.13", 4370)
#zk = zklib.ZKLib("192.168.85.27", 4370)
zk = zklib.ZKLib("kuwait.gulfgate.com", 4380)
ret = zk.connect()
print "connection:", ret

if ret:
    print "Disable Device", zk.disableDevice()
    print "Device Name:", zk.deviceName()
    print "Get Attendance:"
    attendance = zk.getAttendance()
    # attendance = zk.testatt()

    if attendance:
	#print attendance
        g_list = []
        # att_list = [(att[0], att[1], att[2].date(), att[2].time()) for att in attendance if
        #             int(att[0]) == 10000 and att[2].date() == datetime.date(2017, 3, 21)]
        att_list = [(att[0], att[1], att[2].date(), att[2].time()) for att in attendance]
	# print att_list
        att_list.sort(key=itemgetter(0))
        att_group = groupby(att_list, itemgetter(0))
        for k, g in att_group:
            g_list.append(list(g))
	#print g_list
        for j in g_list:
            if len(j) > 0:
                js_list = []
                js_group = groupby(j, itemgetter(2))
                for k, g in js_group:
                    js_list.append(list(g))
		#print js_list
                if js_list:
                    for fin in js_list:
                        if len(fin) > 1:
                            print "----------------- MINMAX -----------------"
                            pp.pprint(min(fin, key=itemgetter(3)))
                            pp.pprint(max(fin, key=itemgetter(3)))
                        else:
                            print "-------------- SINGLE -------------"
                            pp.pprint(fin[0])
    else:
        print "----------------- NO RECORD --------------"
        # pp.pprint (js_list)
        #print "Clearing  Data!"
        #clean_data = zk.clearAttendance()
        #print clean_data

else:
    print "Error"

print "Get Time:", zk.getTime()

print "Enable Device", zk.enableDevice()

print "Disconnect:", zk.disconnect()
