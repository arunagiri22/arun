# -*- coding: utf-8 -*-

from openerp import models, fields, api, exceptions
from zklib import zklib, zkconst
import logging
from pytz import timezone, utc, all_timezones
from datetime import datetime, date
from operator import itemgetter
from itertools import groupby

_logger = logging.getLogger(__name__)


class HREmployee(models.Model):
    _inherit = 'hr.employee'
    
    otherid=fields.Char('Bio ID')
    is_night_shift = fields.Boolean('Night Shift', help="Night shift should overlap between days.")
    warning =fields.Integer('sign-out-Warning' ,compute='warning_signout')
    penalty_signout = fields.Float('Signout Penalty')
    
    @api.one
    def warning_signout(self):
        count=self.env['hr.attendance'].search_count([('employee_id', '=', self.id),('action','!=','sign_out')])
        self.warning=count
    
    
class HRAttendance(models.Model):
    _inherit = 'hr.attendance'

    device_id = fields.Many2one('zk.fingerprint.devices')


class ZKDevices(models.Model):
    _name = 'zk.fingerprint.devices'
    _description = 'ZK devices class definitions'

    @api.model
    def _tz_get(self):
        # put POSIX 'Etc/*' entries at the end to avoid confusing users - see bug 1086728
        return [(tz, tz) for tz in sorted(
            all_timezones, key=lambda tz: tz if not tz.startswith('Etc/') else '_')]

    name = fields.Char(string='Device Name', required=True)
    ip_address = fields.Char(string='IP Address', required=True)
    ip_port = fields.Integer(string='Port Number', required=True, default="4370")
    active = fields.Boolean()
    schedule = fields.Boolean()
    version = fields.Char(readonly=True)
    tz = fields.Selection(_tz_get, string="Time Zone", required=True)
    model = fields.Char(readonly=True)
    
    @api.model
    def getuser(self):
        """
        Function use to get all the registered users
        at the biometric device
        """
        with ConnectToDevice(self.ip_address, self.ip_port) as zk:
            users = zk.getUser()
        return users
    
    @api.model
    def create_user(self):
        """
        function uses to assure that all users are already
        created 
        """
        biometric_user_obj = self.env['zk.user']
        users = self.getuser()
        openerp_users = biometric_user_obj.search([
            ('device_id', '=', self.id), ], )
        openerp_users_id = [user.zk_user_id for user in openerp_users]
        for k,v in users.items():
            if int(v[0]) not in openerp_users_id:
                biometric_user_obj.create({
                     'zk_user_id': int(v[0]),
                     'name': v[1],
                     'device_id': self.id, }
                )
    
    
    def _get_time(self,name,employee):
        reason_pool = self.env['hr.action.reason']
        reason = reason_pool.search([('category_id', '=', employee.category_ids[0].id),('action_type','=','sign_in')])
        return reason.total_hours <= float(str(name).split(' ')[1].split(':')[0])
    
    @api.one
    @api.depends('ip_address', 'ip_port')
    def button_test_connection(self):
        try:
            zk = zklib.ZKLib(self.ip_address, self.ip_port)
            if zk.connect():
                zk.disconnect()
                raise exceptions.Warning('Connection successful')
            else:
                raise exceptions.Warning('Connection Failed')
        except RuntimeError, e:
            raise exceptions.Warning('Connection Failed: %s' % e)

    @api.one
    @api.depends('ip_address', 'ip_port')
    def button_get_diagnoses(self):
        try:
            zk = zklib.ZKLib(self.ip_address, self.ip_port)
            if zk.connect():
                zk.disableDevice()
                self.version = zk.version()
                self.model = zk.platform()
                zk.enableDevice()
                zk.disconnect()
            else:
                raise exceptions.Warning('Connection Failed')
        except RuntimeError, e:
            raise exceptions.Warning('Error: %s' % e)

    @api.one
    @api.depends('ip_address', 'ip_port')
    def button_clear_attendance(self):
        zk = zklib.ZKLib(self.ip_address, self.ip_port)
        if zk.connect():
            try:
                zk.disableDevice()
                zk.clearAttendance()
                zk.enableDevice()
                zk.disconnect()
            except RuntimeError, e:
                raise exceptions.Warning('Error: %s' % e)
        else:
            raise exceptions.Warning('Connection Failed')
        return {
            'warning': {
                'title': "Attendance Cleared",
                'message': "All Data on the target device has been cleared",
            }
        }

    @staticmethod
    def _group_attendance(attendance):
        g_list = []
        js_list = []
        att_list = [
            (att[0], att[1], att[2].date(), att[2].time()) for att in attendance if
            att[2].date() == date.today()
        ]
        att_list.sort(key=itemgetter(0))
        att_group = groupby(att_list, itemgetter(0))
        for k, g in att_group:
            g_list.append(list(g))
        if g_list:
            for j in g_list:
                js_group = groupby(j, itemgetter(2))
                for k, g in js_group:
                    js_list.append(list(g))
        return js_list

    @api.model
    def _cron_get_full_attendance(self):
        device_obj = self.search([('schedule', '=', True), ('active', '=', True)])
        for device_id in device_obj:
            zk = zklib.ZKLib(device_id.ip_address, int(
                device_id.ip_port))
            if not zk.connect():
                continue
            _logger.info('Device Connected: %s' % device_id.name)
            zk.enableDevice()
            zk.disableDevice()
            attendance = zk.getAttendance()
            if not attendance:
                _logger.warning('Unable to get attendance...!')
                zk.enableDevice()
                zk.disconnect()
                _logger.info('Device Disconnected: %s' % device_id.name)
                continue
            _logger.info('Attendance Found...!')
            js_list = self._group_attendance(attendance)
            if js_list:
                _logger.info('Records Sorted!')
                local_tz = timezone(device_id.tz)
                for att in js_list:
                    employee_id = self.env['hr.employee'].search([
                        ('otherid', '=', att[0][0])
                    ])
                    if not employee_id:
                        continue
                    if len(att) == 1:
                        att_ts = datetime.combine(att[0][2], att[0][3])
                        local_timestamp = local_tz.localize(att_ts, is_dst=None)
                        local_timestamp = local_timestamp.astimezone(utc)
                        timestamp = local_timestamp.replace(tzinfo=None)
                        # Check if shift employee to use device state instead
                        if employee_id.is_night_shift:
                            # state = lambda x: 'sign_in' if att[0][1] == 0 else 'sign_out'
                            state = 'sign_in' if att[0][1] == 0 else 'sign_out'
                            if not self.env['hr.attendance'].search([
                                ('employee_id', '=', employee_id.id),
                                ('name', '=', fields.Datetime.to_string(timestamp)),
                                ('action', '=', state)
                            ]):
                                try:
                                    att_id = self.env['hr.attendance'].create({
                                        'name': fields.Datetime.to_string(timestamp),
                                        'employee_id': employee_id.id,
                                        'action': state,
                                        'device_id': self.id,
                                    })
                                    _logger.info("Att ID: %s" % att_id)
                                except:
                                    continue
                            continue

                        if not self.env['hr.attendance'].search([
                            ('employee_id', '=', employee_id.id),
                            ('name', '=', fields.Datetime.to_string(timestamp)),
                            ('action', '=', 'sign_in')
                        ]):
                            try:
                                att_id = self.env['hr.attendance'].create({
                                    'name': fields.Datetime.to_string(timestamp),
                                    'employee_id': employee_id.id,
                                    'action': 'sign_in',
                                    'is_absent':self._get_time(fields.Datetime.to_string(timestamp),employee_id),
                                    'device_id': self.id,
                                    
                                })
                                _logger.info("Att ID: %s" % att_id)
                            except:
                                continue
                    else:
                        min_att = min(att, key=itemgetter(3))
                        max_att = max(att, key=itemgetter(3))
                        min_ts = datetime.combine(min_att[2], min_att[3])
                        max_ts = datetime.combine(max_att[2], max_att[3])
                        local_timestamp_min = local_tz.localize(min_ts, is_dst=None)
                        local_timestamp_min = local_timestamp_min.astimezone(utc)
                        timestamp_min = local_timestamp_min.replace(tzinfo=None)
                        local_timestamp_max = local_tz.localize(max_ts, is_dst=None)
                        local_timestamp_max = local_timestamp_max.astimezone(utc)
                        timestamp_max = local_timestamp_max.replace(tzinfo=None)
                        if not self.env['hr.attendance'].search([
                            ('employee_id', '=', employee_id.id),
                            ('name', '=', fields.Datetime.to_string(timestamp_min)),
                            ('action', '=', 'sign_in')
                        ]):
                            try:
                                att_id = self.env['hr.attendance'].create({
                                    'name': fields.Datetime.to_string(timestamp_min),
                                    'employee_id': employee_id.id,
                                    'action': 'sign_in',
                                    'is_absent':self._get_time(fields.Datetime.to_string(timestamp),employee_id),
                                    'device_id': self.id,
                                })
                                _logger.info("IN Att ID: %s" % att_id)
                            except:
                                continue
                        att_obj = self.env['hr.attendance'].search([
                            ('name', '>', fields.Datetime.to_string(timestamp_min)),
                            ('name', '<=', fields.Datetime.to_string(timestamp_max)),'&',
                            ('employee_id', '=', employee_id.id),
                            ('action', '=', 'sign_out')
                        ], limit=1, order="name DESC")
                        if att_obj:
                            if len(att_obj) == 1:
                                att_obj.write({
                                    'name': fields.Datetime.to_string(timestamp_max),
                                })
                                _logger.info("Updating record: %s" % att_obj.id)
                            elif len(att_obj) > 1:
                                raise Warning("multiple sign outs occur")
                        else:
                            try:
                                att_id = self.env['hr.attendance'].create({
                                    'name': fields.Datetime.to_string(timestamp_max),
                                    'employee_id': employee_id.id,
                                    'action': 'sign_out',
                                    'device_id': self.id,
                                })
                                _logger.info("OUT Att ID: %s" % att_id)
                            except:
                                continue

            zk.enableDevice()
            zk.disconnect()
            _logger.info('Device Disconnected: %s' % device_id.name)





class ConnectToDevice(object):
    """
    Class uses to assure connetion to a device and closing of the same
    It is using to disable the device when it is been reading or busy
    """

    def __init__(self, ip_address, ip_port):
        try:
            #~ zk = ZkOpenerp(ip_address, port)
            zk = zklib.ZKLib(ip_address, ip_port)
            zk.connect()
        except:
            raise UserError(
                _('Unexpected error: {error}'.format(error=sys.exc_info()),)
            )
        zk.disableDevice()
        self.zk = zk

    def __enter__(self):
        """
        return biometric connection
        """
        return self.zk

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        enable device and close connection
        """
        self.zk.enableDevice()  # noqa: W0104
        self.zk.disconnect()
