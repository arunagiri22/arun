# -*- coding: utf-8 -*-
{
    'name': "ZK BioAttendance FirstIN/LastOUT",

    'summary': """
        ZK Bio-fingerprint devices module which fetching device data into hr_attendance module
        """,

    'description': """
        This module was written to extend the functionality of HR Attendance to support ZK devices and allow you
                to get attendance data directly from the device to Odoo systems.

                It uses First IN Last OUT (FILO) to correct attendance mistakes by the users.
                Supporting multi devices multi timezones. So you can sign IN/OUT from different locations.
                
                Also, you can specify night shift employees to ignore FILO and use regular sign IN/OUT button
                on the devices.
                
    """,

    'author': "Arunagiri K",
    'website': "http://edwebsite.pappaya.co.uk",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Human Resources',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['hr', 'hr_attendance','hr_holidays'],
    'license': 'AGPL-3',
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'zk_fingerprint_view.xml',
        'zk_user_view.xml',
        'zk_fingerprint_cron.xml',
        'zk_user_wizard_view.xml',
        'wizards_view.xml',
    ],
    'installable': True,
    'application': False,
}
