# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.osv import fields
from openerp.tools.translate import _

class ref_generic_request(osv.osv_memory):

    #create a table in osv_memory, with the columns you need in to fill in your wizard
    _name='ref.generic.request'

    _columns = {
        'reformulation_info': fields.text('Reformulation instructions', help='Instructions for the requestor justification the reformulation needs'),
            }
    # create a function that will save the info from your wizard into your model (active_id is the id of the record you called the wizard from, so you will save the info entered in wizard is that record)
    def save_info(self, cr, uid, ids, context=None):
        if 'active_id' in context:
            info=self.browse(cr,uid,ids)[0].reformulation_info
            self.pool.get('generic.request').write(cr,uid,context['active_id'],{'reformulation_info' : info, 'needs_reformulation': 1})
        return {
                'type': 'ir.actions.act_window_close',
         }
ref_generic_request()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
