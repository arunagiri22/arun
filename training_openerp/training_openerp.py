import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp import api, tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as OE_DATEFORMAT

class courses(osv.osv):
	_name = 'training.courses'
	_columns = {
				
				'name':fields.char('Course Title',size=64, required=True),
				'information':fields.text('Information'),
				'coordinator_id':fields.many2one('res.users','Co-ordinator',size=64,select=True),
				'session_ids':fields.one2many('training.session','courses_id','Session'),
	}
	_sql_constraints=[
					('unique_name','unique(name)','The name must be unique courses !',)
	]
	
	#~ def _check_information(self,cr,uid,ids,context=None):
		#~ check= True
		#~ courses = self.browse(cr,uid,ids,context=context)[0]
		#~ if courses.name == courses.information:
			#~ return false 
		#~ return check 
		
		
courses()


class session(osv.osv):
	
	def _get_attendance_percent(self,cr,uid,ids,field,arg,context=None):
	   val=self.browse(cr,uid,ids,context=context)
	   result={}
	   for x in val:
		   result[x.id] = (len(x.participant_ids) / float(x.seats)) * 100.0
	   return result
	   
	def onchange_arithmetic_quota(self,cr,uid,ids,seats,participant_ids):
		return{
				'value':{'quota_presence_percent':(len(participant_ids) / float(seats))* 100}
				}
	    
		if seat <=0:
					val = {
					
					'warning':{
					
								 'title' : 'Attention',
								 'message' :'The number of seats must be above 0',
						}
			
			
					}
		return val
		   
	_name = 'training.session'
	_columns= {
				'name':fields.char('Course title',size=64,required=True),
				'date_from':fields.date('Date From'),
				'duration':fields.float('Duration',digits=(16,2),help= "Duration within a Day"),
				'seats':fields.float('Number Of Seats'),
				'instructor_id':fields.many2one('res.partner','Instructor',domain=[('instructor', '=', True)]),
				'courses_id':fields.many2one('training.courses','Courses',required=True, ondelete='cascade'),
				'participant_ids':fields.one2many('training.participant','session_id', 'Participant'),
				'quota_presence_percent':fields.function( _get_attendance_percent,type='float',string='Quota Seats'),
	
	
	}
	
	_defaults = {
		'seats': 1,
		}
session()




class participant(osv.osv):
	_name= 'training.participant'
	_rec_name= 'participant_id'
	_columns = {
				 'participant_id':fields.many2one('res.partner','Participant',required=True,ondelete='cascade'),
				 'session_id':fields.many2one('training.session','Session',required=True,ondelete='cascade'),
	
	}
participant()

class student(osv.osv):
	_name='training.student'
	
	
	def _calculate_age(self, cr, uid, ids, field_name, arg, context=None):
        
			res = dict.fromkeys(ids, False)
			for ee in self.browse(cr, uid, ids, context=context):
				if ee.dob:
					dBday = datetime.strptime(ee.dob, OE_DATEFORMAT).date()
					dToday = datetime.now().date()
					res[ee.id] = (dToday - dBday).days / 365
			return res
	
	def _get_default_currency(self, cr, uid, context=None):
			res = self.pool.get('res.company').search(cr, uid, [('code','=','INR')], context=context)
			return res and res[0] or False

	
	_columns= {
	            'student':fields.char('Student', size=64),
	            'currency_id':fields.many2one('res.currency','Currency',required=True),
	            'dob':fields.date('Date Of Birth'),
	            'gender':fields.selection([('male','Male'),('female','Female')],'Gender',),
	            'age': fields.function(_calculate_age, type='integer', method=True, string='Age'),
	            'maths':fields.float('Maths'),
	            'science':fields.float('Science'),
	            'tamil':fields.float('Tamil'),
	            #~ 'total':fields.function(_get_total, store=False,String='Total' ),
	
	
	
	
	}
	
	_defaults = {
	 
					 'currency_id': _get_default_currency,
	 
	 
	 }
	#~ def _get_total(self, cr, uid ,ids, context=None):
		  #~ so = self.browse(cr, uid, ids, context=context)[0]
		  #~ total= (so.c1 or 0.0) + (so.c2 or 0.0) + (so.c3 or 0.0)
		  #~ return self.write(cr, uid , ids, {'total' : total})
		
			  
student()



class partner(osv.osv):
	_inherit='res.partner'
	_columns={
				'instructor':fields.boolean('Instructor'),
	
	}
partner()


