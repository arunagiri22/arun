{

    'name': 'Training Openerp',
    'version':'1.0',
	'author': 'Arunagirik',
	'category':'Custom Modules',
	'website':'http://www.arunagiri4u.com',
	'description':
		"""
		Training For Openerp
		
		""",
	'depends':['base'],
	'data': ['training_openerp_view.xml',
	'wizard/ref_generic_request.xml',
	],
	'active': False,
	'installable': True,	


}
