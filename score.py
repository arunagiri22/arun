letter = ['A','B','C','D','F']
def score(marks):
    if marks >= 90:
        letter = 'A'
    else:
        if marks >=80:
            letter = 'B'
        else:
            if marks >=70:
                letter = 'C'
            else:
                if marks >=60:
                    letter = 'D'
                else:
                    letter = 'F'
    return letter
