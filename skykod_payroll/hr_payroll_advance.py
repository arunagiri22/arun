from openerp import models,fields
from datetime import datetime

class hr_payroll_adavance(models.Model):
    
    _name='hr.payroll.advance'
    
    
    employee_id=fields.Many2one('hr.employee','Employee',required=True)
    date=fields.Date('Date',required=True)
    amt=fields.Float('Amount')
    state=fields.Selection(selection=[('draft', 'Pending'), ('cancel', 'Cancelled'),('paid', 'Paid')],string='Status',default='draft')
    
    
    def pay_advance(self,cr,uid,ids,context):
        return self.write(cr,uid,ids,{'state':'paid'})
    
    def cancel_advance(self,cr,uid,ids,context):
        return self.write(cr,uid,ids,{'state':'cancel'})