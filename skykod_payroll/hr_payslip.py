
from openerp import models
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT


    
class hr_payroll(models.Model):
    _inherit='hr.payslip'
    
    
    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
        
        print "helooooooooooo"

        res=super(hr_payroll,self).onchange_employee_id(cr,uid,ids,date_from,date_to,employee_id,contract_id,context)
        if res['value']['contract_id']==False:
            return res
        else:
            advance_obj = self.pool.get('hr.payroll.advance')
            leave_obj = self.pool.get('hr.holidays')
            contract=self.pool.get('hr.contract').browse(cr,uid,contract_id)
            overtime_obj = self.pool.get('hr.payroll.overtime')
            d1 = datetime.strptime(date_from, "%Y-%m-%d")
            d2 = datetime.strptime(date_to, "%Y-%m-%d")
            l_ids=leave_obj.search(cr,uid,[('state','=','validate'),('employee_id','=',int(employee_id)),('date_from','>=',date_from),('date_to','<=',date_to)])
            records=leave_obj.browse(cr,uid,l_ids)
            dic={'sch':0,'unl':0,'usl':0,'ph':0,'anl':0,'ab':0,'mtl':0,'ptl':0,'cpl':0,'hpf':0,'hph':0,'hpn':0,'np':0}
            tot=0
            t_allow=contract.trans
            m_allow=contract.meal
            w_allow=contract.work
            s_allow=contract.shift
           
            for rec in records:
                if rec.number_of_days!=-0.5:
                    t_allow=t_allow+(5*rec.number_of_days)
                    m_allow=m_allow+(2*rec.number_of_days)
                if rec.holiday_status_id.name=='SCH' :
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['sch']+=rec.number_of_days 
                if rec.holiday_status_id.name=='UNL':
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['unl']+=rec.number_of_days 
                if rec.holiday_status_id.name=='USL': 
                    dic['usl']+=rec.number_of_days 
                if rec.holiday_status_id.name=='PH': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['ph']+=rec.number_of_days
                if rec.holiday_status_id.name=='ANL': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['anl']+=rec.number_of_days 
                if rec.holiday_status_id.name=='AB': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['ab']+=rec.number_of_days 
                if rec.holiday_status_id.name=='MTL': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['mtl']+=rec.number_of_days 
                if rec.holiday_status_id.name=='PTL': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['ptl']+=rec.number_of_days 
                if rec.holiday_status_id.name=='CPL': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['cpl']+=rec.number_of_days 
                if rec.holiday_status_id.name=='HPF': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['hpf']+=rec.number_of_days 
                if rec.holiday_status_id.name=='HPH': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['hph']+=rec.number_of_days 
                if rec.holiday_status_id.name=='HPN': 
                    w_allow=w_allow+(3*rec.number_of_days)
                    if rec.shift=='d':
                            s_allow=s_allow+(4*rec.number_of_days)
                    if rec.shift=='n':
                            s_allow=s_allow+(5*rec.number_of_days)
                    dic['hpn']+=rec.number_of_days 
                tot+=rec.number_of_days
            print tot,"11111111111111"
            print dic,"22222222222222"
            att_bonus=0.0
            i = 0
            total_leaves= abs(dic['ph'])+abs(dic['anl'])+abs(dic['usl'])+abs(dic['ab'])+abs(dic['sch'])+abs(dic['unl'])+abs(dic['mtl'])+abs(dic['ptl'])+abs(dic['cpl'])+abs(dic['hpf'])+abs(dic['hph'])+abs(dic['hpn'])
            print total_leaves,"33333333333"
            print abs(dic['usl']),"444444444"
            if contract.attbonus:
                if tot==0:
                    att_bonus=120.0
                elif tot!=0:
                    if(total_leaves == abs(dic['usl'])):
                        att_bonus = 120.0
                    elif (total_leaves != abs(dic['usl'])):
                        if(abs(dic['sch']) == 1 or abs(dic['unl']) == 1):
                            att_bonus=60.0
                        else:
                            i=i+1
                        if(abs(dic['sch']) == 1.5 or abs(dic['unl']) == 1.5):
                            att_bonus=30.0
                        else:
                            i=i+1
                        if(abs(dic['sch']) == 2 or abs(dic['unl']) == 2):
                            att_bonus=30.0
                        else:
                            i=i+1
                        if(abs(dic['sch']) > 2 or abs(dic['unl']) > 2):
                            att_bonus=0.0
                        else:
                            i=i+1
                        if(abs(dic['ab']) > 0):
                            att_bonus=0.0
                        else:
                            i=i+1
                        if(abs(dic['mtl']) > 0 or abs(dic['hpf']) > 0 or abs(dic['hph']) > 0 or abs(dic['hpn']) > 0):
                            att_bonus = 0.0
                        else:
                            i=i+1
                        if i==6:
                            att_bonus = 120.0
                    
                print i,'943904u9034uu'
            
            """special Allowance"""
            special=contract.special-(contract.special/26)*-(dic['sch']+dic['unl']+dic['ab']+dic['anl']+dic['ph'])
            
            w_all={
                  'code':'WALL',
                  'contract_id':int(contract_id),
                  'sequence':4,
                  'amount':w_allow if w_allow>0 else 0,
                  'name':'Work Allowance'
                  }   
            m_all={
                  'code':'MALL',
                  'contract_id':int(contract_id),
                  'sequence':5,
                  'amount':m_allow if m_allow>0 else 0,
                  'name':'Meal Allowance'
                  }  
            
            t_all={
                  'code':'TALL',
                  'contract_id':int(contract_id),
                  'sequence':3,
                  'amount':t_allow if t_allow>0 else 0,
                  'name':'Transport Allowance'
                  }   
            att={
                  'code':'ATT',
                  'contract_id':int(contract_id),
                  'sequence':2,
                  'amount':att_bonus,
                  'name':'Attendance Bonus'
                  }
            shift_all={
                  'code':'SFT',
                  'contract_id':int(contract_id),
                  'sequence':7,
                  'amount':s_allow if s_allow>0 else 0,
                  'name':'Shift Allowance'
                       }
           
            spe_all={
                  'code':'SPEALL',
                  'contract_id':int(contract_id),
                  'sequence':10,
                  'amount':"{0:.2f}".format(round(special,2)) if special>0 else 0,
                  'name':'Special Allowance'
                  }
            res['value']['input_line_ids'].append(att)
            res['value']['input_line_ids'].append(t_all)
            res['value']['input_line_ids'].append(m_all) 
            res['value']['input_line_ids'].append(w_all) 
            res['value']['input_line_ids'].append(shift_all) 
            res['value']['input_line_ids'].append(spe_all) 
            leave={
                   'name':'Loss Of Pay',
                 'sequence': 2,
                 'code': 'LEAVES',
                 'number_of_days': -(dic['unl']+dic['ab']+dic['usl']+dic['np']),
                 'number_of_hours': -((dic['unl']+dic['ab']+dic['usl']+dic['np'])*8),
                 'contract_id': int(contract_id),
                   }
            dw={
                   'name':'Work DW',
                 'sequence': 2,
                 'code': 'DW',
                 'number_of_days': 26+dic['ph']+dic['anl']+dic['usl']+dic['ab']+dic['sch']+dic['unl']+dic['mtl']+dic['ptl']+dic['cpl']+dic['hpf']+dic['hph']+dic['hpn'],
                 'number_of_hours': ((26+dic['ph']+dic['anl']+dic['usl']+dic['ab']+dic['sch']+dic['unl']+dic['mtl']+dic['ptl']+dic['cpl']+dic['hpf']+dic['hph']+dic['hpn'])*8),
                 'contract_id': int(contract_id),
                   }
            ph={
                   'name':'Public Holiday',
                 'sequence': 2,
                 'code': 'PH',
                 'number_of_days': -dic['ph'],
                 'number_of_hours': -((dic['ph'])*8),
                 'contract_id': int(contract_id),
                   }
            al={
                   'name':'Annual Leaves',
                 'sequence': 2,
                 'code': 'AL',
                 'number_of_days': -dic['anl'],
                 'number_of_hours': -((dic['anl'])*8),
                 'contract_id': int(contract_id),
                   }
            ls={
                   'name':'Shutdown Leaves',
                 'sequence': 2,
                 'code': 'LS',
                 'number_of_days': -dic['usl'],
                 'number_of_hours': -((dic['usl'])*8),
                 'contract_id': int(contract_id),
                   }
            unl={
                   'name':'Unpaid Leaves',
                 'sequence': 2,
                 'code': 'UNL',
                 'number_of_days': -dic['unl'],
                 'number_of_hours': -((dic['unl'])*8),
                 'contract_id': int(contract_id),
                   }
            sch={
                   'name':'Sick Leaves',
                 'sequence': 2,
                 'code': 'SCH',
                 'number_of_days': -dic['sch'],
                 'number_of_hours': -((dic['sch'])*8),
                 'contract_id': int(contract_id),
                   }
            ab={
                   'name':'Absent',
                 'sequence': 2,
                 'code': 'AB',
                 'number_of_days': -dic['ab'],
                 'number_of_hours': -((dic['ab'])*8),
                 'contract_id': int(contract_id),
                   }
            mtl={
                   'name':'Maternity Leave',
                 'sequence': 2,
                 'code': 'MTL',
                 'number_of_days': -dic['mtl'],
                 'number_of_hours': -((dic['mtl'])*8),
                 'contract_id': int(contract_id),
                   }
            ptl={
                   'name':'Paternity Leave',
                 'sequence': 2,
                 'code': 'PTL',
                 'number_of_days': -dic['ptl'],
                 'number_of_hours': -((dic['ptl'])*8),
                 'contract_id': int(contract_id),
                   }
            cpl={
                   'name':'Compassionate Leave',
                 'sequence': 2,
                 'code': 'CPL',
                 'number_of_days': -dic['cpl'],
                 'number_of_hours': -((dic['cpl'])*8),
                 'contract_id': int(contract_id),
                   }
            hpf={
                   'name':'Hospitalization Full',
                 'sequence': 2,
                 'code': 'HPF',
                 'number_of_days': -dic['hpf'],
                 'number_of_hours': -((dic['hpf'])*8),
                 'contract_id': int(contract_id),
                   }
            hph={
                   'name':'Hospitalization Half',
                 'sequence': 2,
                 'code': 'HPH',
                 'number_of_days': -dic['hph'],
                 'number_of_hours': -((dic['hph'])*8),
                 'contract_id': int(contract_id),
                   }
            hpn={
                   'name':'Hospitalization ',
                 'sequence': 2,
                 'code': 'HPN',
                 'number_of_days': -dic['hpn'],
                 'number_of_hours': -((dic['hpn'])*8),
                 'contract_id': int(contract_id),
                   }
            
          
            work={
                'name':'Total Working Days',
                 'sequence': 1,
                 'code': 'WORK',
                 'number_of_days': 26,
                 'number_of_hours': (26*8),
                 'contract_id': int(contract_id),
                   }
            res['value']['worked_days_line_ids']=[]
            res['value']['worked_days_line_ids'].append(work)
            res['value']['worked_days_line_ids'].append(leave)
            res['value']['worked_days_line_ids'].append(dw)
            res['value']['worked_days_line_ids'].append(al)
            res['value']['worked_days_line_ids'].append(ls)
            res['value']['worked_days_line_ids'].append(ph)
            res['value']['worked_days_line_ids'].append(unl)
            res['value']['worked_days_line_ids'].append(sch)
            res['value']['worked_days_line_ids'].append(ab)
            res['value']['worked_days_line_ids'].append(mtl)
            res['value']['worked_days_line_ids'].append(ptl)
            res['value']['worked_days_line_ids'].append(cpl)
            res['value']['worked_days_line_ids'].append(hpf)
            res['value']['worked_days_line_ids'].append(hph)
            res['value']['worked_days_line_ids'].append(hpn)
            a_ids=advance_obj.search(cr,uid,[('state','=','paid'),('employee_id','=',int(employee_id)),('date','>=',date_from),('date','<=',date_to)])
            a_records=advance_obj.browse(cr,uid,a_ids)
            adv_sum=0
            for rec in a_records:
                adv_sum+=rec.amt
            adv={
                  'code':'ADV',
                  'contract_id':int(contract_id),
                  'sequence':1,
                  'amount':adv_sum,
                  'name':'Advance'
                  }
            res['value']['input_line_ids'].append(adv)


            ot1={
                'name':'Overtime-1',
                 'sequence': 3,
                 'code': 'HOT1',
                 'number_of_days': 0,
                 'number_of_hours': 0,
                 'contract_id': int(contract_id),
                   }
            ot2={
                'name':'Overtime-2',
                 'sequence': 4,
                 'code': 'HOT2',
                 'number_of_days': 0,
                 'number_of_hours': 0,
                 'contract_id': int(contract_id),
                   }
            ot3={
                'name':'Overtime-3',
                 'sequence': 5,
                 'code': 'HOT3',
                 'number_of_days': 0,
                 'number_of_hours': 0,
                 'contract_id': int(contract_id),
                   }
            
            res['value']['worked_days_line_ids'].append(ot1)
            res['value']['worked_days_line_ids'].append(ot2)
            res['value']['worked_days_line_ids'].append(ot3)

            
      
            return res
    
    
   
