from openerp import models,fields
from datetime import datetime

class hr_payroll_overtime(models.Model):
    
    _name='hr.payroll.overtime'
    
    
    employee_id=fields.Many2one('hr.employee','Employee',required=True)
    date=fields.Date('Date',required=True)
    hot1=fields.Float('HOT1')
    hot2=fields.Float('HOT2')
    hot3=fields.Float('HOT3')
    is_public=fields.Boolean('Public Holiday')
    state=fields.Selection(selection=[('draft', 'Pending'), ('cancel', 'Cancelled'),('confirm', 'Confirm')],string='Status',default='draft')
    
    
    def confirm_overtime(self,cr,uid,ids,context):
        return self.write(cr,uid,ids,{'state':'confirm'})
    
    def cancel_overtime(self,cr,uid,ids,context):
        return self.write(cr,uid,ids,{'state':'cancel'})

class hr_holidays(models.Model):
    _inherit='hr.holidays'
    
    