# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Malaysian Payroll',
    'version': '1',
    'description': """
Malaysian Payroll Salary Rules.
============================================================================

    -Configuration of hr_payroll for Malaysia localization
    -All main contributions rules for Malaysia payslip.
    * New payslip report
    * Employee Contracts
    * Allow to configure Basic / Gross / Net Salary
    * Employee PaySlip
    * Allowance / Deduction
    - Payroll Advice and Report
    - Payroll Summary and Report
    - Yearly Salary by Head and Yearly Salary by Employee Report
    """,
    'category': 'Payroll',
    'author': "Arun\Anand",
    'website': 'www.skygst.com',
    'license': 'AGPL-3',
    'depends': [
        'web','hr_payroll',
    ],
    'data': [ 'salary_structure.xml','contract_employee_advance_view.xml',
    'security/ir.model.access.csv','security/ir_rule.xml'
    #~ 'hr_payroll_overtime.xml'
        
    ],
    'qweb': [
        
    ],
    'installable': True,
    'auto_install': False,
}
