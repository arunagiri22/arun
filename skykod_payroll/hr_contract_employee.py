from openerp import models,fields,api


class hr_contract(models.Model):
    _inherit='hr.contract'
   
   
   
    levy=fields.Float('Levy')
    arrear=fields.Float('Arrears')
    other_allow=fields.Float('Other Allowance')
    attbonus=fields.Boolean('Attendance Bonus')
    trans=fields.Float('Transport Allowance')
    meal=fields.Float('Meal Allowance')
    work=fields.Float('Working Allowance')
    shift=fields.Float('Shift Allowance')
    special=fields.Float('Special Allowance')
    lateness=fields.Float('Lateness Deduction')
    itax=fields.Float('PCB')
    
    
    
class hr_employee(models.Model):
    _inherit='hr.employee'
    
    ic_id= fields.Char('IC number', size=64)
    wife_work=fields.Boolean('Is Spouse Working')
    child_18=fields.Integer('No of Children < 18years Old')
    date_joined=fields.Date('Date Joined')
    date_resigned=fields.Date('Date Resigned')
    
