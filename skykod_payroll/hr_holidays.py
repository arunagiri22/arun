
from openerp.osv import fields, osv
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT

class hr_holidays(osv.osv):
    _inherit='hr.holidays'
    
    _columns={
    'shift':fields.selection([('d','Day shift'),('n','Night Shift')],'Shift')
              }

    def onchange_date_from(self,cr,uid,ids, date_to, date_from):
        result=super(hr_holidays,self).onchange_date_from(cr,uid,ids,date_to,date_from)
        print result
        if date_from:
                my_date = fields.datetime.context_timestamp(cr,uid,datetime.strptime(date_from,DEFAULT_SERVER_DATETIME_FORMAT))
                print my_date
                if my_date.hour>=7 and my_date.hour<19:
                  
                    result['value']['shift']='d'
                else:
                    result['value']['shift']='n'
                return result
        else:
            return result