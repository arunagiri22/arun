{
    'name': "Legacy project integration",
    'version': "1.0",
    'author': "thepearson",
    'description': "Projects",
    'category': "Tools",
    'depends': ['project'],
    'data': ['legacy_projects.xml'],
    'demo': [],
    'installable': True,
}
