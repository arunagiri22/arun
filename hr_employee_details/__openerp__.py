{
    'name': 'Employee Details',
    'version': '1',
    'description': """

    
    """,
    'category': 'Payroll',
    'author': "GetOpenerp",
    'website': 'www.getopenrp.com',
    'license': 'AGPL-3',
    'depends': [
        'web','hr_payroll',
    ],
    'data': [ 'hr_employee_detail.xml',
        
    ],
    'qweb': [
        
    ],
    'installable': True,
    'auto_install': False,
}
