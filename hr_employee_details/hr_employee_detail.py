import logging

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.modules.module import get_module_resource
from openerp import models,fields,api
from openerp.tools.translate import _


class hr_employee_detail(models.Model):
    _inherit = "res.partner.bank"
    
    
    branch = fields.Char('Branch')
    ifsc_code = fields.Char('Ifsc Code')
    micr_code = fields.Char('MICR Code')
    contact_num = fields.Char('Contact Number')
    
    
    
class hr_employee(models.Model):
    _inherit = "hr.employee"
    
    
    country_of_issue = fields.Char('Country Of Issue')
    passport_exp_date = fields.Date('Passport Expiry Date')
    visa_no = fields.Char('Visa No')
    work_permit_no = fields.Char('Work Permit No')
    work_permit_exp = fields.Date('Work Permit Expiry')
    contract_start_date = fields.Date('Contract start Date')
    contract_exp_date = fields.Date('Contract Expiry Date')
    income_tax_no = fields.Char('Income Tax No(PAN)')
    pf_acc_no = fields.Integer('PF Account No')
    eps_acc_no = fields.Integer('EPS Account No')
    pf_date_join =fields.Date('PF Date Join')
    esi_no = fields.Integer('ESI No') 
    esi_dispensary =fields.Char('ESI Dispensary')
    date_of_joining = fields.Date('Date Of Joining')
