<?php

/*

* 2007-2012 PrestaShop

* NOTICE OF LICENSE

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

* DISCLAIMER

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2012 PrestaShop SA

*  @version  Release: $Revision: 14011 $

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*/

if (!defined('_PS_VERSION_'))exit;

require_once 'classes/PrestaerpClassInclude.php';

class Prestaerpmultishop extends Module{	

	const INSTALL_SQL_FILE = 'install.sql';	

	public function __construct(){

		$this->name = 'prestaerpmultishop';
		$this->tab = 'administration';
		$this->version = '0.1';
		$this->author = 'WebKul Software Pvt Ltd';
		$this->need_instance = 0;		
     
		parent::__construct();
		$this->displayName = $this->l('POB : Multi-Shop Extension');
		$this->description = $this->l('This module allows you to export your shop from Prestashop to Odoo.');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall it?');
	}
		
	public function insertTab() {
		$tab1  = $this->installTab('AdminErpShop','Shop','OdooMapping');
		return true;
	}

	public function installTab($class_name,$tab_name,$tab_parent_name=false) {
		$tab = new Tab();
		$tab->active = 1;
		$tab->class_name = $class_name;
		$tab->name = array();
		foreach (Language::getLanguages(true) as $lang)
			$tab->name[$lang['id_lang']] = $tab_name;
		if($tab_parent_name) {
			$tab->id_parent = (int)Tab::getIdFromClassName($tab_parent_name);
		} else {
			$tab->id_parent = 0;
		}

		$tab->module = $this->name;
		return $tab->add();
	}

	public function deleteTab() {
		$delete1 = $this->uninstallTab('AdminErpShop');		
		return true;
	}
	
	public function uninstallTab($class_name) {
		$id_tab = (int)Tab::getIdFromClassName($class_name);
		if ($id_tab)
		{
		$tab = new Tab($id_tab);
		return $tab->delete();
		}
		else
		return false;
	}

	public function deleteTables() {	
       if(Configuration::getGlobalValue("drop_tables") == 1){
		$d9 = Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_shop_merge');
		if(!$d9)
			return False;
	    }
		else
			return True;
	}

	public function setMultiShopOff(){
		Configuration::updateGlobalValue('multi_shop',False);
		return True;
	}
  
	public function install(){
		$check_pob = Db::getInstance()->getRow("select `id_module` from `"._DB_PREFIX_."module` where `name`='prestaerp' and `active`=1");
		if ($check_pob){
			if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
				return (false);

			else if (!$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
				return (false);
			$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
			$sql = preg_split("/;\s*[\r\n]+/", $sql);
			foreach ($sql AS $query)
				if($query)
					if(!Db::getInstance()->execute(trim($query)))
						return false;			

			if (!parent::install()  OR !$this->insertTab() ) 

				return false;     

			return true;
		}else{
			$this->errors[] = Tools::displayError('<ul>
				<li>'.$this->l("In order to install this Module you must install 'POB : PrestaShop-Odoo Bridge' first.").'</li>
				<li>'.$this->l("Please contact our support team for further details.").'</li>
			</ul>');
			return False;
		}
	}


	public function uninstall(){	
		if(parent::uninstall() == false ||	 !$this->deleteTab() ||	 !$this->deleteTables() ||	 !$this->setMultiShopOff())
			return false;     
		else
			return true;	
	}
}

?>