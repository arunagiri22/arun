# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.osv.orm import Model
import datetime
from openerp import tools
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

class payslip_epf_report_wizard(Model):

    _name = 'payslip.epf.report.wizard'

    _columns = {
        'employee_ids': fields.many2many('hr.employee', 'ppm_hr_employee_rel1','emp_id','employee_id','Employee Name', required=False),
        'period_id': fields.many2one('account.period', 'Period', required=True),
    }

    def print_epf_report(self, cr, uid, ids, context):
        data = self.read(cr, uid, ids)[0]
        datas = {
            'ids': [],
            'form': data,
            'model':'hr.payslip',
        }
        return {'type': 'ir.actions.report.xml', 'report_name': 'hrpayslipepf_receipt', 'datas': datas}

payslip_epf_report_wizard()