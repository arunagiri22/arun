# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.osv.orm import Model

class payslip_incometax_report_wizard(Model):

    _name = 'payslip.incometax.report.wizard'

    _columns = {
                'employee_ids': fields.many2many('hr.employee', 'ppm_hr_employee_rel2','emp_id','employee_id','Employee Name', required=False),
                'period_id': fields.many2one('account.period', 'Period', required=True),
    }

    def print_incometax_report(self, cr, uid, ids, context):
        data = self.read(cr, uid, ids)[0]
        period_id = data['period_id'][0]
        period_data = self.pool.get('account.period').browse(cr, uid, period_id)
        start_date = period_data.date_start
        end_date = period_data.date_stop
        data.update({'date_from': start_date, 'date_to': end_date})
        datas = {
            'ids': [],
            'form': data,
            'model':'hr.payslip',
        }
        return {'type': 'ir.actions.report.xml', 'report_name': 'hr_payslip_report', 'datas': datas}

payslip_incometax_report_wizard()