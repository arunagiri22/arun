# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.osv.orm import Model
import base64
import tempfile
from openerp.tools.translate import _
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
from openerp import tools
class epf_txt_file_wizard(Model):

    _name = 'epf.txt.file.wizard'

    _columns = {
        'employee_ids': fields.many2many('hr.employee', 'hr_employe_rel11','employe_id','employee_id','Employee payslip', required=False),
        'period_id': fields.many2one('account.period', 'Period', required=True),
    }

    def download_epf_txt_file(self, cr, uid, ids, context):
        data = self.read(cr, uid, ids, [])[0]
        period_id = False
        if data.get('period_id'):
            period_id = data.get('period_id')[0]
        context.update({'employe_id': data['employee_ids'], 'period_id': period_id})
        return {
              'name': _('Binary'),
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'binary.epf.text.wizard',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': context,
              }
        
epf_txt_file_wizard()

class binary_epf_text_wizard(osv.TransientModel):
    
    _name = 'binary.epf.text.wizard'
    
    _columns = {
        'name':fields.char('Name' , size=32),
        'rst_file': fields.binary('Click On DownLoad Link To Download File', readonly=True),
        'rst_file': fields.binary('Click On Download Link To Download Text File', readonly=True),

    }
    
    def _generate(self, cr, uid, context=None):
        payslip_obj = self.pool.get('hr.payslip')
        period_obj = self.pool.get('account.period')
        if context is None:
            context = {}
        employ_id = context.get('employe_id')
        period_id = context.get('period_id')
        if not period_id or not employ_id:
            return False
        period_data = period_obj.browse(cr, uid, period_id, context=context)
        start_date = period_data.date_start
        end_date = period_data.date_stop
        end_date_obj = datetime.datetime.strptime(end_date, DEFAULT_SERVER_DATE_FORMAT)
        end_date_obj = end_date_obj + relativedelta(months=1)
        current_date = datetime.datetime.today()
        yearmonthdate = current_date.strftime('%Y%m%d')
        monthyear = end_date_obj.strftime('%m%Y')
        tgz_tmp_filename = tempfile.mktemp('.'+"txt")
        tmp_file = open(tgz_tmp_filename, "wr")
        try:
            payslip_ids = payslip_obj.search(cr, uid, [('date_from', '>=', start_date), ('date_from','<=',end_date),('employee_id', 'in', employ_id)], order='employee_name')
            payslips = payslip_obj.browse(cr, uid, payslip_ids)
            totalcontribution = totalemployercontibution = 0.0
            total_record = epf_no_total =0
            for payslip in payslips:
                epf_no_total += payslip.employee_id.epf_no
                for line in payslip.line_ids:
                    if line.code in ['EPFY']:
                        totalcontribution += line.total
                    
                    if line.code in ['EPFE']:
                        totalemployercontibution += line.total
            
            totalcontribution = totalcontribution * 100
            new_totalscse = int(round(totalcontribution))
            if new_totalscse < 0 :
                new_totalscse = new_totalscse * -1
            final_totalscse = '%0*d' % (15, new_totalscse)
            
            totalemployercontibution = totalemployercontibution * 100
            new_totalscsy = int(round(totalemployercontibution))
            if new_totalscsy < 0 :
                new_totalscsy = new_totalscsy * -1
            final_totalscsy = '%0*d' % (15, new_totalscsy)
            header_record = '00'.ljust(2) + \
                            'EPF MONTHLY FORM A'.ljust(18) + \
                            tools.ustr(yearmonthdate).ljust(8) + \
                            '00001'.ljust(5) + \
                            tools.ustr(final_totalscse).ljust(15) + \
                            tools.ustr(final_totalscsy).ljust(15) + \
                            tools.ustr(epf_no_total).rjust(21,'0') + \
                            tools.ustr('').ljust(45) + "\r\n"
            tmp_file.write(header_record)
            
            second_header_record = '01'.ljust(2) + \
                                '0000000000006117228'.ljust(19) + \
                                tools.ustr(monthyear).ljust(6) + \
                                'DSK'.ljust(3) + \
                                '00001'.ljust(5) + \
                                '0000'.ljust(4) + \
                                '0000'.ljust(4) + \
                                '0000000'.ljust(4) + \
                                tools.ustr('').ljust(79) + "\r\n"
            tmp_file.write(second_header_record)
            
            for payslip in payslips:
                total_record += 1
                gross = epfe = epfy = 0.0
                for line in payslip.line_ids:
                    if line.code in ['GROSS']:
                        gross = line.amount
                    
                    if line.code in ['EPFY']:
                        epfy = line.amount
                    
                    if line.code in ['EPFE']:
                        epfe = line.amount
                
                gross = gross * 100
                new_gross = int(round(gross))
                if new_gross < 0 :
                    new_gross = new_gross * -1
                final_gross = '%0*d' % (15, new_gross)
                
                epfy = epfy * 100
                new_epfy = int(round(epfy))
                if new_epfy < 0 :
                    new_epfy = new_epfy * -1
                final_new_epfy = '%0*d' % (9, new_epfy)
                
                epfe = epfe * 100
                new_epfe = int(round(epfe))
                if new_epfe < 0 :
                    new_epfe = new_epfe * -1
                final_new_epfe = '%0*d' % (9, new_epfe)
                
                actual_detail_record = '02'.ljust(2) + \
                                    tools.ustr(payslip.employee_id.epf_no and str(payslip.employee_id.epf_no).rjust(19,'0') or ' '.ljust(19,'0')) + \
                                    tools.ustr(payslip.employee_id.identification_id and tools.ustr(payslip.employee_id.identification_id).ljust(15) or ' '.ljust(15)) + \
                                    tools.ustr(payslip.employee_id.name and payslip.employee_id.name.ljust(40) or ' '.ljust(40)) + \
                                    tools.ustr(payslip.employee_id.user_id.login and payslip.employee_id.user_id.login.ljust(20) or ' '.ljust(20)) + \
                                    tools.ustr(final_new_epfy).ljust(9) + \
                                    tools.ustr(final_new_epfe).ljust(9) + \
                                    tools.ustr(final_gross).ljust(15) + "\r\n"
                tmp_file.write(actual_detail_record)
            
            total_employee_contribution = total_employer_contribution = total_epfey = 0.0
            for payslip in payslips:
                for line in payslip.line_ids:
                    if line.code in ['EPFE']:
                        total_employee_contribution += line.total
                    
                    if line.code in ['EPFY']:
                        total_employer_contribution += line.total
                    
                    if line.code in ['EPFY', 'EPFE']:
                        total_epfey += line.total

            total_employee_contribution = total_employee_contribution * 100
            new_total_employee_contribution = int(round(total_employee_contribution))
            if new_total_employee_contribution < 0 :
                new_total_employee_contribution = new_total_employee_contribution * -1
            final_new_total_employee_contribution = '%0*d' % (15, new_total_employee_contribution)
            
            total_employer_contribution = total_employer_contribution * 100
            new_total_employer_contribution = int(round(total_employer_contribution))
            if new_total_employer_contribution < 0 :
                new_total_employer_contribution = new_total_employer_contribution * -1
            final_new_total_employer_contribution = '%0*d' % (15, new_total_employer_contribution)
            
            total_epfey = total_epfey * 100
            new_total_epfey = int(round(total_epfey))
            if new_total_epfey < 0 :
                new_total_epfey = new_total_epfey * -1
            final_new_total_epfey = '%0*d' % (21, new_total_epfey)
            total_record = '%0*d' % (7, total_record)
            footer_record = '99'.ljust(2) + \
                            tools.ustr(total_record).ljust(7) + \
                            tools.ustr(final_new_total_employer_contribution).ljust(15) + \
                            tools.ustr(final_new_total_employee_contribution).ljust(15) + \
                            tools.ustr(final_new_total_epfey).ljust(21) + \
                            tools.ustr('').ljust(69)
            tmp_file.write(footer_record)
        
        finally:
            tmp_file.close()
        
        file = open(tgz_tmp_filename, "rb")
        out = file.read()
        file.close()
        
        return base64.b64encode(out)
    
    
    _defaults = {
        'name':'EPFORMA',
        'rst_file': _generate,
    }
    
binary_epf_text_wizard()
