# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.osv.orm import Model
import base64
import tempfile
from openerp.tools.translate import _
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from openerp import tools
class socso_txt_file_wizard(Model):

    _name = 'socso.txt.file.wizard'

    _columns = {
        'employee_ids': fields.many2many('hr.employee', 'ppm_hr_employe_rel10','empt_id','employee_id','Employee payslip', required=False),
        'period_id': fields.many2one('account.period', 'Period', required=True),
    }

    def download_socso_txt_file(self, cr, uid, ids, context):
        data = self.read(cr, uid, ids, [])[0]
        period_id = False
        if data.get('period_id'):
            period_id = data.get('period_id')[0]
        context.update({'employe_id': data['employee_ids'], 'period_id': period_id})
        return {
              'name': _('Binary'),
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'binary.text.wizard',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': context,
              }

socso_txt_file_wizard()

class binary_text_wizard(osv.TransientModel):

    _name = 'binary.text.wizard'

    _columns = {
                'name':fields.char('Name' , size=32),
                'rst_file': fields.binary('Click On Download Link To Download File', readonly=True),
                'rst_file': fields.binary('Click On Download Link To Download Text File', readonly=True),
            }

    def _generate(self, cr, uid, context=None):
        payslip_obj = self.pool.get('hr.payslip')
        period_obj = self.pool.get('account.period')
        if context is None:
            context = {}
        employ_id = context.get('employe_id')
        period_id = context.get('period_id')
        if not period_id or not employ_id:
            return False
        period_data = period_obj.browse(cr, uid, period_id, context=context)
        start_date = period_data.date_start
        end_date = period_data.date_stop
        end_date_obj = datetime.datetime.strptime(end_date, DEFAULT_SERVER_DATE_FORMAT)
        monthyear = end_date_obj.strftime('%m%y')
        
        tgz_tmp_filename = tempfile.mktemp('.'+"txt")
        payslip_ids = payslip_obj.search(cr, uid, [('date_from', '>=', start_date), ('state', 'in', ['draft', 'done', 'verify']), ('date_from','<=',end_date),('employee_id', 'in', employ_id)])
        tmp_file = open(tgz_tmp_filename, "wr")
        if not payslip_ids:
            tmp_file.write(' ')
        
        payslips = payslip_obj.browse(cr, uid, payslip_ids)
        for payslip in payslips:
            totalscsey=0.0
            for line in payslip.line_ids:
                if line.code in ['SCSY', 'SCSE']:
                    totalscsey += line.total
            totalscsey = totalscsey * 100
            totalscsey = int(round(totalscsey))
            if totalscsey < 0 :
                totalscsey = totalscsey * -1
            out = 'A3609732P'.ljust(9) + \
                    tools.ustr(payslip.employee_id.identification_id and tools.ustr(payslip.employee_id.identification_id)[:12].upper().ljust(12) or ' '.ljust(12)) + \
                    tools.ustr(payslip.employee_id.no_perkeso and tools.ustr(payslip.employee_id.no_perkeso)[:9].upper().ljust(9) or ' '.ljust(9)) + \
                    monthyear.ljust(4) + \
                    tools.ustr((payslip.employee_id.name and payslip.employee_id.name)[:45].upper().ljust(45) or ' '.ljust(45)) + \
                    tools.ustr(totalscsey or '').ljust(4) + "\r\n"
            tmp_file.write(out)

        tmp_file.close()

        file = open(tgz_tmp_filename, "rb")
        out = file.read()
        file.close()
        return base64.b64encode(out)

    _defaults = {
        'name':'BRG8A.TXT',
        'rst_file': _generate,
    }
    
binary_text_wizard()
