# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import upload_xls_wizard
import payslip_details_report_wizard
import view_bank_summary_report_wizard
import hr_payroll_summary_wizard
import view_cheque_summary_report_wizard
import payslip_summary_wizard
import payslip_epf_report_wizard
import payslip_incometax_report_wizard
import socso_txt_file_wizard
import epf_txt_file_wizard
import payroll_generic_summary_wiz
import payroll_exchequer_report
import bank_list_xls
import export_employee_summary_wiz
import comput_confirm_payslip_wiz

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: