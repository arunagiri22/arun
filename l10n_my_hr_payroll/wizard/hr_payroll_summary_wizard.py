# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.osv.orm import Model
from openerp.tools.translate import _
from openerp import tools

class hr_payroll_summary_wizard(Model):

    _name = 'hr.payroll.summary.wizard'

    _columns = {
        'employee_ids': fields.many2many('hr.employee', 'ppm_hr_employe_rel','empl_id','employee_id','Employee payslip', required=False),
        'salary_rule_ids': fields.many2many('hr.salary.rule', 'ppm_hr_employe_salary_rule_rel','salary_rule_id','employee_id','Employee payslip'),
        'period_id': fields.many2one('account.period', 'Period', required=True),
        'export_report' : fields.selection([('pdf','PDF')] , "Export")
    }

    _defaults = {
        'export_report': "pdf"
    }

    def print_hr_payroll(self, cr, uid, ids, context):
        if context is None:
            context = {}
        data = self.read(cr, uid, ids)[0]
        period_id = data['period_id'][0]
        period_data = self.pool.get('account.period').browse(cr, uid, period_id)
        start_date = period_data.date_start
        end_date = period_data.date_stop
        if data.get("export_report") == "pdf":
            res_user = self.pool.get("res.users").browse(cr, uid,uid,context=context)
            data.update({'currency': " " + tools.ustr(res_user.company_id.currency_id.symbol), 'company': res_user.company_id.name, 'date_from': start_date, 'date_to': end_date})
#            data.update({'company': res_user.company_id.name, 'date_from': start_date, 'date_to': end_date})
            datas = {
                'ids': [],
                'form': data,
                'model':'hr.payslip',
            }
            return {'type': 'ir.actions.report.xml', 'report_name': 'hr_payroll_summary_receipt', 'datas': datas}
        
hr_payroll_summary_wizard()