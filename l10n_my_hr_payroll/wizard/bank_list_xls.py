# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
import base64
import xlwt
from cStringIO import StringIO
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import locale

class bank_list_export_summay(osv.TransientModel):

    _name = 'bank.list.export.summay'

    def _get_excel_export_bank_list_data(self, cr, uid, context=None):
        if context is None:
            context = {}
        workbook = xlwt.Workbook()
        worksheet = workbook.add_sheet('Sheet 1')
        font = xlwt.Font()
        font.bold = True
        
        style_header = xlwt.easyxf('font: name Courier New, bold on, color black, height 196; align: wrap off;')
        style_header_underline = xlwt.easyxf('font: name Courier New, bold on, color black, height 196, underline single; align: wrap off;')
        style_header_underline_bold_off = xlwt.easyxf('font: name Courier New, bold off, color black, height 196, underline single; align: wrap off;')
        style_simple = xlwt.easyxf('font: name Courier New, bold off, color black, height 198; align: wrap off;')
        style_simple1 = xlwt.easyxf('font: name Courier New, bold off, color black, height 198; align: wrap off, vert centre, horiz left;')
        style_simple2 = xlwt.easyxf('font: name Courier New, bold off, color black, height 198; align: wrap off, vert centre, horiz right;')
        
        payslip_obj = self.pool.get('hr.payslip')
        employee_obj = self.pool.get('hr.employee')
        period_obj = self.pool.get('account.period')
        bank_details_obj = self.pool.get('hr.bank.details')
        
        date_from = period_obj.browse(cr, uid, context.get('period_id')).date_start
        date_to = period_obj.browse(cr, uid, context.get('period_id')).date_stop
        date_formate = datetime.datetime.strptime(str(date_to), DEFAULT_SERVER_DATE_FORMAT).strftime('%d/%m/%Y')
        month_year = datetime.datetime.strptime(str(date_to), DEFAULT_SERVER_DATE_FORMAT).strftime('%m/%y')
        
        row = 0
        worksheet.col(0).width = 2650
        worksheet.col(1).width = 2650
        worksheet.col(2).width = 2650
        worksheet.col(3).width = 2650
        worksheet.col(4).width = 2650
        worksheet.col(5).width = 2650
        worksheet.col(6).width = 2650
        worksheet.col(7).width = 2650
        worksheet.col(8).width = 2650
        worksheet.col(9).width = 2650
        worksheet.col(10).width = 2650
        worksheet.col(11).width = 2650
        worksheet.col(12).width = 3500
        
        company_name=self.pool.get('res.users').browse(cr, uid, uid, context).company_id.name
        
        worksheet.write(row, 0, company_name, style_header)
        row += 2
        worksheet.write(row, 0, 'NAME', style_header)
        worksheet.write(row, 3, ':', style_header)
        worksheet.write(row, 4, 'BANK ADVICE LISTING', style_header)
        row += 1
        worksheet.write(row, 0, 'PAGE NO. ', style_header)
        worksheet.write(row, 3, ':', style_header)
        worksheet.write(row, 4, '1.00', style_header)
        row += 2
        worksheet.write(row, 0, 'DATE PRINTED', style_header)
        worksheet.write(row, 3, ':', style_header)
        worksheet.write(row, 4, tools.ustr(date_formate or ''), style_header)
        row += 1
        worksheet.write(row, 0, 'PAYROLL FOR MONTH', style_header)
        worksheet.write(row, 3, ':', style_header)
        worksheet.write(row, 4, tools.ustr(month_year or ''), style_header)
        row += 1
        worksheet.write(row, 0, 'PAYMODE', style_header)
        worksheet.write(row, 3, ':', style_header)
        worksheet.write(row, 4, 'Monthly', style_header)
        row += 3
        worksheet.write(row, 0, 'NO.', style_header_underline)
        worksheet.write(row, 1, 'EMPNO', style_header_underline)
        worksheet.write(row, 3, 'NAME', style_header_underline)
        worksheet.write(row, 7, 'IC NO.', style_header_underline)
        worksheet.write(row, 9, 'BANK ACCOUNT', style_header_underline)
        worksheet.write(row, 12, 'AMOUNT', style_header_underline)
        row += 2
        worksheet.write(row, 0, 'Bank : HSBC - HSBC BANK MALAYSIA BERHAD', style_header)
        row += 2
        head_count_row = row
        row += 1
        
        sequence = 0
        total_net_amount = 0.0
        for emp in employee_obj.browse(cr, uid, context.get('employee_ids')):
            bank_detail_ids = bank_details_obj.search(cr, uid, [('bank_emp_id','=', emp.id)])
            if bank_detail_ids:
                bank_data = bank_details_obj.browse(cr, uid, bank_detail_ids[0])
                payslip_ids = payslip_obj.search(cr, uid, [('employee_id', '=', emp.id), ('date_from', '>=', date_from), ('date_to', '<=', date_to), ('state', 'in', ['draft', 'done', 'verify'])])
                net_amount = 0.00
                for payslip in payslip_obj.browse(cr, uid, payslip_ids):
                    for line in payslip.line_ids:
                        if line.code == 'NET':
                            net_amount += line.total
                            total_net_amount += line.total
                if net_amount:
                    sequence += 1
                    worksheet.write(row, 0, int(sequence), style_simple1)
                    worksheet.write(row, 1, tools.ustr(emp.user_id and emp.user_id.login or ''), style_simple)
                    worksheet.write(row, 3, tools.ustr(emp.name or ''), style_simple)
                    worksheet.write(row, 7, tools.ustr(emp.identification_id or ''), style_simple)
                    worksheet.write(row, 9, tools.ustr(bank_data.bank_ac_no or ''), style_simple)
                    worksheet.write(row, 12, tools.ustr(locale.format("%.2f", float(net_amount), grouping=True)), style_simple2)
                    row += 1
        
        row += 4
        worksheet.write(head_count_row, 0, '{Head Count - ' + tools.ustr(int(sequence)) + '}', style_header)
        worksheet.write(row, 0, 'Total No. of Records', style_header)
        worksheet.write(row, 3, int(sequence), style_simple1)
        row += 1
        worksheet.write(row, 0, 'GRAND TOTAL :', style_header)
        worksheet.write(row, 12, tools.ustr(locale.format("%.2f", float(total_net_amount), grouping=True)), style_simple2)
        row += 1
        worksheet.write(row, 0, 'Kindly debit our Account Number ', style_simple)
        worksheet.write(row, 4, '303216642001', style_header_underline_bold_off)
        worksheet.write(row, 7, 'to the following amount RM', style_simple)
        worksheet.write(row, 9, tools.ustr(locale.format("%.2f", float(total_net_amount), grouping=True)), style_simple)
        row += 4
        worksheet.write(row, 0, 'Authorised Signature : _______________________', style_simple)
        worksheet.write(row, 9, 'Prepared By : ____________________', style_simple)
        row += 4
        worksheet.write(row, 0, 'Company Chop', style_simple)
        worksheet.write(row, 9, 'Checked By : ____________________', style_simple)
        
        fp = StringIO()
        workbook.save(fp)
        fp.seek(0)
        data = fp.read()
        fp.close()
        return base64.b64encode(data)

    _columns = {
        "file":fields.binary("Click On Download Link To Download Xls File", readonly=True),
        "name":fields.char("Name" , size=32)
    }
    
    _defaults = {
        'name':"Bank List.xls",
        'file': _get_excel_export_bank_list_data
    }

bank_list_export_summay()

class bank_list_xls(osv.TransientModel):

    _name = 'bank.list.xls'

    _columns = {
        'employee_ids': fields.many2many('hr.employee', 'ppm_hr_emp_bank_list_xls_rel','emp_id','employee_id','Employee Name'),
        'period_id': fields.many2one('account.period', 'Period', required=True),
    }

    def download_bank_list_xls_file(self, cr, uid, ids, context):
        data = self.read(cr, uid, ids)[0]
        period_id = data['period_id'][0]
        period_data = self.pool.get('account.period').browse(cr, uid, period_id)
        context.update({'employee_ids': data['employee_ids'], 'period_id': period_id})
        return {
              'name': _('Binary'),
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'bank.list.export.summay',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': context,
          }

bank_list_xls()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: