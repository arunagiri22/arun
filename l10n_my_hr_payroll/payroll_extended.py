# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv.orm import Model
from openerp.osv import fields, osv
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime
from dateutil import parser, rrule
from datetime import timedelta
import copy
import math
import calendar
from dateutil.relativedelta import relativedelta
from openerp.tools.safe_eval import safe_eval
#openerp.tools.safe_eval._ALLOWED_MODULES.append('math')

def _offset_format_timestamp(src_tstamp_str, src_format, dst_format, ignore_unparsable_time=True, context=None):
    """
    Convert a source timestamp string into a destination timestamp string, attempting to apply the
    correct offset if both the server and local timezone are recognized, or no
    offset at all if they aren't or if tz_offset is false (i.e. assuming they are both in the same TZ).

    @param src_tstamp_str: the str value containing the timestamp.
    @param src_format: the format to use when parsing the local timestamp.
    @param dst_format: the format to use when formatting the resulting timestamp.
    @param server_to_client: specify timezone offset direction (server=src and client=dest if True, or client=src and server=dest if False)
    @param ignore_unparsable_time: if True, return False if src_tstamp_str cannot be parsed
                                   using src_format or formatted using dst_format.

    @return: destination formatted timestamp, expressed in the destination timezone if possible
            and if tz_offset is true, or src_tstamp_str if timezone offset could not be determined.
    """
    if not src_tstamp_str:
        return False

    res = src_tstamp_str
    
    if src_format and dst_format:
        try:
            dt_value = datetime.strptime(src_tstamp_str,src_format)
            if context.get('tz',False):
                try:
                    import pytz
                    src_tz = pytz.timezone(context['tz'])
                    dst_tz = pytz.timezone('UTC')
                    src_dt = src_tz.localize(dt_value, is_dst=True)
                    dt_value = src_dt.astimezone(dst_tz)
                except Exception,e:
                    pass
            res = dt_value.strftime(dst_format)
        except Exception,e:
            if not ignore_unparsable_time:
                return False
            pass
    return res

class ir_cron(Model):
    """ Model describing cron jobs (also called actions or tasks).
    """
    _inherit = "ir.cron"
    
    def change_contract_default_name_year(self, cr, uid, reminder_to_change_year_number=None, context=None):
        if context is None:
            context = {}
        user_data = self.pool.get('res.users').browse(cr, uid, uid, context)
        context.update({'tz': user_data.tz})
        if reminder_to_change_year_number:
            cron_data = self.browse(cr, uid, reminder_to_change_year_number, context)
            str_dt_current =  _offset_format_timestamp(cron_data.nextcall, '%Y-%m-%d %H:%M:%S', DEFAULT_SERVER_DATETIME_FORMAT, context=context)
            cr.execute("UPDATE ir_cron SET nextcall=%s WHERE id=%s", (str_dt_current, reminder_to_change_year_number))
        return True

ir_cron()

class res_partner_bank(Model):
    _inherit = 'res.partner.bank'
    
    _columns = {
            'branch_id':fields.char("Branch ID", size=48),
    }
res_partner_bank()

class payroll_extended(Model):
    _inherit = 'hr.payslip.input'
    
    _columns = {
        'code': fields.char('Code', size=52, required=False, readonly= False,help="The code that can be used in the salary rules"),
        'contract_id': fields.many2one('hr.contract', 'Contract', required=False, help="The contract for which applied this input"),
    }
payroll_extended()
class hr_payslip_worked_days(Model):

    _inherit = 'hr.payslip.worked_days'

    _columns = {
        'code': fields.char('Code', size=52, required=False, readonly= False, help="The code that can be used in the salary rules"),
        'contract_id': fields.many2one('hr.contract', 'Contract', required=False, help="The contract for which applied this input"),
    }
hr_payslip_worked_days() 

class hr_payslip_line(Model):

    _inherit = 'hr.payslip.line'
    
    _columns = {
        'contract_id':fields.many2one('hr.contract', 'Contract', required=False, select=True),
        'employee_id':fields.many2one('hr.employee', 'Employee', required=False),
    }
hr_payslip_line()

class hr_salary_rule(osv.osv):
    _inherit ='hr.salary.rule'
    _columns = {
        'code':fields.char('Code', size=64, required=False, help="The code of salary rules can be used as reference in computation of other rules. In that case, it is case sensitive."),
        'id': fields.integer('ID', readonly=True),
        'hr_salary_rule_glcode_ids': fields.one2many('hr.salary.rule.glcode', 'salaryrule_id', 'Salary Rule GL Code'),
    }
hr_salary_rule()

class hr_contribution_register(Model):
    _inherit = 'hr.contribution.register'
    _columns = {
            'register_glcode_ids': fields.one2many('hr.salary.rule.glcode', 'register_id', 'Contribution GL Code')
    }
hr_contribution_register()

class hr_salary_rule_glcode(Model):
    _name = 'hr.salary.rule.glcode'
    _rec_name = 'salaryrule_id'
    _columns = {
        'register_id': fields.many2one('hr.contribution.register', 'GL Code'),
        'salaryrule_id': fields.many2one('hr.salary.rule', 'GL Code'),
        'gl_code_id': fields.many2one('salary.report.for.exchequer.glcode', 'GL Code', required=True),
        'emp_categ_id': fields.many2one('hr.employee.category', 'Employee Category', required=True),
        'saved_back_cpf': fields.boolean('Saved Back CPF'),
        'saved_back_salary': fields.boolean('Saved Back Salary'),
        'nagative': fields.boolean('Negative'),
        'apply_bank_cheque': fields.selection([('apply_for_bank', 'Apply for Bank'), ('apply_for_cheque', 'Apply For Cheque')], string='Apply for Bank/Cheque'),
    }
hr_salary_rule_glcode()

class salary_report_for_exchequer_glcode(Model):
    _name = 'salary.report.for.exchequer.glcode'
    _columns = {
            'name': fields.char('GL Code', size=32, required=True),
    }
    _sql_constraints = [
        ('name_uniq', 'UNIQUE(name)', 'The GL Code Name must be unique !'),
    ]
salary_report_for_exchequer_glcode()

class hr_payslip(Model):
    
    _inherit = 'hr.payslip'
    _order = 'employee_name'

    def _get_salary_computation_ytd_total(self, cr, uid, ids, name, args, context=None):
        res={}
        payslip_obj = self.pool.get('hr.payslip')
        for data in self.browse(cr, uid, ids, context):
            res[data.id] = {
                    'total_ytd_gross': 0.0,
                    'total_ytd_employee': 0.0,
                    'total_ytd_pcb': 0.0,
                    'remaining_month': 0,
            }
            total_ytd_gross = total_ytd_employee = total_ytd_pcb = 0.00
            remaining_month = 0
            current_date_from = datetime.strptime(data.date_from, DEFAULT_SERVER_DATE_FORMAT)
            month = current_date_from.strftime('%m')
            remaining_month = 12 - int(month)
            if int(month) > 1:
                year = current_date_from.strftime('%Y')
                year_start_date = '%s-01-01' % (year)
                last_day_of_previous_month = (datetime.strptime(data.date_from , DEFAULT_SERVER_DATE_FORMAT) - relativedelta(days=1)).strftime(DEFAULT_SERVER_DATE_FORMAT)
                payslip_ids = payslip_obj.search(cr, uid, [('employee_id', '=', data.employee_id.id),
                                                           ('date_from', '>=', year_start_date),
                                                           ('date_from', '<=', last_day_of_previous_month)])
                for payslip in payslip_obj.browse(cr, uid, payslip_ids):
                    for line in payslip.line_ids:
                        if line.code == 'GROSS':
                            total_ytd_gross += line.amount
                        if line.code == 'EPFE':
                            total_ytd_employee += line.amount
                        if line.code == 'PCB2013':
                            total_ytd_pcb += line.amount
            res[data.id]['total_ytd_gross'] = total_ytd_gross or 0.0
            res[data.id]['total_ytd_employee'] = total_ytd_employee or 0.0
            res[data.id]['total_ytd_pcb'] = total_ytd_pcb or 0.0
            res[data.id]['remaining_month'] = remaining_month or 0
        return res

    _columns = {
        'cheque_number':fields.char("Cheque Number", size=64),
        'active': fields.boolean('Pay'),
        'pay_by_cheque': fields.boolean('Pay By Cheque'),
        'employee_name': fields.related('employee_id', 'name', type="char", size=256, string="Employee Name", store=True),
        'total_ytd_gross': fields.function(_get_salary_computation_ytd_total, string='Total YTD Gross', type='float', multi="salary_computation_all"),
        'total_ytd_employee': fields.function(_get_salary_computation_ytd_total, string='Total YTD Employee', type='float', multi="salary_computation_all"),
        'total_ytd_pcb': fields.function(_get_salary_computation_ytd_total, string='Total YTD PCB', type='float', multi="salary_computation_all"),
        'remaining_month': fields.function(_get_salary_computation_ytd_total, string='Remaining Month', type='integer', multi="salary_computation_all"),
        
    }

    _defaults = {
        'active':True,
    }

    def get_date_from_range(self, cr, uid, ids, from_date, to_date, context=None):
        '''
            Returns list of dates from from_date tp to_date
            @param from_date: Starting date for range
            @param to_date: Ending date for range
            @return : Returns list of dates from from_date to to_date  
        '''
        dates = []
        if from_date and to_date:
            dates = list(rrule.rrule(rrule.DAILY, 
                             dtstart=parser.parse(from_date), 
                             until=parser.parse(to_date)))
        return dates

    def get_worked_day_lines(self, cr, uid, contract_ids, date_from, date_to, context=None):
        """
        @param contract_ids: list of contract id
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """
        if date_from and date_to:
            date_from_cur = datetime.strptime(date_from, DEFAULT_SERVER_DATE_FORMAT)
            previous_month_obj = parser.parse(date_from_cur.strftime(DEFAULT_SERVER_DATE_FORMAT)) - relativedelta(months=1)
            total_days = calendar.monthrange(previous_month_obj.year, previous_month_obj.month)[1]
            first_day_of_previous_month = datetime.strptime("1-" + str(previous_month_obj.month) + "-" + str(previous_month_obj.year) ,'%d-%m-%Y')
            last_day_of_previous_month = datetime.strptime(str(total_days) + "-" + str(previous_month_obj.month) + "-" + str(previous_month_obj.year) ,'%d-%m-%Y')
            date_from = datetime.strftime(first_day_of_previous_month, DEFAULT_SERVER_DATE_FORMAT)
            date_to = datetime.strftime(last_day_of_previous_month, DEFAULT_SERVER_DATE_FORMAT)

        results = super(hr_payslip,self).get_worked_day_lines(cr, uid, contract_ids, date_from, date_to, context=context)
        def was_on_leave(employee_id, datetime_day, context=None):
            res = False
            day = datetime_day.strftime("%Y-%m-%d")
            holiday_ids = self.pool.get('hr.holidays').search(cr, uid, [('state','=','validate'),('employee_id','=',employee_id),('type','=','remove'),('date_from','<=',day),('date_to','>=',day)])
            if holiday_ids:
                res = self.pool.get('hr.holidays').browse(cr, uid, holiday_ids, context=context)[0]
            return res
        holiday_obj = self.pool.get('hr.holidays')
        calendar_obj = self.pool.get('resource.calendar')
        final_results  = []
        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
            if not contract.working_hours:
                continue
            leaves = {}
            day_from = datetime.strptime(date_from,"%Y-%m-%d")
            day_to = datetime.strptime(date_to,"%Y-%m-%d")
            nb_of_days = (day_to - day_from).days + 1
            for day in range(0, nb_of_days):
                new_day_format = (day_from + timedelta(days=day)).strftime("%Y-%m-%d")
                holiday_ids = holiday_obj.search(cr, uid, [('state','=','validate'),('employee_id','=',contract.employee_id.id),
                                                    ('type','=','remove'),('date_from','<=',new_day_format),('date_to','>=',new_day_format),
                                                    ('holiday_status_id.weekend_calculation', '=', True)])
                if holiday_ids:
                    leave_type = holiday_obj.browse(cr, uid, holiday_ids[0], context=context)
                else:
                    leave_type = False
                    working_hours_on_day = calendar_obj.working_hours_on_day(cr, uid, contract.working_hours, day_from + timedelta(days=day), context)
                    if working_hours_on_day:
                        leave_type = was_on_leave(contract.employee_id.id, day_from + timedelta(days=day), context=context)
                if leave_type:
                    if leave_type.leave_type not in ['pm', 'am']:
                        no_of_day = 1.0
                    else:
                        no_of_day = 0.5 
                    if leave_type.holiday_status_id.name in leaves:
                        leaves[leave_type.holiday_status_id.name]['number_of_days'] += no_of_day
                        leaves[leave_type.holiday_status_id.name]['number_of_hours'] += working_hours_on_day * no_of_day
                    else:
                        leaves[leave_type.holiday_status_id.name] = {
                            'name': leave_type.holiday_status_id.name2,
                            'sequence': 5,
                            'code': leave_type.holiday_status_id.name,
                            'number_of_days': no_of_day,
                            'number_of_hours': working_hours_on_day * no_of_day,
                            'contract_id': contract.id,
                        }
            final_results += copy.deepcopy(results)
            for rec in results:
                if rec.get('code') in leaves and leaves.get(rec.get('code', {})):
                    final_results.remove(rec)
                    final_results.append(leaves.get(rec.get('code')))
            for key,val in leaves.items():
                flag = True
                for rec in results:
                    if key == rec.get('code'):
                        flag = False
                if flag:
                    final_results.append(leaves.get(key))
        return final_results

    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id, contract_id, context=None):
        result = super(hr_payslip,self).onchange_employee_id(cr, uid, ids, date_from, date_to, employee_id, contract_id, context=context)
        if date_from and date_to:
            current_date_from = date_from
            current_date_to = date_to
            date_from_cur = datetime.strptime(date_from, DEFAULT_SERVER_DATE_FORMAT)
            previous_month_obj = parser.parse(date_from_cur.strftime(DEFAULT_SERVER_DATE_FORMAT)) - relativedelta(months=1)
            total_days = calendar.monthrange(previous_month_obj.year, previous_month_obj.month)[1]
            first_day_of_previous_month = datetime.strptime("1-" + str(previous_month_obj.month) + "-" + str(previous_month_obj.year) ,'%d-%m-%Y')
            last_day_of_previous_month = datetime.strptime(str(total_days) + "-" + str(previous_month_obj.month) + "-" + str(previous_month_obj.year) ,'%d-%m-%Y')
            date_from = datetime.strftime(first_day_of_previous_month, DEFAULT_SERVER_DATE_FORMAT)
            date_to = datetime.strftime(last_day_of_previous_month, DEFAULT_SERVER_DATE_FORMAT)

            dates = list(rrule.rrule(rrule.DAILY, dtstart=parser.parse(date_from), until=parser.parse(date_to)))
            sunday = saturday = weekdays = 0
            for day in dates:
                if day.weekday() == 5:
                    saturday += 1
                elif day.weekday() == 6:
                    sunday += 1
                else:
                    weekdays += 1

            res = {'code':'TTLPREVDAYINMTH','name':'Total number of days for previous month','number_of_days':len(dates), 'sequence': 2,'contract_id' : contract_id}
            result.get('value').get('worked_days_line_ids').append(res)
            res = {'code':'TTLPREVSUNINMONTH','name':'Total sundays in previous month','number_of_days':sunday, 'sequence': 3,'contract_id' : contract_id}
            result.get('value').get('worked_days_line_ids').append(res)
            res = {'code':'TTLPREVSATINMONTH','name':'Total saturdays in previous month','number_of_days':saturday, 'sequence': 4,'contract_id' : contract_id}
            result.get('value').get('worked_days_line_ids').append(res)
            res = {'code':'TTLPREVWKDAYINMTH','name':'Total weekdays in previous month','number_of_days':weekdays, 'sequence': 5,'contract_id' : contract_id}
            result.get('value').get('worked_days_line_ids').append(res)

            dates = list(rrule.rrule(rrule.DAILY, dtstart=parser.parse(current_date_from), until=parser.parse(current_date_to)))
            sunday = saturday = weekdays = 0
            for day in dates:
                if day.weekday() == 5:
                    saturday += 1
                elif day.weekday() == 6:
                    sunday += 1
                else:
                    weekdays += 1
            res = {'code':'TTLCURRDAYINMTH','name':'Total number of days for current month','number_of_days':len(dates), 'sequence': 2,'contract_id' : contract_id}
            result.get('value').get('worked_days_line_ids').append(res)
            res = {'code':'TTLCURRSUNINMONTH','name':'Total sundays in current month','number_of_days':sunday, 'sequence': 3,'contract_id' : contract_id}
            result.get('value').get('worked_days_line_ids').append(res)
            res = {'code':'TTLCURRSATINMONTH','name':'Total saturdays in current month','number_of_days':saturday, 'sequence': 4,'contract_id':contract_id}
            result.get('value').get('worked_days_line_ids').append(res)
            res = {'code':'TTLCURRWKDAYINMTH','name':'Total weekdays in current month','number_of_days':weekdays, 'sequence': 5,'contract_id':contract_id}
            result.get('value').get('worked_days_line_ids').append(res)
        if employee_id:
            holiday_status_obj = self.pool.get("hr.holidays.status")
            holiday_status_ids = holiday_status_obj.search(cr, uid, [])
            for holiday_status in holiday_status_obj.browse(cr, uid, holiday_status_ids, context=context):
                flag = False
                for payslip_data in result["value"].get("worked_days_line_ids"):
                    if payslip_data.get("code") == holiday_status.name:
                        flag = True
                if not flag:
                    res = {'code':holiday_status.name,'name':holiday_status.name2,'number_of_days':0.0, 'sequence': 0,'contract_id' : contract_id}
                    result.get('value').get('worked_days_line_ids').append(res)
        return result

    def compute_sheet(self, cr, uid, ids, context=None):
        result = super(hr_payslip, self).compute_sheet(cr, uid, ids, context=context)
        slip_line_pool = self.pool.get('hr.payslip.line')
        lines = []
        for payslip in self.browse(cr, uid, ids, context=context):
            for line in payslip.line_ids:
                if line.amount == 0:
                    lines.append(line.id)
        if lines:
            slip_line_pool.unlink(cr, uid, lines, context=context)
        return result

hr_payslip()

class hr_contract(Model):
    
    _inherit = 'hr.contract'

    _columns = {
        'wage_to_pay': fields.float('Wage To Pay'),
        'category_dsc': fields.many2one('contract.category.dsc', 'Category DSC'),
        'category_mrb': fields.many2one('contract.category.mrb', 'Category MRB'),
        'rate_per_hour': fields.float('Rate per hour for part timer')
    }

    _defaults = {
        'name': '/'
    }

    def create(self, cr, uid, vals, context=None):
        vals.update({'name': self.pool.get('ir.sequence').get(cr, uid, 'hr.contract')})
        return super(hr_contract,self).create(cr, uid, vals, context=context)

    def reminder_to_change_year_number(self, cr, uid, context=None):
        sequence_obj = self.pool.get('ir.sequence')
        sequence_id = sequence_obj.search(cr, uid, [('code', '=', 'hr.contract')])
        sequence_obj.write(cr, uid, sequence_id, {'number_next': 1})
        return True

hr_contract()

class contract_category_dsc(Model):
    _name = 'contract.category.dsc'

    _columns = {
        'name': fields.char('Description', size=64),
        'd_amount': fields.float('Deduction for individual'),
        's_amount': fields.float('Deduction for spouse'),
        'c_amount': fields.float('Number of qualifying children'),
        'sequence': fields.integer('Sequence', size=1)
    }

contract_category_dsc()

class contract_category_mrb(Model):
    _name = 'contract.category.mrb'

    _columns = {
        'name': fields.char('Description', size=64),
        'm_amount': fields.float('M Amount'),
        'r_amount': fields.float('R Amount'),
        'b13_amount': fields.float('B (Category 1 & 3) Amount'),
        'b2_amount': fields.float('B (Category 2) Amount'),
        'sequence': fields.integer('Sequence', size=1)
    }

contract_category_mrb()

class hr_employee(Model):
    
    _inherit="hr.employee"
    
    _columns = {
            'epf_no': fields.integer('EPF No.'),
            'ahli_kwsp_note':fields.char('Ahli Kwsp Note', size=64),
            'no_perkeso': fields.char('No Perkeso', size=64),
    }
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        """
            Override Search method for put filter on current working status. 
        """
        if context and context.get('batch_start_date') and context.get('batch_end_date'):
            contract_obj = self.pool.get('hr.contract')
            active_contract_employee_list = []
            contract_ids = contract_obj.search(cr, uid,['|',('date_end','>=',context.get('batch_start_date')),('date_end','=',False),('date_start','<=',context.get('batch_end_date'))])
            for contract in contract_obj.browse(cr, uid, contract_ids):
                active_contract_employee_list.append(contract.employee_id.id)
            args.append(('id', 'in', active_contract_employee_list))
        return super(hr_employee, self).search(cr, uid, args, offset, limit, order, context=context, count=count)
    
hr_employee()

class hr_payslip_run(Model):

    _inherit = 'hr.payslip.run'
    _description = 'Payslip Batches'
    def open_payslip_employee(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if not ids:
            return True
        payslip_batch_data = self.browse(cr, uid, ids[0], context)
        context.update({'default_date_start': payslip_batch_data.date_start, 'default_date_end': payslip_batch_data.date_end})
        return {'name': ('Payslips by Employees'),
                'context': context,
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'hr.payslip.employees',
                'type': 'ir.actions.act_window',
                'target': 'new',
        }

hr_payslip_run()

class hr_payslip_employees(osv.TransientModel):
    
    _inherit = 'hr.payslip.employees'

    _columns = {
        'date_start': fields.date('Date From'),
        'date_end': fields.date('Date To'),
    }

hr_payslip_employees()

#class many2many_mod2(fields.many2many):
#    def get(self, cr, obj, ids, name, user=None, offset=0, context=None, values=None):
#        if context is None:
#            context = {}
#        context.update({'active_test': False})
#        return super(many2many_mod2, self).get(cr, obj, ids, name, user, offset, context=context)

class res_users(Model):
    
    _inherit = 'res.users'
    
    _columns = {
        'user_ids': fields.many2many('res.users', 'ppm_res_user_payroll_rel','usr_id','user_id','User Name'),
    }
res_users()

class cost_center(osv.osv):
    _name = 'cost.center'
    _columns = {
        'name': fields.char('Name',size=64, required=True)
    }
cost_center()

class product_category(osv.osv):
    _inherit = "product.category"
    
    _columns = {
        'cost_center_id':fields.many2one('cost.center','Cost center'),
    }
product_category()

class pcb_salary_rule(osv.Model):
    _name = 'pcb.salary.rule'

    _columns = {
        'from': fields.float('From'),
        'to': fields.float('To'),
        'k': fields.float('K'),
        'ka1': fields.float('KA1'),
        'ka2': fields.float('KA2'),
        'ka3': fields.float('KA3'),
        'ka4': fields.float('KA4'),
        'ka5': fields.float('KA5'),
        'ka6': fields.float('KA6'),
        'ka7': fields.float('KA7'),
        'ka8': fields.float('KA8'),
        'ka9': fields.float('KA9'),
        'ka10': fields.float('KA10'),
        'ka11': fields.float('KA11'),
        'ka12': fields.float('KA12'),
        'ka13': fields.float('KA13'),
        'ka14': fields.float('KA14'),
        'ka15': fields.float('KA15'),
        'ka16': fields.float('KA16'),
        'ka17': fields.float('KA17'),
        'ka18': fields.float('KA18'),
        'ka19': fields.float('KA19'),
        'ka20': fields.float('KA20'),
    }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
