# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "ppm_payroll",
    "version": "1.301",
    "depends": [
        "base", "hr_payroll","account_voucher","hr_recruitment","hr_evaluation","hr_holiday_extended","crm","product",
    ],
    'author': 'Prestige',
    "category": "HR",
    "description": """
        This module provide wizard to upload the csv file in payslip.
    """,
    "init_xml": [],
    'update_xml': [
       'security/group.xml',
       'security/hr.salary.rule.category.csv',
       'security/hr.contribution.register.csv',
       'salary_rule.xml',
       'rule_input.xml',
#       'security/hr.rule.input.csv',
#       'security/hr.salary.rule.csv',
     
       'security/ir.model.access.csv',
       'payroll_extended_view.xml',
       'wizard/upload_xls_wizard_view.xml',
       'wizard/payslip_summary_wizard_view.xml',
       'ppm_payroll_extended_report.xml',
       'payroll_sequence.xml',
       'payroll_schedule_data.xml',
       "wizard/export_employee_summary_wiz_view.xml",
       'wizard/payroll_exchequer_report_view.xml',
       'wizard/comput_confirm_payslip_wiz_view.xml',
       
    ],
    'installable': True,
    'auto_install':False,
    'application':True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
