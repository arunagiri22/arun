# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.report import report_sxw
import datetime
from openerp import tools
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

class hr_payslip_epf_report(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(hr_payslip_epf_report, self).__init__(cr, uid, name, context=context)
        self.totalepfy = 0.0
        self.totalepfe = 0.0
        self.totalrm = 0.0
        self.totalrecord = 0
        self.localcontext.update({
            'get_name': self.get_name,
            'get_date': self.get_date,
            'get_monthyear': self.get_monthyear,
            'get_totalepfy': self.get_totalepfy,
            'get_totalepfe': self.get_totalepfe,
            'get_totalrm': self.get_totalrm,
            'get_totalrm1': self.get_totalrm1,
            'get_totalrecord': self.get_totalrecord,
            'get_payroll_admin': self.get_payroll_admin,
           })

    def get_payroll_admin(self):
        mod_obj = self. pool.get('ir.model.data')
        grp_result = mod_obj.get_object(self.cr, self.uid, 'ppm_payroll', 'group_hr_payroll_admin')
        payroll_admin_name = ''
        if grp_result and grp_result.users:
            for user in grp_result.users:
                payroll_admin_name += user.name + ', '
        return payroll_admin_name

    def get_monthyear(self, date_to):
        date_to = self.pool.get('account.period').browse(self.cr, self.uid, date_to[0]).date_stop
        end_date = datetime.datetime.strptime(date_to, DEFAULT_SERVER_DATE_FORMAT)
        monthyear = end_date.strftime('%m%Y')
        return monthyear

    def get_date(self):
        date = datetime.datetime.today()
        date1 = datetime.datetime.date(date)
        date_formate = date1.strftime('%d/%m/%Y')
        return date_formate

    def get_name(self, name, period_id):
        payslip_obj = self.pool.get('hr.payslip')
        period_obj = self.pool.get('account.period')
        records = []
        res = {}
        date_from = period_obj.browse(self.cr, self.uid, period_id[0]).date_start
        date_to = period_obj.browse(self.cr, self.uid, period_id[0]).date_stop
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from', '<=', date_to), ('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        totalrecord = 0
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            totalrecord += 1
            epfy = 0.0
            epfe = 0.0
            net = 0.0
            for line in payslip.line_ids:
                if line.code == 'EPFY':
                    epfy = line.total
                if line.code == 'EPFE':
                    epfe = line.total
                if line.code == 'NET':
                    net = line.total
            res = {
                   'seq': totalrecord,
                   'name': payslip.employee_id.name,
                   'identification_no': payslip.employee_id.identification_id,
                   'userid': payslip.employee_id.identification_id,
                   'ahli': payslip.employee_id.ahli_kwsp_note,
                   'epfy': epfy or 0.0,
                   'epfe': epfe,
                   'net': net,
                }
            self.totalepfy += res['epfy']
            self.totalepfe += res['epfe']
            records.append(res)

        new_list = []
        
        epfy_grand_total_after = 0.0
        epfe_grand_total_after = 0.0
        epfy_grand_total_before = 0.0
        epfe_grand_total_before = 0.0
        page_no = 0
        for row in range(0, len(records) / 11 + 1):
            page_no += 1
            epfy_total = 0.0
            epfe_total = 0.0
            
            val = records[row * 11: row * 11 + 11]
            if not new_list:
                epfy_grand_total_before = 0.0
            else:
                epfy_grand_total_before = epfy_grand_total_after
            if not new_list:
                epfe_grand_total_before = 0.0
            else:
                epfe_grand_total_before = epfe_grand_total_after
            
            for i in range(0, len(val)):
                epfy_total = val[i].get('epfy')
                epfy_grand_total_after += epfy_total or 0.0
                self.totalrecord += 1
                epfe_total = val[i].get('epfe')
                epfe_grand_total_after += epfe_total or 0.0
            if val:
                new_list.append({'page_number': page_no, 'total_after_1': epfy_grand_total_after, 'total_after_2': epfe_grand_total_after , 'total_before_1': epfy_grand_total_before, 'total_before_2':epfe_grand_total_before, 'records': val})
        
        self.totalrm = self.totalepfe + self.totalepfy
        return new_list

    def get_totalepfy(self):
        return self.totalepfy

    def get_totalepfe(self):
        return self.totalepfe

    def get_totalrm(self):
        return self.totalrm

    def get_totalrm1(self, name, period_id):
        payslip_obj = self.pool.get('hr.payslip')
        period_obj = self.pool.get('account.period')
        res = {}
        totalepfy = 0.0
        totalepfe = 0.0
        date_from = period_obj.browse(self.cr, self.uid, period_id[0]).date_start
        date_to = period_obj.browse(self.cr, self.uid, period_id[0]).date_stop
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from', '<=', date_to), ('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            
            epfy = 0.0
            epfe = 0.0
            net = 0.0
            for line in payslip.line_ids:
                if line.code == 'EPFY':
                    epfy = line.total
                if line.code == 'EPFE':
                    epfe = line.total
                if line.code == 'NET':
                    net = line.total
            res = {
                   'epfy': epfy,
                   'epfe': epfe,
                   'net': net,
                }
            totalepfy += res['epfy']
            totalepfe += res['epfe']
        totalrm = totalepfe + totalepfy
        return totalrm or 0.00

    def get_totalrecord(self):
        return self.totalrecord

report_sxw.report_sxw('report.hrpayslipepf_receipt', 'hr.payslip', 'addons/l10n_my_hr_payroll/report/hr_payslip_epf_report.rml', parser=hr_payslip_epf_report)
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
