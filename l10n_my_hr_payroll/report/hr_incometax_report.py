# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.report import report_sxw
import datetime
from openerp import tools
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

class hr_payslip_incometax_report(report_sxw.rml_parse):
    
    def __init__(self,cr,uid,name,context):
        super(hr_payslip_incometax_report,self).__init__(cr,uid,name,context=context)
        self.totalpcb = 0.0
        self.totalcp38 = 0.0
        self.totalpcb_cp38 = 0.0
        self.totalrecord = 0
        self.localcontext.update({
            'get_name': self.get_name,
            'get_monthname': self.get_monthname,
            'get_year': self.get_year,
            'get_totalpcb': self.get_totalpcb,
            'get_totalcp38': self.get_totalcp38,
            'get_totalpcb_cp38': self.get_totalpcb_cp38,
            'get_totalpcb1': self.get_totalpcb1,
            'get_totalcp381': self.get_totalcp381,
            'get_totalrecord': self.get_totalrecord,
            'get_totalpcb_cp381': self.get_totalpcb_cp381,
           })
    
    def get_monthname(self, date_to):
        end_date = datetime.datetime.strptime(date_to, DEFAULT_SERVER_DATE_FORMAT)
        month = end_date.strftime('%B')
        return month
    
    def get_year(self, date_to):
        end_date = datetime.datetime.strptime(date_to, DEFAULT_SERVER_DATE_FORMAT)
        year = end_date.strftime('%Y')
        return year
    
    def get_name(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        list=[]
        res={}
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            PCB=0.0
            CP38=0.0
            for line in payslip.line_ids:
                if line.code.startswith('PCBCAT'):
                    PCB = line.total
                if line.code == 'PCBCP38':
                    CP38 = line.total
            res = {
                   'name': payslip.employee_id.name,
                   'identification_no': payslip.employee_id.identification_id,
                   'otherid': payslip.employee_id.otherid,
                   'userid': payslip.employee_id.user_id.name,
                   'passportno': payslip.employee_id.passport_id,
                   'statecode': payslip.employee_id.emp_state_id.code,
                   'pcb': PCB,
                   'cp38': CP38,
                }
            self.totalpcb += res['pcb']
            self.totalcp38 += res['cp38']
            list.append(res)
        self.totalpcb_cp38 = self.totalpcb + self.totalcp38
        return list
    
    def get_totalpcb(self):
        return self.totalpcb
    def get_totalcp38(self):
        return self.totalcp38
    def get_totalpcb_cp38(self):
        return self.totalpcb_cp38
    
    def get_totalpcb1(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        res={}
        totalpcb = 0.0
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            PCB=0.0
            for line in payslip.line_ids:
                if line.code.startswith('PCBCAT'):
                    PCB = line.total
            res = {
                   'pcb': PCB,
                }
            totalpcb += res['pcb']
        return totalpcb
    
    def get_totalcp381(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        res={}
        totalcp38 = 0.0
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            self.totalrecord += 1
            CP38=0.0
            for line in payslip.line_ids:
                if line.code == 'PCBCP38':
                    CP38 = line.total
            res = {
                   'cp38': CP38,
                }
            totalcp38 += res['cp38']
        return totalcp38
    
    def get_totalpcb_cp381(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        res={}
        totalpcb = 0.0
        totalcp38 = 0.0
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            PCB=0.0
            CP38=0.0
            for line in payslip.line_ids:
                if line.code.startswith('PCBCAT'):
                    PCB = line.total
                if line.code == 'PCBCP38':
                    CP38 = line.total
            res = {
                   'pcb': PCB,
                   'cp38': CP38,
                }
            totalpcb += res['pcb']
            totalcp38 += res['cp38']
        return totalpcb + totalcp38
    
    def get_totalrecord(self):
        return self.totalrecord

report_sxw.report_sxw('report.hr_payslip_report','hr.payslip','addons/l10n_my_hr_payroll/report/hr_incometax_report.rml',parser=hr_payslip_incometax_report)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
