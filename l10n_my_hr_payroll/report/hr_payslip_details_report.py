# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.report import report_sxw
import datetime
from openerp import tools
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

class hr_payslip_details_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(hr_payslip_details_report, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_details_by_rule_category': self.get_details_by_rule_category,
            'get_name': self.get_name,
            'get_earningtotal': self.get_earningtotal,
            'get_deductiontotal': self.get_deductiontotal,
            'get_contributiontotal': self.get_contributiontotal,
            'get_netpay': self.get_netpay,
            'get_date': self.get_date,
        })

    def get_date(self, period_id):
        period_data = self.pool.get('account.period').browse(self.cr, self.uid, period_id[0])
        start_date = datetime.datetime.strptime(period_data.date_start, DEFAULT_SERVER_DATE_FORMAT)
        month = start_date.strftime('%B %Y')
        return month
    
    def get_name(self, name, period_id):
        print "period_id::", period_id
        if not period_id:
            return []
        period_data = self.pool.get('account.period').browse(self.cr, self.uid, period_id[0])
        payslip_obj = self.pool.get('hr.payslip')
        res={}
        list = []
        pay = {'monthly': 'Monthly',
            'quarterly':'Quarterly',
            'semi-annually':'Semi-annually',
            'annually':'Annually',
            'weekly':'Weekly',
            'bi-weekly':'Bi-weekly',
            'bi-monthly':'Bi-monthly',
            }
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', period_data.date_start), ('date_from','<=',period_data.date_stop),('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            res = {
                   'number': payslip.employee_id.user_id and payslip.employee_id.user_id.login or '',
                   'employee_id': payslip.employee_id,
                   'payslip_id': payslip.id,
                   'name': payslip.employee_id.name,
                   'department_id': payslip.employee_id.department_id and payslip.employee_id.department_id.name or '',
                   'jobname': payslip.employee_id.job_id and payslip.employee_id.job_id.name or '',
                   'identification_no': payslip.employee_id.identification_id,
                   'bankname': payslip.employee_id.bank_detail_ids and payslip.employee_id.bank_detail_ids[0].bank_name or '',
                   'bank_acc_no': payslip.employee_id.bank_detail_ids and payslip.employee_id.bank_detail_ids[0].bank_ac_no or '',
                   'pay_rate': pay.get(payslip.contract_id.schedule_pay),
                   'wage': payslip.contract_id.wage,
                   'datefrom': period_data.date_start,
                   'dateto': period_data.date_stop,
                }
            list.append(res)
        return list
    
    def get_details_by_rule_category(self, payslip_id):
        payslip_obj = self.pool.get('hr.payslip')
        res={}
        basic_list = []
        payslip = payslip_obj.browse(self.cr, self.uid, payslip_id)
        basic = []
        deduction = []
        contribution = []
        NO_OF_RECORDS = 7
        overtime_hours = 0
        for input in payslip.input_line_ids:
            if input.code == 'INPUTOT':
                overtime_hours = input.amount
        overtime_rate = 0.0
        if overtime_hours and payslip.contract_id:
            overtime_rate = (payslip.contract_id.wage / 26 / 8 *1.5) * 1
            overtime_rate = overtime_rate.__trunc__()  + (((overtime_rate % 1) * 100 ).__trunc__()) * 0.01

        for line in payslip.line_ids:
            if line.category_id.code in ['BASIC', 'ALW', 'ADDT', 'CATBONUS']:
                if len(basic) <= NO_OF_RECORDS:
                    salary_rule_code = line.code
                    if line.code == 'ADDOT':
                        salary_rule_code = line.name +"    " + str(overtime_hours) + " Hour(s)" + "    " + str(overtime_rate)
                    basic.append({'code':salary_rule_code, 'total': line.total})
            if line.category_id.code in ['DED', 'DEDT', 'EPF_E','SCS_E', 'CATLHDN']:
                if len(deduction) <= NO_OF_RECORDS:
                    deduction.append({'dcode':line.code, 'dtotal': line.total})
            if line.category_id.code in ['EPF_Y', 'SOCSO_Y', 'COMP']:
                if len(contribution) <= NO_OF_RECORDS:
                    contribution.append({'ccode':line.code, 'ctotal': line.total})
        if len(basic) < NO_OF_RECORDS:
            for res in range(0, NO_OF_RECORDS-len(basic)):
                basic.append({'code':'', 'total': ''})
        if len(deduction) < NO_OF_RECORDS:
            for res in range(0, NO_OF_RECORDS-len(deduction)):
                deduction.append({'dcode':'', 'dtotal': ''})
        if len(contribution) < NO_OF_RECORDS:
            for res in range(0, NO_OF_RECORDS-len(contribution)):
                contribution.append({'ccode':'', 'ctotal': ''})
        for res in range(0,NO_OF_RECORDS):
            result = {'code': basic[res]['code'], 'total': basic[res]['total'],
                      'dcode':deduction[res]['dcode'], 'dtotal': deduction[res]['dtotal'],
                      'ccode':contribution[res]['ccode'], 'ctotal': contribution[res]['ctotal']}
            basic_list.append(result)
        return basic_list
    
    def get_earningtotal(self, payslip_id):
        payslip_obj = self.pool.get('hr.payslip')
        res={}
        total2=0.0
        payslip = payslip_obj.browse(self.cr, self.uid, payslip_id)
        for line in payslip.line_ids:
            if line.category_id.code in ['BASIC', 'ALW', 'ADDT', 'CATBONUS']:
                res = {
                       'total': line.total
                    }
                total2+=res['total']
        return total2
    
    def get_deductiontotal(self, payslip_id):
        payslip_obj = self.pool.get('hr.payslip')
        total1=0.0
        payslip = payslip_obj.browse(self.cr, self.uid, payslip_id)
        for line in payslip.line_ids:
            if line.category_id.code in ['DED', 'DEDT', 'EPF_E','SCS_E', 'CATLHDN']:
                total1 += line.total
        return total1
    
    def get_contributiontotal(self, payslip_id):
        payslip_obj = self.pool.get('hr.payslip')
        total=0.0
        payslip = payslip_obj.browse(self.cr, self.uid, payslip_id)
        for line in payslip.line_ids:
            if line.category_id.code in ['EPF_Y', 'SOCSO_Y', 'COMP']:
                total += line.total
        return total
    
    def get_netpay(self, payslip_id):
        payslip_obj = self.pool.get('hr.payslip')
        net=0.0
        payslip = payslip_obj.browse(self.cr, self.uid, payslip_id)
        for line in payslip.line_ids:
            if line.code in ['NET', 'net']:
                net = line.total
        return net

report_sxw.report_sxw('report.payslipdetails', 'hr.payslip', 'addons/l10n_my_hr_payroll/report/hr_payslip_details_report.rml', parser=hr_payslip_details_report)
