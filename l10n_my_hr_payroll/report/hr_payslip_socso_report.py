# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.report import report_sxw
import datetime
from openerp import tools
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

class hr_payslip_socso_report(report_sxw.rml_parse):
    
    def __init__(self,cr,uid,name,context):
        super(hr_payslip_socso_report,self).__init__(cr,uid,name,context=context)
        self.totalrecord = 0
        self.finaltotalscsey=0.0
        self.localcontext.update({
            'get_name': self.get_name,
            'get_date': self.get_date,
            'get_finaltotalscsey': self.get_finaltotalscsey,
            'get_finaltotalscsey1': self.get_finaltotalscsey1,
            'get_month': self.get_month,
            'get_year': self.get_year,
            'get_totalrecord': self.get_totalrecord,
           })
    
    def get_month(self, date_to):
        end_date = datetime.datetime.strptime(date_to, DEFAULT_SERVER_DATE_FORMAT)
        month = end_date.strftime('%m')
        return month
    
    def get_year(self, date_to):
        end_date = datetime.datetime.strptime(date_to, DEFAULT_SERVER_DATE_FORMAT)
        year = end_date.strftime('%y')
        return year
    
    def get_date(self, date_to):
        end_date = datetime.datetime.strptime(date_to, DEFAULT_SERVER_DATE_FORMAT)
        date = end_date.strftime('%d %B %Y')
        return date
    
    def get_name(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        records=[]
        res={}
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            totalscsey=0.0
            for line in payslip.line_ids:
                if line.code in ['SCSY', 'SCSE']:
                    totalscsey += line.total
            res = {
                   'name': payslip.employee_id.name,
                   'identification_no': payslip.employee_id.identification_id,
                   'no_perkeso': payslip.employee_id.no_perkeso,
                   'joindate': payslip.employee_id.join_date,
                   'totalscsey': totalscsey,
                }
            self.finaltotalscsey += res['totalscsey']
            records.append(res)
            
        new_list = []
        page_no = 0
        scsey_grand_total_after = 0.0
        scsey_grand_total_before = 0.0
        for row in range(0, len(records)/30 +1):
            scsey_total = 0.0
            page_no += 1
            val = records[row*30: row*30 + 30]
            
            if not new_list:
                scsey_grand_total_before = 0.0
            else:
                scsey_grand_total_before = scsey_grand_total_after
            
            for i in range(0, len(val)):
                scsey_total = val[i].get('totalscsey')
                scsey_grand_total_after += scsey_total or 0.0
                self.totalrecord += 1
            if val:
                new_list.append({'page_number': page_no,'total_after_1': scsey_grand_total_after, 'total_before_1': scsey_grand_total_before, 'records': val})
            
        return new_list
    
    def get_finaltotalscsey(self):
        return self.finaltotalscsey
    
    def get_finaltotalscsey1(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        res={}
        finaltotalscsey = 0.0
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),('employee_id', 'in', name), ('state', 'in', ['draft', 'done', 'verify'])])
        for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
            
            totalscsey=0.0
            for line in payslip.line_ids:
                if line.code in ['SCSY', 'SCSE']:
                    totalscsey += line.total
            res = {
                   'totalscsey': totalscsey,
                }
            finaltotalscsey += res['totalscsey']
        return finaltotalscsey
    
    def get_totalrecord(self):
        return self.totalrecord

report_sxw.report_sxw('report.borang8a','hr.payslip','addons/l10n_my_hr_payroll/report/hr_payslip_socso_report.rml',parser=hr_payslip_socso_report)
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: