# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.report import report_sxw
import datetime
from datetime import date

class hr_payroll_summary(report_sxw.rml_parse):
    
    def __init__(self,cr,uid,name,context):
        super(hr_payroll_summary,self).__init__(cr,uid,name,context=context)
        self.result_emp = []
        self.result_emp1 = []
        self.final_group_total = []
        self.localcontext.update({
            'get_name': self.get_name,
            'datetime': datetime,
            'finalgrouptotal': self.finalgrouptotal,
            'get_groupname': self.get_groupname,
           })

    def get_groupname(self, date_from, date_to):
        start_date = datetime.datetime.strptime(date_from, "%Y-%m-%d")
        month = start_date.strftime('%m')
        list = []
        year = start_date.strftime('%Y')
        res = {
               'period': month,
               'year': year,
            }
        list.append(res)
        return list
    
    def get_name(self, emp_ids, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        hr_depart_obj = self.pool.get('hr.department')
        employee_obj = self.pool.get('hr.employee')
        result = {}
        total = {}
        employee_ids = employee_obj.search(self.cr, self.uid, [('id', 'in', emp_ids)])
        for employee in employee_obj.browse(self.cr, self.uid, employee_ids):
            payslip_ids = payslip_obj.search(self.cr, self.uid, [('employee_id','=',employee.id),('date_from', '>=', date_from),
                                                                 ('date_from','<=',date_to), ('state', 'in', ['draft', 'done', 'verify'])])
            net = twage = lvd = exa = exd = gross = cpf = pf = 0.0
            if not payslip_ids:
                continue
            for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
                for rule in payslip.details_by_salary_rule_category:
                    if rule.code == 'BASIC':
                        twage += rule.total
                    elif rule.code == 'NET':
                        net += rule.total
                    elif rule.code == 'LVD':
                        lvd += rule.total
                    elif rule.code == 'EXA':
                        exa += rule.total
                    elif rule.code == 'EXD':
                        exd += rule.total
                    elif rule.code == 'GROSS':
                        gross += rule.total
                    elif rule.code == 'CPF':
                        cpf += rule.total
                    elif rule.code == 'PF':
                        pf += rule.total
            payslip_result = {
                                'ename': payslip.employee_id.name or '', 
                                'eid': payslip.employee_id and payslip.employee_id.user_id and payslip.employee_id.user_id.login or '',
                                'twage': payslip.employee_id.contract_id.wage,
                                'twage': twage,
                                'net': net,
                                'lvd': lvd,
                                'exa': exa,
                                'exd': exd,
                                'gross': gross,
                                'cpf': cpf,
                                'pf': pf,
                        }
            if payslip.employee_id.department_id:
                if payslip.employee_id.department_id.id in result:
                    result.get(payslip.employee_id.department_id.id).append(payslip_result)
                else:
                    result.update({payslip.employee_id.department_id.id: [payslip_result]})
            else:
                if 'Undefine' in result:
                    result.get('Undefine').append(payslip_result)
                else:
                    result.update({'Undefine': [payslip_result]})
        finaltwage = finalnet = finallvd = finalexa =finalexd = finalgross = finalcpf = finalpf = 0
        final_result = {}
        for key, val in result.items():
            if key == 'Undefine':
                category_name = 'Undefine'
            else:
                category_name = hr_depart_obj.browse(self.cr, self.uid, key).name
            total = {'name': category_name, 'twage': 0.0, 'net': 0.0, 'lvd': 0.0, 'exa': 0.0, 'exd': 0.0, 'gross':0.0, 'cpf': 0.0, 'pf': 0.0}
            for line in val:
                for field in line:
                    if field in total:
                        total.update({field:  total.get(field) + line.get(field)})
            final_result[key] = {'lines': val, 'total': total}
            finaltwage += total['twage']
            finalnet += total['net']
            finallvd += total['lvd']
            finalexa += total['exa']
            finalexd += total['exd']
            finalgross += total['gross']
            finalcpf += total['cpf']
            finalpf += total['pf']
        final_total = {
                       'twage' : finaltwage,
                       'net' : finalnet,
                       'lvd' : finallvd,
                       'exa' : finalexa,
                       'exd' : finalexd,
                       'gross' : finalgross,
                       'cpf' : finalcpf,
                       'pf' : finalpf,
                }
        self.final_group_total.append(final_total)
        return final_result.values()

    def finalgrouptotal(self):
        return self.final_group_total

report_sxw.report_sxw('report.hr_payroll_summary_receipt','hr.payslip','addons/l10n_my_hr_payroll/report/hr_payroll_summary_report.rml',parser=hr_payroll_summary)
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: