# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.report import report_sxw
import datetime

class hr_bank_summary_report(report_sxw.rml_parse):
    
    def __init__(self,cr,uid,name,context):
        super(hr_bank_summary_report,self).__init__(cr,uid,name,context=context)
        self.localcontext.update({
            'get_info': self.get_info,
            'datetime': datetime,
            'get_totalrecord': self.get_totalrecord,
            'get_total': self.get_total,
        })
    
    def get_info(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        hr_depart_obj = self.pool.get('hr.department')
        employee_obj = self.pool.get('hr.employee')
        bank_obj = self.pool.get('hr.bank.details')
        hr_department_search_id =  hr_depart_obj.search(self.cr, self.uid, [])
        result = {}
        payslip_data= {}
        department_info = {}
        final_result = {}
        
        employee_ids = employee_obj.search(self.cr, self.uid, [('bank_detail_ids','!=',False), ('id', 'in', name), ('department_id', '=', False)])
        department_total_amount = 0.0
        new_employee_ids = []
        if employee_ids:
            bank_ids = bank_obj.search(self.cr, self.uid, [('bank_emp_id', 'in', employee_ids)], order="bank_name, bank_code, branch_code")
            for bank in bank_obj.browse(self.cr, self.uid, bank_ids):
                if bank.bank_emp_id.id not in new_employee_ids:
                    new_employee_ids.append(bank.bank_emp_id.id)
            blank_emp_ids = list(set(employee_ids).difference(set(new_employee_ids)))
            new_employee_ids += blank_emp_ids
        for employee in employee_obj.browse(self.cr, self.uid, new_employee_ids):
            payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),
                                                                 ('employee_id', '=' , employee.id), ('pay_by_cheque','=',False), ('state', 'in', ['draft', 'done', 'verify'])])
            net = 0.0
            if not payslip_ids:
                continue
            for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
                if not payslip.employee_id.department_id.id:
                    for line in payslip.line_ids:
                        if line.code == 'NET':
                            net += line.total
            print "::::::::::::", employee, employee.bank_detail_ids
            payslip_data = {
                            'bank_name':employee.bank_detail_ids and employee.bank_detail_ids[0].bank_name or '',
                            'bank_id':employee.bank_detail_ids and employee.bank_detail_ids[0].bank_code or '',
                            'branch_id':employee.bank_detail_ids and employee.bank_detail_ids[0].branch_code or '',
                            'employee_id':employee and employee.user_id and employee.user_id.login or ' ',
                            'employee_name':employee.name,
                            'account_number':employee.bank_detail_ids and employee.bank_detail_ids[0].bank_ac_no or '',
                            'amount':net,
            }
            department_total_amount += net
            if 'Undefine' in result:
                result.get('Undefine').append(payslip_data)
            else:
                result.update({'Undefine': [payslip_data]})
        department_total = {'total': department_total_amount, 'department_name': 'Total Undefine'}
        
        if 'Undefine' in department_info:
            department_info.get('Undefine').append(department_total)
        else:
            department_info.update({'Undefine': [department_total]})
                
        
        
        for hr_department in hr_depart_obj.browse(self.cr, self.uid, hr_department_search_id):
            employee_ids = employee_obj.search(self.cr, self.uid, [('bank_detail_ids','!=',False), ('id', 'in', name), ('department_id', '=', hr_department.id)])
            new_employee_ids = []
            if employee_ids:
                bank_ids = bank_obj.search(self.cr, self.uid, [('bank_emp_id', 'in', employee_ids)], order="bank_name, bank_code, branch_code")
                for bank in bank_obj.browse(self.cr, self.uid, bank_ids):
                    if bank.bank_emp_id.id not in new_employee_ids:
                        new_employee_ids.append(bank.bank_emp_id.id)
                blank_emp_ids = list(set(employee_ids).difference(set(new_employee_ids)))
                new_employee_ids += blank_emp_ids
            department_total_amount = 0.0
            for employee in employee_obj.browse(self.cr, self.uid, new_employee_ids):
                payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),
                                               ('employee_id', '=' , employee.id), ('pay_by_cheque','=',False), ('state', 'in', ['draft', 'done', 'verify'])])
                net = 0.0
                if not payslip_ids:
                    continue
                for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
                    for line in payslip.line_ids:
                        if line.code == 'NET':
                            net += line.total
                print "::::::::::::", employee, employee.bank_detail_ids
                payslip_data = {
                            'bank_name':employee.bank_detail_ids and employee.bank_detail_ids[0].bank_name or '',
                            'bank_id':employee.bank_detail_ids and employee.bank_detail_ids[0].bank_code or '',
                            'branch_id':employee.bank_detail_ids and employee.bank_detail_ids[0].branch_code or '',
                            'employee_id':employee and employee.user_id and employee.user_id.login or ' ',
                            'employee_name':employee.name,
                            'account_number':employee.bank_detail_ids and employee.bank_detail_ids[0].bank_ac_no or '',
                            'amount':net,
                }
                department_total_amount += net
                if hr_department.id in result:
                    result.get(hr_department.id).append(payslip_data)
                else:
                    result.update({hr_department.id: [payslip_data]})
            department_total = {'total': round(department_total_amount, 2), 'department_name': "Total " + hr_department.name}
            if hr_department.id in department_info:
                department_info.get(hr_department.id).append(department_total)
            else:
                department_info.update({hr_department.id: [department_total]})
        for key, val in result.items():
            final_result[key] = {'lines': val, 'departmane_total': department_info[key] }
        return final_result.values()
    
    def get_total(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        
        total_ammount = 0
        payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('pay_by_cheque','=',False), ('employee_id.bank_detail_ids','!=',False), ('date_from','<=',date_to),('employee_id', 'in' , name), ('state', 'in', ['draft', 'done', 'verify'])])
        if payslip_ids:
            for payslip in payslip_obj.browse(self.cr, self.uid, payslip_ids):
                for line in payslip.line_ids:
                    if line.code == 'NET':
                        total_ammount+=line.total
        return total_ammount
    
    def get_totalrecord(self, name, date_from, date_to):
        payslip_obj = self.pool.get('hr.payslip')
        employee_obj = self.pool.get('hr.employee')
        count = 0
        for employee in employee_obj.browse(self.cr, self.uid, name):
            payslip_ids = payslip_obj.search(self.cr, self.uid, [('date_from', '>=', date_from), ('date_from','<=',date_to),
                                                                 ('employee_id', '=' , employee.name), ('pay_by_cheque','=',False), ('employee_id.bank_detail_ids','!=',False), ('state', 'in', ['draft', 'done', 'verify'])])
            if payslip_ids:
                count += 1
        return count

report_sxw.report_sxw('report.ppm_bank_summary_receipt','hr.payslip','addons/l10n_my_hr_payroll/report/hr_bank_summary_report.rml',parser=hr_bank_summary_report)
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: