from openerp.osv import osv,fields

class ParticularReport(osv.AbstractModel):
    _name = 'report.skykod_payslip_summary.template_ded_summary'
    
    def render_html(self,cr,uid,ids,data=None,context=None):
        print "hello"
        report_obj = self.pool['report']
        report = report_obj._get_report_from_name(cr,uid,'skykod_payslip_summary.template_ded_summary')
        total={'adv':0.0,'att':0.0,'sft':0.0,'work':0.0,'spe':0.0,'meal':0.0,'tra':0.0,'alw_ded':0.0}
        rec=self.pool[report.model].browse(
                cr, uid, ids, context=context
            )
        rec.ensure_one()
        for item in rec.line_ids:
                total['adv']+=item.adv
                total['att']+=item.ata_ded
                total['sft']+=item.shif_ded
                total['work']+=item.wa_ded
                total['spe']+=item.spa_ded
                total['meal']+=item.ma_ded
                total['tra']+=item.ta_ded
                total['alw_ded']+=item.all_dew
                
                
        docargs = {
        'tot':total,
        'doc_model': report.model,
            'docs': self.pool[report.model].browse(
                cr, uid, ids, context=context
            ),
         }
     
        return report_obj.render(cr,uid,ids,'skykod_payslip_summary.template_ded_summary', docargs,context=context)
        