#-*- coding:utf-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from datetime import datetime
from openerp.osv import osv
from openerp.report import report_sxw
from openerp.tools import amount_to_text_en

class payroll_summary_report(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(payroll_summary_report, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'get_month': self.get_month,
            'convert': self.convert,
            'get_detail': self.get_detail,
            'get_net_total': self.get_net_total,
            'get_ot_total': self.get_ot_total,
            'get_adv_total': self.get_adv_total,
            'get_alw_total': self.get_alw_total,
            'get_esocso_total': self.get_esocso_total,
            'get_socso_total': self.get_socso_total,
            'get_epmf_total': self.get_epmf_total,
            'get_epf_total': self.get_epf_total,
            'get_pcb_total': self.get_pcb_total,
            'get_gross_total': self.get_gross_total,
            'get_basic_total': self.get_basic_total,
            'get_allow_tot_total': self.get_allow_tot_total,
            'get_dla_total': self.get_dla_total,
            'get_levy_total': self.get_levy_total,
            'get_spa_total': self.get_spa_total,
            'get_ata_total': self.get_ata_total,
            'get_wa_total': self.get_wa_total,
            'get_oa_total': self.get_oa_total,
            'get_ma_total': self.get_ma_total,
            'get_ta_total': self.get_ta_total,
        })
        self.context = context
        
    def get_month(self, input_date):
        payslip_pool = self.pool.get('hr.payslip')
        res = {
               'from_name': '', 'to_name': ''
               }
        slip_ids = payslip_pool.search(self.cr, self.uid, [('date_from','<=',input_date), ('date_to','>=',input_date)], context=self.context)
        if slip_ids:
            slip = payslip_pool.browse(self.cr, self.uid, slip_ids, context=self.context)[0]
            from_date = datetime.strptime(slip.date_from, '%Y-%m-%d')
            to_date =  datetime.strptime(slip.date_to, '%Y-%m-%d')
            res['from_name']= from_date.strftime('%d')+'-'+from_date.strftime('%B')+'-'+from_date.strftime('%Y')
            res['to_name']= to_date.strftime('%d')+'-'+to_date.strftime('%B')+'-'+to_date.strftime('%Y')
        return res

    def convert(self, amount, cur):
        return amount_to_text_en.amount_to_text(amount, 'en', cur);
    
    
    def get_net_total(self):
        return self.total_net
    
    def get_ot_total(self):
        return self.total_ot

    def get_adv_total(self):
        return self.total_adv
        
    def get_alw_total(self):
        return self.total_alw
    
    def get_esocso_total(self):
        return self.total_esocso
    
    def get_socso_total(self):
        return self.total_socso
    
    def get_epmf_total(self):
        return self.total_epmf
    
    def get_epf_total(self):
        return self.total_epf
    
    def get_pcb_total(self):
        return self.total_pcb
    
    def get_gross_total(self):
        return self.total_gross
    
    def get_basic_total(self):
        return self.total_basic
    
    def get_allow_tot_total(self):
        return self.total_alw_tot
   
    def get_dla_total(self):
        return self.total_dla
    
    def get_levy_total(self):
        return self.total_levy
    
    def get_spa_total(self):
        return self.total_spa
    
    def get_ata_total(self):
        return self.total_ata
    
    def get_wa_total(self):
        return self.total_wa
    
    def get_oa_total(self):
        return self.total_oa
    
    def get_ma_total(self):
        return self.total_ma
    
    def get_ta_total(self):
        return self.total_ta
    
    def get_detail(self, line_ids):
        result = []
        self.total_net = 0.00
        self.total_ot = 0.00
        self.total_adv = 0.00
        self.total_alw = 0.00
        self.total_esocso = 0.00
        self.total_socso = 0.00
        self.total_epmf = 0.00
        self.total_epf = 0.00
        self.total_pcb = 0.00
        self.total_gross = 0.00
        self.total_basic = 0.00
        self.total_alw_tot = 0.00
        self.total_dla = 0.00
        self.total_levy = 0.00
        self.total_spa = 0.00
        self.total_ata = 0.00
        self.total_wa = 0.00
        self.total_oa = 0.00
        self.total_ma = 0.00
        self.total_ta = 0.00
        for l in line_ids:
            res = {}
            res.update({
                    'name': l.employee_id.name,
                    'identification_id': l.identification_id,
                    'number': l.number,
                    'net': l.net,
                    'gross': l.gross,
                    'basic': l.basic,
                    'pcb': l.pcb,
                    'epf': l.epf,
                    'epmf': l.epmf,
                    'socso': l.socso,
                    'esocso': l.esocso,
                    'alw': l.alw,
                    'ot': l.ot,
                    'all_dew': l.all_dew,
                    'dla': l.dla,
                    'levy': l.levy,
                    
                    })
            self.total_net += l.net
            self.total_ot += l.ot
           
            self.total_alw += l.alw
            self.total_esocso += l.esocso
            self.total_socso += l.socso
            self.total_epmf += l.epmf
            self.total_epf += l.epf
            self.total_pcb += l.pcb
            self.total_gross += l.gross
            self.total_basic += l.basic
            self.total_alw_tot += l.all_dew
            self.total_dla += l.dla
            self.total_levy += l.levy
            self.total_spa += l.spa
          
            result.append(res) 
        return result

class wrapped_report_payroll_summary(osv.AbstractModel):
    _name = 'report.skykod_payslip_summary.report_payrollsummary'
    _inherit = 'report.abstract_report'
    _template = 'skykod_payslip_summary.report_payrollsummary'
    _wrapped_report_class = payroll_summary_report
