from openerp.osv import osv,fields

class ParticularReport(osv.AbstractModel):
    _name = 'report.skykod_payslip_summary.template_alw_summary'
    
    def render_html(self,cr,uid,ids,data=None,context=None):
        print "hello"
        report_obj = self.pool['report']
        report = report_obj._get_report_from_name(cr,uid,'skykod_payslip_summary.template_alw_summary')
        total={'adv':0.0,'att':0.0,'sft':0.0,'work':0.0,'out':0.0,'spe':0.0,'meal':0.0,'tra':0.0,'alw':0.0}
        rec=self.pool[report.model].browse(
                cr, uid, ids, context=context
            )
        rec.ensure_one()
        for item in rec.line_ids:
                total['adv']+=item.adv
                total['att']+=item.ata
                total['work']+=item.wa
                total['out']+=item.oa
                total['spe']+=item.spa
                total['meal']+=item.ma
                total['tra']+=item.ta
                total['alw']+=item.alw
                total['sft']+=item.shif
                
        docargs = {
        'tot':total,
        'doc_model': report.model,
            'docs': self.pool[report.model].browse(
                cr, uid, ids, context=context
            ),
         }
     
        return report_obj.render(cr,uid,ids,'skykod_payslip_summary.template_alw_summary', docargs,context=context)
        