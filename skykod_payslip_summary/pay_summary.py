import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
from calendar import isleap

from openerp.tools.translate import _
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
   
   #PAYROLL SUMMARY# 
class payroll_summary(osv.osv):
    '''
    Employee Summary
    '''
    _name = 'hr.payroll.summary'
    _description = 'Employee Summary'
    _columns = {
        'user_id': fields.many2one('res.users', 'Username',),
        'name':fields.char('Reference', readonly=True, required=True, states={'draft': [('readonly', False)]},),
        'note': fields.text('Description'),
        'date': fields.date('Date', readonly=True, required=True, states={'draft': [('readonly', False)]}, help="Advice Date is used to search Payslips"),
        'state':fields.selection([
            ('draft', 'Draft'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),
        ], 'Status', select=True, readonly=True),
        'num': fields.char('Reference',),
        'line_ids': fields.one2many('hr.payroll.summary.line', 'summary_id', 'Employee Salary', states={'draft': [('readonly', False)]}, readonly=True, copy=True),
        'chaque_nos': fields.char('Cheque Numbers'),
        'neft': fields.boolean('NEFT Transaction', help="Check this box if your company use online transfer for salary"),
        'company_id':fields.many2one('res.company', 'Company', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'bank_id':fields.many2one('res.bank', 'Bank', readonly=True, states={'draft': [('readonly', False)]}, help="Select the Bank from which the salary is going to be paid"),
        'batch_id': fields.many2one('hr.payslip.run', 'Batch', readonly=True),
    }

    _defaults = {
        'date': lambda * a: time.strftime('%Y-%m-%d'),
        'state': lambda * a: 'draft',
        'company_id': lambda self, cr, uid, context: \
                self.pool.get('res.users').browse(cr, uid, uid,
                    context=context).company_id.id,
        'note': "Please make the payroll transfer from above account number to the below mentioned account numbers towards employee salaries:"
    }
    
    
    def compute_summary(self, cr, uid, ids, context=None):
        """
        summary - Create Summary lines in Employee summary and
        compute Summary lines.
        @param cr: the current row, from the database cursor,
        @param uid: the current users ID for security checks,
        @param ids: List of Summarys IDs
        @return: Summary lines
        @param context: A standard dictionary for contextual values
        """
        summary_line_pool = self.pool.get('hr.payroll.summary.line')
        payslip_line_pool = self.pool.get('hr.payslip.line')
        payslip_pool = self.pool.get('hr.payslip')
        
        
        for summary in self.browse(cr, uid, ids, context=context):
            old_line_ids = summary_line_pool.search(cr, uid, [('summary_id', '=', summary.id)], context=context)
            if old_line_ids:
                summary_line_pool.unlink(cr, uid, old_line_ids, context=context)
            slip_ids = payslip_pool.search(cr, uid, [('date_from', '<=', summary.date), ('date_to', '>=', summary.date), ('state', '=', 'done')], context=context)
            for slip in payslip_pool.browse(cr, uid, slip_ids, context=context):
                if not slip.employee_id.bank_account_id and not slip.employee_id.bank_account_id.acc_number:
                    raise osv.except_osv(_('Error!'), _('Please define bank account for the %s employee') % (slip.employee_id.name))
                spe,sft,att,wa,ma,ta=0,0,0,0,0,0
                
                for inp in slip.input_line_ids:
                    if inp.code=='SPEALL':
                        spe=inp.amount
                    if inp.code=='SFT':
                        sft=inp.amount
                    if inp.code=='ATT':
                        att=inp.amount
                    if inp.code=='WALL':
                        wa=inp.amount
                    if inp.code=='MALL':
                        ma=inp.amount
                    if inp.code=='TALL':
                        ta=inp.amount
                line_ids1 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'NET')], context=context)
                line_ids2 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'GROSS')], context=context)
                line_ids3 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'BASIC')], context=context)
                line_ids4 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'ITAX')], context=context)
                line_ids5 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'EPF DEDUCT')], context=context)
                line_ids6 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'EPMF DEDUCT')], context=context)
                line_ids7 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'SOCSO DEDUCT')], context=context)
                line_ids8 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'ESOCSO DEDUCT')], context=context)
                line_ids9 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'NET ALLOW')], context=context)
                line_ids10 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'ADVANCE')], context=context)
                line_ids11 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'OT ALLOW')], context=context)
                line_ids12 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'ALLOW DEDUCT')], context=context)
                line_ids13 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'LEAVE')], context=context)
                line_ids14 = payslip_line_pool.search(cr, uid, [('slip_id', '=', slip.id), ('code', '=', 'LEVY')], context=context)
                
                summary_line={}
                if line_ids2:
                    line2 = payslip_line_pool.browse(cr, uid, line_ids2, context=context)[0]
                    summary_line.update({'gross':line2.total})
                if line_ids3:
                    line3 = payslip_line_pool.browse(cr, uid, line_ids3, context=context)[0]
                    summary_line.update({'basic':line3.total})
                if line_ids4:
                    line4 = payslip_line_pool.browse(cr, uid, line_ids4, context=context)[0]
                    summary_line.update({'pcb':line4.total})
                if line_ids5:
                    line5 = payslip_line_pool.browse(cr, uid, line_ids5, context=context)[0]
                    summary_line.update({'epf':line5.total})
                if line_ids6:
                    line6 = payslip_line_pool.browse(cr, uid, line_ids6, context=context)[0]
                    summary_line.update({'epmf':line6.total})
                if line_ids7:
                    line7 = payslip_line_pool.browse(cr, uid, line_ids7, context=context)[0]
                    summary_line.update({'socso':line7.total})
                if line_ids8:
                    line8 = payslip_line_pool.browse(cr, uid, line_ids8, context=context)[0]
                    summary_line.update({'esocso':line8.total})
                if line_ids9:
                    line9 = payslip_line_pool.browse(cr, uid, line_ids9, context=context)[0]
                    summary_line.update({'alw':line9.total})
                if line_ids10:
                    line10 = payslip_line_pool.browse(cr, uid, line_ids10, context=context)[0]
                    summary_line.update({'adv':line10.total})
                if line_ids11:
                    line11 = payslip_line_pool.browse(cr, uid, line_ids11, context=context)[0]
                    summary_line.update({'ot':line11.total})
                if line_ids12:
                    line12 = payslip_line_pool.browse(cr, uid, line_ids12, context=context)[0]
                    summary_line.update({'all_dew':line12.total})
                if line_ids13:
                    line13 = payslip_line_pool.browse(cr, uid, line_ids13, context=context)[0]
                    summary_line.update({'dla':line13.total})
                if line_ids14:
                    line14 = payslip_line_pool.browse(cr, uid, line_ids14, context=context)[0]
                    summary_line.update({'levy':line14.total})
                summary_line.update({'spa':spe})
                summary_line.update({'shif':sft})
                summary_line.update({'ata':att})
                summary_line.update({'wa':wa})
                summary_line.update({'ma':ma})
                summary_line.update({'ta':ta})
                summary_line.update({'oa':slip.contract_id.other_allow})
                summary_line.update({'spa_ded':slip.contract_id.special-spe})
                summary_line.update({'ata_ded':120-att if slip.contract_id.attbonus else 0})
                summary_line.update({'wa_ded':slip.contract_id.work-wa})
                summary_line.update({'ma_ded':slip.contract_id.meal-ma})
                summary_line.update({'ta_ded':slip.contract_id.trans-ta})
                summary_line.update({'shif_ded':slip.contract_id.shift-sft})
                
                if line_ids1:
                    line = payslip_line_pool.browse(cr, uid, line_ids1, context=context)[0]
                    summary_line.update({
                            'summary_id': summary.id,
                            'name': slip.employee_id.bank_account_id.acc_number,
                            'bank_bic': slip.employee_id.bank_account_id.bank_bic,
                            'employee_id': slip.employee_id.id,
                            'identification_id': slip.employee_id.identification_id ,
                            'number': slip.number ,
                            'ic_id': slip.employee_id.ic_id,
                            'mobile_phone': slip.employee_id.mobile_phone ,
                            'net': line.total,
                    })
                
                    summary_line_pool.create(cr, uid, summary_line, context=context)
        return True
   
    def confirm_sheet(self, cr, uid, ids, context=None):
        """
        confirm Summary - confirmed Summary after computing Summary Lines..
        @param cr: the current row, from the database cursor,
        @param uid: the current users ID for security checks,
        @param ids: List of confirm Summarys IDs
        @return: confirmed Summary lines and set sequence of Summary.
        @param context: A standard dictionary for contextual values
        """
        seq_obj = self.pool.get('ir.sequence')
        for summary in self.browse(cr, uid, ids, context=context):
            if not summary.line_ids:
                raise osv.except_osv(_('Error!'), _('You can not confirm Payroll summary without summary lines.'))
            DATETIME_FORMAT = "%Y-%m-%d"
            summary_date = datetime.strptime(summary.date, DATETIME_FORMAT)
            summary_year = summary_date.strftime('%m') + '-' + summary_date.strftime('%Y')
            #~ number = seq_obj.get(cr, uid, 'summary.advice')
            #~ sequence_num = 'PAY' + '/' + summary_year + '/' + number
            self.write(cr, uid, [summary.id], { 'state': 'confirm'}, context=context)
        return True
        
    def set_to_draft(self, cr, uid, ids, context=None):
        """Resets Summary as draft.
        """
        return self.write(cr, uid, ids, {'state':'draft'}, context=context)
    
    def cancel_sheet(self, cr, uid, ids, context=None):
        """Marks Summary as cancelled.
        """
        return self.write(cr, uid, ids, {'state':'cancel'}, context=context)
        
    def onchange_company_id(self, cr, uid, ids, company_id=False, context=None):
        res = {}
        if company_id:
            company = self.pool.get('res.company').browse(cr, uid, [company_id], context=context)[0]
            if company.partner_id.bank_ids:
                res.update({'bank_id': company.partner_id.bank_ids[0].bank.id})
        return {
            'value':res
        }

class payroll_summary_line(osv.osv):
    '''
       Employee Summary Lines
    '''
    def onchange_employee_id(self, cr, uid, ids, employee_id=False, context=None):
        res = {}
        hr_obj = self.pool.get('hr.employee')
        if not employee_id:
            return {'value': res}
        employee = hr_obj.browse(cr, uid, [employee_id], context=context)[0]
        res.update({'name': employee.bank_account_id.acc_number , 'ifsc_code': employee.bank_account_id.bank_bic or ''})
        res.update({'name': employee.bank_account_id.bank_bic ,})
        res.update({'identification_id': employee.identification_id ,})
        return {'value': res}

    _name = 'hr.payroll.summary.line'
    _description = 'Employee Summary Lines'
    _columns = {
        'summary_id': fields.many2one('hr.payroll.summary', 'Employee Summary'),
        'number': fields.char('Reference'),
        'employee_id': fields.many2one('hr.employee', 'Employee', required=True),
        'identification_id': fields.char('Emp Number', size=32, readonly=True),
        #~ 'bank_bic': fields.char('Bank Code', size=32, readonly=True),
        #~ 'ic_id': fields.char('IC number', size=11),
        'net': fields.float('NET', digits_compute=dp.get_precision('Payroll')),
        'gross': fields.float('GROSS', digits_compute=dp.get_precision('Payroll')),
        'basic': fields.float('BASIC', digits_compute=dp.get_precision('Payroll')),
        'pcb': fields.float('PCB', digits_compute=dp.get_precision('Payroll')),
        'epf': fields.float('EPF', digits_compute=dp.get_precision('Payroll')),
        'epmf': fields.float('EPMF', digits_compute=dp.get_precision('Payroll')),
        'socso': fields.float('SOCSO', digits_compute=dp.get_precision('Payroll')),
        'esocso': fields.float('ESOCSO', digits_compute=dp.get_precision('Payroll')),
        'alw': fields.float('ALW', digits_compute=dp.get_precision('Payroll')),
        #'loan': fields.float('LOAN', digits_compute=dp.get_precision('Payroll')),
        'ot': fields.float('OT', digits_compute=dp.get_precision('Payroll')),
        #'arre': fields.float('ARREARS', digits_compute=dp.get_precision('Payroll')),
        'all_dew':fields.float('Allow Deduction',digits_compute=dp.get_precision('Payroll')),
        'adv':fields.float('Advance',digits_compute=dp.get_precision('Payroll')),
        'dla': fields.float('Leave Deduct', digits_compute=dp.get_precision('Payroll')),
        'levy': fields.float('LEVY', digits_compute=dp.get_precision('Payroll')),
        'spa': fields.float('Special Allowance', digits_compute=dp.get_precision('Payroll')),
        'shif': fields.float('Shift Allowance', digits_compute=dp.get_precision('Payroll')),
        'ata': fields.float('Attendance Allowance', digits_compute=dp.get_precision('Payroll')),
        'wa': fields.float('working Allowance', digits_compute=dp.get_precision('Payroll')),
        'oa': fields.float('Other Allowance', digits_compute=dp.get_precision('Payroll')),
        'ma': fields.float('Meal Allowance', digits_compute=dp.get_precision('Payroll')),
        'ta': fields.float('Transport Allowance', digits_compute=dp.get_precision('Payroll')),
        'company_id': fields.related('summary_id', 'company_id', type='many2one', required=False, relation='res.company', string='Company', store=True),
        'spa_ded': fields.float('Special Alw Ded', digits_compute=dp.get_precision('Payroll')),
        'ata_ded': fields.float('Attend Alw Ded', digits_compute=dp.get_precision('Payroll')),
        'wa_ded': fields.float('Work Ale ded', digits_compute=dp.get_precision('Payroll')),
        'ma_ded': fields.float('Meal Alw Ded', digits_compute=dp.get_precision('Payroll')),
        'ta_ded': fields.float('Transport Alw Ded', digits_compute=dp.get_precision('Payroll')),
        'shif_ded': fields.float('Shift Alw Ded', digits_compute=dp.get_precision('Payroll')),
       
    }
    _defaults = {
        'debit_credit': 'C',
    }
    

class hr_payslip(osv.osv):
    '''
    Employee Pay Slip
    '''
    _inherit = ['hr.payslip','mail.thread', 'ir.needaction_mixin']
    _description = 'Pay Slips'
    _track = {
        'state': {
            'hr.mt_payslip_confirmed': lambda self, cr, uid, obj, ctx=None: obj.state in ['manual'],
            'hr.mt_payslip_sent': lambda self, cr, uid, obj, ctx=None: obj.state in ['sent']
        },
    }
    _columns = {
        'advice_id': fields.many2one('hr.payroll.advice', 'Bank Advice', copy=False),
        'summary_id': fields.many2one('hr.payroll.summary', 'Employee Summary',copy=False),
        
    
    }
