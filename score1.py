letter = ['A','B','C','D','F']
def score(marks):
    if marks >= 90:
        letter = 'A'
    elif marks >= 80:
        letter = 'B'
    elif marks >= 70:
        letter = 'C'
    elif marks >= 60:
        letter = 'D'
    else:
        letter = 'F'
    return letter
