import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp import api, tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as OE_DATEFORMAT


class student(osv.osv):
	
	_name='student'
	_columns={
	             'name': fields.char('Student Name', help='the name'),
	             'first_name': fields.char('first name',size=30, help='the first name'), 
	             'birth_date': fields.date('birth date',size=30,required=True, help='the birth date'),
	             'email': fields.char('email',size=50, help='the email'),
	             'phone': fields.char('phone',size=30, help='the phone'),
	             'level':fields.many2one('speciality','Student Speciality'),
	
	}
student()

class speciality(osv.osv):
    _name = 'speciality'
    _columns = {
        'name': fields.char('Name specialty',size=50,required=True, help='the name specialty'),
        'level': fields.char('Level',size=50, help='the level'),
    }
speciality()

