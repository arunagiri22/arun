# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name" : "Leaves Management Extension",
    "version" : "1.0",
    "author" :"Serpent Consulting Services Pvt. Ltd.",
    "website" : "http://www.serpentcs.com",
    "category": "Human Resources",
    "description" : 
    '''
    Module to manage leave request approval.
    Documents can attached with a leave request.
    Calculate remaining leaves and carry forward to the next year.
    Carry forwarded leaves period.
    Public holiday lists and pdf report directly emailed to employees.
    ''',
    "depends" : [
        "hr_holidays",
        "hr_attendance",
        "hr_recruitment",
        "hr_evaluation",
        "hr_contract",
#        "prestige_hr_applicant_extended",
        "account"
    ],
    "init_xml": [],
    "update_xml": [
#        "security/group.xml",
        "wizard/hr_refuse_leave_view.xml",
        "hr_holiday_ex_view.xml",
        "wizard/inactive_wizard_view.xml",
        "board_hr_holidays_view.xml",
#        "security/ir.model.access.csv",
        "report/public_holiday_rml.xml",
        'data/demo_data.xml',
#        "hr_holiday_ex_data.xml",
    ],
    "installable": True,
    "auto_install": False,
    "application": True,
}
