# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw

class document_expiry_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(document_expiry_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_emp_name': self.get_emp_name,
            'get_doc_type': self.get_doc_type,
        })

    def get_emp_name(self, employee_id):
        return self.pool.get('hr.employee').browse(self.cr, self.uid, int(employee_id)).name or ''


    def get_doc_type(self, doc_type_id):
        return self.pool.get('document.type').browse(self.cr, self.uid, doc_type_id).name or ''

report_sxw.report_sxw('report.document.expiry', 'employee.immigration', 'hr_holiday_extended/report/document_expiry_report.rml', parser=document_expiry_report, header=False)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: