# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
import time

class inactive_status(osv.osv_memory):
    _name='inactive.status'
    
    _columns = {
        'inact_date' : fields.datetime('Inactive Date'),
        'reason':fields.text('Reason')
    }
    
    _defaults = {
        'inact_date' : lambda *a: time.strftime('%Y-%m-%d %H:%M:%S')
    }
    
    def submit(self,cr,uid,ids,context=None):
        wiz_recs = self.browse(cr, uid, ids,context=context)
        emp_obj = self.pool.get('hr.employee')
        
        for wiz_rec in wiz_recs:
            vals = {
                'inact_date':wiz_rec.inact_date,
                'reason' :wiz_rec.reason,
            }
            emp_obj.write(cr, uid, context['active_id'],vals, context=context)
        return True
    
    def cancel(self, cr, uid, ids, context=None):
        return {'type': 'ir.actions.act_window_close'}
inactive_status()