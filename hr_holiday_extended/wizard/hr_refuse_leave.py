# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2015 OpenERP SA (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import netsvc

class refuse_leave(osv.osv_memory):
    _name = 'refuse.leave'
    
    _columns ={
        'reason' : fields.text ('Reason'), 
    }
    def add_reason(self, cr, uid, ids, context=None):
        hr_holidays_obj = self.pool.get('hr.holidays')
        if not context.get('active_id'):
            return {}
        user_data = self.pool.get('res.users').browse(cr, uid, uid, context)
        wf_service = netsvc.LocalService('workflow')
        hr_holidays_data = hr_holidays_obj.browse(cr, uid, context.get('active_id'))
        for data in self.browse(cr, uid, ids, context):
            if data.reason:
                new_data = ''
                if context.get('cancel'):
                    new_data = "Leave Cancelled Reason. (%s) \n--------------------------------------------------------------------------" % user_data.name
                elif context.get('refuse'):
                    new_data = "Leave Refused Reason. (%s) \n----------------------------------------------------------------------------"  % user_data.name
                orignal_note = ''
                if hr_holidays_data.notes:
                    orignal_note = hr_holidays_data.notes
                reason = orignal_note + "\n\n" + new_data + "\n\n" + data.reason or ''
                hr_holidays_obj.write(cr, uid, [context.get('active_id')], {'notes': reason, 'rejection' : data.reason})
        if context.get('cancel'):
            wf_service.trg_validate(uid, 'hr.holidays', context.get('active_id'), 'cancel', cr)
        if context.get('refuse'):
            wf_service.trg_validate(uid, 'hr.holidays', context.get('active_id'), 'refuse', cr)
        return {'type' : 'ir.actions.act_window_close'}
        
refuse_leave()