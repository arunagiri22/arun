from datetime import datetime, timedelta
import time
from openerp import tools, api
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow

class partner(osv.Model):

    _name = _inherit = 'res.partner'

    _columns = {
            
            'property_account_hirepurchase_debtor': fields.property(
            type='many2one',
            relation='account.account',
            string="Hire Purchase Debtor",
            help="This account will be used instead of the default one as the hire purchase debtor",),
        
            'property_account_hirepurchase_creditor': fields.property(
            type='many2one',
            relation='account.account',
            string="Hire Purchase Interest Suspense",
            help="This account will be used instead of the default one as the hire purchase debtor",),
            
            'property_cash_deposit': fields.property(
            type='many2one',
            relation='account.account',
            string="Hire Purchase Cash Deposit",
            help="This account will be used instead of the default one as the cash deposit",),
            
            'property_purchase_secondhand': fields.property(
            type='many2one',
            relation='account.account',
            string="Hire Purchase Second Hand",
            help="This account will be used instead of the default one as the purchase second hand",),
            }
    
    def _commercial_fields(self, cr, uid, context=None):
        return super(res_partner, self)._commercial_fields(cr, uid, context=context) + ['property_account_hirepurchase_debtor',
        'property_account_hirepurchase_creditor','property_cash_deposit', 'property_purchase_secondhand']

partner()
