##############################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2014 Mr <mr@A>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
###############################################################################

{
    'name': 'Malaysian - Accounting',
    'version': '1.0',
    'description': """
Malaysian Accounting: Chart of Account For SkyGST.
=======================================

Malaysian accounting chart and localization.
    """,
    'author': 'Aravinth',
    'website': 'http://www.aravinth.co.in',
    'category': 'Localization/Account Charts',
    'depends': [
        'base',
        'account',
        'account_chart'
    ],
    'demo': [],
    'data': [
        'account_view.xml',
        'partner_view.xml',
        'account.account.type.csv',
        'account.account.template.csv',
        'account.tax.code.template.csv',
        'account.chart.template.csv',
        'account.tax.template.csv',
        'res.country.state.csv',
        'l10n_my_wizard.xml',
    ],
    'auto_install': False,
    'installable': True,
    'images': ['images/config_chart_l10n_my.jpeg','images/l10n_my_chart.jpeg'],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
