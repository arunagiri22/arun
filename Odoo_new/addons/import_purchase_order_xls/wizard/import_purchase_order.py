from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from datetime import datetime

class import_purchase_order(models.TransientModel):
    _name='import.purchase.order'
    _description = 'Import Purchase Order Wizard'
    
    order_id = fields.Many2one('purchase.order', 'Purchase Order')
    item_ids = fields.One2many('import.purchase.order.line', 'import_order_id', 'Items')
    invalid_code_ids = fields.Text('Invalid codes')
    
    def default_get(self, cr, uid, fields, context=None):
        if context is None: context = {}
        res = super(import_purchase_order, self).default_get(cr, uid, fields, context=context)
        res['order_id'] = context.get('order_id', False)
        res['item_ids'] = context.get('item_ids', False)
        res['invalid_code_ids'] = context.get('item_ids', False)
        return res
        
        
    @api.multi
    def wizard_view(self):
        view = self.env.ref('import_purchase_order_xls.view_ipox_purchase_order_import_wizard')

        return {
            'name': _('Order details'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'import.purchase.order',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': self.ids[0],
            'context': self.env.context,
        }
    
    @api.multi
    def process_import(self,mode):
        items = self.item_ids
        order = self.order_id
        for item in items:
            line_id = item.line_id
            res = self.env['purchase.order.line'].onchange_product_id()
            res.update({'name' : item.product_id.name})
            res.update({'date_planned':datetime.now()})
            res.update({'product_uom':item.product_uom_id.ids[0]})
            res.update({'price_unit' : item.product_id.list_price})
            taxes_id = res.get('taxes_id',False)
            if taxes_id:
                res.update({'taxes_id' : [(6,0,taxes_id)]})
            
            if item.price:
                res.update({'price_unit' : item.price})
                
            res.update({
                         'product_id' : item.product_id.id,
                         'order_id' : self.order_id.id ,
                         'product_qty' : item.quantity
                       })
            
            if item.description:
                res.update({'name': item.description,})
                
            if mode == 'w':
                line_id.write(res)
            else:
                line_id = self.env['purchase.order.line'].create(res)
        order.write({})        
        return True
    
    @api.multi
    def do_append(self):
        '''Append order items'''
        self.process_import(mode = 'c')
        return {'type': 'ir.actions.act_window_close'}
    
    @api.multi
    def do_update(self):
        '''Update existing items'''
        self.process_import(mode = 'w')
        return {'type': 'ir.actions.act_window_close'}
    
        
class import_purchase_order_line(models.TransientModel):
    _name='import.purchase.order.line'
    _description = 'Import Purchase Order Items'    
    
    import_order_id = fields.Many2one('import.purchase.order', 'Order')
    line_id = fields.Many2one('purchase.order.line', 'Order Line')
    description = fields.Char('Description')
    product_id = fields.Many2one('product.product', 'Product')
    product_name = fields.Char('Name')
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure')
    quantity = fields.Float('Quantity', digits=dp.get_precision('Product Unit of Measure'), default = 1.0)
    price = fields.Float('Price', digits=dp.get_precision('Product Unit of Measure'), default = 0.0)
    
    
    