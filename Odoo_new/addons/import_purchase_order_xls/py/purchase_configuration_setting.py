from openerp import models, fields

class purchase_config_settings(models.TransientModel):
    _inherit = 'purchase.config.settings'
    
    group_visible_import_purchase_order = fields.Boolean('Import purchase order line from excel file ?', 
                        implied_group='purchase.group_purchase_order_line_import',                                     
                        help = 'You can allow user to import purchase order line from excel file.')
    
    