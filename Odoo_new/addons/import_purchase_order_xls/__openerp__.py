{
    'name': 'Import Purchase Order From Excel',
    'version': '9.0',
    'category': 'Purchases',
    'summary' : 'Import/Export/Update large Purchase order from Excel',
    'description': """
        This module gives you facility to import purchase order lines through the excel file, so you can import a large purchase order easily
        
        Dependencies:
        
        This module requires external packages python-openpyxl & python-xlrd. 
        
        you can install this package by perform following commands on terminal,
        
        sudo apt-get install python-openpyxl
        
        sudo apt-get install python-xlrd
        
        For more information about our services in Odoo, please contact us on info@emiprotechnologies.com
        """,
    'author': 'Emipro Technologies Pvt. Ltd.',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',
    'depends': ['purchase'],
    'images': ['static/description/main_screen.png'],
    'data': [
                'security/purchase_security_group.xml',
                'wizard/process_purchase_order.xml',
                'wizard/import_purchase_order.xml',
                'view/configuration_setting.xml',
            ],
    'installable': True,
    'auto_install': False,
    'application' : True,
    'external_dependencies' : {'python' : ['openpyxl', 'xlrd'], },
    'price': 40.00,
    'currency': 'EUR',

}
