# -*- coding: utf-8 -*-
##############################################################################
#
#   OpenERP, Open Source Management Solution
#    Copyright (C) 2013 webkul
#	 Author :
#				www.webkul.com	
#
##############################################################################
{
    'name': 'POB Maintainance Module (Stock Comparision)',
    'version': '1.0',
    'category': 'POB : Maintenance Module',
    'sequence': 1,    
    'description': """
POB : Maintenance Module for Product Stock Comparision
=============================================================================

This Module helps in comparing and correcting stock(s) between Odoo and Prestashop.

You get to know the products who have different stock on Odoo and Prestashop,
along with their details.

For any doubt or query email us at support@webkul.com or raise a Ticket on http://webkul.com/ticket/

	""",
	'author' : 'Webkul Software Pvt Ltd.',
	'website' : 'http://www.webkul.com',
	'depends' : ['pob'],
	'data' : ['pob_maintainance_stock_view.xml'],
	'installable': True,
	'active': True,	
	'auto_install':False
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: