 # -*- coding: utf-8 -*-
##############################################################################
#		
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 webkul
#	 Author :
#				www.webkul.com	
#
##############################################################################

import sys
from time import strptime
import time
from decimal import *
from xmlrpclib import *
import xmlrpclib
import urllib
import datetime
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import prestapi
from prestapi import PrestaShopWebService,PrestaShopWebServiceDict,PrestaShopWebServiceError,PrestaShopAuthenticationError

################## ..........Prestashop Product inheritance .........##################

def condition_stock_error(operand, left, right):	
	if operand == '=':
		operand = '=='
	else:
		operand = '!='
	result = eval(' '.join((str(left),operand,str(right))))
	return result

def condition_stock_error(operand, left, right):	
	if operand == '=':
		operand = '=='
	else:
		operand = '!='
	result = eval(' '.join((str(left),operand,str(right))))
	return result

class pob_maintainance_stock(osv.osv):
	_inherit = "prestashop.product"

	def _get_ps_stock(self,cr,uid,prestashop,presta_product_id,presta_product_attribute_id,context=None):
		try:
			if int(presta_product_attribute_id)>0:
				stock_search=prestashop.get('stock_availables',options={'filter[id_product]':presta_product_id,'filter[id_product_attribute]':presta_product_attribute_id})	
				stock_id = stock_search['stock_availables']['stock_available']['attrs']['id']
				if stock_id:
					stock_data = prestashop.get('stock_availables', stock_id)
					return str(stock_data['stock_available']['quantity'])				
			elif int(presta_product_id)>0 and presta_product_attribute_id==0:				
				product_data = prestashop.get('products',presta_product_id)
				stock_id = product_data['product']['associations']['stock_availables']['stock_available']['id']
				if stock_id:
					stock = prestashop.get('stock_availables', stock_id)
					return str(stock['stock_available']['quantity'])
			return ''
		except Exception,e:
			return ''
			
	def _get_configurations(self, cr, uid, context=None):
		res={}
		config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
		if not config_id:
			raise osv.except_osv(_('Error'), _("Connection needs one Active Configuration setting."))
		if len(config_id)>1:
			raise osv.except_osv(_('Error'), _("Sorry, only one Active Configuration setting is allowed."))
		else:
			obj=self.pool.get('prestashop.configure').browse(cr,uid,config_id[0])
			res['url']=obj.api_url
			res['key']=obj.api_key
		return res
	
	def _get_ps_product_stock(self, cr, uid, ids, field_names=None, arg=False, context=None):
		if context is None:
			context = {}
		res={}
		for id in ids:
			res[id]='Not Available'
		ps_conf = self._get_configurations(cr, uid)
		try:
			prestashop = PrestaShopWebServiceDict(ps_conf['url'],ps_conf['key'])
		except Exception, e:
			raise osv.except_osv(_('Connection Error'), _("Error : "+str(e)))
		for obj in self.browse(cr, uid, ids, context=context):
			res[obj.id] = self._get_ps_stock(cr,uid,prestashop,obj.presta_product_id,obj.presta_product_attr_id)
		return res
			
	def _get_oe_product_stock(self, cr, uid, ids, field_names=None, arg=False, context=None):
		""" Finds the incoming and outgoing quantity of product.
		@return: Dictionary of values
		"""
		if not field_names:
			field_names = []
		if context is None:
			context = {}
		res = {}
		# ps_conf = self._get_configurations(cr, uid)
		all_active_ids = self.search(cr,uid,[('product_name','!=',False)])
		map_dict = {}
		all_pids=[]
		for obj in self.browse(cr, uid,all_active_ids, context=context):
			map_dict[obj.id]=obj.product_name.id
			all_pids.append(obj.product_name.id)
		# raise osv.except_osv(_("DFDSGFDSGDFS"),_(map_dict))
		for id in ids:
			res[id] = {}.fromkeys(field_names, 0.0)
		for f in field_names:
			c = context.copy()
			if f == 'qty_available':				
				c.update({ 'states': ('done',), 'what': ('in', 'out') })
			if f == 'outgoing_qty':
				c.update({ 'states': ('confirmed','waiting','assigned'), 'what': ('out') })
			stock = self.pool.get('product.product')._product_available(cr, uid, all_pids, context=c)
			for id in ids:
				res[id][f] = stock.get(map_dict.get(id), 0.0)[f]
				# raise osv.except_osv(_("DSGFDFD"),_(stock))	


		return res
	
	def _get_report(self, cr, uid, ids, field_names=None, arg=False, context=None):
		res = {}
		l = []
		for id in ids:
			res[id]=False
		for obj in self.browse(cr, uid,ids,context=context):
			oe_avail = int(obj.qty_available)+int(obj.outgoing_qty)
			l.append(obj.ps_qty_available)
			if not obj.ps_qty_available=="''":		
				if str(oe_avail)!=obj.ps_qty_available:
					res[obj.id]=True
			else:
				res[obj.id]=False		
		return res


	def _stock_error_search(self, cr, uid, obj, name, args, context=None):
		if not len(args):
			return []
		search_ids=[]		
		op = args[0][1]
		operand2 = args[0][2]
		ids = self.search(cr, uid, [])		
		res = self.read(cr,uid,ids,['stock_error'])		
		for data in res:
			if condition_stock_error(op,data['stock_error'],operand2):
				search_ids.append(data['id'])
		if not search_ids:
			return [('id', '=', 0)]
		return [('id', 'in', [x for x in search_ids])]



		
	_columns = {
	'qty_available': fields.function(_get_oe_product_stock, multi='qty_available',
            type='integer',
            string='OE (Quantity On Hand)'),
	'outgoing_qty': fields.function(_get_oe_product_stock, multi='outgoing_qty',
            type='integer',
            string='OE (Outgoing Quantity)'),
	'ps_qty_available': fields.function(_get_ps_product_stock,
            type='text',
            string='PS (Quantity On Hand)'),
	'stock_error': fields.function(_get_report,type='boolean',fnct_search=_stock_error_search, string='Stock Mis-match'),
	}
pob_maintainance_stock()
