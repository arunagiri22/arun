# -*- coding: utf-8 -*-
{
    'name': 'Automatic Reordering Rule',
    'version': '1.0',
    'website' : '',
    'category': '',
    'summary': 'Automatic re-ordering rule on product creation',
    'description': """


""",
    'author': 'Openstow',
    'depends': ['base',  'procurement'],
    'data': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
