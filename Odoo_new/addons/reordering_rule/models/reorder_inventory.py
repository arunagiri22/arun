from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp import workflow
import openerp.addons.decimal_precision as dp

from openerp import tools, api
import csv
import cStringIO
import base64
import xlwt



class reorder_inventory(osv.osv):
    _name = 'reorder.inventory'
    _description = 'create reorder rules as per selected products or catogories'
    
    
    
    
    
    _columns = {
            'name': fields.char('Reordering Reference', required=True,),
            'warehouse_id':fields.many2one('stock.warehouse', 'Warehouse',),
            #~ 'location_id12': fields.many2one('stock.location', 'Inventoried Location', required=True),
            #~ 'product_bool1':fields.boolean('Product'),
            #~ 'category_bool1':fields.boolean('Category'),
            #~ 'product_ids' : fields.many2many('product.product','product_id','reorder_id','product_reorder_relatn','Products'),
            #~ 'category_ids' : fields.many2one('product.category','Category'),
           'state' : fields.selection([ ('draft', 'Draft'), ('confirmed', 'Confirmed'),('cancel', 'Cancelled') ], 'State', select=True), 
           'line_ids':fields.one2many('reorder.inventory.line', 'reorder_rel', 'Reordering Rules'),
            
   
            
            
    
    
    
    }
    
    
    
    @api.multi
    def confirm_value(self):
        return self.write({'state':'confirmed'})
    
    
    @api.multi
    def cancel_value(self):
        return self.write({'state':'cancel'})
        
        
    @api.multi
    def action_set_to_draft(self):
        return self.write({'state': 'draft'})
    
class reorder_inventory_line(osv.osv):
    _name = 'reorder.inventory.line'
    
    _columns = {
            #~ 'location_id': fields.many2one('stock.location', 'Location', required=True, select=True),
            'product_val': fields.char('Barcode'),
            'attribute_val': fields.char('Attribute'),
            'location_val':fields.char('Location Value'),
            #~ 'inventory_id': fields.many2one('stock.inventory', 'Inventory', ondelete='cascade', select=True),
            'reorder_rel':fields.many2one('reorder.inventory'),
             #~ 'product_id': fields.many2one('product.product', 'Product', required=True, select=True),
             'min_qty': fields.float('Min Qty'),
             'max_qty': fields.float('Max Qty'),
             'qty_multiple': fields.float('Qty Multiple'),
    
    
    }



        
        
