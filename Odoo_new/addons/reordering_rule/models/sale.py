from openerp import fields, models, api
from datetime import datetime, timedelta

class ProcurementOrder(models.Model):
      _inherit='procurement.order'
  
      schedule_date=fields.Date('Schedule Date')
      prestashop_seq1_procure=fields.Char('Sequence')
      pick_up= fields.Selection([('0', 'No'),('*RET*', 'Yes')], string='Pick Up', store=True, default='0')
      order_types= fields.Selection([('0', 'Ecommerce'),('1', 'Wholesale'),('2', 'Internal')], string='Order Type', store=True,default='0')
    
      @api.model
      def _run_move_create(self, procurement):
         vals = super(ProcurementOrder, self)._run_move_create(procurement)
     	 vals.update({
                'prestashop_seq1_move': procurement.prestashop_seq1_procure,
            	'order_types':procurement.order_types,
                'pick_up': procurement.pick_up,
		'schedule_date':procurement.schedule_date,
             })
     	 return vals



class SaleOrderLine(models.Model):
    _inherit='sale.order.line'  
    
    @api.multi
    def _prepare_order_line_procurement(self, group_id=False):
       vals = super(SaleOrderLine, self)._prepare_order_line_procurement(group_id=group_id)
       vals.update({
           'prestashop_seq1_procure': self.order_id.origin,
           'pick_up': self.order_id.pick_up,
           'order_types': self.order_id.order_types,
           'schedule_date':self.order_id.schedule_date,
        })
       return vals
          

class sale_order(models.Model):
    _inherit = 'sale.order'
    
    schedule_date=fields.Date('Schedule Delivery Date')
    pick_up = fields.Selection([
        ('0', 'No'),
        ('*RET*', 'Yes')], string='Pick Up', store=True, default='0')
  
    order_types = fields.Selection([
        ('0', 'Ecommerce'),
        ('1', 'Wholesale'),
    ('2', 'Internal')], string='Order Type', store=True, default='0')

    
    
            
            
class StockPicking(models.Model):
    _inherit='stock.picking'
    
    sequence_prestashop=fields.Char()
    schedule_date=fields.Date('Schedule Delivery Date')
    
    

    
