from datetime import date, datetime
from dateutil import relativedelta
import json
import time
import sets

import openerp
from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp import SUPERUSER_ID, api, models
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging
from openerp.exceptions import UserError


_logger = logging.getLogger(__name__)

           
class stock_move(osv.osv):
      _inherit='stock.move'
      
      _columns={
            'prestashop_seq1_move':fields.char('Sequence'),
            'pick_up': fields.selection([('0', 'No'),('*RET*', 'Yes')], string='Pick Up', store=True, default='0'),
  	        'order_types': fields.selection([('0', 'Ecommerce'),('1', 'Wholesale'),('2', 'Internal')], string='Order Type', store=True,default='0'),
	        'schedule_date':fields.date('Schedule Delivery Date'),
      }
      
      def _prepare_picking_assign(self, cr, uid, move, context=None):
        res=super(stock_move, self)._prepare_picking_assign(cr, uid,move, context=context)
        res['name']=move.prestashop_seq1_move
	res['order_types'] =move.order_types	
	res['pick_up'] =move.pick_up
	res['schedule_date'] =move.schedule_date
        return res
        
class stock_picking(osv.osv):
      _inherit='stock.picking'

      _columns={
	    'pick_up': fields.selection([('0', 'No'),('*RET*', 'Yes')], string='Pick Up', store=True, default='0'),
  	    'order_types': fields.selection([('0', 'Ecommerce'),('1', 'Wholesale'),('2', 'Internal')], string='Order Type', store=True,default='2'),
      }
      
      def _create_backorder(self, cr, uid, picking, backorder_moves=[], context=None):
        """ Move all non-done lines into a new backorder picking. If the key 'do_only_split' is given in the context, then move all lines not in context.get('split', []) instead of all non-done lines.
        """
        if not backorder_moves:
            backorder_moves = picking.move_lines
        backorder_move_ids = [x.id for x in backorder_moves if x.state not in ('done', 'cancel')]
        if 'do_only_split' in context and context['do_only_split']:
            backorder_move_ids = [x.id for x in backorder_moves if x.id not in context.get('split', [])]

        if backorder_move_ids:
            backorder_id = self.copy(cr, uid, picking.id, {
                'name': picking.name,
                'move_lines': [],
                'pack_operation_ids': [],
                'backorder_id': picking.id,
            })
            backorder = self.browse(cr, uid, backorder_id, context=context)
            self.message_post(cr, uid, picking.id, body=_("Back order <em>%s</em> <b>created</b>.") % (backorder.name), context=context)
            move_obj = self.pool.get("stock.move")
            move_obj.write(cr, uid, backorder_move_ids, {'picking_id': backorder_id}, context=context)

            if not picking.date_done:
                self.write(cr, uid, [picking.id], {'date_done': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)}, context=context)
            self.action_confirm(cr, uid, [backorder_id], context=context)
            self.action_assign(cr, uid, [backorder_id], context=context)
            return backorder_id
        return False



    
    

