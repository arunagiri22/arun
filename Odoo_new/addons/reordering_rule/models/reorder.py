from openerp import fields, models, api

class reorder_inventory(models.Model):
   _inherit = 'reorder.inventory'
   
   @api.multi
   def reorder_rule_data(self):
      for record in self:
          for rec in record.line_ids:
              self.env['stock.warehouse.orderpoint'].create({'product_id':rec.product_id.id, 'product_min_qty':rec.min_qty, 
                 'product_max_qty':rec.max_qty,'qty_multiple':rec.qty_multiple,
                 'location_id':rec.location_id.id , 'warehouse_id':record.warehouse_id.id})
              record.state='confirmed'

class reorder_inventory_line(models.Model):
    _inherit = 'reorder.inventory.line'
    product_id=fields.Many2one('product.product', compute='product_value', readonly=False)
    location_id= fields.Many2one('stock.location', 'Location',compute='location_value', readonly=False)
    
    @api.multi
    def product_value(self):
            ts = self.env['product.product']
            for record in self:
                if record.attribute_val:
                    ts_line=ts.search([('barcode','=', record.product_val),('attribute_value_ids.name','=', record.attribute_val)])
                    if ts_line:
                        record.product_id=ts_line[0]   
                else:
                    ts_line1=ts.search([('barcode','=', record.product_val)])
                    if ts_line1:
                        record.product_id=ts_line1[0] 
                  
    
    @api.multi
    def location_value(self):
            ts = self.env['stock.location']
            for record in self:
                ts_line=ts.search([('name','=', record.location_val),('location_id.name','=',record.reorder_rel.warehouse_id.code)])
                if ts_line:
                   record.location_id=ts_line[0]



class stock_pack_operation(models.Model):
    _inherit = 'stock.picking'
    
    is_file_uploaded=fields.Boolean(compute='location_dest')
    
    @api.depends('location_dest_id','order_types')
    def location_dest(self):
        for record in self:
            if record.location_dest_id.usage  == 'transit' and record.order_types =='2':
                record.is_file_uploaded = True
                
            
                
                
    
    
    
