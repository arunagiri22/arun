# -*- coding: utf-8 -*-
{
    'name': 'Automatic Reordering Rule',
    'version': '1.0',
    'website' : 'https://getopenerp.com',
    'category': 'Reordering',
    'summary': 'Automatic re-ordering rule on product creation',
    'description': """


""",
    'author': 'GetOpenERP: Odoo Official Partner',
    'depends': ['base', 'sale', 'purchase', 'procurement'],
    'data': ['views/reorder_inventory_view.xml','views/stock_inventory_view.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
