{
    'name': 'Import Stock Order From Excel',
    'version': '9.0',
    'category': 'Sales',
    'summary' : 'Import/Export/Update large Sale Order from Excel',
    'description': """
        This module gives you facility to import sale order lines through the excel file, so you can import a large purchase order easily
        
        Dependencies:
        
        This module requires external packages python-openpyxl & python-xlrd. 
        
        you can install this package by perform following commands on terminal,
        
        sudo apt-get install python-openpyxl
        
        sudo apt-get install python-xlrd
        
        For more information about our services in Odoo, please contact us on 
        """,
    'author': 'Get Openstow',
    'maintainer': 'Get Openestow',
    'website': 'www.getopenerp.com',
    'depends': ['base','stock'],
    'images': [],
    'data': [
                'wizard/import_stock_order.xml',
                'wizard/process_stock_order.xml',
                'security/stock_security_group.xml',

                
            ],
    'installable': True,
    'auto_install': False,
    'application' : True,
    'external_dependencies' : {'python' : ['openpyxl', 'xlrd'], },
    

}
