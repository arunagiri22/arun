from StringIO import StringIO
import base64
from openerp import models, fields, api
from openerp.exceptions import except_orm
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


try:
    import openpyxl
    from openpyxl.reader.excel import load_workbook
except ImportError:
    _logger.error('Odoo module import_account_invoice_xls depends on the openpyxl python module')
    openpyxl = None

try:
    import xlrd
except ImportError:
    _logger.error('Odoo module import_account_invoice_xls depends on the xlrd python module')
    xlrd = None
    
    
class process_stock_order_import(models.TransientModel):
    _name='process.stock.order.import'
    _description = 'Process to Import Inventory order From File'
    
    choose_file = fields.Binary('Choose File',filters='*.xlsx')
    file_name = fields.Char('Filename',size=512)
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(process_stock_order_import, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        stock_obj = self.env['stock.picking']
        orders = []
        for order in stock_obj.browse(self.env.context.get('active_ids')):
            if order.state != "draft":
                raise except_orm(('Unable to process..!'), ('You can process with only draft inventory order!.'))
            orders.append(order)
            
        if len(orders) > 1:
            raise except_orm(('Unable to process..!'), ('You can process only one Inventory order at a time!.'))
        
        return res 
    
    @api.one
    def get_file_name(self, name=datetime.strftime(datetime.now(),'%Y%m%d%H%M%S%f')):
        return '/tmp/stock_order_%s_%s.xlsx' %(self.env.uid,name)
    
    
    
    @api.multi
    def get_header(self, worksheet):
        column_header = {}
        columns = worksheet.get_highest_column() + 1        
        for row in worksheet.iter_rows(): 
            if row[0].row > 1 or not next((r for r in row if r.row),None):
                break
            for col in range(1,columns):
                column_header.update({col : str(worksheet.cell(row=1,column=col).value).lower()})
        return column_header
        
        
    @api.one 
    def read_file_xls(self):
        file_name = self.file_name
        file_path = '/tmp/%s_%s'%(datetime.strftime(datetime.now(),'%Y%m%d%H%M%S%f'),file_name)
        fp = open(file_path,'wb')
        fp.write(base64.decodestring(self.choose_file))
        fp.close()
        xl_workbook = xlrd.open_workbook(file_path)
        return xl_workbook
        
        
    def fill_dictionary_from_xls_file(self, xl_sheet,keys):
        dict_list = []
        for row_index in xrange(1, xl_sheet.nrows):
            d = {keys[col_index]: xl_sheet.cell(row_index, col_index).value 
                 for col_index in xrange(xl_sheet.ncols)}
            dict_list.append(d)
        return dict_list 
    
    
    @api.one 
    def read_file(self):
        '''
            Read selected file to import order and return worksheet object to the caller
        '''
        imp_file = StringIO(base64.decodestring(self.choose_file))
        workbook = load_workbook(filename = imp_file, use_iterators=True)
        worksheet = workbook.get_active_sheet()
        return worksheet
        
        
        
        
    @api.one
    def validate_process(self):
        '''
            Validate process by checking all the conditions and return back with sale order object
        '''
        if not self.choose_file:
            raise except_orm(('Unable to process..!'), ('Please select file to process...'))
        
        stock_order = self.env['stock.picking'].browse(self.env.context.get('active_id'))
        if not stock_order:
            raise except_orm(('Unable to process..!'), ('You must need to select one sale order!.'))
        
        return stock_order
    
    
    @api.one
    def validate_fields(self, file_fields):
        '''
            This import pattern requires few fields default, so check it first whether it's there or not.
        '''
        require_fields = ['barcode']
        missing = []
        for field in require_fields:
            if field not in file_fields:
                missing.append(field)
            
        if len(missing) > 0:
            raise except_orm(('Incorrect format found..!'), ('Please provide all the required fields in file, missing fields => %s.' %(missing)))
        
        return True
        
        
        
    def fill_dictionary_from_file(self, worksheet, column_header):
        data = []
        columns = worksheet.get_highest_column() + 1
        #rows = worksheet.get_highest_row() + 1
        for row_num,row in enumerate(worksheet.iter_rows()):
            row_num+=1
            if row[0].row == 1 or not next((r for r in row if r.row),None):
                continue
            test_data = {}
            for col in range(1,columns):
                test_data.update({column_header.get(col) : worksheet.cell(row=row_num,column=col).value})
            data.append(test_data)
        return data  
        
        
        
    @api.multi
    def get_product_data(self,order,product_id,qty):
        res = self.env['stock.move'].onchange_product_id()
                 
        return res
    
    
    @api.one
    def default_quantity(self):
        key_id = self.env.ref('import_stock_order_xls.default_stock_order_qty_confi_parameter').id
        value = self.env['ir.config_parameter'].search([('id','=',key_id)]).value
        if value:
            if isinstance(value, list):
                return value[0]
            return value
        return 0
        
        
        
    @api.multi
    def make_inventory_line(self):
        order = self.validate_process()[0]
        file_name = self.file_name
        index = file_name.rfind('.')
        flag=0
        if index == -1:
            flag=1
        extension = file_name[index+1:]
        
        
        if flag or extension not in ['xlsx','xls']:
            raise except_orm(('Incorrect file format found..!'), ('Please provide only .xlsx or .xls file to import order data!!!'))
        
        product_obj = self.env['product.product']
        line_obj = self.env['stock.move']
        column_header={}
        
        try:
            if extension=='xlsx':
                worksheet = self.read_file()[0]
                column_header = self.get_header(worksheet)
                file_header = column_header.values()
            else:
                worksheet = self.read_file_xls()[0]
                worksheet = worksheet.sheet_by_index(0)
                file_header = [worksheet.cell(0, col_index).value.lower() for col_index in xrange(worksheet.ncols)]
        except Exception,e:
            raise Warning("Something is wrong.\n %s"%(str(e)))
            
            
        not_matched = []
        invalid_code = []
        quantity = 0
        if self.validate_fields(file_header):
            if extension=='xlsx':
                order_data = self.fill_dictionary_from_file(worksheet,column_header)
            else:
                order_data = self.fill_dictionary_from_xls_file(worksheet,file_header) 
            
            default_quantity = 0
            if 'qty' not in file_header:
                default_quantity = float(self.default_quantity()[0])
                
            
            for data in order_data:
                line_data = {}
                l_id = data.get('id',0)
                if default_quantity:
                    data.update({'qty' : default_quantity})
                    quantity = default_quantity
                else:
                    if type(data.get('qty')) in [None,str]:
                        quantity = 0
                    else:
                        if data.get('qty'):
                            quantity = float(data.get('qty',0.0)) or 0.0
    
    
                barcode = data.get('barcode','') and str(data.get('barcode',''))                 
                barcode = barcode and barcode.strip()
                
                if not barcode and l_id:
                    domain = [('id','=',int(l_id)),('picking_id','=',order.id)]
                    l_obj = line_obj.search(domain)
                    if not l_obj:
                        continue
                    l_obj.write({'product_uom_qty' : quantity})
                    continue
                    
                    
                product_id = product_obj.search([('barcode','=',barcode)])
                if not product_id:
                    invalid_code.append(barcode)
                    continue    
                print "####################",order.location_dest_id.id
                if product_id:   
                    product = product_id and product_id[0] or False
                    line_data = {} 
                    
                    line_data.update(product_id=product.id)
                    line_data.update({'location_id':order.location_id.id})
                    line_data.update({'location_dest_id':order.location_dest_id.id})
                    line_data.update({'product_uom':product.uom_id.id})
                    line_data.update({'name' : product.name})
                    
                    
                    line_data.update({'product_uom_qty' : quantity})
                    
                    
                    if l_id:
                        domain = [('id','=',int(l_id)),('picking_id','=',order.id)]
                        l_obj = line_obj.search(domain)
                        if not l_obj:
                            continue
                        if l_obj and l_obj.product_id and l_obj.product_id.id == product.id and l_obj.picking_id.id == order.id:
                            l_obj.write(line_data)
                        continue
                        
                    line_ids= line_obj.search([('product_id','=', product.id),('picking_id','=',order.id)])
                    if line_ids:
                        vals = {}
                        vals.update({
                                'product_id' : product.id,
                                'name' : product.name,
                                'product_uom_id' : product.uom_id.id,
                                'product_name' : product.name,
                                'location_dest_id':location_dest_id.id,
                                'location_id':location_id.id,
                                'quantity': quantity,
                                })
                        not_matched.append(vals)
                    else:
                        line_data.update({
                                'picking_id' : order.id,
                            })
                        line_obj.create(line_data)
                
            
            if len(not_matched) > 0 or len(invalid_code) > 0:
                import_stock_id = self.env['import.stock.order'].create({'picking_id': order.id})
                order_lines = []
                invalid_code_lines = ""
                none_in_file = False
                if len(not_matched) > 0:
                    for vals in not_matched:
                        vals.update({'import_stock_id' : import_stock_id.id})
                        import_line_id = self.env['import.stock.order.line'].create(vals)
                        order_lines.append(import_line_id.id)
                        
                        
                if len(invalid_code) > 0:
                    if 'None' in invalid_code:
                        invalid_code = filter(lambda x: x != 'None', invalid_code)
                        none_in_file = True  
                    invalid_code_lines = ', '.join(map(str,invalid_code))
                    import_stock_id.write({'invalid_code_ids' : invalid_code_lines})
                ctx = self.env.context.copy()
                ctx.update({'import_stock_id':import_stock_id.id,'item_ids' : order_lines, 'picking_id' : order.id, 'invalid_code_ids' : invalid_code_lines,'none_in_file':none_in_file})
                return import_stock_id.with_context(ctx).wizard_view()
        order.write({})
        return True
                        
                        
                        
    @api.multi
    def export_inventory_line(self):
        line_obj = self.env['stock.move']
        line_ids = line_obj.search([('picking_id','in',self.env.context.get('active_ids'))])
        filename = self.get_file_name()[0]
        
        wb = openpyxl.workbook.Workbook()
        worksheet = wb.create_sheet(index=0)
        
        worksheet.cell(coordinate='A1').value = 'ID'
        worksheet.cell(coordinate='B1').value = 'BarCode'
        worksheet.cell(coordinate='C1').value = 'Name'
        worksheet.cell(coordinate='D1').value = 'Qty'
        row =2 
                
        for line in line_ids:
            worksheet.cell(coordinate='A%d'%(row)).value = line.id
            worksheet.cell(coordinate='B%d'%(row)).value = line.product_id.barcode
            worksheet.cell(coordinate='C%d'%(row)).value = line.name
            worksheet.cell(coordinate='D%d'%(row)).value = line.product_uom_qty
            row = row + 1 
        
        wb.save(filename)
        order_name = line_ids and line_ids[0].picking_id.name or ''
        file_read = open(filename,'rb')
        out = base64.encodestring(file_read.read())
        self.write({'choose_file' : out,'file_name':'stock_order_line_%s.xlsx'%(order_name) or '',})    
           
        return  {
                    'name': 'Import/Export Order Lines',
                    'view_type': 'form',
                    'res_model': 'process.stock.order.import',
                    'res_id':self.ids[0],
                    'view_id': False,
                    'view_mode': 'form',
                    'context': self.env.context,
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'nodestroy': True,
                }
    
