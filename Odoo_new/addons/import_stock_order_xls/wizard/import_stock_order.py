from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from datetime import datetime


class import_stock_order(models.TransientModel):
    _name='import.stock.order'
    _description = 'Import Inventory wizard'
    
    picking_id=fields.Many2one('stock.picking','Inventory')
    item_ids=fields.One2many('import.stock.order.line','import_stock_id')
    invalid_code_ids=fields.Text('Invalid Codes')
    

    def default_get(self, cr, uid, fields, context=None):
        if context is None: context = {}
        res = super(import_stock_order, self).default_get(cr, uid, fields, context=context)
        res['picking_id'] = context.get('picking_id', False)
        res['item_ids'] = context.get('item_ids', False)
        res['invalid_code_ids'] = context.get('item_ids', False)
        return res
        
        
    @api.multi
    def wizard_view(self):
        view = self.env.ref('import_stock_order_xls.view_isox_stock_order_import_wizard')

        return {
            'name': _('Inventory details'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'import.stock.order',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': self.ids[0],
            'context': self.env.context,
        }
        
        
    @api.multi
    def process_import(self,mode):
        items = self.item_ids
        order = self.picking_id
        for item in items:
            line_id = item.line_id
            res = self.env['stock.move'].onchange_product_id()
            res.update({'name' : item.product_id.name})
            res.update({'product_uom':item.product_uom_id.ids[0]})
            res.update({'location_dest_id':item.location_dest_id.id})
            res.update({
                         'product_id' : item.product_id.id,
                         'picking_id' : self.picking_id.id ,
                         'product_qty' : item.quantity
                       })
                       
            if mode == 'w':
                line_id.write(res)
                
            else:
                line_id = self.env['stock.move'].create(res)
        order.write({})        
        return True 
        
    @api.multi
    def do_append(self):
        '''Append order items'''
        self.process_import(mode = 'c')
        return {'type': 'ir.actions.act_window_close'}
        
    @api.multi
    def do_update(self):
        '''Update existing items'''
        self.process_import(mode = 'w')
        return {'type': 'ir.actions.act_window_close'}


class import_stock_order_line(models.TransientModel):
    _name='import.stock.order.line'
    _description = 'Import Inventory Items'
    
    import_stock_id = fields.Many2one('import.stock.order','Order')
    line_id = fields.Many2one('stock.move','Move Line')
    product_id = fields.Many2one('product.product', 'Product')
    product_name = fields.Char('Name')
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure')
    location_dest_id=fields.Many2one('stock.location', 'Parent Location')
    quantity = fields.Float('Quantity', digits=dp.get_precision('Product Unit of Measure'), default = 1.0)
