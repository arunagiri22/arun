# -*- encoding: utf-8 -*-
{
    "name" : "GetOpenERP: Auto File Backup- FTP",
    "version" : "1.0",
    "author" : "GetOpenERP: Odoo Official Partner",
    "website" : "https://getopenerp.com",
    "category" : "File Backup",
    "summary": "Auto File Backup to FTP Server",
    "description": """
    The file auto backup allows following things:
                      
1) Backup of files periodically, you can change how often you want to take backup from: 
Go to Settings / Technical / Automation / Scheduled actions.
Search the action for delivery order backup.
Set it active and choose how often you wish to take backups.
2) Download the delivery order csv file, parse it and set delivery order to done based on csv data
""",
    "depends" : ['base', 'stock'],
    "data": [
         'data/auto_file_backup_data.xml',
         'data/stock_ftp_download_data.xml',
         'views/auto_file_backup_views.xml',
         'views/stock_picking_inherit_views.xml',
    ],
    "demo_xml" : [],
    "active": False,
    "installable": True
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
