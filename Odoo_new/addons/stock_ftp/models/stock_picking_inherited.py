from openerp import _, api, fields, models

class StockPicking(models.Model):
    _inherit = "stock.picking"

    is_file_uploaded = fields.Boolean(string="Is File Uploaded")
    url_follow_up = fields.Char(string="URL Follow-up")
class stock_location(models.Model):
    _inherit='stock.location'
    warehouse_rel=fields.One2many('stock.warehouse','lot_stock_id')    
