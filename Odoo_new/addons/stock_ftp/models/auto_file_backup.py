# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import os
import time
import re
import logging
import tempfile
import csv, codecs, cStringIO
import shutil
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

try:
    import ftplib
except ImportError:
    raise ImportError('This module needs ftplib to automaticly write backups to the FTP through FTP.')

from openerp import api, fields, models, _
from openerp import tools
from openerp.exceptions import UserError
from openerp.tools.safe_eval import safe_eval


_logger = logging.getLogger(__name__)


class StockPickingBackup(models.TransientModel):
    _name = 'stock.picking.config.settings'
    _inherit = 'res.config.settings'
            
    #Columns local server
    name = fields.Char('Name', required='True',help='Database you want to schedule backups for')
    # bkp_dir = fields.Char('Backup Directory', help='Absolute path for storing the backups', required='True', default='/odoo/backups')

    #Columns for external server (SFTP)
    sftppath = fields.Char('Path external server', help="The location to the folder where the dumps should be written to. For example /odoo/backups/.\nFiles will then be written to /odoo/backups/ on your remote server.")
    sftpip = fields.Char('IP Address SFTP Server', help="The IP address from your remote server. For example 192.168.0.1")
    sftpport = fields.Integer("SFTP Port", help="The port on the FTP server that accepts SSH/SFTP calls.", default=22)
    sftpusername = fields.Char('Username SFTP Server', help="The username where the SFTP connection should be made with. This is the user on the external server.")
    sftppassword = fields.Char('Password User SFTP Server', help="The password from the user where the SFTP connection should be made with. This is the password from the user on the external server.")
    download_dir = fields.Char('Download Directory', help='Absolute path for storing the backups', required='True', default='/odoo/backups')
    download_processed_dir = fields.Char('Download Processed Directory', help='Absolute path for storing the backups', required='True', default='/odoo/backups')
    

    @api.model
    def get_default_auth_signup_template_user_id(self, fields):
        icp = self.env['ir.config_parameter']
        # we use safe_eval on the result, since the value of the parameter is a nonempty string
        return {
            'name': safe_eval(icp.get_param('stock_ftp.name', 'False')),
            # 'bkp_dir': safe_eval(icp.get_param('stock_ftp.bkp_dir', 'False')),
            'sftppath': safe_eval(icp.get_param('stock_ftp.sftppath', 'False')),
            'sftpip': safe_eval(icp.get_param('stock_ftp.sftpip', 'False')),
            'sftpport': safe_eval(icp.get_param('stock_ftp.sftpport', 'False')),
            'sftpusername': safe_eval(icp.get_param('stock_ftp.sftpusername', 'False')),
            'sftppassword': safe_eval(icp.get_param('stock_ftp.sftppassword', 'False')),
            'download_dir': safe_eval(icp.get_param('stock_ftp.download_dir', 'False')),
            'download_processed_dir': safe_eval(icp.get_param('stock_ftp.download_processed_dir', 'False')),
        }

    @api.multi
    def set_auth_signup_template_user_id(self):
        config = self[0]
        icp = self.env['ir.config_parameter']
        # we store the repr of the values, since the value of the parameter is a required string
        icp.set_param('stock_ftp.name', repr(config.name))
        # icp.set_param('stock_ftp.bkp_dir', repr(config.bkp_dir))
        icp.set_param('stock_ftp.sftppath', repr(config.sftppath))
        icp.set_param('stock_ftp.sftpip', repr(config.sftpip))
        icp.set_param('stock_ftp.sftpport', repr(config.sftpport))
        icp.set_param('stock_ftp.sftpusername', repr(config.sftpusername))
        icp.set_param('stock_ftp.sftppassword', repr(config.sftppassword))
        icp.set_param('stock_ftp.download_dir', repr(config.download_dir))
        icp.set_param('stock_ftp.download_processed_dir', repr(config.download_processed_dir))

    @api.multi
    def test_sftp_connection(self):
        #Check if there is a success or fail and write messages 
        messageTitle = ""
        messageContent = ""
        icp = self.env['ir.config_parameter']
        try:
            ipHost = safe_eval(icp.get_param('stock_ftp.sftpip', 'False'))
            portHost = safe_eval(icp.get_param('stock_ftp.sftpport', 'False')) # TODO: What is the use of porthost now, default is 21 for ftplib
            usernameLogin = safe_eval(icp.get_param('stock_ftp.sftpusername', 'False'))
            passwordLogin = safe_eval(icp.get_param('stock_ftp.sftppassword', 'False'))

            #Connect with external server over SFTP, so we know sure that everything works.
            srv = ftplib.FTP(ipHost)
            srv.login(usernameLogin, passwordLogin)
            srv.close()
            #We have a success.
            messageTitle = "Connection Test Succeeded!"
            messageContent = "Everything seems properly set up for FTP back-ups!"
        except Exception, e:
            messageTitle = "Connection Test Failed!"
            if len(ipHost) < 8:
                messageContent += "\nYour IP address seems to be too short.\n"
            messageContent += "Here is what we got instead:\n"
        if "Failed" in messageTitle:
            raise UserError(_(messageContent + "%s") % tools.ustr(e))
        else:
            raise UserError(_(messageContent))

    @api.model
    def schedule_upload_delivery(self):
        # TODO: Check picking_type_code is related, check related non stored works in search domain or now ?
        delivery_order = self.env['stock.picking'].search([('state', '=', 'assigned'), ('picking_type_id.code', 'in', ['outgoing','internal']), ('is_file_uploaded', '=', False)])
        print "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs",delivery_order
        account_obj=self.env['account.invoice']
        print "***********************",account_obj
        icp = self.env['ir.config_parameter']
        for order in delivery_order:
            account_id=account_obj.search([('state','=','paid'),('origin','=', order.origin)])
            print "DSDASDASDSADS",account_id.state, account_id
            bkp_file='%s.csv' % ((order.name).replace('/', '-'))
            tmp_dir = tempfile.mkdtemp()
            file_path = os.path.join(tmp_dir, bkp_file)
            with open(file_path,"wb") as datafile:
                c = csv.writer(datafile, delimiter=";")
		if order.partner_id:
                   c.writerow([
                               'CAB',
                               order.name,
                               order.partner_id.name or '',
                               order.partner_id.street or '',
                               order.partner_id.country_id.code or '',
                               order.partner_id.zip or '',
                               order.partner_id.city or '',
                               order.partner_id.phone or '',
                               order.note or '',
                               order.partner_id.parent_id.email or '',
                               order.carrier_id.service_code or '',
			       order.order_types or '',
   			       order.pick_up or '',
			       order.schedule_date or '',
                               ])
                if order.location_dest_id.warehouse_rel:
                   c.writerow([
                               'CAB',
                               order.name,
                               order.location_dest_id.warehouse_rel.partner_id.name,
                               order.location_dest_id.warehouse_rel.partner_id.street or '',
                               order.location_dest_id.warehouse_rel.partner_id.country_id.code or '',
                               order.location_dest_id.warehouse_rel.partner_id.zip or '',
                               order.location_dest_id.warehouse_rel.partner_id.city or '',
                               order.location_dest_id.warehouse_rel.partner_id.phone or '',
                               '',
                               order.location_dest_id.warehouse_rel.partner_id.email or '',
                               order.carrier_id.service_code or '', 
                               ])
                for line in order.move_lines:
                    c.writerow(['LIN', order.name, line.product_id.barcode or '', line.csv_qty or ''])

                datafile.close()
            try:
                ipHost = safe_eval(icp.get_param('stock_ftp.sftpip', 'False'))
                usernameLogin = safe_eval(icp.get_param('stock_ftp.sftpusername', 'False'))
                passwordLogin = safe_eval(icp.get_param('stock_ftp.sftppassword', 'False'))
                pathToWriteTo = safe_eval(icp.get_param('stock_ftp.sftppath', 'False'))
                #Connect with external server over SFTP
                srv = ftplib.FTP(ipHost)
                srv.login(usernameLogin, passwordLogin)
                pathToWriteTo = re.sub('([/]{2,5})+','/',pathToWriteTo)
                _logger.info('ftp remote path: %s' % pathToWriteTo)

                try:
                    time.sleep(0.001)
                    srv.cwd(pathToWriteTo)
                except Exception, e:
                    currentDir = ''
                    for dirElement in pathToWriteTo.split('/'):
                        currentDir += dirElement + '/'
                        try:
                            srv.cwd(currentDir)
                        except:
                            _logger.info('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                            #Make directory and then navigate into it
                            srv.mkd(currentDir, mode=777)
                            srv.cwd(currentDir)
                            pass
                srv.cwd(pathToWriteTo)
                #Loop over all files in the directory.
                for f in os.listdir(tmp_dir):
                        fullpath = os.path.join(tmp_dir, f)
                        if os.path.isfile(fullpath):
                           for order in delivery_order:
                               if order.state=='assigned':
                                   for account in account_id:
                                       if account.state=='paid':
                                             print "******t**********************************",account.state
                                             _logger.info('The file %s is not yet on the remote FTP Server ------ Copying file' % fullpath)
                                             fhandlestore = open(fullpath, 'r')
                                             srv.storlines("STOR " + bkp_file, fhandlestore)
                                             order.is_file_uploaded = True
                                             _logger.info('Copying File % s------ success' % fullpath)
                #Navigate in to the correct folder.
                srv.cwd(pathToWriteTo)
                srv.close()
                os.removedirs(tmp_dir)
            except Exception:
                _logger.info('Exception! We couldn\'t back up to the FTP server..')


    @api.model
    def schedule_process_delivery(self):
        # TODO: note that we will read from ftp server and process it by checking existance and reference number
        icp = self.env['ir.config_parameter']
        tmp_dir = tempfile.mkdtemp()
        try:
            ipHost = safe_eval(icp.get_param('stock_ftp.sftpip', 'False'))
            portHost = safe_eval(icp.get_param('stock_ftp.sftpport', 'False'))
            usernameLogin = safe_eval(icp.get_param('stock_ftp.sftpusername', 'False'))
            passwordLogin = safe_eval(icp.get_param('stock_ftp.sftppassword', 'False'))
            downloadDirectory = safe_eval(icp.get_param('stock_ftp.download_dir', 'False'))
            processedDirectory = safe_eval(icp.get_param('stock_ftp.download_processed_dir', 'False'))
            #Connect with external server over FTP
            srv = ftplib.FTP(ipHost)
            srv.login(usernameLogin, passwordLogin)
            srv_move = ftplib.FTP(ipHost)
            srv_move.login(usernameLogin, passwordLogin)
            # TODO: To check with ftplib if keepalive exist
            #set keepalive to prevent socket closed / connection dropped error
#             srv._transport.set_keepalive(30)
#             srv_move._transport.set_keepalive(30)
            #Move to the correct tmp_directory on external server. If the user made a typo in his path with multiple slashes (/odoo//backups/) it will be fixed by this regex.
            downloadDirectory = re.sub('([/]{2,5})+','/',downloadDirectory)
            _logger.debug('sftp remote path: %s' % downloadDirectory)
            processedDirectory = re.sub('([/]{2,5})+','/',processedDirectory)
            _logger.debug('sftp remote path: %s' % processedDirectory)
            try:
                # time.sleep(0.001)
                srv.cwd(downloadDirectory)
            except IOError, e:
                print "\n\nIOError :::: ",e 
                #Create directory and subdirs if they do not exist.
                currentDir = ''
                for dirElement in downloadDirectory.split('/'):
                    currentDir += dirElement + '/'
                    try:
                        srv.cwd(currentDir)
                    except:
                        _logger.info('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                        #Make directory and then navigate into it
                        srv.mkd(currentDir)
                        srv.cwd(currentDir)
                        pass
            srv.cwd(downloadDirectory)

            try:
                srv_move.cwd(processedDirectory)
            except IOError:
                #Create directory and subdirs if they do not exist.
                currentDir = ''
                for dirElement in processedDirectory.split('/'):
                    currentDir += dirElement + '/'
                    try:
                        srv_move.cwd(currentDir)
                    except:
                        _logger.info('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                        #Make directory and then navigate into it
                        srv_move.mkd(currentDir)
                        srv_move.cwd(currentDir)
                        pass
            srv_move.cwd(processedDirectory)

            all_files = srv.nlst()
            #rr = re.compile("^[0-9a-zA-Z]+$")
            #all_files = filter(lambda x: not rr.match(x), all_files)
            all_files = filter(lambda x: not (x == "." or x == ".."), all_files)
            all_files = filter(lambda x: x.startswith(("exp_", "EXP_")), all_files)
            for file in all_files:
                # TODO: Check is directory or is file, we should consider only files
                # Create temporary file, if one of the record fails, 
                # add done flag for records which are processed and replace the temporary file in datas directory,
                # if file is completely processed then move it to processed directory
                file_path = os.path.join(tmp_dir, file)
                datafile = open(file_path, mode='wb')
                #with open(file_path, mode='ar+') as datafile:
                    # temporary_file = srv.get(file, file_path)
                srv.retrbinary('RETR ' + file, datafile.write)
                datafile.close()
                success_list = []
                data_lines = []
                datafile = open(file_path, mode='r')
                for line in datafile.readlines():
                    line_fields = line and line.split(";") or False
                    if line_fields:
                        order_reference = line_fields[0]
                        url = ''
                        if len(line_fields) > 1:
                            url = line_fields[1]
                        delivery_order = self.env['stock.picking'].search([('name', '=', order_reference)])
                        delivery_order.url_follow_up = url
                        try:
                            data_lines.append(True)
                            if delivery_order.move_lines and delivery_order.pack_operation_ids:
                                self.process_delivery_order(delivery_order)
                                success_list.append(True)
                            else:
                                pass
                                # TODO: keep as it is and add flag done for processed orders and keep file there as it is and while reading line check if there is process flag available or not
                        except Exception, e:
                            print "\n\nException eeeee ",e
                datafile.close()
                if len(data_lines) == len(success_list):
                    datafile = open(file_path, mode='r')
                    srv.delete(file)
                    srv_move.storlines("STOR " + file, datafile)
                    datafile.close()
            srv.close()
            srv_move.close()
            shutil.rmtree(tmp_dir)
        except Exception, e:
            _logger.info('Exception! We couldn\'t back up to the FTP server..')

    def process_delivery_order(self, delivery_order):
        # availabe_orders = self.env['stock.picking'].search([('state', '=', 'assigned')])
        
        for order in delivery_order:
            # TODO: We will always consider assigned i.e. available orders so we don't need following code, only keep code from pack_operation_ids loop
            if order.state == 'draft':
                order.action_confirm()
                if order.state != 'assigned':
                    order.action_assign()
                    if order.state != 'assigned':
                        raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
            for pack in order.pack_operation_ids:
                if pack.product_qty > 0:
                    pack.write({'qty_done': pack.product_qty})
                else:
                    pack.unlink()
            order.do_new_transfer()

        # availabe_orders.action_done())

####For Incoming reciept
    @api.model
    def schedule_upload_incoming_reciept(self):
        incoming_reciepts = self.env['stock.picking'].search([('state', '=', 'done'), ('picking_type_id.code', '=', 'incoming'), ('is_file_uploaded', '=', False)])
        icp = self.env['ir.config_parameter']
        for reciept in incoming_reciepts:
            bkp_file='%s.csv' % ((reciept.name).replace('/', '-'))
            tmp_dir = tempfile.mkdtemp()
            file_path = os.path.join(tmp_dir, bkp_file)
            with open(file_path, "wb") as datafile:
                 c = csv.writer(datafile, delimiter=";")

                 c.writerow([
                             'CAB',
                             reciept.origin or '',
                             reciept.purchase_id and reciept.purchase_id.date_order or '',
                             reciept.partner_id.name or '',
                             reciept.partner_id.street or '',
                             reciept.partner_id.country_id.code or '',
                             reciept.partner_id.zip or '',
                             reciept.partner_id.city or '',
                             reciept.partner_id.email or '',
                             ])

            for line in reciept.move_lines:
                    c.writerow(['LIN', reciept.origin or '', line.name, line.product_id.barcode or '', line.product_uom_qty])

            datafile.close()
            try:
                ipHost = safe_eval(icp.get_param('stock_ftp.sftpip', 'False'))
                usernameLogin = safe_eval(icp.get_param('stock_ftp.sftpusername', 'False'))
                passwordLogin = safe_eval(icp.get_param('stock_ftp.sftppassword', 'False'))
                pathToWriteTo = safe_eval(icp.get_param('stock_ftp.sftppath', 'False'))
                #Connect with external server over FTP
                srv = ftplib.FTP(ipHost)
                srv.login(usernameLogin, passwordLogin)
                pathToWriteTo = re.sub('([/]{2,5})+','/',pathToWriteTo)
                _logger.info('ftp remote path: %s' % pathToWriteTo)

                try:
                    time.sleep(0.001)
                    srv.cwd(pathToWriteTo)
                except Exception, e:
                    currentDir = ''
                    for dirElement in pathToWriteTo.split('/'):
                        currentDir += dirElement + '/'
                        try:
                            srv.cwd(currentDir)
                        except:
                            _logger.info('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                            #Make directory and then navigate into it
                            srv.mkd(currentDir, mode=777)
                            srv.cwd(currentDir)
                            pass
                srv.cwd(pathToWriteTo)
                #Loop over all files in the directory.
                for f in os.listdir(tmp_dir):
                    fullpath = os.path.join(tmp_dir, f)
                    if os.path.isfile(fullpath):
                        _logger.info('The file %s is not yet on the remote FTP Server ------ Copying file' % fullpath)
                        fhandlestore = open(fullpath, 'r')
                        srv.storlines("STOR " + bkp_file, fhandlestore)
                        reciept.is_file_uploaded = True
                        _logger.info('Copying File % s------ success' % fullpath)

                #Navigate in to the correct folder.
                srv.cwd(pathToWriteTo)
                srv.close()
                os.removedirs(tmp_dir)
            except Exception:
                _logger.info('Exception! We couldn\'t back up to the FTP server..')
