

# -*- coding: utf-8 -*-
{
    'name': 'Payment Voucher',
    'version': '1.0',
    'website' : '',
    'category': '',
    'summary': 'Report payment',
    'description': """


""",
    'author': '',
    'depends': ['base','account'],
    'data': ['views/report_payment.xml',
            'views/account_voucher_report.xml',
            'views/account_voucher.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}






