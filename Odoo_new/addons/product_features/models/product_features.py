from openerp import models, fields, api, _


class product_feature(models.Model):
    _name='product.feature'
    _description = 'Product Feature'
    
    name = fields.Char('Name')
    sequence1= fields.Integer('Sequence', help="Determine the display order")
    feature_value_ids=fields.One2many('product.feature.value','feature_id',)

class product_feature_value(models.Model):
    _name='product.feature.value'
    _description = 'Product Feature Value'
    
    name=fields.Char('Values')
    sequence =fields.Integer('Sequence',help="Determine the display order")
    feature_id=fields.Many2one('product.feature','Features')
    product_ids= fields.Many2many('product.product', id1='att_id', id2='prod_id', string='Variants', readonly=True)
