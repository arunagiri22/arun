{
    'name': 'Product Features',
    'version': '9.0',
    'category': 'Product',
    'summary' : 'Product Features',
    'description': """
        This module gives you facility to Features Of Product 
        
    
        """,
    'author': '',
    'maintainer':'', 
    'website': '',
    'depends': ['base','product'],
    'images': [],
    'data': ['views/product_feature_view.xml'],
    'installable': True,
    'auto_install': False,
    'application' : True,
    

}
