# -*- coding: utf-8 -*-
{
    'name': 'Customer Company Filter',
    'version': '1.0',
    'website' : '',
    'category': 'sales',
    'summary': 'Filter Only company type customer',
    'description': """


""",
    'author': '',
    'depends': ['base','sale'],
    'data': ['sale_view.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
