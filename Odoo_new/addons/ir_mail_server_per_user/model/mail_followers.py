# -*- coding: utf-8 -*-
from openerp.osv import osv

class mail_followers(osv.Model):
    """ Class holding notifications pushed to partners. Followers and partners
        added in 'contacts to notify' receive notifications. """
    _inherit = 'mail.followers'
    
    def _notify(self, cr, uid, message_id, partners_to_notify=None, context=None,
                force_send=False, user_signature=True):
        ctx = context and context.copy() or {}
        ctx.update({'user_id_ept':uid})    
        return super(mail_followers,self)._notify(cr,uid,message_id,partners_to_notify,ctx,force_send,user_signature) 
