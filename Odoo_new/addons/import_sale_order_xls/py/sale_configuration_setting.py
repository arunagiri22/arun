from openerp import models, fields


class sale_config_settings(models.TransientModel):
    _inherit = 'sale.config.settings'
    
    group_visible_import_sale_order=fields.Boolean('Import sale order line from excel file ?', 
                        implied_group='sale.group_sale_order_line_import',                                     
                        help = 'You can allow user to import sale order line from excel file.')
