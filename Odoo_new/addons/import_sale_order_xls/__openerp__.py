{
    'name': 'Import Sale Order From Excel',
    'version': '9.0',
    'category': 'Sales',
    'summary' : 'Import/Export/Update large Sale Order from Excel',
    'description': """
        This module gives you facility to import sale order lines through the excel file, so you can import a large Sale order easily
        
        Dependencies:
        
        This module requires external packages python-openpyxl & python-xlrd. 
        
        you can install this package by perform following commands on terminal,
        
        sudo apt-get install python-openpyxl
        
        sudo apt-get install python-xlrd
        
        For more information about our services in Odoo, please contact us on 
        """,
    'author': 'Get Openstow',
    'maintainer': 'Get Openestow',
    'website': 'www.getopenerp.com',
    'depends': ['sale'],
    'images': [],
    'data': [
                'view/sale_configuration_setting.xml',
                'security/sale_security_group.xml',
                'wizard/import_sale_order.xml',
                'wizard/process_sale_order.xml',
                
            ],
    'installable': True,
    'auto_install': False,
    'application' : True,
    'external_dependencies' : {'python' : ['openpyxl', 'xlrd'], },
    

}
