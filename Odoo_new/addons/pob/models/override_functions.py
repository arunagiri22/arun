#!/usr/bin/env python
# -*- coding: utf-8 -*- 
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from pob import _unescape
from openerp import workflow
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import prestapi
from prestapi import PrestaShopWebService,PrestaShopWebServiceDict,PrestaShopWebServiceError,PrestaShopAuthenticationError
############## Override classes #################

class product_template(osv.osv):    
    _inherit = 'product.template'

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('prestashop'):
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
            if vals.has_key('description'):
                vals['description'] = _unescape(vals['description'])
            if vals.has_key('description_sale'):
                vals['description_sale'] = _unescape(vals['description_sale'])
        template_id = super(product_template, self).create(cr, SUPERUSER_ID, vals, context=context)
        if context.has_key('prestashop'):
            variant_ids_ids = self.browse(cr,SUPERUSER_ID,template_id).product_variant_ids
            temp = {'template_id':template_id}
            if len(variant_ids_ids)==1:
                temp['product_id'] = variant_ids_ids[0].id
            else:
                temp['product_id'] = -1
            self.pool.get('prestashop.product.template').create(cr,SUPERUSER_ID,{'template_name':template_id,'erp_template_id':template_id,'presta_product_id':context['presta_id']})
            self.pool.get('prestashop.product').create(cr,SUPERUSER_ID,{'product_name':temp['product_id'],'erp_template_id':template_id,'presta_product_id':context['presta_id'],'erp_product_id':temp['product_id']})
            return temp
        return template_id

    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        map_obj = self.pool.get('prestashop.product.template')
        if context.has_key('prestashop'):
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
            if vals.has_key('description'):
                vals['description'] = _unescape(vals['description'])
            if vals.has_key('description_sale'):
                vals['description_sale'] = _unescape(vals['description_sale'])
        return super(product_template,self).write(cr,uid,ids,vals,context=context)
    _columns = {
        'template_mapping_id': fields.one2many('prestashop.product.template', 'template_name', string='PrestaShop Information',readonly="1"),
        'extra_categ_ids': fields.many2many('product.category','product_categ_rel','product_id','categ_id',string='Product Categories', domain="[('type', '=', 'normal')]",help="Select additional categories for the current product")
    }
product_template()  

class product_product(osv.osv): 
    _inherit = 'product.product'

    def check_for_new_price(self, cr, uid,template_id,value_id,price_extra,context=None):
        if context is None:
            context = {}
        product_attribute_price=self.pool.get('product.attribute.price')
        exists = product_attribute_price.search(cr,uid,[('product_tmpl_id','=',template_id),('value_id','=',value_id)])
        if not exists:
            temp ={'product_tmpl_id':template_id,'value_id':value_id,'price_extra':price_extra}
            pal_id = product_attribute_price.create(cr,uid,temp)
            return True
        else:
            pal_id = exists[0]
            product_attribute_price.write(cr,uid,pal_id,{'price_extra':price_extra})
            return True

    def check_for_new_attrs(self, cr, uid,template_id,ps_attributes,context=None):
        if context is None:
            context = {}
        product_template=self.pool.get('product.template')
        product_attribute_line=self.pool.get('product.attribute.line')
        all_values = []
        for attribute_id in ps_attributes:
            exists = product_attribute_line.search(cr,uid,[('product_tmpl_id','=',template_id),('attribute_id','=',int(attribute_id))])
            if not exists:
                temp ={'product_tmpl_id':template_id,'attribute_id':attribute_id}
                pal_id = product_attribute_line.create(cr,uid,temp)
            else:
                pal_id = exists[0]
            product_attribute_line.write(cr,uid,pal_id,{'value_ids':[[4,int(ps_attributes[attribute_id][0])]]})
            all_values.append(int(ps_attributes[attribute_id][0]))
        return [[6,0,all_values]]

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('prestashop_variant'):
            template_obj = self.pool.get('product.template').browse(cr,SUPERUSER_ID,vals['product_tmpl_id'])
            vals['name'] = template_obj.name
            vals['description'] = template_obj.description
            vals['description_sale'] = template_obj.description_sale
            vals['type'] = template_obj.type
            vals['categ_id'] = template_obj.categ_id.id
            vals['uom_id'] = template_obj.uom_id.id
            vals['uom_po_id'] = template_obj.uom_po_id.id
            vals['default_code'] = _unescape(vals['default_code'])
            if vals.has_key('ps_attributes'):
                vals['attribute_value_ids']=self.check_for_new_attrs(cr,SUPERUSER_ID,template_obj.id,vals['ps_attributes'])
        erp_id =  super(product_product, self).create(cr, SUPERUSER_ID, vals, context=context)
        if context.has_key('prestashop_variant'):
            prestashop_product = self.pool.get('prestashop.product')
            exists = prestashop_product.search(cr,SUPERUSER_ID,[('erp_template_id','=',vals['product_tmpl_id']),('presta_product_attr_id','=',0)])
            if exists:
                pp_map = prestashop_product.browse(cr,SUPERUSER_ID,exists[0])
                if pp_map.product_name:
                    self.write(cr,SUPERUSER_ID,pp_map.product_name.id,{'active':False})
                prestashop_product.unlink(cr,SUPERUSER_ID,exists)
            prestashop_product.create(cr,SUPERUSER_ID,{'product_name':erp_id,'erp_template_id':template_obj.id,'presta_product_id':context['presta_id'],'erp_product_id':erp_id,'presta_product_attr_id':context['presta_attr_id']})
        return erp_id
    
    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        map_obj = self.pool.get('prestashop.product')
        if not context.has_key('prestashop'):
            if ids:
                if type(ids) == list:
                    erp_id=ids[0]
                else:
                    erp_id=ids
                map_ids = map_obj.search(cr, uid, [('erp_product_id', '=',erp_id)])
                if map_ids:
                    map_obj.write(cr, uid, map_ids[0], {'need_sync':'yes'})
        if context.has_key('prestashop_variant'):
            if type(ids) == list:
                erp_id=ids[0]
            else:
                erp_id=ids
            template_obj = self.pool.get('product.product').browse(cr,uid,erp_id).product_tmpl_id
            vals['name'] = template_obj.name
            vals['description'] = template_obj.description
            vals['description_sale'] = template_obj.description_sale
            vals['type'] = template_obj.type
            vals['categ_id'] = template_obj.categ_id.id
            vals['uom_id'] = template_obj.uom_id.id
            vals['uom_po_id'] = template_obj.uom_po_id.id
            vals['default_code'] = _unescape(vals['default_code'])
            if vals.has_key('ps_attributes'):
                vals['attribute_value_ids']=self.check_for_new_attrs(cr,uid,template_obj.id,vals['ps_attributes'])
            if vals.has_key('wk_extra_price'):
                if type(ids) == list:
                    erp_id=ids[0]
                else:
                    erp_id=ids
                obj = self.browse(cr, uid, erp_id)
                if not vals['wk_extra_price']:
                    vals['wk_extra_price'] = '0.0'
                vals['wk_extra_price'] =(obj.wk_extra_price) + (float(vals['wk_extra_price'])- (obj.price_extra))

        return super(product_product,self).write(cr,uid,ids,vals,context=context)
    _columns = {
        'product_mapping_id': fields.one2many('prestashop.product', 'product_name', string='PrestaShop Information',readonly="1"),
    }
product_product()

class product_category(osv.osv):    
    _inherit = 'product.category'
    
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('prestashop'):
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
        return super(product_category, self).create(cr, uid, vals, context=context)
        
    def write(self,cr,uid,ids,vals,context=None):
        if context is None:
            context = {}
        map_obj = self.pool.get('prestashop.category')
        if not context.has_key('prestashop'):
            if ids:
                if type(ids) == list:
                    erp_id=ids[0]
                else:
                    erp_id=ids
                map_ids = map_obj.search(cr, uid, [('erp_category_id', '=',erp_id)])
                if map_ids:
                    map_obj.write(cr, uid, map_ids[0], {'need_sync':'yes'})
        else:
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
        return super(product_category,self).write(cr,uid,ids,vals,context=context)
product_category()

class res_partner(osv.osv):
    _inherit = 'res.partner'
    
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('prestashop'):
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
            if vals.has_key('street'):
                vals['street'] = _unescape(vals['street'])
            if vals.has_key('street2'):
                vals['street2'] = _unescape(vals['street2'])
            if vals.has_key('city'):
                vals['city'] = _unescape(vals['city'])
            partner_id = super(res_partner, self).create(cr, uid, vals, context=context)
            if partner_id:
                self.pool.get('prestashop.customer').create(cr,uid,{'customer_name':partner_id,'erp_customer_id':partner_id,'presta_customer_id':context.get('customer_id'),'presta_address_id':context.get('address_id')})
            return partner_id
        return super(res_partner, self).create(cr, uid, vals, context=context)
    
    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        map_obj = self.pool.get('prestashop.customer')
        if context.has_key('prestashop'):
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
            if vals.has_key('street'):
                vals['street'] = _unescape(vals['street'])
            if vals.has_key('street2'):
                vals['street2'] = _unescape(vals['street2'])
            if vals.has_key('city'):
                vals['city'] = _unescape(vals['city'])
        return super(res_partner,self).write(cr,uid,ids,vals,context=context)
res_partner()

class delivery_carrier(osv.osv):
    _inherit = 'delivery.carrier'
    
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('prestashop'):
            vals['name'] = _unescape(vals['name'])
            vals['partner_id'] = self.pool.get('res.users').browse(cr, uid, uid).company_id.id
        return super(delivery_carrier, self).create(cr, uid, vals, context=None)    

    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('prestashop'):
            vals['name'] = _unescape(vals['name'])
        return super(delivery_carrier, self).write(cr, uid, ids, vals, context=None)    
delivery_carrier()

class product_attribute(osv.osv):   
    _inherit = 'product.attribute'
    
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('prestashop'):
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
        return super(product_attribute, self).create(cr, uid, vals, context=context)
        
    def write(self,cr,uid,ids,vals,context=None):
        if context is None:
            context = {}                        
        if context.has_key('prestashop'):
            if vals.has_key('name'):
                vals['name'] = _unescape(vals['name'])
        return super(product_attribute,self).write(cr,uid,ids,vals,context=context)
product_attribute()

class sale_order(osv.osv):
    _inherit = "sale.order"

    def _get_ecommerces(self, cr, uid, context=None):
        res = super(sale_order, self)._get_ecommerces(cr, uid, context)
        res.append(('prestashop','Prestashop'))
        return res


    def manual_prestashop_invoice(self,cr,uid,ids,context=None):
        error_message=''
        status='no'
        config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
        if not config_id:
            error_message='Connection needs one Active Configuration setting.'
            status='no'
        if len(config_id)>1:
            error_message='Sorry, only one Active Configuration setting is allowed.'
            status='no'
        else:
            obj=self.pool.get('prestashop.configure').browse(cr,uid,config_id[0])
            url=obj.api_url
            key=obj.api_key
            try:
                prestashop = PrestaShopWebServiceDict(url,key)
            except PrestaShopWebServiceError,e:
                error_message='Invalid Information, Error %s'%e
                status='no'
            except IOError, e:
                error_message='Error %s'%e
                status='no'
            except Exception,e:
                error_message="Error,Prestashop Connection in connecting: %s" % e
                status='no'
            if prestashop:
                order_id=self.pool.get('wk.order.mapping').get_id(cr,uid,'prestashop','prestashop',ids[0])
                if order_id:
                    try:
                        inv_data = prestashop.get('order_invoices', options={'schema': 'blank'})
                        data=prestashop.get('orders',order_id)
                        inv_data['order_invoice'].update({
                                                        'id_order' : order_id,
                                                        'total_wrapping_tax_incl': data['order']['total_wrapping_tax_incl'],
                                                        'total_products': data['order']['total_products'],
                                                        'total_wrapping_tax_excl': data['order']['total_wrapping_tax_excl'],
                                                        'total_paid_tax_incl': data['order']['total_paid_tax_incl'],
                                                        'total_products_wt': data['order']['total_products_wt'],
                                                        'total_paid_tax_excl': data['order']['total_paid_tax_excl'],
                                                        'total_shipping_tax_incl': data['order']['total_shipping_tax_incl'],
                                                        'total_shipping_tax_excl': data['order']['total_shipping_tax_excl'],
                                                         'delivery_number': data['order']['delivery_number'],
                                                         'number' : '1'
                                                        })
                        invoice=prestashop.add('order_invoices', inv_data)
                    except Exception,e:
                        error_message="Error %s, Error in getting Blank XML"%str(e)
                        status='no'
                        
                else:
                    return True
        
        
    def manual_invoice(self, cr, uid, ids, context=None):
        if context is None:
            context={}
        mod_obj = self.pool.get('ir.model.data')                
        inv_ids = set()
        inv_ids1 = set()
        for id in ids:
            for record in self.pool.get('sale.order').browse(cr, uid, id).invoice_ids:
                inv_ids.add(record.id)
        #inv_ids would have old invoices if any
        for id in ids:
            workflow.trg_validate(uid, 'sale.order', id, 'manual_invoice', cr)
            for record in self.pool.get('sale.order').browse(cr, uid, id).invoice_ids:
                inv_ids1.add(record.id)
        inv_ids = list(inv_ids1.difference(inv_ids))        
        res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
        res_id = res and res[1] or False,
        #manual_prestashop_invoice method is used to create an invoice on prestashop end...         
        # if not context.has_key('prestashop'):
        #   config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
        #   if len(config_id)>0:
        #       self.manual_prestashop_invoice(cr,uid,ids,context)
        return {
            'name': _('Customer Invoices'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.invoice',
            'context': "{'type':'out_invoice'}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': inv_ids and inv_ids[0] or False,
        }
    
    def manual_prestashop_invoice_cancel(self,cr,uid,ids,context=None):
        error_message=''
        status='yes'
        config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
        if not config_id:
            error_message='Connection needs one Active Configuration setting.'
            status='no'
        if len(config_id)>1:
            error_message='Sorry, only one Active Configuration setting is allowed.'
            status='no'
        else:
            obj=self.pool.get('prestashop.configure').browse(cr,uid,config_id[0])
            url=obj.api_url
            key=obj.api_key
            try:
                prestashop = PrestaShopWebServiceDict(url,key)
            except PrestaShopWebServiceError,e:
                error_message='Invalid Information, Error %s'%e
                status='no'
            except IOError, e:
                error_message='Error %s'%e
                status='no'
            except Exception,e:
                error_message="Error,Prestashop Connection in connecting: %s" % e
                status='no'
            if prestashop:
                order_id=self.pool.get('wk.order.mapping').get_id(cr,uid,'prestashop','prestashop',ids[0])
                if order_id:
                    try:
                        order_his_data=prestashop.get('order_histories', options={'schema': 'blank'})
                        order_his_data['order_history'].update({
                        'id_order' : order_id,
                        'id_order_state':6
                        })
                        state_update=prestashop.add('order_histories?sendemail=1', order_his_data)
                    except Exception,e:
                        error_message="Error %s, Error in getting Blank XML"%str(e)
                        status='no'
                        
                else:
                    return True
                
    def action_cancel(self, cr, uid, ids, context=None):        
        if context is None:
            context = {}
        sale_order_line_obj = self.pool.get('sale.order.line')
        for sale in self.browse(cr, uid, ids, context=context):
            for inv in sale.invoice_ids:
                if inv.state not in ('draft', 'cancel'):
                    raise osv.except_osv(
                        _('Cannot cancel this sales order!'),
                        _('First cancel all invoices attached to this sales order.'))
            for r in self.read(cr, uid, ids, ['invoice_ids']):
                for inv in r['invoice_ids']:
                    workflow.trg_validate(uid, 'account.invoice', inv, 'invoice_cancel', cr)
            sale_order_line_obj.write(cr, uid, [l.id for l in  sale.order_line],
                    {'state': 'cancel'})
        self.write(cr, uid, ids, {'state': 'cancel'})
        ##manual_prestashop_invoice_cancel method is used to cancel an order on prestashop end...
        if not context.has_key('prestashop'):           
            config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
            cancel = self.pool.get('prestashop.configure').browse(cr, uid, config_id).cancelled
            if cancel:
                if len(config_id)>0:
                    self.manual_prestashop_invoice_cancel(cr,uid,ids,context)       
        return True 

sale_order()

class account_invoice(osv.osv):
    _inherit="account.invoice"


    def confirm_paid(self, cr, uid, ids, context=None):
        res = super(account_invoice, self).confirm_paid(cr, uid, ids)
        if not context.has_key('prestashop'):
            config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
            paid = self.pool.get('prestashop.configure').browse(cr, uid, config_id).invoiced
            if paid:
                if len(config_id)>0:
                    return self.pool.get('account.invoice').manual_prestashop_paid(cr,uid,ids)
     
    def manual_prestashop_paid(self,cr,uid,ids,context=None):
        order_name=self.browse(cr,uid,ids).origin   
                
        sale_id=self.pool.get('sale.order').search(cr,uid,[('name','=',order_name)])
        if sale_id:
            error_message=''
            status='yes'
            config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
            if not config_id:
                error_message='Connection needs one Active Configuration setting.'
                status='no'
            if len(config_id)>1:
                error_message='Sorry, only one Active Configuration setting is allowed.'
                status='no'
            else:
                obj=self.pool.get('prestashop.configure').browse(cr,uid,config_id[0])
                url=obj.api_url
                key=obj.api_key
                try:
                    prestashop = PrestaShopWebServiceDict(url,key)
                except PrestaShopWebServiceError,e:
                    error_message='Invalid Information, Error %s'%e
                    status='no'
                except IOError, e:
                    error_message='Error %s'%e
                    status='no'
                except Exception,e:
                    error_message="Error,Prestashop Connection in connecting: %s" % e
                    status='no'
                if prestashop:
                    order_id=self.pool.get('wk.order.mapping').get_id(cr,uid,'prestashop','prestashop',sale_id[0])
                    if order_id:
                        try:
                            order_his_data=prestashop.get('order_histories', options={'schema': 'blank'})
                            order_his_data['order_history'].update({
                            'id_order' : order_id,
                            'id_order_state':2
                            })
                            state_update=prestashop.add('order_histories?sendemail=1', order_his_data)
                        except Exception,e:
                            error_message="Error %s, Error in getting Blank XML"%str(e)
                            status='no'                         
                    else:
                        return True
    
    _columns = {
        'ps_inv_ref': fields.char('Prestashop invoice Ref.',size=100),
    }                       
account_invoice()

class stock_picking(osv.osv):
    _name = "stock.picking"
    _inherit="stock.picking"

    def manual_prestashop_shipment(self,cr,uid,ids,context=None):
        order_name=self.pool.get('stock.picking').browse(cr,uid,ids[0]).origin              
        sale_id=self.pool.get('sale.order').search(cr,uid,[('name','=',order_name)])
        if sale_id:
            error_message=''
            status='yes'
            config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
            if not config_id:
                error_message='Connection needs one Active Configuration setting.'
                status='no'
            if len(config_id)>1:
                error_message='Sorry, only one Active Configuration setting is allowed.'
                status='no'
            else:
                obj=self.pool.get('prestashop.configure').browse(cr,uid,config_id[0])
                url=obj.api_url
                key=obj.api_key
                try:
                    prestashop = PrestaShopWebServiceDict(url,key)
                except PrestaShopWebServiceError,e:
                    error_message='Invalid Information, Error %s'%e
                    status='no'
                except IOError, e:
                    error_message='Error %s'%e
                    status='no'
                except Exception,e:
                    error_message="Error,Prestashop Connection in connecting: %s" % e
                    status='no'
                if prestashop:
                    order_id = False
                    got_id=self.pool.get('wk.order.mapping').search(cr,uid,[('ecommerce_channel','=','prestashop'),('erp_order_id','=',sale_id[0])])
                    if got_id:
                        order_id=self.pool.get('wk.order.mapping').browse(cr,uid,got_id[0]).ecommerce_order_id
                    if order_id:
                        try:
                            order_his_data=prestashop.get('order_histories', options={'schema': 'blank'})
                            order_his_data['order_history'].update({
                            'id_order' : order_id,
                            'id_order_state':4
                            })
                            state_update=prestashop.add('order_histories?sendemail=1', order_his_data)
                        except Exception,e:
                            error_message="Error %s, Error in getting Blank XML"%str(e)
                            status='no'
                            
        return True


    @api.cr_uid_ids_context
    def do_transfer(self, cr, uid, picking_ids, context=None): 
        super(stock_picking, self).do_transfer(cr, uid, picking_ids, context = context)
        if not context.has_key('prestashop'):
            config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
            shipped = self.pool.get('prestashop.configure').browse(cr, uid, config_id).delivered
            if shipped:
                if len(config_id)>0:
                    self.manual_prestashop_shipment(cr, uid, picking_ids, context)
        return True
        
        
    
    def export_tracking_no_to_prestashop(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        get_carrier_data = []
        presta_id = []
        order_carrier_id = False
        error_message = ''
        message = ''
        up_length = 0
        config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])
        if not config_id:
            raise osv.except_osv(_('Error'), _("Connection needs one Active Configuration setting."))
        if len(config_id)>1:
            raise osv.except_osv(_('Error'), _("Sorry, only one Active Configuration setting is allowed."))     
        else:
            obj = self.pool.get('prestashop.configure').browse(cr,uid,config_id[0])
            url = obj.api_url
            key = obj.api_key
            try:
                prestashop = PrestaShopWebServiceDict(url,key)
            except Exception,e:
                raise osv.except_osv(_('Error %s')%str(e), _("Invalid Information"))
            if prestashop:              
                picking_obj = self.pool.get('stock.picking').browse(cr, uid, ids[0])
                sale_order_id = picking_obj.sale_id.id
                track_ref = picking_obj.carrier_tracking_ref
                if not track_ref:
                    track_ref = ''
                if sale_order_id:
                    check = self.pool.get('wk.order.mapping').search(cr, uid, [('erp_order_id','=',sale_order_id)])
                    if check:
                        presta_id = self.pool.get('wk.order.mapping').browse(cr, uid, check[0]).ecommerce_order_id
                    if presta_id:
                        try:
                            get_carrier_data = prestashop.get('order_carriers',options={'filter[id_order]':presta_id})
                        except Exception,e:
                            error_message="Error %s, Error in getting Carrier Data"%str(e)
                        try:
                            if get_carrier_data['order_carriers']:
                                order_carrier_id = get_carrier_data['order_carriers']['order_carrier']['attrs']['id']
                            if order_carrier_id:
                                data = prestashop.get('order_carriers',order_carrier_id)
                                data['order_carrier'].update({
                                    'tracking_number' : track_ref,                  
                                    })
                                try:
                                    return_id = prestashop.edit('order_carriers',order_carrier_id, data)
                                    up_length = up_length + 1
                                except Exception,e:
                                    error_message = error_message + str(e)

                        except Exception,e:
                            error_message = error_message + str(e)
            if not error_message:               
                if up_length == 0:
                    message = "No Prestashop Order record fetched in selected stock movement record!!!"
                else:
                    message = 'Carrier Tracking Reference Number Successfully Updated to Prestashop!!!'
            else:               
                message = "Error in Updating: %s"%(error_message)
            partial_id = self.pool.get('pob.message').create(cr, uid, {'text':message}, context=context)
            return {'name':_("Message"),
                    'view_mode': 'form',
                    'view_id': False,
                    'view_type': 'form',
                    'res_model': 'pob.message',
                    'res_id': partial_id,
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                    'domain': context,                               
                }
                
    def do_new_transfer(self, cr, uid, ids, context=None):
        self.export_tracking_no_to_prestashop(cr, uid, ids, context=context)
        pack_op_obj = self.pool['stock.pack.operation']
        data_obj = self.pool['ir.model.data']
        for pick in self.browse(cr, uid, ids, context=context):
            to_delete = []
            if not pick.move_lines and not pick.pack_operation_ids:
                raise UserError(_('Please create some Initial Demand or Mark as Todo and create some Operations. '))
            # In draft or with no pack operations edited yet, ask if we can just do everything
            if pick.state == 'draft' or all([x.qty_done == 0.0 for x in pick.pack_operation_ids]):
                # If no lots when needed, raise error
                picking_type = pick.picking_type_id
                if (picking_type.use_create_lots or picking_type.use_existing_lots):
                    for pack in pick.pack_operation_ids:
                        if pack.product_id and pack.product_id.tracking != 'none':
                            raise UserError(_('Some products require lots, so you need to specify those first!'))
                view = data_obj.xmlid_to_res_id(cr, uid, 'stock.view_immediate_transfer')
                wiz_id = self.pool['stock.immediate.transfer'].create(cr, uid, {'pick_id': pick.id}, context=context)
                return {
                     'name': _('Immediate Transfer?'),
                     'type': 'ir.actions.act_window',
                     'view_type': 'form',
                     'view_mode': 'form',
                     'res_model': 'stock.immediate.transfer',
                     'views': [(view, 'form')],
                     'view_id': view,
                     'target': 'new',
                     'res_id': wiz_id,
                     'context': context,
                 }
            # Check backorder should check for other barcodes
            if self.check_backorder(cr, uid, pick, context=context):
                view = data_obj.xmlid_to_res_id(cr, uid, 'stock.view_backorder_confirmation')
                wiz_id = self.pool['stock.backorder.confirmation'].create(cr, uid, {'pick_id': pick.id}, context=context)
                return {
                         'name': _('Create Backorder?'),
                         'type': 'ir.actions.act_window',
                         'view_type': 'form',
                         'view_mode': 'form',
                         'res_model': 'stock.backorder.confirmation',
                         'views': [(view, 'form')],
                         'view_id': view,
                         'target': 'new',
                         'res_id': wiz_id,
                         'context': context,
                     }
            for operation in pick.pack_operation_ids:
                if operation.qty_done < 0:
                    raise UserError(_('No negative quantities allowed'))
                if operation.qty_done > 0:
                    pack_op_obj.write(cr, uid, operation.id, {'product_qty': operation.qty_done}, context=context)
                else:
                    to_delete.append(operation.id)
            if to_delete:
                pack_op_obj.unlink(cr, uid, to_delete, context=context)
        self.do_transfer(cr, uid, ids, context=context)
        return


stock_picking()

class account_voucher(osv.osv):
    _inherit="account.voucher"
    
    def button_proforma_voucher(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        # raise osv.except_osv(_('Error!'),_('context=%s')%(context))   
        self.signal_workflow(cr, uid, ids, 'proforma_voucher')
        if not context.has_key('prestashop'):
            config_id=self.pool.get('prestashop.configure').search(cr,uid,[('active','=',True)])    
            if len(config_id)>0:                        
                self.pool.get('account.invoice').manual_prestashop_paid(cr,uid,[context['invoice_id']],context)

        return {'type': 'ir.actions.act_window_close'}
account_voucher()


# Skeleton Overrides:

class wk_order_mapping(osv.osv):
    _inherit = "wk.order.mapping"

    def get_id(self,cr,uid,shop,object,ur_id,context=None):
        if context is None:
            context = {}
        if shop=='prestashop':
            presta_id=False
            got_id=self.search(cr,uid,[('ecommerce_channel','=',object),('erp_order_id','=',ur_id)])
            if got_id:
                presta_id=self.browse(cr,uid,got_id[0]).ecommerce_order_id
            return presta_id
        elif shop=='openerp':
            erp_id=False
            got_id=self.search(cr,uid,[('ecommerce_channel','=',object),('ecommerce_order_id','=',ur_id)])
            if got_id:
                erp_id=self.browse(cr,uid,got_id[0]).erp_order_id
            return erp_id
        else:
            return "Shop not found"
    
    def get_all_ids(self,cr,uid,shop,object,context=None):
        if context is None:
            context = {}
        all_ids=[]
        if shop=='prestashop':
            got_ids=self.search(cr,uid,[('ecommerce_channel','=',object)])
            for i in got_ids:
                all_ids.append(i.ecommerce_order_id)
            return all_ids
        elif shop=='openerp':
            got_ids=self.search(cr,uid,[('ecommerce_channel','=',object)])
            for i in got_ids:
                all_ids.append(self.browse(cr,uid,i).erp_order_id)
            return all_ids
        else:
            return "Shop not found"

class wk_skeleton(osv.osv):
    _inherit = "wk.skeleton"

    def get_prestashop_configuration_data(self, cr, uid, context=None):
        res = {}
        search_config = self.pool.get('prestashop.configure').search(cr, uid, [('active','=',True)])
        if search_config:
            obj = self.pool.get('prestashop.configure').browse(cr, uid, search_config[0])
            search_wh = self.pool.get('stock.warehouse').search(cr, uid, [('lot_stock_id','=',obj.pob_default_stock_location.id)])
            res = {
                    'ecommerce_channel':'prestashop',
                    'order_policy':'manual',
                    'team_id': obj.team_id.id,
                    'user_id': obj.salesperson.id,
                    'payment_term_id' : obj.payment_term.id,
                    'warehouse_id': search_wh and search_wh[0] or False
                }
        return res

    
    def get_prestashop_virtual_product_id(self, cr, uid, order_line, context=None):
        if context.has_key('ecommerce') and context['ecommerce']=="prestashop":
            if context.has_key('type') and context['type']=='shipping':
                carrier = context.get('carrier_id', False)
                if carrier:
                    obj = self.pool.get('delivery.carrier').browse(cr, uid, carrier)
                    erp_product_id = obj.product_id.id
            if context.has_key('type') and context['type']=='voucher':
                ir_values = self.pool.get('ir.values')
                erp_product_id = ir_values.get_default(cr, uid, 'product.product', 'pob_discount_product')
                if not erp_product_id:
                    erp_product_id=self.pool.get('product.product').create(cr, uid, {'sale_ok':False,'name':'Voucher','type':'service','description_sale':'','list_price':0.0,})
                    ir_values.set_default(cr, uid, 'product.product', 'pob_discount_product', erp_product_id)
        return erp_product_id
        # return True

    

    def turn_odoo_connection_off(self, cr, uid, context=None):
        """ To be inherited by bridge module for making connection Inactive on Odoo End"""
        config_values = self.pool.get('prestashop.configure').search(cr, uid,[('active','=',True)])
        if config_values:
            self.pool.get('prestashop.configure').write(cr, uid,config_values[0],{'active':False})
            context.update({
                'config_id' : config_values[0]
                })
        return context

    def turn_odoo_connection_on(self, cr, uid, context=None):
        """ To be inherited by bridge module for making connection Active on Odoo End"""
        if context.has_key('config_id'): 
            self.pool.get('prestashop.configure').write(cr, uid, context.get('config_id'), {'active':True})
        return True
