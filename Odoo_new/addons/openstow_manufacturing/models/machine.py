from openerp import models, fields, api, _

class Machine(models.Model):
	_name='machine'
	
	@api.multi
	@api.depends('name', 'oem_code')
	def name_get(self):
		result = []
		for machine in self:
			name=machine.manufacturer_id.name
			name = '[' + str(machine.oem_code) + ']' + ' ' + str(name) +  ' ' + '[' + str(machine.model) + ']'
			result.append((machine.id, name))
		return result
	
	name = fields.Char('Name')
	manufacturer_id = fields.Many2one('manufacturer','Manufacturer Name')
	model=fields.Char("Model")
	oem_code=fields.Char("OEM Code")
	part_type=fields.Selection([('cone','Cone'),('metal_jaw_plate','Metal Jaw Plate'),('cheek_plate','Cheek Plate'),('other_liner','Other Liner'),('spares','Spares')], 'Part Type',)
	material_ids=fields.One2many('machine.material','machine_id', String='OEM Material Spec')
	
class machine_material(models.Model):
    _name='machine.material'
    
    name = fields.Char('Part Name')
    material_id = fields.Many2one('material', string='Material Code')
    machine_id = fields.Many2one('machine', string='Machine')
    
   
class manufacturer(models.Model):
    _name="manufacturer"
    
    name = fields.Char('Manufacturer Name')
    
