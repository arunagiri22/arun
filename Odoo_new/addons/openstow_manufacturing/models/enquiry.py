from openerp import models, fields, api, _
from datetime import date, datetime
from dateutil import relativedelta
import time

class crm_template(models.Model):
    _inherit ="crm.lead"
    enq_code=fields.Char(default=lambda self: self.env['ir.sequence'].next_by_code('crm.lead'))
    enq_date=fields.Date(default=fields.Date.today())
    enq_visibility = fields.Selection([('high', 'High'),('low','Low'),('medium','Medium')],'Enquiry Visibility')
    enquiry_line = fields.One2many('part.line','part_id',string='Enquiry Lines')
    extra_product_ids = fields.One2many('extra.product.lines', 'lead_id', string='Extra Poducts')
    communication_mode = fields.Selection([('verbal','Verbal'),('telephone', 'Telephone'),('site_visit', 'Site Visit'),('walk_in','Walk In')],'Mode of Communication')
    enquiry_address_id=fields.One2many('enquiry.address.line','enquiry_address',string='Address Line')
    cust_code = fields.Char('Partner Code')
    
    def on_change_partner_id(self, cr, uid, ids, partner_id, context=None):
        values = {}
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            partner_name = (partner.parent_id and partner.parent_id.name) or (partner.is_company and partner.name) or False
            values = {
                'partner_name': partner_name,
                'contact_name': (not partner.is_company and partner.name) or False,
                'title': partner.title and partner.title.id or False,
                'street': partner.street,
                'street2': partner.street2,
                'city': partner.city,
                'state_id': partner.state_id and partner.state_id.id or False,
                'country_id': partner.country_id and partner.country_id.id or False,
                'email_from': partner.email,
                'phone': partner.phone,
                'mobile': partner.mobile,
                'fax': partner.fax,
                'zip': partner.zip,
                'function': partner.function,
                'cust_code':partner.new_seq,
            }
        return {'value': values}

class part_line(models.Model):
    _name ="part.line"
    
    part_quant=fields.Char('Quantity In Hand')
    part_code=fields.Char('Part Code')
    oe_no=fields.Char('OE Number')
    part_id = fields.Many2one('crm.lead', string='Enquiry Reference', required=True)
    product_id = fields.Many2one('product.product', string='Product')

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        for rec in self:
            rec.part_code = rec.product_id.default_code
            rec.part_quant = rec.product_id.qty_available
            rec.oe_no = rec.product_id.oem_machine_id.oem_code
            
    @api.model
    def create(self, vals):
        product_id = self.env['product.product'].browse(vals['product_id'])
        if product_id:
            vals['part_code'] = product_id.default_code
            vals['part_quant'] = product_id.qty_available
            vals['oe_no'] = product_id.oem_machine_id.oem_code
        return super(part_line, self).create(vals)
    
    @api.multi
    def write(self, values):
        product_id = values.get('product_id')
        if product_id:
            values['part_code'] = product_id.default_code
            values['part_quant'] = product_id.qty_available
            values['oe_no'] = product_id.oem_machine_id.oem_code
        result = super(part_line, self).write(values)
        return result
                
class extra_product_lines(models.Model):
    _name ="extra.product.lines"
    
    lead_id = fields.Many2one('crm.lead')
    name=fields.Char('Product Name')
    oe_no=fields.Char('OE Number')
    manufacturer_name = fields.Char('Manufacturer Name')
    model=fields.Char("Machine Model")
    
class enquiry_address_line(models.Model):
    _name="enquiry.address.line"
    
    enquiry_address = fields.Many2one('crm.lead')
    name = fields.Char('Name')
    address=fields.Char('Address')
    mobile=fields.Char('Mobile')

