from openerp import models, fields, api, _
from datetime import date, datetime
from dateutil import relativedelta 
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import time
import math

class custom_sale(models.Model):
    _inherit ="sale.order"
    delivery_terms = fields.Char('Delivery Terms')
    validity_days = fields.Char('Validity Days',compute="compute_days")
    customer_code = fields.Char('Customer Code')
    sale_attach=fields.Binary('Attachment')

    @api.multi
    def compute_days(self):
		date_format = '%Y-%m-%d'
		from_dt = datetime.strptime(str(datetime.today().date()), date_format)
		if self.validity_date:
			to_dt  = datetime.strptime(str(self.validity_date), date_format)
			duration = to_dt - from_dt
			self.validity_days = duration.days
  
    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        """
        Update the following fields when the partner is changed:
        - Pricelist
        - Payment term
        - Invoice address
        - Delivery address
        """
        if not self.partner_id:
            self.update({
                'partner_invoice_id': False,
                'partner_shipping_id': False,
                'payment_term_id': False,
                'fiscal_position_id': False,
            })
            return

        addr = self.partner_id.address_get(['delivery', 'invoice'])
        values = {
            'pricelist_id': self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False,
            'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
            'note': self.with_context(lang=self.partner_id.lang).env.user.company_id.sale_note,
        }

        if self.partner_id.user_id:
            values['user_id'] = self.partner_id.user_id.id
        if self.partner_id.team_id:
            values['team_id'] = self.partner_id.team_id.id
            values['customer_code'] = self.partner_id.new_seq
            self.update(values)