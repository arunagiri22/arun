from openerp import models, fields, api, _

class custom_product(models.Model):
    _inherit ="product.template"
    
    #product_code = fields.Char('Product Code')
    oem_machine_id = fields.Many2one('machine', string='OE Machine')
    oem_no = fields.Char('OE Number')
    # Drawing
    drawing_weight=fields.Float(string="Drawing Weight")
    stock_weight=fields.Float("Stock Weight")
    machine_weight=fields.Float(string="Machine Weight")
    hs_code=fields.Char("Hs code")
    drawing_status=fields.Char("Drawing Status")
    drawing_revision=fields.Char("Drawing Revision")
    machine_type=fields.Selection([('casted','Casted',),('blocked','Block Machined')],string='Production Method',default='casted')
    outsourced=fields.Boolean("Outsourced")
    # Pattern 
    pattern_location=fields.Selection([('head_office','Head Office'),('plant','Plant'),('vendor','Vendor')],string='Pattern Location')
    part_type=fields.Selection([('cone','Cone'),('metal_jaw_plate','Metal Jaw Plate'),('cheek_plate','Cheek Plate'),('other_liner','Other Liner'),('spares','Spares')], 'Part Type',)
    core_box=fields.Char("Core Box")
    pattern_status=fields.Char("Pattern Status")
    pattern_availability =fields.Selection([('yes','Yes'),('no','No',)],string='Pattern Availability ')
    pattern_material =fields.Selection([('wood','Wood'),('aluminium','Aluminium')],string='Pattern Material ')
    product_categ=fields.Selection([('spares','Spares'),('casting','Casting'),('job_work','Job Work')],string='Product Sub Category')
    dimension_ids=fields.One2many('product.dimension.line','prod_id', String='Product Dimension')
    material_ids=fields.One2many('product.material.line','prod_id', String='Product Material')
    pattern_reision_ids=fields.One2many('pattern.revision.line','prod_id', String='Product Pattern Revision')
    
#     @api.multi
#     def calc_value(self):
#         for rec in self:
#             #rec.first_value = "".join(item[0].upper() for item in rec.name)
#             output = ""
#             for i in rec.name.split():
#                 output += i[0]
#             rec.first_value = output
#             
# 
#     @api.depends('product_code','first_value')
#     def total_value(self):
#         for rec in self:
#             if rec.product_code:
#                 rec.second_value = str(rec.first_value) + '-' + str(rec.product_code)
#                 
#     @api.depends('categ_id')
#     def categ_pro(self):
#         for rec in self:
#             rec.categ_value = rec.categ_id.complete_name   
#             
#     @api.depends('categ_id','categ_value')
#     def comp_categ(self):
#         for rec in self:
#             output = ""
#             for i in str(rec.categ_value).replace('/',' ').split():
#                 output += i[0]
#                 rec.third_value = output   
#             
#     @api.depends('product_code','first_value','second_value','categ_value','third_value')
#     def conc_vals(self):
#         for rec in self:
#             if rec.product_code:
#                 rec.final_value = str(rec.third_value) + '-' +  str(rec.second_value)   
                
#     @api.multi            
#     @api.onchange('revision_id')
#     def onchange_revision(self):
#         for rec in self:
#             rec.revision_code=rec.revision_id.name
#             rec.changes=rec.revision_id.changes
#             rec.drawing_revision=rec.revision_id.drawing_revision
#             rec.original_size=rec.revision_id.original_size
#             rec.new_size=rec.revision_id.new_size
#             rec.approved_by=rec.revision_id.approved_by                    
# 
#     @api.multi
#     @api.onchange('material_id')
#     def onchange_material(self):
#         for rec in self:
#             rec.material_code=rec.material_id.name
#             rec.material_config=rec.material_id.material_config

class product_dimension_line(models.Model):
    _name='product.dimension.line'
    
    name = fields.Char('Inspection Specification Value')
    dimen_id=fields.Many2one('product.dimension','Inspection Specification Code')
    prod_id=fields.Many2one('product.template','Product')
    _sql_constraints = [
        ('dim_name_uniq', 'unique (dimen_id,prod_id)', "Dimension Already exists !"),
    ]
    
class product_dimension(models.Model):
    _name='product.dimension'
    
    name = fields.Char('Dimension Name')
    code =fields.Char('Code')
    _sql_constraints = [
        ('dim_name_uniq', 'unique (name)', "Name already exists !"),
        ('dim_code_uniq', 'unique (code)', "Code already exists !"),
    ]

class product_material_line(models.Model):
    _name='product.material.line'
    
    name = fields.Char('Name')
    material_id=fields.Many2one('material','Material Code')
    prod_id=fields.Many2one('product.template','Product')
    
class pattern_revision_line(models.Model):
    _name='pattern.revision.line'
    
    name=fields.Char('Revision Code')
    changes=fields.Char('Changes')
    date=fields.Datetime('Date')
    original_size=fields.Float('Original Size')
    new_size=fields.Float('New Size')
    approved_by=fields.Many2one('res.users',String='Approved By')
    prod_id=fields.Many2one('product.template','Product')
    
class material(models.Model):
    _name='material'
    
    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for mat in self:
            name = '[' + str(mat.code) + ']' + ' ' + str(mat.name) +  ' ' + '[' + str(mat.material_config) + ']'
            result.append((mat.id, name))
        return result
    
    name=fields.Char('Material')
    code=fields.Char('Code')
    material_config=fields.Char('Material Config')
    
    _sql_constraints = [
        ('mat_name_uniq', 'unique (name)', "Name already exists !"),
        ('mat_code_uniq', 'unique (code)', "Code already exists !"),
    ]
 
    
