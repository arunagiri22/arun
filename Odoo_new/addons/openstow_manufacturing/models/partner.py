from openerp import models, fields, api, _
from datetime import datetime
from dateutil import relativedelta
import time


class res_partner(models.Model):
    _inherit ="res.partner"
    retail_type = fields.Boolean()
    oe_type = fields.Boolean()
    engineering_type = fields.Boolean()
    corporate_type = fields.Boolean()
    trader_type = fields.Boolean()
    export_type = fields.Selection([('international', 'Export(International)'),('domestic','Domestic')],'Export Type')
    creation_date=fields.Datetime()
    month = fields.Integer(compute='calc_month')
    cust_sequence = fields.Char()
    new_seq = fields.Char(compute='calc_seq')
    
    @api.multi
    @api.depends('name', 'cust_sequence')
    def name_get(self):
        result = []
        for part in self:
            name = '[' + str(part.cust_sequence) + ']' + ' ' + str(part.name) 
            result.append((part.id, name))
        return result
   
    @api.depends('creation_date')
    def calc_month(self):
    	for rec in self:
            if rec.creation_date:
    		          rec.month = int(datetime.strptime(rec.creation_date, "%Y-%m-%d %H:%M:%S").strftime('%m'))

    @api.depends('cust_sequence')
    def calc_seq(self):
	   for rec in self:
			if rec.month == 1:
				rec.new_seq = 'A' + rec.cust_sequence
			elif rec.month == 2:
				rec.new_seq = 'B' + rec.cust_sequence
			elif rec.month == 3:
				rec.new_seq = 'C' + rec.cust_sequence
			elif rec.month == 4:
				rec.new_seq = 'D' + rec.cust_sequence
			elif rec.month == 5:
				rec.new_seq = 'E' + rec.cust_sequence
			elif rec.month == 6:
				rec.new_seq = 'F' + rec.cust_sequence
			elif rec.month == 7:
				rec.new_seq = 'G' + rec.cust_sequence
			elif rec.month == 8:
				rec.new_seq = 'H' + rec.cust_sequence
			elif rec.month == 9:
				rec.new_seq = 'I' + rec.cust_sequence
			elif rec.month == 10:
				rec.new_seq = 'J' + rec.cust_sequence
			elif rec.month == 11:
				rec.new_seq = 'K' + rec.cust_sequence
			elif rec.month == 12:
				rec.new_seq = 'L' + rec.cust_sequence
			else:
				rec.new_seq = rec.cust_sequence   

