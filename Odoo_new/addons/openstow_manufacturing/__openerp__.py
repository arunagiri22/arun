{
 'name': "Manufacturing",
 'version': '1.0',
 'depends': ['base','sale','stock','sale_layout','sale_crm','delivery'],
 'author': "GetOpenERP (Part of OpenStow Technologies)",
 'category': '',
 'description': """
    
 """,
# data files always loaded at installation
 'data': [
  #~ 'security/group.xml',
  #~ 'security/sequence.xml',
  #~ 'wizard/wiz_revision_log_view.xml',
  #~ 'views/sale_view.xml',
  #~ 'views/product_view.xml',
  #~ 'views/enquiry_view.xml',
  #~ 'views/partner_view.xml',
  #~ 'views/machine_view.xml',
  #~ 'report/quotation_report_view.xml',
  #~ 'report/enquiry_report.xml',
  #~ 'report/contract_review_report.xml',
  #~ 'report/packing_list_report.xml',
  #~ 'report/complaint_assessment_report.xml',
  #~ 'report/customer_complaint_report.xml',
  #~ 'report/order_acceptance_report.xml',
  #~ 'report/survey_report.xml',
  #~ 'report/sale_order_report.xml',
  #~ 'report/packing_note_report.xml',
  #~ 'views/report.xml',
  #~ 'views/menu_view.xml',
   ],
 'demo': [],
}
