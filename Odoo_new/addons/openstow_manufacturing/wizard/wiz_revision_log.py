# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
import datetime
from datetime import date
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.exceptions import UserError


class wiz_revision_log(osv.osv_memory):

    _name = 'wiz.revision.log'
    _columns = {
        'name': fields.char('Name'),
        'changes': fields.char('Changes'),
        'original_size': fields.float('Original Size'),
        'new_size': fields.float('New Size'),
        'approved_by':fields.many2one('res.users','Approved By')
         }

    def act_update_log(self, cr, uid, ids, context=None):
        context = dict(context or {})

        revision_obj = self.pool.get('pattern.revision.line')
        prod_id = context.get('active_ids', False)
        wizard = self.browse(cr, uid, ids[0], context=context)
        name=wizard.name
        changes=wizard.changes
        original_size=wizard.original_size
        new_size=wizard.new_size
        today_date = datetime.datetime.utcnow().date()
        revision_obj.create(cr,uid,{'date':today_date,'prod_id':prod_id[0],'changes':changes,'name':name,'original_size':original_size,'new_size':new_size,'approved_by':uid})
        return True