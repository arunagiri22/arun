from openerp import fields, models, api


class stock_picking(models.Model):
    _inherit= 'stock.picking'
    carrier_tracking_ref=fields.Char(compute='tracking_number_data')
    
    @api.depends('url_follow_up')
    def tracking_number_data(self):
        for record in self:
            record.carrier_tracking_ref=record.url_follow_up
            
class stock_move(models.Model):
    _inherit='stock.move'
    
    csv_qty =fields.Integer(string ='CSV QTY',compute='copy_csv')
    
    @api.depends('product_uom_qty')
    def copy_csv(self):
        for record in self:
            record.csv_qty=record.product_uom_qty
      
    
    
