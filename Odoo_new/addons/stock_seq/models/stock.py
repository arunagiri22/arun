from openerp import _, api, fields, models


class stock_seq(models.Model):
    _inherit='stock.picking'
    
    seq_code=fields.Char('Packing',compute='copy_seq')
    
    @api.multi
    @api.depends('name')
    def copy_seq(self):
        for record in self:
            cp = record.name[2:]
            record.seq_code = str('PL')+cp
    

