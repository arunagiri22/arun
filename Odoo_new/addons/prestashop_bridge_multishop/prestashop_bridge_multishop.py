# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import workflow
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api

def _unescape(text):
    ##
    # Replaces all encoded characters by urlib with plain utf8 string.
    #
    # @param text source text.
    # @return The plain text.
    from urllib import unquote_plus
    return unquote_plus(text.encode('utf8'))

############## Inherited PrestaShop classes #################
class force_done(osv.osv):          
    _inherit="force.done"

    def create_shop(self, cr, uid, data, context=None):
        
        if context is None: context = {}
        company_id=self.pool.get('res.company')._company_default_get(cr, uid,'sale.order')
        warehouse_id=self.pool.get('stock.warehouse').search(cr, uid, [('company_id','=',company_id)],limit=1)[0]
        shop = {
               'name': data.get('name'),
               'warehouse':warehouse_id
                }
        shop_id=self.pool.get('wk.sale.shop').create(cr, uid, shop)
        return shop_id

class wk_skeleton(osv.osv):
    _inherit="wk.skeleton"

    def create_order(self, cr, uid, order_data, context=None):
        context = context or {}
        # check order_data for min no of keys presen or not
        order_name,order_id,status,status_message = "",False,True,"Order Successfully Created."
        sale_data = {
        'partner_id'            :order_data['partner_id'], # Customer
        'partner_invoice_id'    :order_data['partner_invoice_id'], # Invoice Address
        'partner_shipping_id'   :order_data['partner_shipping_id'], # Delivery Address
        'pricelist_id'          :order_data['pricelist_id'], # Pricelist
        'origin'                :order_data['ecommerce_order_ref'], # eCommerce Order Ref
        'ecommerce_channel'     :order_data['ecommerce_channel'] # eCommerce Channel
        }
        if order_data.has_key('date_order'): # Order Date
            sale_data['date_order'] = order_data['date_order']
        if order_data.has_key('note'): # Terms and conditions
            sale_data['note'] = order_data['note']
        if order_data.has_key('payment_method_id'): # Payment Method
            sale_data['payment_method'] = order_data['payment_method_id']
        if order_data.has_key('carrier_id'): # Carrier ID
            sale_data['carrier_id'] = order_data['carrier_id']
            print"VVCVCVCVCVCVCVCVCVCVC",order_data.has_key('carrier_id'),
        if order_data.get('shop_id'):
                print "############################",order_data.get('shop_id')
                sale_data.update({'wk_shop':order_data['shop_id']})
                wk_warehouse = self.pool.get('wk.sale.shop').browse(cr, uid, int(order_data['shop_id'])).warehouse.id
                sale_data.update({'warehouse_id':wk_warehouse})
        config_data = self.get_default_configuration_data(cr, uid, order_data['ecommerce_channel'], context)
        sale_data.update(config_data)
        # if config_data.has_key('team_id'):  Sales Team
        #   sale_data['team_id'] = config_data['team_id']
        # if config_data.has_key('payment_term_id'):  Payment Term
        #   sale_data['payment_term_id'] = config_data['payment_term_id']
        # if config_data.has_key('user_id'):  Salesperson
        #   sale_data['user_id'] = config_data['user_id']
        try:
            order_id = self.pool.get('sale.order').create(cr, uid, sale_data, context=context)
            order_name = self.pool.get('sale.order').read(cr, uid, order_id, ['name'], context=context)['name']
            self.create_order_mapping(cr, uid, {
                'ecommerce_channel':order_data['ecommerce_channel'],
                'erp_order_id':order_id,
                'ecommerce_order_id':order_data['ecommerce_order_id'],
                'name':order_data['ecommerce_order_ref'],
                }, context=context)
        except Exception, e:
            status_message = "Error in creating order on Odoo: %s"%str(e)
            status = False
        finally:
            return {
                'order_id': order_id,
                'order_name': order_name,
                'status_message': status_message,
                'status': status
            }

class wk_sale_shop(osv.osv):    
    _name = 'wk.sale.shop'

    _columns = {
        'name': fields.char('Shop Name',size=100),
        'warehouse':fields.many2one('stock.warehouse', 'Warehouse'),
        'stock_location': fields.related('warehouse', 'lot_stock_id', type='many2one', string="Stock Location", relation='stock.location'),

    }
wk_sale_shop()  

class sale_order(osv.osv):
    _inherit = "sale.order"

    _columns = {     
        'wk_shop':fields.many2one('wk.sale.shop', 'Odoo Shop')
        }

sale_order()
