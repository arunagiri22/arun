# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import workflow
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api

############## Inherited PrestaShop classes #################

class wk_sale_shop(osv.osv):	
	_name = 'wk.sale.shop'

	_columns = {
        'name': fields.char('Shop Name',size=100),
		'warehouse':fields.many2one('stock.warehouse', 'Warehouse'),
		'stock_location': fields.related('warehouse', 'lot_stock_id', type='many2one', string="Stock Location", relation='stock.location'),

    }
wk_sale_shop()	

class sale_order(osv.osv):
	_inherit = "sale.order"

	_columns = {     
		'wk_shop':fields.many2one('wk.sale.shop', 'Odoo Shop')
		}

sale_order()