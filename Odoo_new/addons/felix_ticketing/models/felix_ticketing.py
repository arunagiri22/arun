from openerp import fields, models, api


class felix_ticketing(models.Model):
    _name = 'felix.ticketing'
    
    name=fields.Char('Felix_TicketID')
    sender_name=fields.Char('Sender-Name')
    sender_id=fields.Char('Sender Id')
    sender_direct_dial=fields.Char('Sender-Direct-Dial')
    cham_branch1=fields.Many2many('cham.branch', 'res_id', 'res_idds' ,string='cham branch')
    cham_mail=fields.Char('Cham Mail')
    kategory=fields.Many2one('category',string='Kategory')
    start_date=fields.Date('Start Date')
    end_date=fields.Date('End Date')
    due_date=fields.Date('Due Date')
    remarks=fields.Text('Remarks')
    pripriority=fields.Many2one('res.partner',string='PriPriority')
    first_name=fields.Char(string='First Name',related='pripriority.name')
    todo_email=fields.Char(string='Email',related='pripriority.email')
    todo_phone=fields.Char(string='Phone',related='pripriority.phone')
    todo_content=fields.Text(string='Content')
    
    
    
    @api.multi
    def create_partner(self):
        for record in self:
            self.env['res.partner'].create({'name':record.name})

class cham_branch(models.Model):
    _name='cham.branch'
    
    name = fields.Char('Name')
    chamber_number=fields.Char('Chamber Number')
    chambranch=fields.Char('Chamb Branch')
    chamb_id=fields.Integer('ID')
    
class category(models.Model):
    _name='category'
    
    
    name=fields.Char('Kategory')
