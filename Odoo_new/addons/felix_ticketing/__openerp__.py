# -*- coding: utf-8 -*-
{
    'name': 'Felix Ticketing system',
    'version': '1.0',
    'website' : '',
    'category': '',
    'summary': 'Ticketing System',
    'description': """


""",
    'author': '',
    'depends': ['base'],
    'data': ['views/felix_ticketing_view.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
