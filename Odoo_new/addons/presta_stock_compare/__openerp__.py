# -*- coding: utf-8 -*-
{
    'name': 'Automatic add Barcode and Quantity From Ftp Server',
    'version': '1.0',
    'website' : '',
    'category': '',
    'summary': 'Automatic Add Barcode And Quantity FromFtp Server ',
    'description': """


""",
    'author': 'Openstow',
    'depends': ['base', 'stock','stock_ftp'],
    'data': ['views/stock_details.xml','data/stock_ftp_download_data.xml','views/import_exception.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
