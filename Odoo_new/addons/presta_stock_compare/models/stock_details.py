from openerp import models, fields, api, _
from ftplib import FTP
import base64
import sys
import ftplib
import fnmatch
import os
import time
import re
import logging
import tempfile
import csv, codecs, cStringIO
#~ from datetime import datetime, timedelta
import datetime
import shutil
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
try:
    import ftplib
except ImportError:
    raise ImportError('This module needs ftplib to automaticly write backups to the FTP through FTP.')

from openerp import api, fields, models, _
from openerp import tools
from openerp.exceptions import UserError
from openerp.tools.safe_eval import safe_eval
_logger = logging.getLogger(__name__)


class StockPickingBackup(models.TransientModel):
    _inherit = 'stock.picking.config.settings'
    
    
    
    @api.model
    def process_stock_compare(self):
        # TODO: note that we will read from ftp server and process it by checking existance and reference number
        icp = self.env['ir.config_parameter']
        tmp_dir = tempfile.mkdtemp()
        product_obj=self.env['product.product']
        stock_line_com_obj = self.env['stock.detail.line']
        import_except_obj=self.env['import.exception']
        #~ import_except_id=import_except_obj.search([])
        #~ import_except_id.unlink()
        
        try:
            ipHost = safe_eval(icp.get_param('stock_ftp.sftpip', 'False'))
            portHost = safe_eval(icp.get_param('stock_ftp.sftpport', 'False'))
            usernameLogin = safe_eval(icp.get_param('stock_ftp.sftpusername', 'False'))
            passwordLogin = safe_eval(icp.get_param('stock_ftp.sftppassword', 'False'))
            downloadDirectory = safe_eval(icp.get_param('stock_ftp.download_dir', 'False'))
            processedDirectory = safe_eval(icp.get_param('stock_ftp.download_processed_dir', 'False'))
            #Connect with external server over FTP
            srv = ftplib.FTP(ipHost)
            srv.login(usernameLogin, passwordLogin)
            srv_move = ftplib.FTP(ipHost)
            srv_move.login(usernameLogin, passwordLogin)
            # TODO: To check with ftplib if keepalive exist
            #set keepalive to prevent socket closed / connection dropped error
#             srv._transport.set_keepalive(30)
#             srv_move._transport.set_keepalive(30)
            #Move to the correct tmp_directory on external server. If the user made a typo in his path with multiple slashes (/odoo//backups/) it will be fixed by this regex.
            downloadDirectory = re.sub('([/]{2,5})+','/',downloadDirectory)
            _logger.debug('sftp remote path: %s' % downloadDirectory)
            processedDirectory = re.sub('([/]{2,5})+','/',processedDirectory)
            _logger.debug('sftp remote path: %s' % processedDirectory)
            try:
                # time.sleep(0.001)
                srv.cwd(downloadDirectory)
            except IOError, e:
                print "\n\nIOError :::: ",e 
                #Create directory and subdirs if they do not exist.
                currentDir = ''
                for dirElement in downloadDirectory.split('/'):
                    currentDir += dirElement + '/'
                    try:
                        srv.cwd(currentDir)
                    except:
                        _logger.info('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                        #Make directory and then navigate into it
                        srv.mkd(currentDir)
                        srv.cwd(currentDir)
                        pass
            srv.cwd(downloadDirectory)

            try:
                srv_move.cwd(processedDirectory)
            except IOError:
                #Create directory and subdirs if they do not exist.
                currentDir = ''
                for dirElement in processedDirectory.split('/'):
                    currentDir += dirElement + '/'
                    try:
                        srv_move.cwd(currentDir)
                    except:
                        _logger.info('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                        #Make directory and then navigate into it
                        srv_move.mkd(currentDir)
                        srv_move.cwd(currentDir)
                        pass
            srv_move.cwd(processedDirectory)
            all_files = srv.nlst()
            for file in all_files:
                print"&&&&&&&&&&&&& file &&&&&&&&&&&&&&&",file
                file_path = os.path.join(tmp_dir, file)
                print"######## file_path ########",file_path
                datafile = open(file_path, mode='wb')
                print"@@@@@@@@@  datafile@@@@@@@@",datafile
                #with open(file_path, mode='ar+') as datafile:
                    # temporary_file = srv.get(file, file_path)
                srv.retrbinary('RETR ' + file, datafile.write)
                datafile.close()
                success_list = []
                data_lines = []
                datafile = open(file_path, mode='r')
                sdetail_obj = self.env['stock.details']
                
                file_name = file[3:-4]
                dt = datetime.datetime.strptime(file_name,"%d%m%Y").date()
                print"%%%%%%",dt
                stock_obj = sdetail_obj.search([('name' ,'=',file )])
                if stock_obj:
                    stock_compare_id= stock_obj
                else:
                   stock_compare_id = sdetail_obj.create({'name':file,'upate_qty_date':dt})
                val_dic={}
                lst=[]
                
                for line in datafile.readlines():
                    line_fields = line and line.split(",") or False
                    if line_fields:                       
                        barcode= line_fields[0]
                        qty = line_fields[1]
                        lst.append({'name':barcode,'stock_compare_id':stock_compare_id.id,'qty':qty,})
                        try:
                            data_lines.append(True)
                        except Exception, e:
                            print "\n\nException eeeee ",e
                for val in lst:
                    name=val.get('name')
                    qty= val.get('qty')
                    compare_id=val.get('stock_compare_id')
                    product_id = product_obj.search([('barcode' ,'=',name )])
                    if product_id:                        
                        st_obj = stock_line_com_obj.search([('name' ,'=',name),('stock_detail_id','=',compare_id)])  
                                         
                        if len(st_obj) > 0:  
                            st_obj.write({'qty':qty})                                                     
                            continue
                        sline=stock_line_com_obj.create({'name':name,'stock_detail_id':compare_id,'qty':qty,'product_id': product_id.id})                        
                    else:  
                        import_except=self.env['import.exception'].create({'name':name,'exception':'Product Not Found'})
                        
                datafile.close()
                datafile = open(file_path, mode='r')
                print"!!!!!!!!!!!!!!!!   datafile!!!!!!!!!!1",datafile
                print"file",file
                srv.delete(file)
                print"^^^^^^^^^  srv_move ^^^^^^^^^",srv_move
                srv_move.storlines("STOR " + file, datafile)
                print"((((((((((((((( "
                datafile.close()
            srv.close()
            srv_move.close()
            shutil.rmtree(tmp_dir)
        except Exception, e:
            _logger.info('Exception! We couldn\'t back up to the FTP server..')


class stock_details(models.Model):
    _name='stock.details'
    
    name=fields.Char('Name',readonly=True)
    create_date = fields.Datetime('Create Date',readonly=True)
    upate_qty_date=fields.Datetime('Update qty Date')    
    compare_line_ids = fields.One2many('stock.detail.line', 'stock_detail_id', string='Order Lines') 
    _sql_constraints = [
        ('stock_update_date_uniq', 'unique (upate_qty_date)', "Update Qty Date already exists !"),
    ]
    #~ @api.multi
    #~ @api.depends('name')
    #~ def update_date(self):
        #~ for record in self:
            #~ dt = record.name[3:-4]
            #~ dt1 =  datetime.datetime.strptime(dt,"%d%m%Y").date()
            #~ record.upate_qty_date = dt1
    
    
class StockDetailLine(models.Model):
    _name = 'stock.detail.line'
    
    stock_detail_id = fields.Many2one('stock.details', 'Stock Compare')
    name=fields.Char('Barcode')
    qty=fields.Char('Quantity')
    product_id = fields.Many2one('product.product', 'Product')

    
