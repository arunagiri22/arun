from openerp import fields, models, api
from ftplib import FTP
import base64
import sys
import ftplib
import fnmatch
import os
import time
import re
import logging
import tempfile
import datetime


class import_exception(models.Model):
    _name = 'import.exception'
       
    
    exception =fields.Char('Exception',readonly=True)
    create_date = fields.Datetime('Create Date')
    name=fields.Char('Barcode',readonly=True)
 
