# -*- coding: utf-8 -*-
{
    'name': 'VetClinic',
    'version': '1.0',
    'website' : '',
    'category': '',
    'summary': 'Student Information',
    'description': """


""",
    'author': 'Arunagiri',
    'depends': ['base','sale', 'purchase'],
    'data': ['vetclinic_view.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
