# -*- coding: utf-8 -*-
{
    'name': 'Student',
    'version': '1.0',
    'website' : '',
    'category': '',
    'summary': 'Student Information',
    'description': """


""",
    'author': 'Arunagiri',
    'depends': ['base'],
    'data': ['views/student_view.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
