# -*- coding: utf-8 -*-
{
    'name': 'Delivery service Code',
    'version': '1.0',
    'website' : '',
    'category': '',
    'summary': 'Delivery service code',
    'description': """


""",
    'author': 'Openstow',
    'depends': ['base', 'stock','delivery'],
    'data': ['delivery_carrier.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
