from openerp import fields, models, api


class delivery_carrier(models.Model):
    _inherit= 'delivery.carrier'
    
    service_code=fields.Char('Service Code')
