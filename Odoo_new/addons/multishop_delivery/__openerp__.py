{
    'name': 'Multishop Delivery',
	'sequence': 1,
	'summary':'Multishop For Delivery',
    'version': '2.0',
    'category': '',
    'description': """	
    Module For Multishop in Delivery
    """,
    'author': 'Getopenerp',
    'depends': ['base','stock','pob','prestashop_bridge_multishop'],
    'website': 'www.getopenerp.com',
    'data': ['views/multishop_delivery_view.xml'],
    'installable': True,
    'active': False,
    
}
