from datetime import datetime, timedelta
from openerp import SUPERUSER_ID
from openerp import api, fields, models, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError
from openerp.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

class StockPickings(models.Model):
    _inherit='stock.picking'
    
    wk_shop=fields.Many2one('wk.sale.shop',compute='multi_shop')
    
    
    @api.multi
    def multi_shop(self):
        for record in self:
            for rec in record.sale_id:
                record.wk_shop=rec.wk_shop
