# -*- coding: utf-8 -*-
{
    'name': 'Add selection field in Sale order & Invoice seq',
    'version': '1.0',
    'website' : 'www.getopenerp.com',
    'category': 'Partner',
    'summary': 'Add selction field and Invoice seq',
    'description': """
            

""",
    'author': 'GetOpenStow(Part Of GetOpenErp)',
    'depends': ['base','sale','account'],
    'data': ['views/sale_view.xml','views/sale_sequence.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
