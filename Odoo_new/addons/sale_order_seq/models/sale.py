from openerp import fields, models, api


class sale (models.Model):
    _inherit='sale.order'
        
    store_name=fields.Selection([('default', 'Default'), ('mallorca', 'Mallorca'),('barcelona','Barcelona'),('antibes','Antibes')],string='Store Name')
        
    @api.multi
    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a sales order. This method may be
        overridden to implement custom invoice generation (making sure to call super() to establish
        a clean extension chain).
        """
        self.ensure_one()
        journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
        if not journal_id:
            raise UserError(_('Please define an accounting sale journal for this company.'))
        invoice_vals = {
            'name': self.client_order_ref or '',
            'origin': self.name,
            'type': 'out_invoice',
            'account_id': self.partner_invoice_id.property_account_receivable_id.id,
            'partner_id': self.partner_invoice_id.id,
            'journal_id': journal_id,
            'currency_id': self.pricelist_id.currency_id.id,
            'comment': self.note,
            'payment_term_id': self.payment_term_id.id,
            'fiscal_position_id': self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
            'company_id': self.company_id.id,
            'user_id': self.user_id and self.user_id.id,
            'team_id': self.team_id.id,
            'store_name':self.store_name
        }
        return invoice_vals
        
class account_invoice(models.Model):
    _inherit='account.invoice'
       
    store_name=fields.Selection([('default', 'Default'), ('mallorca', 'Mallorca'),('barcelona','Barcelona'),('antibes','Antibes')],string='Store Name')

    
        
class account_move(models.Model):
    _inherit='account.move'
    
    
    @api.multi
    def post(self):
        invoice = self._context.get('invoice', False)
        self._post_validate()

        for move in self:
            move.line_ids.create_analytic_lines()
            if move.name == '/':
                new_name = False
                journal = move.journal_id

                if invoice and invoice.move_name and invoice.move_name != '/':
                    new_name = invoice.move_name
                else:
                    if journal.sequence_id:
                        # If invoice is actually refund and journal has a refund_sequence then use that one or use the regular one
                        sequence = journal.sequence_id
                        if invoice and invoice.type in ['out_refund', 'in_refund'] and journal.refund_sequence:
                            sequence = journal.refund_sequence_id

                        new_name = sequence.with_context(ir_sequence_date=move.date).next_by_id()
                    else:
                        raise UserError(_('Please define a sequence on the journal.'))
                  
               
                if invoice.store_name=='mallorca':
                    new_name = self.env['ir.sequence'].next_by_code('account.invoice.mal') or '/'
                    

                elif invoice.store_name=='barcelona':
                    new_name = self.env['ir.sequence'].next_by_code('account.invoice.bar') or '/'
        
                elif invoice.store_name=='antibes':
                    new_name = self.env['ir.sequence'].next_by_code('account.invoice.ant') or '/'
                
                if new_name:
                    move.name = new_name
        return self.write({'state': 'posted'})
        
 
        
    
