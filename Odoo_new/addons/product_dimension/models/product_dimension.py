from openerp import fields, models, api


class product_template(models.Model):
    _inherit='product.product'
    
    dimension_ids=fields.One2many('product.dimension.line','product_template', String='Product Dimension')
    
    
    
    
class product_dimension_line(models.Model):
    _name='product.dimension.line'
    
    product_template=fields.Many2one('product.product')
    name = fields.Char('Name')
    l1 = fields.Float('L1')
    l2 =fields.Float('L2')
    l3=fields.Float('L3')
    b1=fields.Float('B1')
    b2=fields.Float('B2')
    b3=fields.Float('B3')
