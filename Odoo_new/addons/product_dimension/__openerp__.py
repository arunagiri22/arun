{
    'name': 'Product Dimension',
    'category': 'product information s/y',
    'version': '1.0',
    'author': 'GetOpenErp',
    'depends': ['base','product'],

    'description': """
      product information
    """,
    'data': [   
        'views/product_dimension.xml'

        
    ],
    'demo':[''],
    'installable': True,

}
