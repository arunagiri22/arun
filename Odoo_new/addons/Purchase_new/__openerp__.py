# -*- coding: utf-8 -*-
{
    'name': 'Add field in purchase order lin ',
    'version': '1.0',
    'website' : 'www.getopenerp.com',
    'category': 'Purchase',
    'summary': 'Add the field in purchase order line',
    'description': """
            

""",
    'author': 'GetOpenStow(Part Of GetOpenErp)',
    'depends': ['base','purchase'],
    'data': ['views/purchase_view.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
