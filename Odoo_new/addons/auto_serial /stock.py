from openerp import fields, models, api


class stock_transfer_details_items(models.Model):
   
    _inherit='stock.transfer_details_items'
    
    user_id =fields.Many2one('res.users' , default=lambda self:self.env.user)
    
    user_value=fields.Boolean(compute='user_data_val')
    
    @api.depends('user_id')
    def user_data_val(self):
        for record in self:
            record.user_value=record.user_id.module_serial
    
