# -*- coding: utf-8 -*-
{
    'name': 'Configuration For Restrict Serial number',
    'version': '1.0',
    'website' : '',
    'category': 'Warehouse',
    'summary': 'Restrict serial number from other user',
    'description': """


""",
    'author': '',
    'depends': ['base','stock'],
    'data': ['config_view.xml','stock_view.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
