# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#    Copyright (C) 2012-2013:
#        Agile Business Group sagl (<http://www.agilebg.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
try:
    import json
except ImportError:
    import simplejson as json

import openerp.http as http
from openerp.http import request
from openerp.addons.web.controllers.main import ExcelExport


class ExcelExportView(ExcelExport):
    def __getattribute__(self, name):
        if name == 'fmt':
            raise AttributeError()
        return super(ExcelExportView, self).__getattribute__(name)

    @http.route('/web/export/xls_view', type='http', auth='user')
    def export_xls_view(self, data, token):
        data = json.loads(data)
   
        model = data.get('model', [])
        id=data.get('id',[])
        columns_headers = ['Bank Code','Bank Account No.','Beneficiary Name','ID Number','Reference','IC number','By Salary','Contact Number']
        advice=request.env['hr.payroll.advice'].browse(id)
        
        lst=[]
        #~ rows=[[rec.employee_id.name,rec.identification_id or "Nil",
                        #~ rec.bank_bic or "Nil",rec.ic_id or "Nil",rec.name,rec.number or "Nil",rec.bysal or "Nil",rec.mobile_phone or "Nil"] for rec in advice.line_ids]
        rows=[[rec.bank_bic ,rec.name or "Nil",
                            rec.employee_id.name,rec.identification_id or "Nil", rec.number,rec.ic_id or "Nil",rec.bysal or "Nil",rec.mobile_phone or "Nil"] for rec in advice.line_ids]   
        return  request.make_response(
            self.from_data(columns_headers, rows),
            headers=[
                ('Content-Disposition', 'attachment; filename="%s"'
                 % self.filename(model)),
                ('Content-Type', self.content_type)
            ],
            cookies={'fileToken': token}
        )
