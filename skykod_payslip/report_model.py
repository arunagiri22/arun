from openerp.osv import osv,fields

class ParticularReport(osv.AbstractModel):
    _name = 'report.skykod_payslip.payslip_template'
    
    def render_html(self,cr,uid,ids,data=None,context=None):
        print "hello"
        report_obj = self.pool['report']
        report = report_obj._get_report_from_name(cr,uid,'skykod_payslip.payslip_template')
        hr_payslip=self.pool.get('hr.payslip').browse(cr,uid,ids[0])
        lst=[]
            
        wage=hr_payslip.contract_id.wage if hr_payslip.contract_id.wage>=900.0 else 900.0
        for rec in hr_payslip.worked_days_line_ids:
            if rec.code=='HOT1':
                lst.append(['HOT1',1.5,rec.number_of_days,"{0:.2f}".format((wage/208)*1.5*rec.number_of_hours)])
            if rec.code=='HOT2':
                lst.append(['HOT2',2,rec.number_of_days,"{0:.2f}".format((wage/208)*2*rec.number_of_hours)])
            if rec.code=='HOT3':
                lst.append(['HOT3',3,rec.number_of_days,"{0:.2f}".format((wage/208)*3*rec.number_of_hours)])
       
                
        docargs = {
        'over':lst,
        'doc_model': report.model,
            'docs': self.pool[report.model].browse(
                cr, uid, ids, context=context
            ),
         }
     
        return report_obj.render(cr,uid,ids,'skykod_payslip.payslip_template', docargs,context=context)
        
    
