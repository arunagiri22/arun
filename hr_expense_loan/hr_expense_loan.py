from openerp.osv import fields, osv, expression
import time
from datetime import datetime, timedelta,date
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import netsvc
from lxml import etree
from dateutil.relativedelta import relativedelta

class loan_employee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'
    _columns = {
        'income': fields.float('Monthly Income',digits=(12,3)),
    }
loan_employee()


class hr_expense_loan(osv.osv):

    _name = "hr.expense.loan" 
    _inherit = ['mail.thread', 'ir.needaction_mixin'] 
    _description = 'HR Loan Management' 

    def _get_default_company(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))
        return company_id
    
    def _amount_all_wrapper(self, cr, uid, ids, field_name, arg, context=None):
        """ Wrapper because of direct method passing as parameter for function fields """
        return self._amount_all(cr, uid, ids, field_name, arg, context=context)

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for loan in self.browse(cr, uid, ids, context=context):
            res[loan.id] = {
                'amount_paid': 0.0,
                'amount_pending': 0.0,
            }
            val  = 0.0
            cur = loan.currency_id
                       
            for line in loan.employee_loanlines:
                    val += line.amount

            res[loan.id]['amount_paid'] = cur_obj.round(cr, uid, cur, val)
            res[loan.id]['amount_pending'] = loan.amount - res[loan.id]['amount_paid'] 
        return res
        
    _columns = { 
           
            'name' : fields.char('Name', size=64, select=True, readonly=True), 
            'description' : fields.char('Description', size=128, select=True, required=True, readonly=True, states={'draft':[('readonly',False)], 'approved':[('readonly',False)]}), 
            'date' : fields.date('Date', required=True, select=True, readonly=True, states={'draft':[('readonly',False)], 'approved':[('readonly',False)]}), 
            'employee_id' : fields.many2one('hr.employee', 'Employee', required=True),
            'employee_loanlines': fields.one2many('employee.loanline','loan_id',),
            'tenure': fields.integer('Tenure (Month)',),
            'date_from': fields.date('Date from', required=True, select=1,),
            'date_to': fields.date('Date to', required=True, select=1,),
            'state' : fields.selection([ ('draft', 'Draft'), ('approved', 'Approved'), ('confirmed', 'Confirmed'), ('pending','Pending'), ('cancel', 'Cancelled') ], 'State', select=True), 
            'repayment_method' : fields.selection([ ('quarterly', 'Quarterly'), ('monthly', 'Monthly'), ], 'Repayment Method', readonly=True),
            #~ 'loan_type' : fields.selection([ ('personal loan', 'Personal Loan'), ('home loan', 'Home Loan'), ], 'Loan Type',),
            'repayment_type' : fields.selection([ ('fixed', 'Fixed') ], 'Repayment Type', readonly=True,  required=True),
            'notes' : fields.text('Notes'),
            'origin' : fields.char('Source Document', size=64, help="Reference of the document that generated this Expense Record", readonly=True, states={'draft':[('readonly',False)], 'approved':[('readonly',False)]}),
            'amount' : fields.float('Total Amount', digits_compute=dp.get_precision('Sale Price'), required=True, readonly=True, states={'draft':[('readonly',False)], 'approved':[('readonly',False)]}), 
            'amount_paid':fields.function(_amount_all_wrapper, string='Amount Paid', digits_compute= dp.get_precision('Account'), multi='sums',),
            'amount_pending':fields.function(_amount_all_wrapper, string='Amount Pending', digits_compute= dp.get_precision('Account'), multi='sums',),
            'fixed_amount' : fields.float('Fixed amount', digits_compute=dp.get_precision('Sale Price'), required=False, readonly=True, states={'draft':[('readonly',False)], 'approved':[('readonly',False)]}),
            'create_uid' : fields.many2one('res.users', 'Created by', ), 
            'cancelled_by' : fields.many2one('res.users', 'Cancelled by' ), 
            'date_cancelled': fields.datetime('Date Cancelled'), 
            'approved_by' : fields.many2one('res.users', 'Approved by'), 
            'date_approved' : fields.datetime('Date Approved'), 
            'confirmed_by' : fields.many2one('res.users', 'Confirmed by'), 
            'date_confirmed': fields.datetime('Date Confirmed'), 
            'closed_by' : fields.many2one('res.users', 'Closed by'), 
            #~ 'currency_id' : fields.many2one('res.currency', 'Currency'), 
            'date_closed' : fields.datetime('Date Closed'),
            
            
            }
            
    _defaults = {

                
                'date' : lambda *a: time.strftime(DEFAULT_SERVER_DATE_FORMAT), 'state' : lambda *a: 'draft',
                'repayment_method':'monthly',
                'repayment_type' : 'fixed',
                'currency_id': _get_default_company,
                }
                
    _sql_constraints = [('name_uniq', 'unique(name)', 'Loan record must be unique !')]
    

    def create_instalment(self,cr, uid, ids, context={}):
        for current_form_obj in self.browse(cr, uid, ids, context):
            if not current_form_obj:
                return False
            else:
                 n=int(current_form_obj.tenure)
                 i = 1
                 month= current_form_obj.tenure
                 if current_form_obj.amount  <=0:
                    raise osv.except_osv(_('Invalid Action!'), _('In order to create a instalment, please enter valid loan amount'))
                 while i<=current_form_obj.tenure:
                    amount = current_form_obj.amount / current_form_obj.tenure
                    self.pool.get('employee.loanline').create(cr,uid,{
                    'loan_id':current_form_obj.id,
                    'no_of_instalment':i,
                    'amount':amount,
                    'amount_pending':amount,
                    'status':'due',
                    'due_date': datetime.strptime(current_form_obj.date_from,"%Y-%m-%d") + relativedelta(months=(i-1))
                    })
                    i=i+1
                    month+= 1
                 if n <=0:
                    raise osv.except_osv(_('Invalid Action!'), _('In order to create a instalment, please enter valid tenure!'))
            self.write(cr,uid,current_form_obj.id,{'state':'confirmed'})
        return True


        
    def action_confirm(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context): 
            self.write(cr, uid, ids, {'state':'confirmed', 'confirmed_by' : uid,'date_confirmed':time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)}) 
            for (id,name) in self.name_get(cr, uid, ids): 
                message = _("Loan '%s' is set to Confirmed.") % name 
                self.log(cr, uid, id, message)
        return True
    
    
    def clear(self,cr,uid,ids,context=None):
        for hr_expense_loan in self.browse(cr,uid,ids):
            lines=[]
            for line in hr_expense_loan.employee_loanlines:
                    lines.append(line.id)
            self.pool.get('employee.loanline').unlink(cr,uid,lines)
        return True 
       
    def action_cancel(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context): 
            self.write(cr, uid, ids, {'state':'cancel', 'cancelled_by' : uid,'date_cancelled':time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)}) 
            for (id,name) in self.name_get(cr, uid, ids): 
                message = _("Loan '%s' is set to Cancel.") % name 
                self.log(cr, uid, id, message) 
        return True
        
    
    def set_to_draft(self,cr, uid, ids, context={}):
        for obj in self.browse(cr,uid,ids):
             if obj.state!='cancel':
                  raise osv.except_osv(_('Invalid Action!'), _('Cannot set state to draft for a Loan in %s state!') % (obj.state))
             if obj.state == 'done':
                  raise osv.except_osv(_('Invalid Action!'), _('Cannot set state to draft  for a Loan after completion!'))
        self.clear(cr,uid,ids)
        self.write(cr,uid,ids,{'state':'draft'})
        self.message_post(cr, uid, ids, body=_("Loan is set to draft"))
        return True

    def unlink(self, cr, uid, ids, context=None):
        for t in self.read(cr, uid, ids, ['state'], context=context):
            if t['state'] not in ('draft', 'cancel'):
                raise osv.except_osv(_('Invalid Action!'), _('Cannot delete Loan Instalments.'))
        return super(hr_expense_loan, self).unlink(cr, uid, ids, context=context)



hr_expense_loan()


class employee_loanline(osv.osv):
    _name = "employee.loanline"
    _description = "Property"
    _rec_name="no_of_instalment"
    _columns = {
        'loan_id': fields.many2one('hr.expense.loan', 'Loan', select=True),
        'no_of_instalment': fields.integer('Instalments'),
        'due_date': fields.date('Due Date'),
        'amount': fields.float('Amount'),
        'amount_pending':fields.float('Amount Pending'),
        'status': fields.selection([('due', 'Due'),
                                   ('paid', 'Paid')], 'Status',
                                   required=True, help="""Help Text"""),
    }
employee_loanline()    



class hr_payslip(osv.osv):
    _name = _inherit = 'hr.payslip'
    _columns = {
    'employee_loan_line_id':fields.many2one('employee.loanline',"Employee Loan Line"),
    }
    
    def get_loan_lines(self, cr, uid, contract_ids, payslip_id, amount, context=None):
        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
            employee = contract.employee_id
            obj_rule = self.pool.get('hr.salary.rule')
            rule_id =  obj_rule.search(cr,uid,[('code','=','ADV')])
            rule = obj_rule.browse(cr, uid, rule_id, context=context)[0]
            result = {
                'salary_rule_id': rule.id,
                'contract_id': contract.id,
                'name': rule.name,
                'code': rule.code,
                'category_id': rule.category_id.id,
                'sequence': rule.sequence,
                'appears_on_payslip': rule.appears_on_payslip,
                'condition_select': rule.condition_select,
                'condition_python': rule.condition_python,
                'condition_range': rule.condition_range,
                'condition_range_min': rule.condition_range_min,
                'condition_range_max': rule.condition_range_max,
                'amount_select': rule.amount_select,
                'amount_fix': rule.amount_fix,
                'amount_python_compute': rule.amount_python_compute,
                'amount_percentage': rule.amount_percentage,
                'amount_percentage_base': rule.amount_percentage_base,
                'register_id': rule.register_id.id,
                'amount': amount,
                'employee_id': contract.employee_id.id,
                'quantity': 1,
                'rate': 100.0,
                'slip_id':payslip_id,
            }
        return result
    def compute_sheet(self, cr, uid, ids, context=None):
        slip_line_pool = self.pool.get('hr.payslip.line')
        sequence_obj = self.pool.get('ir.sequence')
        for payslip in self.browse(cr, uid, ids, context=context):
            number = payslip.number or sequence_obj.get(cr, uid, 'salary.slip')
            #delete old payslip lines
            old_slipline_ids = slip_line_pool.search(cr, uid, [('slip_id', '=', payslip.id)], context=context)
#            old_slipline_ids
            if old_slipline_ids:
                slip_line_pool.unlink(cr, uid, old_slipline_ids, context=context)
            if payslip.contract_id:
                #set the list of contract for which the rules have to be applied
                contract_ids = [payslip.contract_id.id]
            else:
                #if we don't give the contract, then the rules to apply should be for all current contracts of the employee
                contract_ids = self.get_contract(cr, uid, payslip.employee_id, payslip.date_from, payslip.date_to, context=context)
            lines = [(0,0,line) for line in self.pool.get('hr.payslip').get_payslip_lines(cr, uid, contract_ids, payslip.id, context=context)]
            self.write(cr, uid, [payslip.id], {'line_ids': lines, 'number': number,}, context=context)
            amount = 0.0
            employee_loan_line_id = False
            due_lines=[]
            payslip_line_obj = self.pool.get('hr.payslip.line')
            if payslip.employee_id:
                hr_expense_loan_obj = self.pool.get('hr.expense.loan')
                loan_ids = hr_expense_loan_obj.search(cr,uid,[('employee_id','=',payslip.employee_id.id),('state', '=','confirmed')])
                if loan_ids:
                    for loan in hr_expense_loan_obj.browse(cr,uid,loan_ids):
                        for line in loan.employee_loanlines:
                            if line.status=='due':
                                due_lines.append(line.id)
                        print due_lines,"56757575"
                        if due_lines:
                            line_id = min(due_lines)     
                            employee_loan_line_id=line_id
                            employee_loan_line_obj = self.pool.get('employee.loanline')
                            line_br = employee_loan_line_obj.browse(cr,uid,line_id)
                            amount = -line.amount_pending
            #~ if amount !=0.0:
            loan_lines_vals = self.get_loan_lines(cr, uid, contract_ids, payslip.id, amount,context=context)
            payslip_line_obj.create(cr, uid, loan_lines_vals, context=context)
            for line in payslip.line_ids:
				if line.salary_rule_id.code =='NET' and  due_lines:
					payslip_line_obj.write(cr,uid,line.id,{'amount':line.amount + amount})
            self.write(cr,uid,payslip.id,{'employee_loan_line_id':employee_loan_line_id})
        return True
    def hr_verify_sheet(self, cr, uid, ids, context=None):
        self.compute_sheet(cr, uid, ids, context)
        for payslip in self.browse(cr, uid, ids, context=context):
            employee_loan_line_obj = self.pool.get('employee.loanline')
            employee_loan_line_br = employee_loan_line_obj.browse(cr,uid,payslip.employee_loan_line_id.id)
            loan_line_vals={
            'amount' : employee_loan_line_br.amount,
            'status': 'paid',}
            employee_loan_line_obj.write(cr,uid,payslip.employee_loan_line_id.id,loan_line_vals)
        return self.write(cr, uid, ids, {'state': 'verify'}, context=context)


hr_payslip()
