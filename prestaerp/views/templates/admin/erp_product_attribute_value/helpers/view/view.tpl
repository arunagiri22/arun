<!-- {if isset($customer_id) && $id == 0}
<div class="alert alert-danger">
	<p>This seller has been removed by admin from prestashop</p>
</div>
{/if} -->

<div id="mp-container-customer">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel clearfix">
				{if isset($attr_val_tpl)}
					<div class="panel-heading">
						<i class="icon-sort-by-attributes-alt"></i>
						{$attr_val_tpl.id|escape:'htmlall':'UTF-8'} - 
						<p>Attribute Value</p>
					</div>
					<div class="form-horizontal">
						<div class="row">
							<label class="control-label col-lg-3">{l s='ID' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$attr_val_tpl.id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>

						<div class="row">
							<label class="control-label col-lg-3">{l s='Creation Date' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$attr_val_tpl.created_on|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>

						<div class="row">
							<label class="control-label col-lg-3">{l s='Erp Attribute Id' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$attr_val_tpl.erp_attribute_id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Prestashop Attribute Id ' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$attr_val_tpl.prestashop_attribute_id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Erp Attribute Value Id ' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$attr_val_tpl.erp_attribute_value_id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Prestashop Attribute Value Id ' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$attr_val_tpl.prestashop_attribute_value_id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Created by' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$attr_val_tpl.created_by|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Status' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">
									{if $attr_val_tpl.is_synch}
										<span class="label label-success">
											<i class="icon-check"></i>
											{l s='Synchronized' mod='pob'}
										</span>
									{else}
										<span class="label label-danger">
											<i class="icon-remove"></i>
											{l s='Not Synchronized' mod='pob'}
										</span>
									{/if}
								</p>
							</div>
						</div>
					</div>
				{/if}
			</div>
		</div>
	</div>
</div>
			






{if isset($avg_rating)}
<script type="text/javascript">
	$('.avg_rating').raty(
	{
		path: '{$modules_dir|escape:'html':'UTF-8'}/marketplace/libs/rateit/lib/img',
		score: {$avg_rating|escape:'html':'UTF-8'},
		readOnly: true,
	});
</script>
{/if}