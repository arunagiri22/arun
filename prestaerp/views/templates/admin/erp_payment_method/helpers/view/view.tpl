<!-- {if isset($customer_id) && $id == 0}
<div class="alert alert-danger">
	<p>This seller has been removed by admin from prestashop</p>
</div>
{/if} -->

<div id="mp-container-customer">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel clearfix">
				{if isset($payment_method_tpl)}
					<div class="panel-heading">
						<i class="icon-money"></i>
						{$payment_method_tpl.id|escape:'htmlall':'UTF-8'} - 
						<p>Payment Method</p>
					</div>
					<div class="form-horizontal">
						<div class="row">
							<label class="control-label col-lg-3">{l s='ID' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$payment_method_tpl.id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>

						<div class="row">
							<label class="control-label col-lg-3">{l s='Creation Date' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$payment_method_tpl.created_on|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>

						<div class="row">
							<label class="control-label col-lg-3">{l s='Erp Payment Id' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$payment_method_tpl.erp_payment_id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Prestashop Payment Id' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$payment_method_tpl.prestashop_payment_id|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Name' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$payment_method_tpl.name|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-lg-3">{l s='Created by' mod='pob'} :</label>
							<div class="col-lg-9">
								<p class="form-control-static">{$payment_method_tpl.created_by|escape:'htmlall':'UTF-8'}</p>
							</div>
						</div>
					</div>
				{/if}
			</div>
		</div>
	</div>
</div>
			






{if isset($avg_rating)}
<script type="text/javascript">
	$('.avg_rating').raty(
	{
		path: '{$modules_dir|escape:'html':'UTF-8'}/marketplace/libs/rateit/lib/img',
		score: {$avg_rating|escape:'html':'UTF-8'},
		readOnly: true,
	});
</script>
{/if}