<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ErpCurrency extends ObjectModel{

    public $erp_currency_id;
    public $prestashop_currency_id;
    
    public static $definition = array(
        'table' => 'erp_currency_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_currency_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_currency_id' => array('type' => self::TYPE_INT,  'required' => true),
        ),
    );

    public function currency_details($id){
        $currency_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_currency_merge` where `id`=" . $id . "");
        if ($currency_info)
            return $currency_info;

        return false;
    }

    public function check_currency($id_currency){
        $check = Db::getInstance()->getRow("SELECT `erp_currency_id`  from `" . _DB_PREFIX_ . "erp_currency_merge` where `prestashop_currency_id`=" . $id_currency . "");
        if ($check['erp_currency_id'] > 0)
            return $check['erp_currency_id'];
        else
            return 0;
    }
    
    public function create_currency($iso_code, $currency_name, $userId, $client){
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("force.done", "string"));
        $msg_ser->addParam(new xmlrpcval("pricelist_currency", "string"));
        $msg_ser->addParam(new xmlrpcval('Pricelist_' . $currency_name, "string"));
        $msg_ser->addParam(new xmlrpcval($iso_code, "string"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()){
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        } 
        else{
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            return array(
                'erp_id' => $erp_id
            );
        }
    }

    public function addto_currency_merge($erp_currency_id, $currency_id, $presta_user = 'Front End'){
        $data = array(
            'erp_currency_id' => $erp_currency_id,
            'prestashop_currency_id' => $currency_id,
            'created_by' => $presta_user
        );
        Db::getInstance()->insert('erp_currency_merge', $data);
    }

    public function check_all_currencies($userId, $client, $presta_user){
        $is_error      = 0;
        $error_message = '';
        $ids           = '';
        $data          = Db::getInstance()->executeS("SELECT `id_currency`,`iso_code`,`name`  from `" . _DB_PREFIX_ . "currency` WHERE `id_currency` NOT IN (SELECT `prestashop_currency_id` FROM `" . _DB_PREFIX_ . "erp_currency_merge`)");

        if (count($data) == 0) {
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                '$ids' => $ids
            );
        }
        foreach ($data as $currency) {
            $erp_currency_id = $this->check_currency($currency['id_currency']);
            if ($erp_currency_id <= 0) {
                $create = $this->create_currency($currency['iso_code'], $currency['name'], $userId, $client);
                if ($create['erp_id'] > 0)
                    $this->addto_currency_merge($create['erp_id'], $currency['id_currency'], $presta_user);
                else{
                    $is_error = 1;
                    $error_message .= $create['error_message'] . ',';
                    $ids .= $currency['id_currency'] . ',';
                }
            }
        }
        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'value' => 1,
            'ids' => $ids
        );
    }

    public function check_specific_currency($currency_id, $userId, $client){
        $erp_currency_id = $this->check_currency($currency_id);
        if ($erp_currency_id > 0)
            return $erp_currency_id;
        else {
            $check  = Db::getInstance()->getRow("SELECT `iso_code`,`name`  from `" . _DB_PREFIX_ . "currency` where `id_currency`=" . $currency_id . "");
            $create = $this->create_currency($check['iso_code'], $check['name'], $userId, $client);
            if ($create['erp_id'] > 0)
                return $create['erp_id'];
            else
                return 1;
        }
    }
    
}