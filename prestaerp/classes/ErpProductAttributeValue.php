<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class ErpProductAttributeValue extends ObjectModel{
    
    public $erp_attribute_id;
    public $erp_attribute_value_id;
    public $prestashop_attribute_id;    
    public $prestashop_attribute_value_id;
    public $translate_state ;
    public $color;
    public $created_by ;
    public $is_synch ;
    
    public static $definition = array(
        'table' => 'erp_attribute_values_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_attribute_id' => array('type' => self::TYPE_INT, 'required' => true),
            'erp_attribute_value_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_attribute_id' => array('type' => self::TYPE_INT,  'required' => true),
            'prestashop_attribute_value_id' => array('type' => self::TYPE_INT,  'required' => true),
            'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'translate_state' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'color' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'is_synch' => array('type' => self::TYPE_BOOL),
        ),
    );

    public function map_attribute($ps_value_id,$erp_value_id,$ps_attribute_id,$erp_attr_id){
        if (!class_exists('xmlrpc_client'))
            include_once 'xmlrpc.inc';
        $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/object");
        $sock   = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/common");
        $msg    = new xmlrpcmsg('login');
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
        $resp = $sock->send($msg);       
        if (!$resp->faultCode()) {
            $userId = $resp->value()->scalarval();
            $arrayVal = array(
                'name' => new xmlrpcval($erp_value_id, "int"),
                'erp_id' => new xmlrpcval($erp_value_id, "string"),
                'presta_id' => new xmlrpcval($ps_value_id, "string"),
                'erp_attr_id' => new xmlrpcval($erp_attr_id, "string"),
                'presta_attr_id' => new xmlrpcval($ps_attribute_id, "string"),
                );
            $msg_ser  = new xmlrpcmsg('execute');
            $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
            $msg_ser->addParam(new xmlrpcval($userId, "int"));
            $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
            $msg_ser->addParam(new xmlrpcval('prestashop.product.attribute.value', "string"));
            $msg_ser->addParam(new xmlrpcval("create", "string"));
            $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
            $resp = $client->send($msg_ser);
            if ($resp->faultCode()) {
                $this->errors[] = Tools::displayError($this->l('Error in creating mapping on Odoo End'));
                $this->display  = 'add';
            }
        }
    }
    public function attribute_value_details($id){
        $attrribute_value_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_attribute_values_merge` where `id`=" . $id . "");
        if ($attrribute_value_info)
            return $attrribute_value_info;

        return false;
    }

    public function check_attribute_value($id_attribute_value){
        $check_erp_id = Db::getInstance()->getRow("SELECT `is_synch`,`erp_attribute_value_id` from `"._DB_PREFIX_."erp_attribute_values_merge` where `prestashop_attribute_value_id`=".$id_attribute_value."");
        if ($check_erp_id['erp_attribute_value_id'] > 0) {
            if ($check_erp_id['is_synch'] == 0)
                return array(
                    -1,
                    $check_erp_id['erp_attribute_value_id']
                );
            else
                return array(
                    $check_erp_id['erp_attribute_value_id']
                );
        } else
            return array(
                0
            );
    }
    
    public function create_attribute_value($name,$erp_attribute_id,$presta_attribute_id,$presta_attribute_value_id,$sequence,$userId, $client){
        $key     = array(
            'name' => new xmlrpcval(str_replace('+', ' ',urlencode($name)), "string"),
            'erp_attribute_id' => new xmlrpcval($erp_attribute_id, "int"),
            'presta_attribute_id' => new xmlrpcval($presta_attribute_id, "int"),
            'presta_attribute_value_id' => new xmlrpcval($presta_attribute_value_id, "int"),
            'sequence' => new xmlrpcval($sequence, "int")
        );
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("force.done", "string"));
        $msg_ser->addParam(new xmlrpcval("create_attribute_value", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        } else {
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            return array(
                'erp_id' => $erp_id
            );
        }
    }
    
    public function addto_attribute_value_merge($prestashop_attribute_value_id,$erp_attribute_value_id,$prestashop_attribute_id,$erp_attribute_id,$presta_user = 'Front End'){
        $data = array(
            'erp_attribute_value_id' => $erp_attribute_value_id,
            'prestashop_attribute_value_id' => $prestashop_attribute_value_id,
            'created_by' => $presta_user,
            'prestashop_attribute_id' => $prestashop_attribute_id,
            'erp_attribute_id' => $erp_attribute_id,
        );
        Db::getInstance()->insert('erp_attribute_values_merge', $data);        
    }    
    
    public function update_attribute_value($erp_attribute_value_id, $id_attribute_value,$erp_attribute_id,$name,$sequence, $userId, $client){
        $erp_attribute_value_list = array(
            new xmlrpcval($erp_attribute_value_id, 'int')
        );
        $key     = array(
            'name' => new xmlrpcval(str_replace('+', ' ',urlencode($name)), "string"),
            'attribute_id' => new xmlrpcval($erp_attribute_id, "string"),
            'sequence' => new xmlrpcval($sequence, "int")
        );
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("product.attribute.value", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_attribute_value_list, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } else {
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_attribute_values_merge` SET `is_synch`=1 where `prestashop_attribute_value_id`=" . $id_attribute_value . "");
            return array(
                'value' => True
            );
        }
    }
    
    public function addto_openerp_merge($erp_id, $presta_id,$object,$userId, $client){
        $arrayVal = array(
            'name' => new xmlrpcval($erp_id, "int"),
            'erp_id' => new xmlrpcval($erp_id, "string"),
            'presta_id' => new xmlrpcval($presta_id, "string")
        );
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval($object, "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'is_error' => 1
            );
        } else {
            return array(
                'is_error' => 0
            );
        }
    }
        
    public function check_all_attribute_values($userId, $client, $presta_user){
        $is_error       = 0;
        $error_message  = '';
        $ids            = '';
        $count          = 0;
        $data = AttributeGroup::getAttributesGroups(Configuration::get('PS_LANG_DEFAULT'));
        $attributes      = new ErpProductAttribute();    
        foreach ($data as $attr_data){
            $erp_attribute_id = $attributes->check_specific_attribute($userId, $client,$attr_data['id_attribute_group'], $presta_user);
            if ($erp_attribute_id){
                $data_attr_value = AttributeGroup::getAttributes(Configuration::get('PS_LANG_DEFAULT'),$attr_data['id_attribute_group']);
                foreach ($data_attr_value as $attr_value_data){
                        $erp_attribute_value_id = $this->check_attribute_value($attr_value_data['id_attribute']);
                        if ($erp_attribute_value_id[0] == 0) {$count = $count+1;
                                $name = $attr_value_data['name'];
                                $sequence = $attr_value_data['position'];
                         
                            $create = $this->create_attribute_value($name,$erp_attribute_id,$attr_data['id_attribute_group'],$attr_value_data['id_attribute'],$sequence,$userId, $client);
                            if ($create['erp_id'] > 0){
                                $this->addto_attribute_value_merge($attr_value_data['id_attribute'], $create['erp_id'],$attr_value_data['id_attribute_group'],$erp_attribute_id,$presta_user);
                            }
                            else {
                                $is_error = 1;
                                $error_message .= $create['error_message'] . ',';
                                $ids .= $attr_value_data['id_attribute'] . ',';
                            }
                         
                         }
                        if ($erp_attribute_value_id[0] < 0) {
                            $count = $count+1;
                            $name = $attr_value_data['name'];
                            $sequence = $attr_value_data['position'];
                            $this->update_attribute_value($erp_attribute_value_id[1],$attr_value_data['id_attribute'],$erp_attribute_id,$name,$sequence,$userId, $client);
                        }
                    }
            }
        
        }
        if ($count == 0) {
            //Nothing to export
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                '$ids' => $ids
            );
        }
        else
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 1,
                'ids' => $ids
            );
    }

    public function check_specific_attribute_value($userId, $client, $presta_attribute_id,$presta_attribute_value_id, $presta_user){
        $attributes      = new ErpProductAttribute();    
        $erp_attribute_id = $attributes->check_specific_attribute($userId, $client,$presta_attribute_id, $presta_user);
        $check = $this->check_attribute_value($presta_attribute_value_id);
        if ($check[0] <=0){
            if ($erp_attribute_id){
                $data_attr_value = AttributeGroup::getAttributes(Configuration::get('PS_LANG_DEFAULT'),$presta_attribute_id);
                foreach ($data_attr_value as $attr_value_data){                        
                        $erp_attribute_value_id = $this->check_attribute_value($attr_value_data['id_attribute']);
                        $name = $attr_value_data['name'];
                        $sequence = $attr_value_data['position'];
                        if ($erp_attribute_value_id[0] == 0) {                 
                            $create = $this->create_attribute_value($name,$erp_attribute_id,$attr_value_data['id_attribute_group'],$attr_value_data['id_attribute'],$sequence,$userId, $client);
                            if ($create['erp_id'] > 0){
                                $erp_value_id =$create['erp_id'];
                                $this->addto_attribute_value_merge($attr_value_data['id_attribute'], $create['erp_id'],$attr_value_data['id_attribute_group'],$erp_attribute_id,$presta_user);
                                
                            }
                            else
                                $erp_value_id = -1;
                         }
                        if ($erp_attribute_value_id[0] < 0) {$count = $count+1;
                            $name = $attr_value_data['name'];
                            $sequence = $attr_value_data['position'];
                            $erp_value_id = $erp_attribute_value_id[0];
                            $this->update_attribute_value($erp_attribute_value_id[1],$attr_value_data['id_attribute'],$erp_attribute_id,$name,$sequence,$userId, $client);                           
                        }
                    }
                 }
                 else
                    $erp_value_id = -1;
            }
            else
                $erp_value_id = $check[0];
            return array('erp_attribute_id'=>(string)$erp_attribute_id,'erp_attr_value_id'=>$erp_value_id);
    }
    
}