<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class ErpProductCategory extends ObjectModel{

    public $erp_category_id;
    public $prestashop_category_id;
    public $created_by ;
    public $is_synch ;
    public $translate_state ;
    public $color;
    
    public static $definition = array(
        'table' => 'erp_category_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_category_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_category_id' => array('type' => self::TYPE_INT,  'required' => true),
            'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'translate_state' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'color' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'is_synch' => array('type' => self::TYPE_BOOL),
        ),
    );

    public function map_cat($erp_cat_id, $prestashop_cat_id, $presta_user){
        $log = new pob_log();
        if (!class_exists('xmlrpc_client'))
            include_once 'xmlrpc.inc';
        $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/object");
        $sock   = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/common");
        $msg    = new xmlrpcmsg('login');
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
        $resp = $sock->send($msg);
        if ($resp->faultCode()) {
                $error_message  = $resp->faultString();             
                $log->logMessage(__FILE__,__LINE__,'Connection Error : Invalid Odoo Information.','ERROR');
                $this->errors[] = Tools::displayError($this->l('Connection Error : Invalid Odoo Information.' . $error_message));
            } else {
                $userId = $resp->value()->scalarval();
                if ($userId <= 0){              
                $log->logMessage(__FILE__,__LINE__,'Invalid UserName or Password. Please check the Odoo configuration.','ERROR');
                $this->errors[] = Tools::displayError($this->l('Invalid UserName or Password. Please check the Odoo configuration.'));}
                else {
                    $presta_user = $this->context->employee->firstname . ' ' . $this->context->employee->lastname . ' (' . $this->context->employee->email . ')';
                    $this->addto_category_merge($erp_cat_id, $prestashop_cat_id, $presta_user, $userId, $client);
                    }
            }
    }

    public function get_cat_info($presta_id){
        $db_object  = Db::getInstance()->getRow("SELECT `id_parent`,`id_shop_default` from `" . _DB_PREFIX_ . "category` where `id_category`=" . $presta_id . "");
        if ($db_object)
            $db_object1 = Db::getInstance()->getRow("SELECT `name` from `" . _DB_PREFIX_ . "category_lang` where `id_category`=".$presta_id ." and `id_shop`=".$db_object['id_shop_default']."");
        else
            $db_object1 = Db::getInstance()->getRow("SELECT `name` from `" . _DB_PREFIX_ . "category_lang` where `id_category`=".$presta_id ."");
        return array(
            'id_parent' => $db_object['id_parent'],
            'name' => $db_object1['name']
        );
    }

    public function check_home(){
        $home_id = Db::getInstance()->getRow("SELECT `erp_category_id` from `" . _DB_PREFIX_ . "erp_category_merge` where `prestashop_category_id`=2");
        if ($home_id['erp_category_id'] > 0)
            return $home_id['erp_category_id'];
        else
            return false;
    }

    public function cat_details($id){
        $cat_info =  Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_category_merge` where `id` = " . $id . "");
        if ($cat_info)
            return $cat_info;

        return false;
    }

    public function check_category($prestashop_id){
        $categ_obj = Db::getInstance()->getRow("SELECT `erp_category_id`,`is_synch`  from `" . _DB_PREFIX_ . "erp_category_merge` where `prestashop_category_id` = " . $prestashop_id . "");
        if ($categ_obj['erp_category_id'] > 0) {
            if ($categ_obj['is_synch'] == 0)
                return array(
                    -1,
                    $categ_obj['erp_category_id']
                );
            else
                return array(
                    $categ_obj['erp_category_id']
                );
        } else
            return array(
                0
            );
    }

    private function create_category($presta_id, $id_parent, $name, $userId, $client, $presta_user){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string")
        );
        $arrayVal = array(
            'name' => new xmlrpcval($name, "string"),
            'type' => new xmlrpcval("normal", "string"),
            'parent_id' => new xmlrpcval($id_parent, "int")
        );
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("product.category", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if (!$resp->faultCode()) {
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            $this->addto_category_merge($erp_id, $presta_id, $presta_user, $userId, $client);
            return array(
                'erp_id' => $erp_id
            );
        }
        else{
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        }
    }

    public function update_category($presta_id, $id_parent, $name, $erp_id, $userId, $client){
       $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string")
        );
        $erp_cat_id = array(
            new xmlrpcval($erp_id, 'int')
        );
        $arrayVal   = array(
            'name' => new xmlrpcval($name, "string"),
            'type' => new xmlrpcval("normal", "string"),
            'parent_id' => new xmlrpcval($id_parent, "int")
        );
        $msg_ser    = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("product.category", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_cat_id, "array"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if (!$resp->faultCode()) {
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_category_merge` set `is_synch`=1 where `prestashop_category_id`=" . $presta_id . "");
        }
        else{
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            }
    }

    public function addto_category_merge($erp_id, $presta_id, $presta_user, $userId, $client){
        $data = array(
            'erp_category_id' => $erp_id,
            'prestashop_category_id' => $presta_id,
            'created_by' => $presta_user
        );
        Db::getInstance()->insert('erp_category_merge', $data);
        $this->addto_openerp_merge($erp_id, $presta_id, $userId, $client);
    }

    public function addto_openerp_merge($erp_id, $presta_id, $userId, $client){
        $arrayVal = array(
            'category_name' => new xmlrpcval($erp_id, "int"),
            'erp_category_id' => new xmlrpcval($erp_id, "int"),
            'presta_category_id' => new xmlrpcval($presta_id, "int")
        );
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("prestashop.category", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'is_error' => 1
            );
        } else {
            return array(
                'is_error' => 0
            );
        }
    }
  
    public function sync_categories($presta_id, $erp_home_id, $userId, $client, $presta_user = 'Front End'){
        $check_obj = $this->check_category($presta_id);
        if ($check_obj[0] == 0) {
            $db_object          = $this->get_cat_info($presta_id);
            $name               = urlencode($db_object['name']);
            $presta_parent_id = $db_object['id_parent'];
            if ($presta_parent_id > 2)
                $erp_parent_id = $this->sync_categories($presta_parent_id, $erp_home_id, $userId, $client, $presta_user);
            else {
                $erp_id = $this->create_category($presta_id, $erp_home_id, $name, $userId, $client, $presta_user);
                return $erp_id['erp_id'];
            }
            $erp_id = $this->create_category($presta_id, $erp_parent_id, $name, $userId, $client, $presta_user);
            return $erp_id['erp_id'];
        } elseif ($check_obj[0] == -1) {
            $db_object           = $this->get_cat_info($presta_id);
            $name                = urlencode($db_object['name']);
            $presta_parent_id = $db_object['id_parent'];
            if ($presta_parent_id > 2)
                $erp_parent_id = $this->sync_categories($presta_parent_id, $erp_home_id, $userId, $client, $presta_user);
            else
                $erp_parent_id = false;
            $this->update_category($presta_id, $erp_parent_id, $name, $check_obj[1], $userId, $client);
            return $check_obj[1];
        } else
            return $check_obj[0];
    }

    public function check_all_categories($userId, $client, $presta_user){
        $data        = Db::getInstance()->executeS("SELECT `id_category` FROM `" . _DB_PREFIX_ . "category`  WHERE `id_category` NOT IN (SELECT `prestashop_category_id` FROM `" . _DB_PREFIX_ . "erp_category_merge` WHERE `is_synch` = 1)");
        $erp_home_id = $this->check_home();
        foreach ($data as $cat_id) {
            if (!in_array($cat_id['id_category'], array(
                1,
                2
            )))
            $this->sync_categories($cat_id['id_category'], $erp_home_id, $userId, $client, $presta_user);
        }
        return 1;
    }

    public function check_specific_category($presta_id, $userId, $client, $presta_user='Front End'){
        $erp_home_id = $this->check_home();
        $erp_id      = $this->sync_categories($presta_id, $erp_home_id, $userId, $client, $presta_user);
        return $erp_id;
    }
    
}