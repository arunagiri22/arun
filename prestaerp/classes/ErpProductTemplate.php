<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class ErpProductTemplate extends ObjectModel{

    public $presta_product_id;
    public $erp_template_id;
    public $translate_state ;
    public $color;
    public $created_by ;
    public $is_synch ;
    
    public static $definition = array(
        'table' => 'erp_product_template_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_template_id' => array('type' => self::TYPE_INT, 'required' => true),
            'presta_product_id' => array('type' => self::TYPE_INT,  'required' => true),
            'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'translate_state' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'color' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'is_synch' => array('type' => self::TYPE_BOOL),
        ),
    );

    public function check_template($ps_product_id){
        $check_erp_id = Db::getInstance()->getRow("SELECT `is_synch`,`erp_template_id` from `" . _DB_PREFIX_ . "erp_product_template_merge` where `presta_product_id`=" . $ps_product_id . "");
        if ($check_erp_id['erp_template_id'] > 0) {
            if ($check_erp_id['is_synch'] == 0)
                return array(
                    -1,
                    $check_erp_id['erp_template_id']
                );
            else
                return array(
                    $check_erp_id['erp_template_id']
                );
        } else{
            return array(
                0
            );
        }
    }
    public function product_details($id){
        $product_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_product_template_merge` where `id`=" . $id . "");
        if ($product_info)
            return $product_info;

        return false;
    }



    public function create_extra_category($presta_product_id, $userId, $client){
        $category      = new ErpProductCategory();
        $category_product_data          = Db::getInstance()->executeS("SELECT `id_category` FROM "._DB_PREFIX_."category_product WHERE `id_product` = ".$presta_product_id."");
        $categ_id = array();
        foreach ($category_product_data as $data){
            $erp_category_id    = $category->check_specific_category($data['id_category'], $userId, $client);
            array_push($categ_id,new xmlrpcval($erp_category_id,"int"));
        }
        if (empty($categ_id))
            return False;
        $categ_ids = new xmlrpcval(array(new xmlrpcval(array(
                                    new xmlrpcval(6,"int"),
                                    new xmlrpcval(0,"int"),
                                    new xmlrpcval(
                                        $categ_id,"array")
                                ), "array")),"array");
        return $categ_ids;
    }



    public function create_template($userId, $client, $name, $description, $description_short, $erp_category_id, $base_price, $wholesale_price, $weight, $image, $ps_product_id, $reference, $presta_user, $ean13, $categ_ids=False, $type = "product"){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string"),
            'presta_id' => new xmlrpcval($ps_product_id, "string"),
        );
        $arrayVal = array(
            'name' => new xmlrpcval($name, "string"),
            'description' => new xmlrpcval($description, "string"),
            'description_sale' => new xmlrpcval($description_short, "string"),
            'type' => new xmlrpcval($type, "string"),
            'categ_id' => new xmlrpcval($erp_category_id, "string"),
            'list_price' => new xmlrpcval($base_price, "double"),
            'standard_price' => new xmlrpcval($wholesale_price, "double"),
            'weight' => new xmlrpcval($weight, "double"),
            'weight_net' => new xmlrpcval($weight, "double"),
            'default_code' => new xmlrpcval($reference, "string"),
            );
        if ($ean13 != ''){
            $arrayVal['ean13'] = new xmlrpcval($ean13, "string");
        }
        if ($image){
            $arrayVal['image'] = new xmlrpcval($image, "string");
        }
        if ($categ_ids){
            $arrayVal['extra_categ_ids'] = $categ_ids;
        }

        $msg_server  = new xmlrpcmsg('execute');
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_server->addParam(new xmlrpcval($userId, "int"));
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_server->addParam(new xmlrpcval("product.template", "string"));
        $msg_server->addParam(new xmlrpcval("create", "string"));
        $msg_server->addParam(new xmlrpcval($arrayVal, "struct"));
        $msg_server->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_server);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'erp_id' => -1,
                'product_id' => -1
            );
        } else {
            $temp_dic = $resp->value()->me['struct'];
            $erp_template_id = $temp_dic['template_id']->me['int'];
            $erp_product_id = $temp_dic['product_id']->me['int'];
            $this->addto_ps_merge($erp_template_id, $ps_product_id, $erp_product_id, $presta_user);          
            return array(
                'erp_id' => $erp_template_id,
                'product_id' => $erp_product_id
            );
        }
    }

    public function update_template($userId, $client, $name, $description, $description_short, $erp_category_id, $base_price, $wholesale_price, $weight, $image, $ps_product_id, $erp_template_id, $reference, $ean13, $categ_ids=False, $type = "product"){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string"),
            'presta_id' => new xmlrpcval($ps_product_id, "string")
        );
        $erp_template_key = array(
            new xmlrpcval($erp_template_id, 'int')
        );

        $arrayVal = array(
            'name' => new xmlrpcval($name, "string"),
            'description' => new xmlrpcval($description, "string"),
            'description_sale' => new xmlrpcval($description_short, "string"),
            'type' => new xmlrpcval($type, "string"),
            'categ_id' => new xmlrpcval($erp_category_id, "string"),
            'list_price' => new xmlrpcval($base_price, "double"),
            'standard_price' => new xmlrpcval($wholesale_price, "double"),
            'weight' => new xmlrpcval($weight, "double"),
            'weight_net' => new xmlrpcval($weight, "double"),
            'image' => new xmlrpcval($image, "string"),
            'default_code' => new xmlrpcval($reference, "string"),
            
            );
        if ($ean13 != ''){
            $arrayVal['ean13'] = new xmlrpcval($ean13, "string");
        }
        if ($categ_ids){
            $arrayVal['extra_categ_ids'] = $categ_ids;
        }

        $msg_server  = new xmlrpcmsg('execute');
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_server->addParam(new xmlrpcval($userId, "int"));
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_server->addParam(new xmlrpcval("product.template", "string"));
        $msg_server->addParam(new xmlrpcval("write", "string"));
        $msg_server->addParam(new xmlrpcval($erp_template_key, "array"));
        $msg_server->addParam(new xmlrpcval($arrayVal, "struct"));
        $msg_server->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_server);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'erp_id' => $erp_template_id,
                'is_error'=>True
            );
        } else {
             Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_product_template_merge` set `is_synch`=1 where `erp_template_id`=" . $erp_template_id . "");          
            return array(
                'erp_id' => $erp_template_id,
                'is_error'=>False
            );
        }
    }

    public function addto_ps_merge($erp_template_id, $ps_product_id, $erp_product_id, $presta_user = 'Front End'){
        $data = array(
            'erp_template_id' => $erp_template_id,
            'presta_product_id' => $ps_product_id,
            'created_by' => $presta_user,
        );
        Db::getInstance()->insert('erp_product_template_merge', $data);
        $data = array(
            'erp_product_id' => $erp_product_id,
            'prestashop_product_id' => $ps_product_id,
            'prestashop_product_attribute_id' => 0,
            'erp_template_id' => $erp_template_id,
            'created_by' => $presta_user,
        );
        Db::getInstance()->insert('erp_product_merge', $data);
    }

    public function validate_ean13($barcode){
        // check to see if barcode is 13 digits long
        if (!preg_match("/^[0-9]{13}$/", $barcode)) {
            return '';
        }

        $digits = $barcode;

        // 1. Add the values of the digits in the 
        // even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits[1] + $digits[3] + $digits[5] +
                    $digits[7] + $digits[9] + $digits[11];

        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;

        // 3. Add the values of the digits in the 
        // odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits[0] + $digits[2] + $digits[4] +
                   $digits[6] + $digits[8] + $digits[10];

        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;

        // 5. The check character is the smallest number which,
        // when added to the result in step 4, produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;

        // if the check digit and the last digit of the 
        // barcode are OK return true;
        if ($check_digit == $digits[12]) {
            return $barcode;
        }

        return '';
    }

    public function check_specific_template($ps_product_id, $userId, $client, $presta_user='Front End', $id_product_attribute=-1){
        $erp = $this->check_template($ps_product_id);
        if ($erp[0] <= 0){
            $id_lang       = (int)Configuration::get('PS_LANG_DEFAULT');
            $category      = new ErpProductCategory();
            $product_data          = Db::getInstance()->getRow("SELECT p.`reference`,p.`id_product`,p.`id_category_default`,p.`ean13`,p.`price`,p.`wholesale_price`,p.`weight`,pl.`name`,pl.`description`,pl.`link_rewrite`,pl.`description_short`
                FROM "._DB_PREFIX_."product  p 
                LEFT JOIN `"._DB_PREFIX_."product_lang` pl ON (pl.`id_product` = p.`id_product` and pl.id_shop=p.id_shop_default and pl.id_lang=".$id_lang.")
                WHERE p.`id_product`=".$ps_product_id."");

            $erp_category_id    =   $category->check_specific_category($product_data['id_category_default'], $userId, $client, $presta_user);
            $name               =   urlencode($product_data['name']);
            $description        =   urlencode($product_data['description']);
            $description_short  =   urlencode($product_data['description_short']);
            $base_price         =   $product_data['price'];
            $wholesale_price    =   $product_data['wholesale_price'];
            $weight             =   $product_data['weight'];
            $ean13		        =   $this->validate_ean13($product_data['ean13']);
            $image              =   $this->get_image_link($product_data['id_product'], $product_data['link_rewrite']);
            $categ_ids = $this->create_extra_category($product_data['id_product'], $userId, $client);

            if ($erp[0] == 0) {
                $create = $this->create_template($userId, $client, $name, $description, $description_short, $erp_category_id, $base_price, $wholesale_price, $weight, $image, $product_data['id_product'], $product_data['reference'], $presta_user, $ean13, $categ_ids);
                $erp_template_id=$create['erp_id'];
                $erp_product_id = $create['product_id'];
                if ($id_product_attribute == 0){
                    $quantity =  StockAvailable::getQuantityAvailableByProduct($ps_product_id);
                    $this->create_product_quantity($erp_product_id, $quantity, $userId, $client);
                    }
                }
                if ($erp[0] < 0) {
                    $erp_template_id = $erp[1];
                    $this->update_template($userId, $client, $name, $description, $description_short, $erp_category_id, $base_price, $wholesale_price, $weight, $image, $product_data['id_product'], $erp_template_id, $product_data['reference'], $ean13,$categ_ids);

                }
            }
        else
            $erp_template_id = $erp[0];
        return $erp_template_id;
    }
    

    public function get_image_link($id_product, $link_rewrite,$id_image=False){ 
        $protocol = Tools::getCurrentUrlProtocolPrefix();
        if (!$id_image){
            $prd_img   = Db::getInstance()->getRow("SELECT `id_image` from `" . _DB_PREFIX_ . "image` where `id_product`=" . $id_product . " and `cover`=1");
            $id_image=$prd_img['id_image'];
        }
        $link      = new LinkCore();
        $image_url = $protocol . $link->getImageLink($link_rewrite, $id_image, 'home_default');
        if (Tools::file_get_contents($image_url)) {
            $content   = Tools::file_get_contents($image_url);
            $imageData = base64_encode($content);
        } else {
        $image_url = $protocol . $link->getImageLink($link_rewrite, $id_product.'-'.$id_image, 'home_default');
        if (Tools::file_get_contents($image_url)) {
            $content   = Tools::file_get_contents($image_url);
            $imageData = base64_encode($content);
        } else {
            return false;
            }
        }
        return $imageData;
    }


    public function create_product_quantity($erp_product_id, $product_quantity, $userId, $client){           
        $arrayVal = array(
            'product_id' => new xmlrpcval($erp_product_id, "int"),
            'new_quantity' => new xmlrpcval($product_quantity, "string"),
            'erp_instance_id' => new xmlrpcval(Configuration::getGlobalValue("erp_instance_conf_id"), "int"),
            );
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("force.done", "string"));
        $msg_ser->addParam(new xmlrpcval("update_quantity", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $resp = $client->send($msg_ser); 
        if ($resp->faultCode()){
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return;
        }
        else{
            return;
        }    
        
    }
    
}