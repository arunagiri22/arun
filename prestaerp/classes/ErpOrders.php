<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ErpOrders extends ObjectModel
{
   public $erp_order_id;
	public $prst_order_id;
	public $erp_invoice_id ;
	public $erp_order_name ;
 	
	public static $definition = array(
		'table' => 'erp_order_merge',
		'primary' => 'id',
		'fields' => array(
			'erp_order_id' => array('type' => self::TYPE_INT, 'required' => true),
			'prst_order_id' => array('type' => self::TYPE_INT,  'required' => true),
			'erp_invoice_id' => array('type' => self::TYPE_INT,  'required' => false),
			'erp_order_name' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
		),
	);

	public function order_details($id){
        $order_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_order_merge` where `id`=" . $id . "");
        if ($order_info)
            return $order_info;

        return false;
    }
    
}