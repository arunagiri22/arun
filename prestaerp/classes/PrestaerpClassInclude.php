<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


	include_once 'ErpCarrier.php';
	include_once 'WkErpConfiguration.php';
	include_once 'ErpCurrency.php';
    include_once 'ErpShop.php';
	include_once 'ErpCustomer.php';
	include_once 'ErpCustomerAddress.php';
	include_once 'ErpOrders.php';
	include_once 'ErpPaymentMethod.php';
	include_once 'ErpProductAttribute.php';
	include_once 'ErpProductAttributeValue.php';
	include_once 'ErpProductCategory.php';
	include_once 'ErpProductCombination.php';
	include_once 'ErpProductTemplate.php';
	include_once 'ErpTaxes.php';
	include_once 'ErpCountry.php';
	include_once 'ErpState.php';
	include_once 'pob_log.php';
	include_once 'xmlrpc.inc';

?>
