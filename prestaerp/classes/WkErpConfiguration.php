<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class WkErpConfiguration extends ObjectModel{

    public static $definition = array(
        'table' => 'configuration',
        'primary' => 'id_configuration'
    );	
	
    public function AllowChanges(){
        if (Configuration::getGlobalValue('ErpUrl') || Configuration::getGlobalValue('ErpPort') || Configuration::getGlobalValue('ErpDatabase')){
            if ((Configuration::getGlobalValue('ErpUrl')!=Tools::getValue('ErpUrl')) || (Configuration::getGlobalValue('ErpPort')!=Tools::getValue('ErpPort')) || (Configuration::getGlobalValue('ErpDatabase')!=Tools::getValue('ErpDatabase'))){
                $chk_customer_merge=Db::getInstance()->getRow("select * from `"._DB_PREFIX_."erp_customer_merge`");
                $chk_address_merge=Db::getInstance()->getRow("select * from `"._DB_PREFIX_."erp_address_merge`");
                $chk_product_merge=Db::getInstance()->getRow("select * from `"._DB_PREFIX_."erp_product_merge`");
                $chk_carrier_merge=Db::getInstance()->getRow("select * from `"._DB_PREFIX_."erp_carrier_merge`");
                $chk_order_merge=Db::getInstance()->getRow("select * from `"._DB_PREFIX_."erp_order_merge`");
                if ($chk_customer_merge>0 || $chk_address_merge>0 || $chk_product_merge>0|| $chk_carrier_merge>0|| $chk_order_merge>0){
                    $log = new pob_log();
                    $log->logMessage(__FILE__,__LINE__,'OpenERP Credentials Has been updated.','WARN');
                    return True;
                }
                else
                    return True;
            }
            else
                return True;
        }
        else
            return True;
    }
    
    public function CheckOrderInvoice(){
        $flag=True;
        $check_order_confirm = Configuration::getGlobalValue('ErpConfirmOrder');
        if (!$check_order_confirm){
            $flag=False;
            Configuration::updateGlobalValue('ErpAutoInvoice',False);            
        } 
        return $flag;  
    }    
    
    public function CheckMultiShop(){
        $flag=True;
        $check_presta_end = Db::getInstance()->getRow("select `id_module` from `"._DB_PREFIX_."module` where `name`='prestaerpmultishop' and `active`=1");
        if (!$check_presta_end){
            $flag=False;
            Configuration::updateGlobalValue('ErpMultiShop',False);            
        }
        $check_data=$this->CheckErpEnd('prestashop_bridge_multishop');
        if($check_data['is_error']){
            $flag=False;
            Configuration::updateGlobalValue('ErpMultiShop',False);  
            $this->errors[] = Tools::displayError($this->l("Login Error: Invalid OpenERP Information."));   
        }
        elseif(!$check_data['is_error'] and !$check_data['is_installed']){
            $flag=False;
            Configuration::updateGlobalValue('ErpMultiShop',False);
              
        }
        return $flag;    
    }

    public function CheckErpEnd($module) {   
        if (!class_exists('xmlrpc_client'))
            include_once 'xmlrpc.inc';
        $sock = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl").":".Configuration::getGlobalValue("ErpPort")."/xmlrpc/common"); 
        $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl").":".Configuration::getGlobalValue("ErpPort")."/xmlrpc/object");
        $msg = new xmlrpcmsg('login');
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
        $response =  $sock->send($msg);
        if (!$response->faultCode()){
            $condition=array(new xmlrpcval( 
                                array(
                                        new xmlrpcval('state', "string"),
                                        new xmlrpcval('=', "string"),
                                        new xmlrpcval('installed', "string")),
                                    "array"),
                            new xmlrpcval(
                                array(
                                        new xmlrpcval('name', "string"),
                                        new xmlrpcval('=', "string"),
                                        new xmlrpcval($module, "string")),
                                    "array")
            );
            $userId = $response->value()->scalarval();
            $msg1   = new xmlrpcmsg('execute');
            $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
            $msg1->addParam(new xmlrpcval($userId, "int"));
            $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
            $msg1->addParam(new xmlrpcval("ir.module.module", "string"));
            $msg1->addParam(new xmlrpcval("search", "string"));
            $msg1->addParam(new xmlrpcval($condition,"array"));
            $response1  = $client->send($msg1);
            if (!$response1->faultCode()){
                $value = $response1->value()->scalarval();
                if ($value)
                    return array('is_error'=>False,'is_installed'=>True);
                else
                    return array('is_error'=>False,'is_installed'=>False);
                        
            }
            else{
                $error_message = $response1->faultString();
                return array('is_error'=>True,'error_message'=>$error_message);
            }
        }
        else{
            $error_message = $response->faultString();
            return array('is_error'=>True,'error_message'=>$error_message);
        }
    }
    
    public function CheckMultiLanguage(){
        $flag=True;
        $check_presta_end = Db::getInstance()->getRow("select `id_module` from `"._DB_PREFIX_."module` where `name`='prestaerpmultilang' and `active`=1");
        if (!$check_presta_end){
            $flag=False;
            Configuration::updateGlobalValue('ErpMultiLang',False);            
        }
        $check_data=$this->CheckErpEnd('pob_extension_multilang');
        if($check_data['is_error']){
            $flag=False;
            Configuration::updateGlobalValue('ErpMultiLang',False);    
            $this->errors[] = Tools::displayError($this->l("Login Error: Invalid OpenERP Information."));   
        }
        elseif(!$check_data['is_error'] and !$check_data['is_installed']){
            $flag=False;
            Configuration::updateGlobalValue('ErpMultiLang',False);
              
        }
        return $flag;    
    }    
    
    public function TestConnection(){   
        if (Configuration::getGlobalValue('ErpUrl') && Configuration::getGlobalValue('ErpPort') && Configuration::getGlobalValue('ErpDatabase') && Configuration::getGlobalValue('ErpUsername') && Configuration::getGlobalValue('ErpPassword')){                   
            $sock = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl").":".Configuration::getGlobalValue("ErpPort")."/xmlrpc/common"); 
            $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl").":".Configuration::getGlobalValue("ErpPort")."/xmlrpc/object");
            $msg = new xmlrpcmsg('login');
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
            $response =  $sock->send($msg);             
            if (!$response->faultCode()){
                $userId = $response->value()->scalarval();
                if (!$userId){
                    return array(
                                'status' => false,
                                'message' => "Invalid Credentials for Connection!!!"
                            );
                }
                
                $condition=array(new xmlrpcval('lang', "string"));
                
                $msg1   = new xmlrpcmsg('execute');
                $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                $msg1->addParam(new xmlrpcval($userId, "int"));
                $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                $msg1->addParam(new xmlrpcval("res.users", "string"));
                $msg1->addParam(new xmlrpcval("read", "string"));
                $msg1->addParam(new xmlrpcval($userId,"int"));
                $msg1->addParam(new xmlrpcval($condition,"array"));
                $response1  = $client->send($msg1);                 
                if (!$response1->faultCode()){                      
                    $value = $response1->value()->scalarval();
                    $lang_code=$value['lang']->me['string'];                        
                    $condition1=array(new xmlrpcval(    
                            array(
                                    new xmlrpcval('code', "string"),
                                    new xmlrpcval('=', "string"),
                                    new xmlrpcval($lang_code, "string")),
                                "array"),
                            );
                    $msg2   = new xmlrpcmsg('execute');
                    $msg2->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                    $msg2->addParam(new xmlrpcval($userId, "int"));
                    $msg2->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                    $msg2->addParam(new xmlrpcval("res.lang", "string"));
                    $msg2->addParam(new xmlrpcval("search", "string"));
                    $msg2->addParam(new xmlrpcval($condition1,"array"));
                    $response2  = $client->send($msg2);                     
                    if (!$response2->faultCode()){
                        $value = $response2->value()->scalarval();
                        $lang_id=$value[0]->me['int'];                      
                    
                        $msg3   = new xmlrpcmsg('execute');
                        $msg3->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                        $msg3->addParam(new xmlrpcval($userId, "int"));
                        $msg3->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                        $msg3->addParam(new xmlrpcval("res.lang", "string"));
                        $msg3->addParam(new xmlrpcval("name_get", "string"));
                        $msg3->addParam(new xmlrpcval($lang_id,"int"));
                        $response3  = $client->send($msg3);
                        if (!$response3->faultCode()){
                            $value = $response3->value()->scalarval();
                            $lang_name=$value[0]->me['array'][1]->me['string'];
                            return array(
                                'status' => true,
                                'message' => "Successfully connected.(Default Language: ".$lang_name." )"
                            );
                        }
                        else{
                            $error_message = $response3->faultString();
                            $log = new pob_log();
                            $log->logMessage(__FILE__,__LINE__,$response3->raw_data);
                            return array(
                                'status' => true,
                                'message' => "Connected but with Error(You can ignore and continue)"
                            );
                        }
                    }
                    else{
                        $error_message = $response2->faultString();
                        $log = new pob_log();
                        $log->logMessage(__FILE__,__LINE__,$response2->raw_data);
                        return array(
                                'status' => true,
                                'message' => "Connected but with Error(You can ignore and continue)"
                            );
                        }
                }
                else{
                    $error_message = $response1->faultString();
                    $log = new pob_log();
                    $log->logMessage(__FILE__,__LINE__,$response1->raw_data);
                    return $error_message;
                }
            }
            else{
                $error_message = $response->faultString();
                $log = new pob_log();
                $log->logMessage(__FILE__,__LINE__,$response->raw_data);
                return array(
                    'status' => false,
                    'message' => 'Error in Connection with Odoo'
                );
            }
        }
    }
    
    public function UpdateConfig(){
       if (!Tools::getValue('ErpUrl') || !Tools::getValue('ErpPort') || !Tools::getValue('ErpDatabase') || !Tools::getValue('ErpUsername') || !Tools::getValue('ErpPassword')){
            return False;
       }
       else{
            $Url      = Tools::getValue('ErpUrl');
            $Port     = Tools::getValue('ErpPort');
            $Database = Tools::getValue('ErpDatabase');
            $Username = Tools::getValue('ErpUsername');
            $Password = Tools::getValue('ErpPassword');
            $DropT    = Tools::getValue('ErpDropTables');
            $MultiShop= Tools::getValue('ErpMultiShop');
            $MultiLang= Tools::getValue('ErpMultiLang');
            $Confirm_order= Tools::getValue('ErpConfirmOrder');
            $Auto_gen_inv= Tools::getValue('ErpAutoInvoice');
            $Auto_upd_status= Tools::getValue('ErpAutoStatusUpdate');
            
            Configuration::updateGlobalValue('ErpUrl', $Url);
            Configuration::updateGlobalValue('ErpPort', $Port);
            Configuration::updateGlobalValue('ErpDatabase', $Database);
            Configuration::updateGlobalValue('ErpUsername', $Username);
            Configuration::updateGlobalValue('ErpPassword', $Password);
            Configuration::updateGlobalValue('ErpDropTables', $DropT);
            Configuration::updateGlobalValue('ErpMultiShop',$MultiShop);
            Configuration::updateGlobalValue('ErpMultiLang',$MultiLang);
            Configuration::updateGlobalValue('ErpConfirmOrder',$Confirm_order);
            Configuration::updateGlobalValue('ErpAutoInvoice',$Auto_gen_inv);
            Configuration::updateGlobalValue('ErpAutoStatusUpdate',$Auto_upd_status);
            return True;
        }   
    }

    public function deletePOBTables(){ 
        $d1=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_customer_merge');
        $d2=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_address_merge');
        $d3=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_product_merge');
        $d4=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_category_merge');
        $d5=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_tax_merge');
        $d6=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_payment_merge'); 
        $d7=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_currency_merge'); 
        $d8=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_carrier_merge');
        $d9=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_attributes_merge');
        $d10=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_attribute_values_merge');
        $d11=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_product_template_merge');
        $d12=Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_order_merge');            
        if(Configuration::getGlobalValue('ErpMultiShop'))
            Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_shop_merge');
        if(Configuration::getGlobalValue('ErpMultiLang'))
            Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'erp_language_merge');
        
        
        if(!$d1 || !$d2 || !$d3 || !$d4 || !$d5 || !$d6 || !$d7 || !$d8 || !$d9 || !$d10 || !$d11 || !$d12)
            return False;
        else
            return True;
    }
    
}