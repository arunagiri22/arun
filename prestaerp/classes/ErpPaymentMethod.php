<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ErpPaymentMethod extends ObjectModel{
    
    public $erp_payment_id;
    public $prestashop_payment_id;
    public $name ;
    
    public static $definition = array(
        'table' => 'erp_payment_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_payment_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_payment_id' => array('type' => self::TYPE_INT,  'required' => true),
            'name' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
        ),
    );
    public function payment_method_details($id){
        $payment_method_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_payment_merge` where `id`=" . $id . "");
        if ($payment_method_info)
            return $payment_method_info;

        return false;
    }

    public function merge_payment_method($name, $userId, $client){
        $payment_arr = array(
            'name' => new xmlrpcval($name, "string")
        );
        $payment     = new xmlrpcmsg('execute');
        $payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $payment->addParam(new xmlrpcval($userId, "int"));
        $payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $payment->addParam(new xmlrpcval("force.done", "string"));
        $payment->addParam(new xmlrpcval("create_payment_method", "string"));
        $payment->addParam(new xmlrpcval($payment_arr, "struct"));
        $resp = $client->send($payment);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        } else {
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            return array(
                'erp_id' => $erp_id
            );
        }
    }

    public function addto_payment_merge($erp_id, $name, $prst_id, $presta_user = 'Front End'){
        $data = array(
            'erp_payment_id' => $erp_id,
            'prestashop_payment_id' => $prst_id,
            'created_by' => $presta_user,
            'name' => $name
        );
        Db::getInstance()->insert('erp_payment_merge', $data);
    }

    public function check_all_payment_methods($userId, $client, $presta_user){
        $is_error      = 0;
        $error_message = '';
        $ids           = '';
        $count         = 0;
        foreach (PaymentModule::getInstalledPaymentModules() as $payment) {
            $prestashop_id = (int) $payment['id_module'];
            $check         = Db::getInstance()->getRow("SELECT *  from `" . _DB_PREFIX_ . "erp_payment_merge` where `prestashop_payment_id`=" . $prestashop_id . "");
            if ($check['erp_payment_id'] <= 0) {
                $module = Module::getInstanceByName($payment['name']);
                if ($module == false) {
                    continue;
                }
                $create = $this->merge_payment_method($module->displayName, $userId, $client);
                if ($create['erp_id'] > 0) {
                    $this->addto_payment_merge($create['erp_id'], $payment['name'], $prestashop_id, $presta_user);
                    $count = 1;
                } else {
                    $is_error = 1;
                    $error_message .= $create['error_message'] . ',';
                    $ids .= $prestashop_id . ',';
                }
            }
        }
        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'value' => $count,
            'ids' => $ids
        );
    }

    public function check_specific_payment_method($name, $userId, $client){
        $check = Db::getInstance()->getRow("SELECT *  from `" . _DB_PREFIX_ . "erp_payment_merge` where `name`='" . $name . "'");
        if ($check['erp_payment_id'] <= 0) {
            $module = Module::getInstanceByName($name);
            $create = $this->merge_payment_method($module->displayName, $userId, $client);
            $this->addto_payment_merge($create['erp_id'], $name, $module->id);
            return $create['erp_id'];
        } else {
            return $check['erp_payment_id'];
        }
    }
    
}