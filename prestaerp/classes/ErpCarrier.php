<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ErpCarrier extends ObjectModel{

    public $erp_carrier_id;
    public $prestashop_carrier_id;
    public $name ;
    
    public static $definition = array(
        'table' => 'erp_carrier_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_carrier_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_carrier_id' => array('type' => self::TYPE_INT,  'required' => true),
            'name' => array('type' => self::TYPE_STRING,  'required' => true, 'size' => 64),
        ),
    );

    public function carrier_details($id){
        $carrier_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_carrier_merge` where `id`=" . $id . "");
        if ($carrier_info)
            return $carrier_info;

        return false;
    }

    public function check_carrier($id_carrier){
        $check_erp_id = Db::getInstance()->getRow("SELECT `is_synch`,`erp_carrier_id` from `" . _DB_PREFIX_ . "erp_carrier_merge` where `prestashop_carrier_id`=" . $id_carrier . "");
        if ($check_erp_id['erp_carrier_id'] > 0) {
            if ($check_erp_id['is_synch'] == 0)
                return array(
                    -1,
                    $check_erp_id['erp_carrier_id']
                );
            else
                return array(
                    $check_erp_id['erp_carrier_id']
                );
        } else
            return array(
                0
            );
    }

    public function create_carrier($name, $userId, $client){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string")
        );
        $key     = array(
            'name' => new xmlrpcval(str_replace('+', ' ',urlencode($name)), "string"),
        );      
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("delivery.carrier", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);        
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");         
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        }
        else{
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            return array(
                'erp_id' => $erp_id
            );
        }
    }

    public function addto_carrier_merge($carrier_id, $erp_carrier_id, $name, $presta_user = 'Front End'){
        $data = array(
            'erp_carrier_id' => $erp_carrier_id,
            'prestashop_carrier_id' => $carrier_id,
            'created_by' => $presta_user,
            'name' => $name
        );
        Db::getInstance()->insert('erp_carrier_merge', $data);
    }

    public function update_carriers($erp_carrier_id, $id_carrier, $name, $userId, $client){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string")
        );
        $erp_carrier_list = array(
            new xmlrpcval($erp_carrier_id, 'int')
        );
        $key     = array(
            'name' => new xmlrpcval($name, "string"),
        );
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("delivery.carrier", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_carrier_list, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } 
        else{
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_carrier_merge` SET `is_synch`=1 where `prestashop_carrier_id`=" . $id_carrier . "");
            return array(
                'value' => True
            );
        }
    }

    public function check_all_carriers($userId, $client, $presta_user){
        $is_error      = 0;
        $error_message = '';
        $ids           = '';
        $check         = Db::getInstance()->executeS("SELECT `id_carrier`,`name`  from `" . _DB_PREFIX_ . "carrier` WHERE `id_carrier` NOT IN (SELECT `prestashop_carrier_id` FROM `" . _DB_PREFIX_ . "erp_carrier_merge` WHERE `is_synch` = 1) and `deleted`=0");

        if (count($check) == 0) {
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                'ids' => $ids
            );
        }
        foreach ($check as $data) {
            $erp_carrier_id = $this->check_carrier($data['id_carrier']);
            if ($erp_carrier_id[0] == 0) {
                $create = $this->create_carrier($data['name'], $userId, $client);
                if ($create['erp_id'] > 0)
                    $this->addto_carrier_merge($data['id_carrier'], $create['erp_id'], $data['name'], $presta_user);
                else {
                    $is_error = 1;
                    $error_message .= $create['error_message'] . ',';
                    $ids .= $data['id_carrier'] . ',';
                }
            }
            if ($erp_carrier_id[0] < 0) {
                $update = $this->update_carriers($erp_carrier_id[1], $data['id_carrier'], $data['name'], $userId, $client);
                if ($update['value'] != True) {
                    $is_error = 1;
                    $error_message .= $update['error_message'] . ',';
                    $ids .= $data['id_carrier'] . ',';
                }
            }
        }
        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'value' => 1,
            'ids' => $ids
        );
    }

    public function check_specific_carrier($carrier_id, $userId, $client){
        $erp   = $this->check_carrier($carrier_id);
        $check = Db::getInstance()->getRow("SELECT `name`  from `" . _DB_PREFIX_ . "carrier` where `id_carrier`=" . $carrier_id . "");
        if ($erp[0] == 0) {
            $create = $this->create_carrier($check['name'], $userId, $client);
            $this->addto_carrier_merge($carrier_id, $create['erp_id'], $check['name']);
            $erp_id=$create['erp_id'];
        }
        if ($erp[0] < 0) {
            $this->update_carriers($erp[1], $check['id_carrier'], $check['name'], $userId, $client);
            $erp_id=$erp[1];
        }
        if ($erp[0] > 0)
            $erp_id=$erp[0];
        return array('name'=>$check['name'],'erp_id'=>$erp_id);
    }

    
}