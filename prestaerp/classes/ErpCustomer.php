<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ErpCustomer extends ObjectModel
{
    public $erp_customer_id;
    public $prestashop_customer_id;
    public $created_by ;
    public $is_synch ;
    
    public static $definition = array(
        'table' => 'erp_customer_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_customer_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_customer_id' => array('type' => self::TYPE_INT,  'required' => true),
            'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'is_synch' => array('type' => self::TYPE_BOOL),
        ),
    );
    public function customer_details($id){
        $customer_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_customer_merge` where `id`=" . $id . "");
        if ($customer_info)
            return $customer_info;

        return false;
    }

    public function update_customer($userId, $client, $erp_customer_id, $customer_id, $key){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string")
        );
        $erp_cust_id = array(
            new xmlrpcval($erp_customer_id, 'int')
        );
        $msg_ser     = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_cust_id, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } else {
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_customer_merge` set `is_synch`=1 where `prestashop_customer_id`=" . $customer_id . "");
            return array(
                'value' => True
            );
        }
    }

    public function create_customer($userId, $client, $customer_id, $key, $presta_user='Front End'){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string"),
            'customer_id' => new xmlrpcval($customer_id, "int"),
            'address_id' => new xmlrpcval('-', "string"),
        );

        $msg_ser     = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        }else{
            $erp_customer_id = $resp->value()->me['int'];
            $data = array(
                'erp_customer_id' => $erp_customer_id,
                'prestashop_customer_id' => $customer_id,
                'created_by' => $presta_user
            );
            Db::getInstance()->insert('erp_customer_merge', $data);
            return array(
                'value' => True,
                'erp_customer_id' => $erp_customer_id,
            );
        }
    }

    private function search_customer($customer_id){
        $check_arr = array();
        $check = Db::getInstance()->getRow("SELECT `is_synch`,`erp_customer_id`  from `" . _DB_PREFIX_ . "erp_customer_merge` where `prestashop_customer_id`=" . $customer_id . "");
        if ($check['erp_customer_id'] > 0) {
            if ($check['is_synch'] == 0) {
                $check_arr[0] = -1;
                $check_arr[1] = $check['erp_customer_id'];
                return $check_arr;
            } else {
                $check_arr[0] = $check['erp_customer_id'];
                return $check_arr;
            }
        } else {
            $check_arr[0] = 0;
            return $check_arr;
        }
    }

    public function check_all_customers($userId, $client, $presta_user){
        $is_error      = 0;
        $error_message = '';
        $ids           = '';
        $data          = Db::getInstance()->executeS("SELECT cus.`id_customer`,cus.`firstname`,cus.`lastname`,cus.`email`, erp.`erp_customer_id`, erp.`is_synch`
        FROM `" . _DB_PREFIX_ . "customer` cus
        LEFT JOIN `"._DB_PREFIX_."erp_customer_merge` erp ON (erp.`prestashop_customer_id` = cus.`id_customer`)
        WHERE  erp.`is_synch` IS NULL or erp.`is_synch` =0");
        if (count($data) == 0) {
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                'ids' => $ids
            );
        }

        foreach ($data as $customer_data) {
            $customer_data['firstname'] = urlencode($customer_data['firstname']);
            $customer_data['lastname']  = urlencode($customer_data['lastname']);
            $key = array(
                'name' => new xmlrpcval($customer_data['firstname'] .' '. $customer_data['lastname'], "string"),
                'person' => new xmlrpcval(true, "boolean"),
                'email' => new xmlrpcval($customer_data['email'], "string"),
            );
            if ($customer_data['is_synch'] == NULL){             
                $create = $this-> create_customer($userId, $client, $customer_data['id_customer'], $key, $presta_user);
                if (!$create['value']){
                    $is_error = 1;
                    $error_message .= $create['error_message'] . ',';
                    $ids .= $customer_data['id_customer'] . ',';
                }
            }
            else 
                $this->update_customer($userId, $client, $customer_data['erp_customer_id'], $customer_data['id_customer'],$key);
        }
        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'value' => 1,
            'ids' => $ids
        );
    }

    public function check_specific_customer($userId, $client, $id_customer, $presta_user='Front End'){
        $check_customer_id = $this->search_customer($id_customer);
        if ($check_customer_id[0] > 0)
            return $check_customer_id[0];
        else{
            $customer_data = Db::getInstance()->getRow("SELECT `id_customer`,`firstname`,`lastname`,`email` FROM `" . _DB_PREFIX_ . "customer` WHERE  `id_customer`=".$id_customer."");

            $customer_data['firstname'] = urlencode($customer_data['firstname']);
            $customer_data['lastname']  = urlencode($customer_data['lastname']);
            $key = array(
                'name' => new xmlrpcval($customer_data['firstname'] .' '. $customer_data['lastname'], "string"),
                'email' => new xmlrpcval($customer_data['email'], "string"),
                'person' => new xmlrpcval(true, "boolean"),
            );

            if ($check_customer_id[0]==0){
                $create = $this-> create_customer($userId, $client, $id_customer, $key, $presta_user);
                if ($create['value']){
                    $erp_customer_id = $create['erp_customer_id'];
                }
                else
                    $erp_customer_id = -1;
            }
            else{
                $this->update_customer($userId, $client, $check_customer_id[1], $id_customer,$key);
                $erp_customer_id = $check_customer_id[1];

            }
            return $erp_customer_id;        
       }
    }
    
}