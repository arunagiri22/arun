<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
class ErpProductCombination extends ObjectModel{
    public $erp_product_id;
    public $erp_template_id;
    public $prestashop_product_id;
    public $prestashop_product_attribute_id;
    public $created_by ;
    public $is_synch ;
    
    public static $definition = array(
        'table' => 'erp_product_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_product_id' => array('type' => self::TYPE_INT, 'required' => true),
            'erp_template_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_product_id' => array('type' => self::TYPE_INT,  'required' => true),
            'prestashop_product_attribute_id' => array('type' => self::TYPE_INT,  'required' => true),
            'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'is_synch' => array('type' => self::TYPE_BOOL),
        ),
    );

    public function check_product($prestashop_product_id,$prestashop_product_attribute_id){
        $check_erp_id = Db::getInstance()->getRow("SELECT `is_synch`,`erp_product_id` from `" . _DB_PREFIX_ . "erp_product_merge` where `prestashop_product_id`=".$prestashop_product_id." and `prestashop_product_attribute_id`=".$prestashop_product_attribute_id."");
        if ($check_erp_id['erp_product_id'] > 0) {
            if ($check_erp_id['is_synch'] == 0)
                return array(
                    -1,
                    $check_erp_id['erp_product_id']
                );
            else
                return array(
                    $check_erp_id['erp_product_id']
                );
        } else
            return array(
                0
            );
    }
    public function product_details($id){
        $product_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_product_merge` where `id`=" . $id . "");
        if ($product_info)
            return $product_info;

        return false;
    }

    public function update_product($userId,$client,$ps_product_id,$ps_product_attr_id,$erp_product_id,$image_variant,$default_code,$ean13,$attr_value_ids,$price_extra){
        $context = array(
            'prestashop_variant' => new xmlrpcval('prestashop_variant', "string"),
            'presta_id' =>  new xmlrpcval($ps_product_id, "string"),
            'presta_attr_id' => new xmlrpcval($ps_product_attr_id, "string"),
            'erp_product_id' => new xmlrpcval($erp_product_id, "int"),
            'create_product_variant' => new xmlrpcval('create_product_variant', "string"),
            'prestashop' => new xmlrpcval('prestashop', "string"),
        );
        $erp_product_key = array(
            new xmlrpcval($erp_product_id, 'int')
        );
        $arrayVal = array(
            'default_code' => new xmlrpcval($default_code,"string"),
            'image_variant' => new xmlrpcval($image_variant,"string"),
            'ps_attributes' => new xmlrpcval($attr_value_ids,"struct"),
            'wk_extra_price' => new xmlrpcval($price_extra,"double"),
            );
        if ($ean13 != ''){
            $arrayVal['ean13'] = new xmlrpcval($ean13,"string");
        }

        $msg_server  = new xmlrpcmsg('execute');
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_server->addParam(new xmlrpcval($userId, "int"));
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_server->addParam(new xmlrpcval("product.product", "string"));
        $msg_server->addParam(new xmlrpcval("write", "string"));
        $msg_server->addParam(new xmlrpcval($erp_product_key, "array"));
        $msg_server->addParam(new xmlrpcval($arrayVal, "struct"));
        $msg_server->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_server);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'erp_id' => -1,
                'is_error'=>True
            );
        } else {
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_product_merge` set `is_synch`=1 where `erp_product_id`=". $erp_product_id. "");
            return array(
                'erp_id' => $erp_product_id,
                'is_error'=>False
            );
        }
    }


    public function create_product($userId,$client,$presta_user,$ps_product_id,$ps_product_attr_id,$erp_template_id,$image_variant,$default_code,$ean13,$attr_value_ids,$price_extra){
        $context = array(
            'prestashop_variant' => new xmlrpcval('prestashop_variant', "string"),
            'presta_id' => new xmlrpcval($ps_product_id, "string"),
            'presta_attr_id' => new xmlrpcval($ps_product_attr_id, "string"),
            'create_product_variant' => new xmlrpcval('create_product_variant', "string")
        );
        
        $arrayVal = array(
            'product_tmpl_id' => new xmlrpcval($erp_template_id,"int"),
            'default_code' => new xmlrpcval($default_code,"string"),
            'image_variant' => new xmlrpcval($image_variant,"string"),
            'ps_attributes' => new xmlrpcval($attr_value_ids,"struct"),
            'wk_extra_price' => new xmlrpcval($price_extra,"double"),
            );
        if ($ean13 != ''){
            $arrayVal['ean13'] = new xmlrpcval($ean13,"string");
        }

        $msg_server  = new xmlrpcmsg('execute');
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_server->addParam(new xmlrpcval($userId, "int"));
        $msg_server->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_server->addParam(new xmlrpcval("product.product", "string"));
        $msg_server->addParam(new xmlrpcval("create", "string"));
        $msg_server->addParam(new xmlrpcval($arrayVal, "struct"));
        $msg_server->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_server);
//                 echo "<pre>";
// var_dump($arrayVal);
// var_dump($resp);
// die;
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        } else {
            $val    = $resp->value()->me;
            $erp_product_id = $val['int'];
            $this->addto_ps_merge($erp_template_id,$ps_product_id,$erp_product_id,$ps_product_attr_id,$presta_user);          
            return array(
                'erp_id' => $erp_product_id
            );
        }
    }

    public function addto_ps_merge($erp_template_id,$ps_product_id,$erp_product_id,$ps_product_attr_id,$presta_user = 'Front End')
    {
        $data = array(
            'erp_product_id' => $erp_product_id,
            'prestashop_product_id' => $ps_product_id,
            'prestashop_product_attribute_id' => $ps_product_attr_id,
            'erp_template_id' => $erp_template_id,
            'created_by' => $presta_user,
        );
        Db::getInstance()->insert('erp_product_merge', $data);
        Db::getInstance()->delete('erp_product_merge','erp_template_id='.$erp_template_id.' and prestashop_product_attribute_id=0');
    }

    public function check_all_products($userId, $client, $presta_user,$id_product=False,$id_attribute=False){
        $template      = new ErpProductTemplate();
        $id_lang       = (int)Configuration::get('PS_LANG_DEFAULT');
        $is_error      = 0;
        $error_message = '';
        $ids           = '';
        $query          = "SELECT p.`id_shop_default`,p.`id_product`,p.`id_category_default`,p.`price`,p.`ean13`,p.`reference`,p.`id_tax_rules_group`,pl.`name`,pl.`description`, pl.`id_shop`,pl.`link_rewrite`,pl.`description_short`,IFNULL(pa.id_product_attribute,0) as id_product_attribute,erp.is_synch,erp.erp_product_id
        FROM ". _DB_PREFIX_."product  p 
        LEFT JOIN `"._DB_PREFIX_."product_lang` pl ON (pl.`id_product` = p.`id_product` and pl.id_shop=p.id_shop_default and pl.id_lang=".$id_lang.")
        LEFT JOIN `"._DB_PREFIX_."product_attribute` pa ON (p.`id_product` = pa.`id_product`)
        LEFT JOIN `"._DB_PREFIX_."erp_product_merge` erp ON (erp.`prestashop_product_id` = p.id_product AND erp.prestashop_product_attribute_id = IFNULL(pa.id_product_attribute,0))
        WHERE (erp.`is_synch` IS NULL or erp.`is_synch` =0)";

        if ($id_product)
            $query =  $query." and p.`id_product` = ".$id_product;
        if ($id_attribute)
            $query =  $query." and pa.`id_product_attribute` = ".$id_attribute;
        $data          = Db::getInstance()->executeS($query);        
        
        if (count($data) == 0) {
            //Nothing to export
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                'ids' => $ids
            );
        }

        foreach ($data as $product_data){    
            $erp_template_id    =   $template->check_specific_template($product_data['id_product'],$userId, $client,$presta_user,$product_data['id_product_attribute']);
            if ($erp_template_id > 0){
                if ($product_data['id_product_attribute']!=0){
                    $attr_detail=$this->getAttributesDetails($id_lang,$product_data['id_product_attribute']);
                    $price_extra = (float)$attr_detail[0]['attribute_price'];                  
                    $product_quantity=$attr_detail[0]['attribute_quantity'];
                    $image_variant        = $template->get_image_link($product_data['id_product'], $product_data['link_rewrite'],$attr_detail[0]['id_image']);
                    $reference  = urlencode($attr_detail[0]['reference']);
                    $ean13      = $this->validate_ean13($attr_detail[0]['ean13']);
                 
                    $check_all_attribute = $this->getAllERPAttributesID($product_data['id_product'],$product_data['id_product_attribute'],$userId, $client, $presta_user);
                    if (!$check_all_attribute['status']){
                        $is_error = 1;
                        $error_message .= 'Error in creating Attribute Values for this product,';
                        $ids .= $product_data['id_product'].',';
                        continue ;
                    }
                    else {
                        $attr_value_ids = $check_all_attribute['erp_attr_values_ids'];
                    }
                    if ($product_data['is_synch']==NULL){                 
                        $create = $this->create_product($userId,$client,$presta_user,$product_data['id_product'],$product_data['id_product_attribute'],$erp_template_id,$image_variant,$reference,$ean13,$attr_value_ids,$price_extra);
                        if ($create['erp_id'] < 0){
                            $is_error = 1;
                            $error_message .= $create['error_message'] . ',';
                            $ids .= $product_data['id_product'] . ',';
                        }else{
                            $this->create_product_quantity($create['erp_id'],$product_quantity,$userId, $client);
                        }                        
                    }
                    else{
                        $update = $this->update_product($userId,$client,$product_data['id_product'],$product_data['id_product_attribute'],$product_data['erp_product_id'],$image_variant,$reference,$ean13,$attr_value_ids,$price_extra);
                        if ($update['is_error']) {
                            $is_error = 1;
                            $error_message .= $update['error_message'].',';
                            $ids .= $product_data['id_product'].',';
                         }else{
                            // $this->create_product_quantity($product_data['erp_product_id'],$product_quantity,$userId, $client);
                        }                                
                    }
                }
                else{
                    if ($product_data['is_synch']=='0'){
                        // $quantity =  StockAvailable::getQuantityAvailableByProduct($product_data['id_product']);
                        // $this->create_product_quantity($product_data['erp_product_id'],$quantity,$userId, $client);
                        Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_product_merge` set `is_synch`=1 where `erp_product_id`=".$product_data['erp_product_id']."");
                           
                    }
                    continue ;
                }
            }
            else{
                $is_error = 1;
                $error_message .= 'Error in creating Template for this product,';
                $ids .= $product_data['id_product'].',';
            }

        }

        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'value' => 1,
            'ids' => $ids
        );
    }

    public function create_product_quantity($erp_product_id,$product_quantity,$userId, $client) {           
        $arrayVal = array(
            'product_id' => new xmlrpcval($erp_product_id, "int"),
            'new_quantity' => new xmlrpcval($product_quantity, "string"),            
            );        
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("force.done", "string"));
        $msg_ser->addParam(new xmlrpcval("update_quantity", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $resp = $client->send($msg_ser); 
        if ($resp->faultCode()) {
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return;
        }
        else{
            return;
        }    
        
    }

    public function check_specific_product($ps_product_id,$ps_product_attr_id,$userId, $client,$presta_user='Front End'){   
        $template      = new ErpProductTemplate();
        $erp = $this->check_product($ps_product_id,$ps_product_attr_id);
        if ($erp[0] <= 0){
            $erp_template_id    =   $template->check_specific_template($ps_product_id,$userId, $client,$presta_user,$ps_product_attr_id);
            if ($erp_template_id > 0){
                $id_lang       = (int)Configuration::get('PS_LANG_DEFAULT');
                $q = "SELECT p.* , pl.`link_rewrite`
                                FROM ". _DB_PREFIX_."product  p 
                                LEFT JOIN `"._DB_PREFIX_."product_lang` pl ON (pl.`id_product` = p.`id_product` and pl.`id_shop`=p.`id_shop_default` and pl.`id_lang`=".$id_lang.") where p.id_product=".$ps_product_id;
                                
                $product_data  = Db::getInstance()->getRow($q);
                if ($ps_product_attr_id!=0){
                    $attr_detail=$this->getAttributesDetails($id_lang,$ps_product_attr_id);
                    $product_quantity=$attr_detail[0]['attribute_quantity'];
                    $price_extra = $attr_detail[0]['attribute_price'];
                    $image_variant        = $template->get_image_link($ps_product_id, $product_data['link_rewrite'],$attr_detail[0]['id_image']);
                    $reference  = urlencode($attr_detail[0]['reference']);
                    $ean13      = $this->validate_ean13($attr_detail[0]['ean13']);

                    $check_all_attribute = $this->getAllERPAttributesID($ps_product_id,$ps_product_attr_id,$userId, $client, $presta_user);
                    if (!$check_all_attribute['status']){
                        return -1;
                    }
                    else{
                        $attr_value_ids = $check_all_attribute['erp_attr_values_ids'];
                    }
                    if ($erp[0] == 0){
                        $create = $this->create_product($userId,$client,$presta_user,$ps_product_id,$ps_product_attr_id,$erp_template_id,$image_variant,$reference,$ean13,$attr_value_ids,$price_extra);
                        
                        if ($create['erp_id'] < 0){
                                $erp_product_id=-1;
                            }
                        else{
                            $erp_product_id=$create['erp_id'];
                            $this->create_product_quantity($create['erp_id'],$product_quantity,$userId, $client); 
                        }
                    }
                    else{
                        $erp_product_id=$erp[1];
                        $this->update_product($userId,$client,$ps_product_id,$ps_product_attr_id,$erp_product_id,$image_variant,$reference,$ean13,$attr_value_ids,$price_extra);
                        // $this->create_product_quantity($erp_product_id,$product_quantity,$userId, $client);
                    }
                }
                else{
                   //update product if needed
                    if ($erp[0] < 0){
                    $product_quantity=StockAvailable::getQuantityAvailableByProduct($ps_product_id);
                    $image_variant        = $template->get_image_link($ps_product_id, $product_data['link_rewrite']);
                    $reference  = urlencode($product_data['reference']);
                    $ean13      = $this->validate_ean13($product_data['ean13']);


                    $this->update_product($userId,$client,$ps_product_id,$ps_product_attr_id,$erp[1],$image_variant,$reference,$ean13,$attr_value_ids);
                     // $this->create_product_quantity($erp[1],$product_quantity,$userId, $client);
                     $erp_product_id = $erp[1];
                    }else{

                        $data = Db::getInstance()->getRow("SELECT erp_product_id from ". _DB_PREFIX_."erp_product_merge where erp_template_id=".$erp_template_id." and prestashop_product_attribute_id=0");
                        $erp_product_id=$data['erp_product_id'];
                    }
                }
            }
            else
                $erp_product_id=-1;
        }
        else
            $erp_product_id=$erp[0];
        return $erp_product_id;
    }


    public function getAllERPAttributesID($ps_product_id,$ps_product_attr_id,$userId, $client, $presta_user){
        $status=True;
        $attr_value      = new ErpProductAttributeValue();
        $attr_value_ids = array();
        $data = $this->getAttributesParams($ps_product_id,$ps_product_attr_id);
        foreach ($data as $attr_data){
            $erp = $attr_value->check_specific_attribute_value($userId, $client, $attr_data['id_attribute_group'],$attr_data['id_attribute'], $presta_user);
            if ($erp['erp_attr_value_id']==-1)
                $status=False;
            else{
                $price_impact = Db::getInstance()->getRow("SELECT price from `"._DB_PREFIX_."product_attribute` where `id_product`=".$ps_product_id." and `id_product_attribute`=".$ps_product_attr_id."");

                if (!$price_impact)
                    $price_impact = 0.0;
                else
                    $price_impact = $price_impact['price'];
                $attr_value_ids[$erp['erp_attribute_id']] = new xmlrpcval(array(
                                                        new xmlrpcval($erp['erp_attr_value_id'],"int"),
                                                        new xmlrpcval($price_impact,"double")
                                                        ), "array");
            }
        }
        return array('status'=>$status, 'erp_attr_values_ids'=>$attr_value_ids);
    }

    public function getAttributesParams($id_product, $id_product_attribute){
        $query = '
            SELECT DISTINCT a.`id_attribute`,a.`id_attribute_group`
            FROM `'._DB_PREFIX_.'attribute` a
            LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
                ON (al.`id_attribute` = a.`id_attribute` AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
            LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
                ON (pac.`id_attribute` = a.`id_attribute`)
            LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                ON (pa.`id_product_attribute` = pac.`id_product_attribute`)
            '.Shop::addSqlAssociation('product_attribute', 'pa').'
            LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
                ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)Context::getContext()->language->id.')
            WHERE pa.`id_product` = '.(int)$id_product.'
                AND pac.`id_product_attribute` = '.(int)$id_product_attribute;
            $result = Db::getInstance()->executeS($query);
            return $result;
        }

    public function getAttributesDetails($id_lang,$id_attribute,$attribute_value_separator = ' - ', $attribute_separator = ', '){
        $data = Db::getInstance()->executeS('SELECT a.`id_attribute`, a.`id_attribute_group`,pac.id_product_attribute, GROUP_CONCAT(agl.`name`, \''.pSQL($attribute_value_separator).'\',al.`name` ORDER BY agl.`id_attribute_group` SEPARATOR \''.pSQL($attribute_separator).'\') as attribute_name,attr.price as attribute_price,attr.reference,attr.ean13,stk.quantity as attribute_quantity,pai.`id_image`
                FROM `'._DB_PREFIX_.'product_attribute_combination` pac 
                LEFT JOIN `'._DB_PREFIX_.'attribute` a ON a.`id_attribute` = pac.`id_attribute`
                LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
                LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int)$id_lang.')
                LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)$id_lang.')
                LEFT JOIN `'._DB_PREFIX_.'product_attribute` attr ON (pac.id_product_attribute = attr.id_product_attribute)
                LEFT JOIN `'._DB_PREFIX_.'stock_available` stk ON (attr.id_product = stk.id_product and attr.id_product_attribute = stk.id_product_attribute)
                LEFT JOIN `'._DB_PREFIX_.'product_attribute_image` pai ON (pai.`id_product_attribute`= '.$id_attribute.')
                WHERE pac.id_product_attribute = '.$id_attribute.'') ;
        return $data;
    }
    
    public function validate_ean13($barcode){
        // check to see if barcode is 13 digits long
        if (!preg_match("/^[0-9]{13}$/", $barcode)) {
            return '';
        }

        $digits = $barcode;

        // 1. Add the values of the digits in the 
        // even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits[1] + $digits[3] + $digits[5] +
                    $digits[7] + $digits[9] + $digits[11];

        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;

        // 3. Add the values of the digits in the 
        // odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits[0] + $digits[2] + $digits[4] +
                   $digits[6] + $digits[8] + $digits[10];

        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;

        // 5. The check character is the smallest number which,
        // when added to the result in step 4, produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;

        // if the check digit and the last digit of the 
        // barcode are OK return true;
        if ($check_digit == $digits[12]) {
            return $barcode;
        }

        return '';
    }
    
}