<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class ErpTaxes extends ObjectModel{
    
    public $erp_tax_id;
    public $prestashop_tax_id;
    public $rate ;
    
    public static $definition = array(
        'table' => 'erp_tax_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_tax_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_tax_id' => array('type' => self::TYPE_INT,  'required' => true),
            'rate' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
        ),
    );
    public function tax_details($id){
        $tax_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_tax_merge` where `id`=" . $id . "");
        if ($tax_info)
            return $tax_info;

        return false;
    }

    public function check_tax($tax_id){
        $check_tax = Db::getInstance()->getRow("SELECT `is_synch`,`erp_tax_id` from `" . _DB_PREFIX_ . "erp_tax_merge` where `prestashop_tax_id`=" . $tax_id . "");
        if ($check_tax) {
            if ($check_tax['is_synch'] == 0)
                return array(
                    -1,
                    $check_tax['erp_tax_id']
                );
            else
                return array(
                    $check_tax['erp_tax_id']
                );
        } else
            return array(
                0
            );
    }

    public function create_tax($tax_rate, $prst_tax_id, $client, $userId){   
        $log = new pob_log();
        $key     = array(
            'name' => new xmlrpcval("PS_" . $tax_rate . "_" . $prst_tax_id . "", "string"),
            'amount' => new xmlrpcval($tax_rate, "string")
        );
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("account.tax", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();          
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data);
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        } else {
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            return array(
                'erp_id' => $erp_id
            );
        }
        return $val['int'];
    }

    public function addto_tax_merge($erp_tax_id, $tax_id, $presta_user = 'Front End'){
        $db_tax  = Db::getInstance()->getRow("SELECT `rate`  from `" . _DB_PREFIX_ . "tax` where `id_tax`=" . $tax_id . "");
        $rate = $db_tax['rate'];
        $data = array(
            'erp_tax_id' => $erp_tax_id,
            'prestashop_tax_id' => $tax_id,
            'created_by' => $presta_user,
            'rate'=>$rate
        );
        Db::getInstance()->insert('erp_tax_merge', $data);
    }

    public function update_taxes($erp_tax_id, $tax_rate, $prst_tax_id, $client, $userId){   
        $log = new pob_log();
        $key          = array(
            'amount' => new xmlrpcval($tax_rate, "string")
        );
        $erp_tax_list = array(
            new xmlrpcval($erp_tax_id, 'int')
        );
        $msg_ser      = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("account.tax", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_tax_list, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();          
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } else {
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_tax_merge` SET `is_synch`=1 where `prestashop_tax_id`=" . $prst_tax_id . "");
            return array(
                'value' => True
            );
        }
    }

    public function check_all_taxes($client, $userId, $presta_user){
        $is_error      = 0;
        $error_message = '';
        $ids           = '';
        $data          = Db::getInstance()->executeS("SELECT `id_tax`,`rate` FROM " . _DB_PREFIX_ . "tax  WHERE `id_tax` NOT IN (SELECT `prestashop_tax_id` FROM `" . _DB_PREFIX_ . "erp_tax_merge` WHERE `is_synch` = 1) and active=1 and deleted = 0");
     
        if (count($data) == 0) {
            //Nothing to export
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                '$ids' => $ids
            );
        }
        foreach ($data as $tax) {
            $check_tax = $this->check_tax($tax['id_tax']);
            if ($check_tax[0] <= 0) {
                $cal_tax = $tax['rate']/100;
                if ($check_tax[0] == 0) {
                    $create = $this->create_tax($cal_tax, $tax['id_tax'], $client, $userId);
                    if ($create['erp_id'] > 0)
                        $this->addto_tax_merge($create['erp_id'], $tax['id_tax'], $presta_user);
                    else {
                        $is_error = 1;
                        $error_message .= $create['error_message'] . ',';
                        $ids .= $tax['id_tax'] . ',';
                    }
                } else {
                    $update = $this->update_taxes($check_tax[1], $cal_tax, $tax['id_tax'], $client, $userId);
                    if ($update['value'] != True) {
                        $is_error = 1;
                        $error_message .= $update['error_message'] . ',';
                        $ids .= $tax['id_tax'] . ',';
                    }
                }
            }
        }
        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'value' => 1,
            'ids' => $ids
        );
    }

    public function check_specific_tax($id_tax, $client, $userId){
        $check_tax = $this->check_tax($id_tax);
        if ($check_tax[0] > 0)
            return $check_tax[0];
        else {
            $db_tax  = Db::getInstance()->getRow("SELECT `rate`  from `" . _DB_PREFIX_ . "tax` where `id_tax`=" . $id_tax . "");
            $cal_tax = $db_tax['rate']/100;
            if ($check_tax[0] == 0) {
                $create = $this->create_tax($cal_tax, $id_tax, $client, $userId);
                $this->addto_tax_merge($create['erp_id'], $id_tax, 'Front End');
                return $create['erp_id'];
            } else {
                $this->update_taxes($check_tax[1], $cal_tax, $id_tax, $client, $userId);
                return $check_tax[1];
            }
        }
    }

    public function check_specific_rule($id_country, $id_tax_rule_group){
        $tax_id = Db::getInstance()->getRow("SELECT `id_tax`  from `" . _DB_PREFIX_ . "tax_rule` where `id_country`=" . $id_country . " and `id_tax_rules_group`=" . $id_tax_rule_group . "");
        if ($tax_id['id_tax'] > 0)
            return $tax_id['id_tax'];
        else
            return 0;
    }
    
}