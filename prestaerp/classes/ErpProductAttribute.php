<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class ErpProductAttribute extends ObjectModel{

    public $erp_attribute_id;
    public $prestashop_attribute_id;
    public $created_by ;
    public $translate_state ;
    public $color ;
    public $is_synch ;
    
    public static $definition = array(
        'table' => 'erp_attributes_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_attribute_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_attribute_id' => array('type' => self::TYPE_INT,  'required' => true),
            'translate_state' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'color' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'is_synch' => array('type' => self::TYPE_BOOL),
        ),
    );
    
    public function check_attribute($id_attribute){
        $check_erp_id = Db::getInstance()->getRow("SELECT `is_synch`,`erp_attribute_id` from `"._DB_PREFIX_."erp_attributes_merge` where `prestashop_attribute_id`=".$id_attribute."");
        if ($check_erp_id['erp_attribute_id'] > 0) {
            if ($check_erp_id['is_synch'] == 0)
                return array(
                    -1,
                    $check_erp_id['erp_attribute_id']
                );
            else
                return array(
                    $check_erp_id['erp_attribute_id']
                );
        } else
            return array(
                0
            );
    }
    
    public function create_attribute($attr_name, $userId, $client){
        $key     = array(
            'name' => new xmlrpcval(str_replace('+', ' ',urlencode($attr_name)), "string"),
        );
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string"),
        );

        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("product.attribute", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        } else {
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            return array(
                'erp_id' => $erp_id
            );
        }
    }

    public function attr_details($id){
        $attr_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_attributes_merge` where `id`=" . $id . "");
        if ($attr_info)
            return $attr_info;

        return false;
    }

    
    public function addto_attribute_merge($attribute_id, $erp_attribute_id, $presta_user = 'Front End'){
        $data = array(
            'erp_attribute_id' => $erp_attribute_id,
            'prestashop_attribute_id' => $attribute_id,
            'created_by' => $presta_user,
        );
        Db::getInstance()->insert('erp_attributes_merge', $data);        
    }
    
    public function update_attribute($erp_attribute_id, $id_attribute, $attr_name, $userId, $client){
        $erp_attribute_list = array(
            new xmlrpcval($erp_attribute_id, 'int')
        );
        $key     = array(
            'name' => new xmlrpcval(str_replace('+', ' ',urlencode($attr_name)), "string"),
        );
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string"),
        );
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("product.attribute", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_attribute_list, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } else {
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_attributes_merge` SET `is_synch`=1 where `prestashop_attribute_id`=" . $id_attribute . "");
            return array(
                'value' => True
            );
        }
    }    
    
    public function addto_openerp_merge($erp_id, $presta_id,$object,$userId, $client){
        $arrayVal = array(
            'name' => new xmlrpcval($erp_id, "int"),
            'erp_id' => new xmlrpcval($erp_id, "string"),
            'presta_id' => new xmlrpcval($presta_id, "string")
        );
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval($object, "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'is_error' => 1
            );
        } else {
            return array(
                'is_error' => 0
            );
        }
    }    
    
    public function check_all_attributes($userId, $client, $presta_user){   
        $is_error       = 0;
        $error_message  = '';
        $ids            = '';
        $count          = 0;
        $data = AttributeGroup::getAttributesGroups(Configuration::get('PS_LANG_DEFAULT'));            
        foreach ($data as $attr_data){
            $erp_attribute_id = $this->check_attribute($attr_data['id_attribute_group']);
            if ($erp_attribute_id[0] == 0) {$count = $count+1;
                $attr_name = $attr_data['name'];             
                $create = $this->create_attribute($attr_name, $userId, $client);
                if ($create['erp_id'] > 0){
                    $this->addto_attribute_merge($attr_data['id_attribute_group'], $create['erp_id'],$presta_user);
                    $this->addto_openerp_merge($create['erp_id'],$attr_data['id_attribute_group'],"prestashop.product.attribute",$userId, $client);
                }
                else {
                    $is_error = 1;
                    $error_message .= $create['error_message'] . ',';
                    $ids .= $attr_data['id_attribute_group'] . ',';
                }
             
             }
            elseif ($erp_attribute_id[0] < 0) {$count = $count+1;
                $attr_name = $attr_data['name'];
                $update = $this->update_attribute($erp_attribute_id[1], $attr_data['id_attribute_group'], $attr_name, $userId, $client);
                if ($update['value'] != True) {
                    $is_error = 1;
                    $error_message .= $update['error_message'] . ',';
                    $ids .= $attr_data['id_attribute_group'] . ',';
                }
            }
        }
        if ($count == 0) {
            //Nothing to export
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                '$ids' => $ids
            );
        }
        else
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 1,
                'ids' => $ids
            );
    }

    public function check_specific_attribute($userId, $client, $presta_attribute_id, $presta_user){
        $check_attribute = $this->check_attribute($presta_attribute_id);
        if ($check_attribute[0] > 0)
            return $check_attribute[0];
        else {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
            $attr_data = Db::getInstance()->getRow('
            SELECT DISTINCT agl.`name`, ag.*, agl.*
            FROM `'._DB_PREFIX_.'attribute_group` ag
            '.Shop::addSqlAssociation('attribute_group', 'ag').'
            LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
                ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND `id_lang` = '.(int)$id_lang.')
            WHERE agl.id_attribute_group='.$presta_attribute_id); 
            $attr_name = $attr_data['name'];     
            if ($check_attribute[0] == 0) {
                $create = $this->create_attribute($attr_name ,$userId, $client);
                if ($create['erp_id'] > 0){
                    $this->addto_attribute_merge($presta_attribute_id, $create['erp_id'],$presta_user);
                    $this->addto_openerp_merge($create['erp_id'],$presta_attribute_id,"prestashop.product.attribute",$userId, $client);
                    return $create['erp_id']; 
                }
                else
                    return False;
            } else {
                $this->update_attribute($check_attribute[1],$presta_attribute_id,$attr_name, $userId, $client);
                return $check_attribute[1];
            }
        }
    }
    
}