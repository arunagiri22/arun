<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ErpCustomerAddress extends ObjectModel{

    public $erp_address_id;
    public $prestashop_address_id;
    public $created_by ;
    public $is_synch ;
    public $id_customer ;
    
    public static $definition = array(
        'table' => 'erp_address_merge',
        'primary' => 'id',
        'fields' => array(
            'erp_address_id' => array('type' => self::TYPE_INT, 'required' => true),
            'prestashop_address_id' => array('type' => self::TYPE_INT,  'required' => true),
            'id_customer' => array('type' => self::TYPE_INT,  'required' => true),
            'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
            'is_synch' => array('type' => self::TYPE_BOOL),
        ),
    );

    public function address_details($id){
        $address_info = Db::getInstance()->getRow("SELECT * from `" . _DB_PREFIX_ . "erp_address_merge` where `id`=" . $id . "");
        if ($address_info)
            return $address_info;

        return false;
    }

    public function check_all_addresses($userId, $client, $presta_user){
        $is_error      = 0;
        $error_message = '';
        $ids           = '';
        $bulk_data          = Db::getInstance()->executeS("SELECT cus.*, erp.`erp_address_id`, erp.`is_synch` FROM `" . _DB_PREFIX_ . "address` cus LEFT JOIN `"._DB_PREFIX_."erp_address_merge` erp ON (erp.`prestashop_address_id` = cus.`id_address`)
        WHERE  erp.`is_synch` IS NULL or erp.`is_synch` =0");

        // echo "<pre>";
        // var_dump($bulk_data);
        // die;

        if (count($bulk_data) == 0) {
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                'ids' => $ids
            );
        }

        foreach ($bulk_data as $data) {
            $customer = new ErpCustomer();
            $erp_customer_id = $customer->check_specific_customer($userId, $client, $data['id_customer'], $presta_user);
            if ($erp_customer_id>0){
                $temp_data = $this->get_state_and_country_id($userId, $client, $data['id_country'], $data['id_state']);
                $erp_country_id = $temp_data['country_id'];
                $erp_state_id = $temp_data['state_id'];
                $data['alias'] = urlencode($data['alias']);
                $data['address1'] = urlencode($data['address1']);
                $data['address2'] = urlencode($data['address2']);
                $data['firstname'] = urlencode($data['firstname']);
                $data['lastname'] = urlencode($data['lastname']);
                $data['company'] = urlencode($data['company']);
                $data['city'] = urlencode($data['city']);
                $name = $data['firstname'] .' '. $data['lastname'];
                $key  = array(
                    'parent_id' => new xmlrpcval($erp_customer_id, "string"),
                    'name' => new xmlrpcval($name, "string"),
                    'street' => new xmlrpcval($data['address1'], "string"),
                    'street2' => new xmlrpcval($data['address2'], "string"),
                    'phone' => new xmlrpcval($data['phone'], "string"),
                    'mobile' => new xmlrpcval($data['phone_mobile'], "string"),
                    'zip' => new xmlrpcval($data['postcode'], "string"),
                    'city' => new xmlrpcval($data['city'], "string"),
                    'country_id' => new xmlrpcval($erp_country_id, "int"),
                    'state_id' => new xmlrpcval($erp_state_id, "int"),
                    'customer' => new xmlrpcval(false, "boolean"),
                    'use_parent_address' => new xmlrpcval(false, "boolean"),
                    'type' => new xmlrpcval('invoice', "string"),
                );

                if ($data['is_synch'] == NULL){             
                    $create = $this-> create_address($userId, $client, $data['id_customer'], $data['id_address'], $key, $presta_user);
                    if (!$create['value']){
                        $is_error = 1;
                        $error_message .= $create['error_message'] . ',';
                        $ids .= $data['id_address'] . ',';
                    }
                }
                else 
                    $this->update_address($userId, $client, $key, $data['erp_address_id'], $data['id_address']);
            }
        }
        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'ids' => $ids,
            'value' => 1
        );
    }

    public function get_state_and_country_id($userId, $client, $id_country, $id_state){
        $erp_country_id = false;
        $erp_state_id   = false;
        if ($id_country != null && $id_country != "" && $id_country != 0) {
            $country          = new ErpCountry();
            $country_iso_code = $country->get_iso($id_country);
            $country_name     = $country->get_country_name($id_country);
            $erp_country_id   = $country->get_country($country_iso_code, $country_name, $userId, $client);
        }
        if ($id_state != null && $id_state != "" && $id_state != 0) {
            $state        = new ErpState();
            $state_dtls   = $state->get_state_dtls($id_state);
            $erp_state_id = $state->check_state($userId, $client, $state_dtls['iso_code'], $state_dtls['name'], $state_dtls['id_country']);
        }
        return array(
            'state_id' => $erp_state_id,
            'country_id' => $erp_country_id,
        );
    }

    public function create_address($userId, $client, $customer_id, $address_id, $key, $presta_user='Front End'){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string"),
            'customer_id' => new xmlrpcval($customer_id, "int"),
            'address_id' => new xmlrpcval($address_id, "string"),
        );

        $msg_ser     = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        }else{
            $erp_address_id = $resp->value()->me['int'];
            $data = array(
                'erp_address_id' => $erp_address_id,
                'prestashop_address_id' => $address_id,
                'created_by' => $presta_user,
                'id_customer' => $customer_id
            );
            Db::getInstance()->insert('erp_address_merge', $data);
            return array(
                'value' => True,
                'erp_address_id' => $erp_address_id,
            );
        }
    }

    public function update_address($userId, $client, $key, $erp_id, $id_address){
        $context = array(
            'prestashop' => new xmlrpcval('prestashop', "string")
        );
        $erp_address_id = array(
            new xmlrpcval($erp_id, 'int')
        );

        $msg_ser     = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_address_id, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,"CRITICAL");
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        }else {
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_address_merge` set `is_synch`=1 where `prestashop_address_id`=" . $id_address . "");
            return array(
                'value' => True
            );
        }
    }
   
    public function search_address($id_address){
        $arr = array();
        $erp_address = Db::getInstance()->getRow("SELECT `is_synch`,`erp_address_id`  from `" . _DB_PREFIX_ . "erp_address_merge` where `prestashop_address_id`=" . $id_address . "");
        if ($erp_address['erp_address_id'] > 0) {
            if ($erp_address['is_synch'] == 1) {
                $arr[0] = $erp_address['erp_address_id'];
                return $arr;
            } else {
                $arr[0] = -1;
                $arr[1] = $erp_address['erp_address_id'];
                return $arr;
            }
        } else {
            $arr[0] = 0;
            return $arr;
        }
    }

    public function check_specific_address($userId, $client, $id_address, $id_customer, $presta_user = 'Front End'){
        $erp_address = $this->search_address($id_address);
        $customer = new ErpCustomer();
        $erp_customer_id = $customer->check_specific_customer($userId, $client, $id_customer, $presta_user);
        $address_data = Db::getInstance()->getRow("SELECT * FROM `" . _DB_PREFIX_ . "address` where `id_address`=". $id_address."");
        $temp_data = $this->get_state_and_country_id($userId, $client, $address_data['id_country'], $address_data['id_state']);
        $erp_country_id = $temp_data['country_id'];
        $erp_state_id = $temp_data['state_id'];

        $address_data['alias'] = urlencode($address_data['alias']);
        $address_data['address1'] = urlencode($address_data['address1']);
        $address_data['address2'] = urlencode($address_data['address2']);
        $address_data['firstname'] = urlencode($address_data['firstname']);
        $address_data['lastname'] = urlencode($address_data['lastname']);
        $address_data['company'] = urlencode($address_data['company']);
        $address_data['city'] = urlencode($address_data['city']);
        $name = $address_data['firstname'] .' '. $address_data['lastname'];
        $key  = array(
            'parent_id' => new xmlrpcval($erp_customer_id, "string"),
            'wk_address_alias' => new xmlrpcval($address_data['alias'], "string"),
            'wk_company' => new xmlrpcval($address_data['company'], "string"),
            'name' => new xmlrpcval($name, "string"),
            'street' => new xmlrpcval($address_data['address1'], "string"),
            'street2' => new xmlrpcval($address_data['address2'], "string"),
            'phone' => new xmlrpcval($address_data['phone'], "string"),
            'mobile' => new xmlrpcval($address_data['phone_mobile'], "string"),
            'zip' => new xmlrpcval($address_data['postcode'], "string"),
            'city' => new xmlrpcval($address_data['city'], "string"),
            'country_id' => new xmlrpcval($erp_country_id, "int"),
            'state_id' => new xmlrpcval($erp_state_id, "int"),
            'customer' => new xmlrpcval(false, "boolean"),
            'type' => new xmlrpcval('invoice', "string"),
            'use_parent_address' => new xmlrpcval(false, "boolean")
        );

        if ($erp_address[0] > 0) {
            $this->update_address($userId, $client, $key, $erp_address[0], $id_address);
            return $erp_address[0];
        }else {
            if ($erp_address[0] == -1){
                $this->update_address($userId, $client, $key, $erp_address[1], $id_address);
                return $erp_address[1];
            }else{
                $create = $this-> create_address($userId, $client, $id_customer, $id_address, $key, $presta_user);
                if ($create['value'])
                    return $create['erp_address_id'];
            }
        }
    }
        
}