<?php
/*
* 2012-2013 PrestaShop-OpenERP Bridge
*
* DISCLAIMER
*
* This file is a part of PrestaShop-OpenERP Bridge.
* Do not edit or add to this file.
* If you wish to customize PrestaShop-OpenERP Bridge for your
* needs please refer to http://www.webkul.com for more information.
*
*  @author Webkul Software Pvt. Ltd. <vinayrks@webkul.com>
*/
class ProductMergeCore extends ObjectModel
{	
	public $erp_product_id;
	public $erp_template_id;
	public $prestashop_product_id;
	public $prestashop_product_attribute_id;
	public $created_by ;
	public $is_synch ;
 	
	public static $definition = array(
		'table' => 'erp_product_merge',
		'primary' => 'id',
		'fields' => array(
			'erp_product_id' => array('type' => self::TYPE_INT, 'required' => true),
			'erp_template_id' => array('type' => self::TYPE_INT, 'required' => true),
			'prestashop_product_id' => array('type' => self::TYPE_INT,  'required' => true),
			'prestashop_product_attribute_id' => array('type' => self::TYPE_INT,  'required' => true),
			'created_by' => array('type' => self::TYPE_STRING,  'required' => false, 'size' => 64),
			'is_synch' => array('type' => self::TYPE_BOOL),
		),
	);

	protected $webserviceParameters = array(		
		'objectMethods' => array(
			'add' => 'addWs',
		),);

	public function addWs($autodate = true, $null_values = false){
		$this->is_synch=1;
		return $this->add($autodate, $null_values);
	}
	

}