<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class WebserviceRequest extends WebserviceRequestCore
{
	public static function getResources()
	{
		$resources=parent::getResources();
		$resources['erp_product_merges'] = array('description' => 'Created by Webkul', 'class' => 'ProductMerge');
		$resources['erp_product_template_merges'] = array('description' => 'Created by Webkul', 'class' => 'ProductTemplateMerge');
		$resources['erp_category_merges'] = array('description' => 'Created by Webkul', 'class' => 'CategoryMerge');
		$resources['erp_attributes_merges'] = array('description' => 'Created by Webkul', 'class' => 'AttributeMerge');
		$resources['erp_attribute_values_merges'] = array('description' => 'Created by Webkul', 'class' => 'AttributeValueMerge');
		ksort($resources);
		return $resources;
	}
}