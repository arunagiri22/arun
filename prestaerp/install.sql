CREATE TABLE IF NOT EXISTS `PREFIX_erp_order_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`prst_order_id` int(11) NOT NULL,
	`presta_module` varchar(255),
	`erp_order_id` int(11) NOT NULL,
	`erp_order_name` varchar(200) NOT NULL,
	`erp_invoice_id` int(11) NOT NULL DEFAULT 0,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX (`prst_order_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_attributes_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_attribute_id` int(11) NOT NULL,
	`prestashop_attribute_id` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
	`translate_state` varchar(255) NOT NULL DEFAULT 'to_translate',
	`color` varchar(32) NOT NULL DEFAULT 'DarkOrange',
  PRIMARY KEY (`id`),
  INDEX (`prestashop_attribute_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_attribute_values_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`prestashop_attribute_id` int(11) NOT NULL,
	`prestashop_attribute_value_id` int(11)NOT NULL,
	`erp_attribute_value_id` int(11)NOT NULL,
	`erp_attribute_id` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
	`translate_state` varchar(255) NOT NULL DEFAULT 'to_translate',
	`color` varchar(32) NOT NULL DEFAULT 'DarkOrange',
  PRIMARY KEY (`id`),
  INDEX (`prestashop_attribute_value_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_address_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_address_id` int(11) NOT NULL,
	`prestashop_address_id` int(11) NOT NULL,
	`id_customer` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX (`prestashop_address_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_tax_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_tax_id` int(11) NOT NULL,
	`prestashop_tax_id` int(11) NOT NULL,
	`rate` decimal(10,3) NOT NULL DEFAULT '0.000',
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_product_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_product_id` int(11) NOT NULL,
	`erp_template_id` int(11) NOT NULL,
	`prestashop_product_id` int(11) NOT NULL,
	`prestashop_product_attribute_id` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX (`prestashop_product_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_erp_product_template_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_template_id` int(11) NOT NULL,
	`presta_product_id` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
	`translate_state` varchar(255) NOT NULL DEFAULT 'to_translate',
	`color` varchar(32) NOT NULL DEFAULT 'DarkOrange',
  PRIMARY KEY (`id`),
  INDEX (`presta_product_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_erp_payment_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_payment_id` int(11) NOT NULL,
	`prestashop_payment_id` int(11) NOT NULL,
	`name` varchar(300) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_customer_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_customer_id` int(11) NOT NULL,
	`prestashop_customer_id` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX (`prestashop_customer_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_currency_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_currency_id` int(11) NOT NULL,
	`prestashop_currency_id` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_category_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_category_id` int(11) NOT NULL,
	`prestashop_category_id` int(11) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
	`translate_state` varchar(255) NOT NULL DEFAULT 'to_translate',
	`color` varchar(32) NOT NULL DEFAULT 'DarkOrange',
  PRIMARY KEY (`id`),
  INDEX (`prestashop_category_id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_erp_carrier_merge` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`erp_carrier_id` int(11) NOT NULL,
	`prestashop_carrier_id` int(11) NOT NULL,
	`name` varchar(100) NOT NULL,
	`created_by` varchar(255) NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_synch` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;