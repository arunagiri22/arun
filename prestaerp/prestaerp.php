<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
    exit;

require_once 'classes/PrestaerpClassInclude.php';

class Prestaerp extends Module{ 
    const INSTALL_SQL_FILE = 'install.sql';

    public function __construct(){
        $this->name = 'prestaerp';
        $this->tab = 'administration';
        $this->version = '4.1.1';
        $this->author = 'WebKul Software Pvt. Ltd.';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('POB : PrestaShop-Odoo Bridge');
        $this->description = $this->l('This module provides a way to export your PrestaShop`s data to Odoo 9. Also, provides a real time synchronization of order(s) from Prestashop to Odoo.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall it?');
    
    }

    public function insertTab() {
        $this->installTab('OdooMapping','Odoo Mapping');
        Configuration::updateGlobalValue('erp-prst-tab-id',(int)Tab::getIdFromClassName('OdooMapping'));
        $this->installTab('AdminWkErpConfiguration', 'Odoo Configuration', 'OdooMapping');
        $this->installTab('AdminErpOrders','Orders','OdooMapping');       
        $this->installTab('AdminErpCustomer','Customers','OdooMapping');       
        $this->installTab('AdminErpCustomerAddress','Customer`s Address','OdooMapping');       
        $this->installTab('AdminErpProductAttribute','Attribute','OdooMapping');       
        $this->installTab('AdminErpProductAttributeValue','Attribute`s Values','OdooMapping');     
        $this->installTab('AdminErpProductCategory','Product`s Category','OdooMapping');       
        $this->installTab('AdminErpProductCombination','All Products/Combinations','OdooMapping');    
        $this->installTab('AdminErpProductTemplate','Product`s Template','OdooMapping');       
        $this->installTab('AdminErpTaxes','Taxes','OdooMapping');       
        $this->installTab('AdminErpCarrier','Carriers','OdooMapping');       
        $this->installTab('AdminErpCurrency','Currency','OdooMapping');       
        $this->installTab('AdminErpPaymentMethod','Payment Methods','OdooMapping');       
        return true;
    }

    public function installTab($class_name,$tab_name,$tab_parent_name=false) {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = $class_name;
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = $tab_name;
        if($tab_parent_name) {
            $tab->id_parent = (int)Tab::getIdFromClassName($tab_parent_name);
        }else {
            $tab->id_parent = 0;
        }
        $tab->module = $this->name;
        return $tab->add();
    }

    public function changePosition(){
        $a = Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "tab` SET `position`=7 where `class_name`='OdooMapping'");
        return true;
    }

    public function deleteTab() {
        $this->uninstallTab('AdminErpOrders');
        $this->uninstallTab('AdminErpCustomer');
        $this->uninstallTab('AdminErpCustomerAddress');
        $this->uninstallTab('AdminErpProductAttribute');
        $this->uninstallTab('AdminErpProductAttributeValue');
        $this->uninstallTab('AdminErpProductCategory');
        $this->uninstallTab('AdminErpProductCombination');
        $this->uninstallTab('AdminErpProductTemplate');
        $this->uninstallTab('AdminErpTaxes');
        $this->uninstallTab('AdminErpCarrier');
        $this->uninstallTab('AdminErpCurrency');
        $this->uninstallTab('AdminErpPaymentMethod');
        $this->uninstallTab('AdminWkErpConfiguration');       
        $this->uninstallTab('OdooMapping');       
        return true;
    }

    public function uninstallTab($class_name) {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        if ($id_tab){
            $tab = new Tab($id_tab);
            return $tab->delete();
        }
        else
            return false;
    }   

    public function install(){
        if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
            return (false);
        else if (!$sql = Tools::file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
            return (false);
        
        $sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);

        foreach ($sql AS $query)
            if($query)
                if(!Db::getInstance()->execute(trim($query)))
                    return false;   

        Configuration::updateGlobalValue('ErpAllowFrontExport',True);
        Configuration::updateGlobalValue('ErpAutoInvoice',True);
        Configuration::updateGlobalValue('ErpConfirmOrder',True);
        Configuration::updateGlobalValue('ErpAutoStatusUpdate',True);
        if (!parent::install() OR !$this->insertTab() OR !$this->changePosition() OR !$this->registerHook('actionPaymentConfirmation') OR !$this->registerHook('actionOrderStatusUpdate') OR !$this->registerHook('displayOrderConfirmation') OR !$this->registerHook('actionObjectProductUpdateAfter') OR !$this->registerHook('actionObjectCategoryUpdateAfter') OR !$this->registerHook('actionValidateOrder') OR !$this->copy_webservice_files() OR !$this->registerHook('actionPobExportTrackingRef') OR !$this->registerHook('displayBackOfficeHeader'))
            return false;
        return true;
    }    

    public function uninstall(){
        $check_multi_shop = Db::getInstance()->getRow("select `id_module` from `"._DB_PREFIX_."module` where `name`='prestaerpmultishop' and `active`=1");
        $check_multi_lang = Db::getInstance()->getRow("select `id_module` from `"._DB_PREFIX_."module` where `name`='prestaerpmultilang' and `active`=1");
        if (!$check_multi_shop && !$check_multi_lang){
            $this->deleteTab();
            $this->remove_webservice_files();
            $this->deleteConfiguration();
            if (!parent::uninstall())
                return false;     
            else
                return true;
        }
        else{   
            $name = '';
            if ($check_multi_shop)
                $name = $name.'"POB : Multi-Shop Extension"';
            if ($check_multi_lang)
                $name = $name.'"POB : Multi-Language Extension"';
            $this->errors[] = Tools::displayError('<ul>
            <li>'.$this->l('In order to uninstall these Module you must un-install '.$name.' first.').'</li>
            <li>'.$this->l('Please contact our support team for further details.').'</li>
            </ul>');
            return false;
        }
    }

    public function hookDisplayBackOfficeHeader(){
        $this->context->controller->addCss($this->_path.'views/css/admin/css/prestaerpmenu.css');
    }

    public function deleteConfiguration() {
        $c1=Configuration::deleteByName('ErpUrl');
        $c2=Configuration::deleteByName('ErpPort');
        $c3=Configuration::deleteByName('ErpDatabase');
        $c4=Configuration::deleteByName('ErpUsername');
        $c5=Configuration::deleteByName('ErpPassword');
        $c6=Configuration::deleteByName('ErpDropTables');
        $c7=Configuration::deleteByName('ErpMultiShop');
        $c8=Configuration::deleteByName('erp-prst-tab-id');
        $c9=Configuration::deleteByName('ErpMultiLang');
        $c10=Configuration::deleteByName('ErpTestConnection');
        $c11=Configuration::deleteByName('ErpConfirmOrder');
        $c12=Configuration::deleteByName('ErpAutoInvoice');
        $c13=Configuration::deleteByName('ErpAutoStatusUpdate');
        if(!$c1 || !$c2 || !$c3 || !$c4 || !$c5 || !$c6 || !$c7 || !$c8 || !$c9 || !$c10 ||$c11 ||$c12 ||$c13)
            return False;
        else
            return True;
    }

    public function deleteTables() {
        if(Configuration::getGlobalValue("ErpDropTables") == 1){
            $d1=Db::getInstance()->executeS('DROP TABLE '._DB_PREFIX_.'erp_attribute_values_merge');
            $d2=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_attributes_merge');
            $d3=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_product_template_merge');
            $d4=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_customer_merge');
            $d5=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_address_merge');
            $d6=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_product_merge');
            $d7=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_category_merge');
            $d8=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_tax_merge');
            $d9=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_payment_merge');
            $d10=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_currency_merge');
            $d11=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_carrier_merge');
            $d12=Db::getInstance()->execute('DROP TABLE '._DB_PREFIX_.'erp_order_merge');
            if(!$d1 || !$d2 || !$d3 || !$d4 || !$d5 || !$d6 || !$d7 || !$d8 || !$d9 || !$d10 || !$d11 || !$d12)
                return False;
        }
        return True;
    }

    public function copy_webservice_files(){
        $override_src = $this->getLocalPath().'rest_api_files'.DIRECTORY_SEPARATOR;
        $override_dest = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR;
        $product_merge =copy($override_src.'ProductMerge.php',$override_dest.'ProductMerge.php');
        $category_merge =copy($override_src.'CategoryMerge.php',$override_dest.'CategoryMerge.php');
        $attribute_merge =copy($override_src.'AttributeMerge.php',$override_dest.'AttributeMerge.php');
        $attribute_value_merge =copy($override_src.'AttributeValueMerge.php',$override_dest.'AttributeValueMerge.php');
        $product_template_merge =copy($override_src.'ProductTemplateMerge.php',$override_dest.'ProductTemplateMerge.php');
        $webservice_request =copy($override_src.'WebserviceRequest.php',$override_dest.'webservice'.DIRECTORY_SEPARATOR.'WebserviceRequest.php');
        if(!$override_src || !$override_dest || !$product_merge || !$category_merge || !$webservice_request || !$attribute_merge || !$attribute_value_merge || !$product_template_merge)
            return False;
        else
            return True;
    }

    public function remove_webservice_files(){
        $override_src = $this->getLocalPath().'rest_api_files'.DIRECTORY_SEPARATOR.'remove'.DIRECTORY_SEPARATOR;
        $override_dest = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR;
        $product_merge =copy($override_src.'ProductMerge.php',$override_dest.'ProductMerge.php');
        $category_merge =copy($override_src.'CategoryMerge.php',$override_dest.'CategoryMerge.php');
        $attribute_merge =copy($override_src.'AttributeMerge.php',$override_dest.'AttributeMerge.php');
        $attribute_value_merge =copy($override_src.'AttributeValueMerge.php',$override_dest.'AttributeValueMerge.php');
        $product_template_merge =copy($override_src.'ProductTemplateMerge.php',$override_dest.'ProductTemplateMerge.php');
        $webservice_request =copy($override_src.'WebserviceRequest.php',$override_dest.'webservice'.DIRECTORY_SEPARATOR.'WebserviceRequest.php');
        if(!$attribute_merge || !$attribute_value_merge || !$product_merge || !$category_merge || !$webservice_request || !$product_template_merge)
            return False;
        else
            return True;
    }

    public function get_erp_connection(){
        if (!class_exists('xmlrpc_client'))
            include 'xmlrpc.inc';
        $sock = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl").":".Configuration::getGlobalValue("ErpPort")."/xmlrpc/common");
        $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl").":".Configuration::getGlobalValue("ErpPort")."/xmlrpc/object");       
        $msg    = new xmlrpcmsg('login');
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
        $resp = $sock->send($msg);      
        if (!$resp->faultCode()){   
            $userId = $resp->value()->scalarval();
            if ($userId > 0){
            return array(
                'client'  =>    $client,
                'user_id' =>    $userId,
                'status'  =>    True
                );}
        else{
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,'Authentication Error','CRITICAL');
            return array(
                'status'  =>False
                );
            }
        }
        else{
            $log = new pob_log();
            $log->logMessage(__FILE__,__LINE__,$resp->raw_data,'CRITICAL: ');
            return array(
                'status'  =>False
                );
        }
    }

    public function hookActionPobExportTrackingRef($params){
        // Hook::exec('actionPobExportTrackingRef', array('id_order'=>$order->id,'carrier' => Tools::getValue('tracking_number')));

        // Above code to be added in Orders controller manually in postProcess()
        return true;
        // $connect = $this->get_erp_connection();             
        // if ($connect['status']==true){
        //     $client =   $connect['client'];
        //     $userId =   $connect['user_id'];

        //     $merge_data = Db::getInstance()->getRow("SELECT `erp_order_id` from ". _DB_PREFIX_."erp_order_merge where prst_order_id=".$params['id_order']."");
        //     if ($merge_data){
        //         $id_order = $merge_data['erp_order_id'];
        //         $ref = $params['carrier'];
        //     }else{
        //         return True;
        //     }
            
        //     $context = array(
        //         'prestashop' => new xmlrpcval('prestashop', "string")
        //         );
        //     $data = array(      
        //         'order_id'      =>new xmlrpcval($id_order,"int"),           
        //         'track_no'      =>new xmlrpcval($ref,"string"),         
        //         );

        //     $msg = new xmlrpcmsg('execute');
        //     $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
        //     $msg->addParam(new xmlrpcval($userId, "int"));
        //     $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
        //     $msg->addParam(new xmlrpcval("force.done", "string"));
        //     $msg->addParam(new xmlrpcval("add_tracking_number", "string"));
        //     $msg->addParam(new xmlrpcval($data, "struct"));
        //     $msg->addParam(new xmlrpcval($context, "struct"));
        //     $response = $client->send($msg);
        // }       
    }

    public function hookActionOrderStatusUpdate($params){
        $log = new pob_log();   
        if (Configuration::getGlobalValue('ErpAutoStatusUpdate') == true){
            $order_state = (int)Tools::getValue('id_order_state');
            if ($order_state){
                $context = array(
                    'ecommerce' => new xmlrpcval('prestashop', "string")
                );
                if ($order_state == 6){     
                    $connect = $this->get_erp_connection();             
                    if ($connect['status'] == true){
                        $client = $connect['client'];
                        $userId = $connect['user_id'];          
                                                
                        $id_order = Tools::getValue('id_order');
                        $erp_order_id = Db::getInstance()->getRow("SELECT *  from `"._DB_PREFIX_."erp_order_merge` where `prst_order_id`=".$id_order."");
                        
                        $order_payment = new xmlrpcmsg('execute');
                        $order_payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));   
                        $order_payment->addParam(new xmlrpcval($userId, "int"));
                        $order_payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));       
                        $order_payment->addParam(new xmlrpcval("wk.skeleton", "string"));
                        $order_payment->addParam(new xmlrpcval("set_order_cancel", "string"));
                        $order_payment->addParam(new xmlrpcval($erp_order_id['erp_order_id'], "int"));
                        $order_payment->addParam(new xmlrpcval($context, "struct"));
                        $order_pay_resp = $client->send($order_payment);
                        if ($order_pay_resp->faultCode()) {
                            $error_message = $order_pay_resp->faultString();                        
                            $log->logMessage(__FILE__,__LINE__,$error_message,"CRITICAL: Error in shipping order: ");  
                        }
                        else
                            $log->logMessage(__FILE__,__LINE__,'Order is set to DELIVERED','INFO');
                    }
                }       

                if($order_state == 4 || $order_state == 5){
                    $id_order =  Tools::getValue('id_order');
                    $erp_order_id = Db::getInstance()->getRow("SELECT `erp_order_id`  from `"._DB_PREFIX_."erp_order_merge` where `prst_order_id`=".$id_order."");

                    $connect = $this->get_erp_connection();
                    if ($connect['status']==true){
                        $client =   $connect['client'];
                        $userId =   $connect['user_id'];

                        $pick_avail = new xmlrpcmsg('execute');
                        $pick_avail->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                        $pick_avail->addParam(new xmlrpcval($userId, "int"));
                        $pick_avail->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                        $pick_avail->addParam(new xmlrpcval("wk.skeleton", "string"));
                        $pick_avail->addParam(new xmlrpcval("set_order_shipped", "string"));
                        $pick_avail->addParam(new xmlrpcval($erp_order_id['erp_order_id'],"int"));
                        $pick_avail->addParam(new xmlrpcval($context, "struct"));
                        $resp = $client->send($pick_avail);
                    }
                }   
            }
        }
    }

    public function hookDisplayOrderConfirmation($params,$order_id=False){
        $log = new pob_log();
        $id_order = false;
        if (isset($params['objOrder']->id))
            $id_order = $params['objOrder']->id;
        else
            $id_order = $params['id_order'];
        if(!$id_order)
            $id_order = (Tools::getValue('id_order')) ? Tools::getValue('id_order'):$order_id;        
        if ($id_order){
            $erp_order_id = Db::getInstance()->getRow("SELECT *  from `"._DB_PREFIX_."erp_order_merge` where `prst_order_id`=".$id_order."");
            if (empty($erp_order_id)) {
                if (!class_exists('xmlrpc_client'))
                    include 'xmlrpc.inc';                
                $connect = $this->get_erp_connection();         
                if ($connect['status']==true){
                    $client =   $connect['client'];
                    $userId =   $connect['user_id'];
                    include_once _PS_MODULE_DIR_.'/prestaerpmultishop/ErpShop.php';
                    $shipping_price = Db::getInstance()->getRow("SELECT `id_carrier`,`shipping_cost_tax_excl`  from `" . _DB_PREFIX_ . "order_carrier` where `id_order`=" .$id_order. " ORDER BY date_add DESC");
                    $order         = Db::getInstance()->getRow("SELECT `current_state`,`id_customer`,`id_address_delivery`,`id_address_invoice`,`id_currency`,`id_shop`,`total_discounts_tax_incl`,`reference`,`total_paid_tax_incl`,`total_shipping_tax_incl`,`total_shipping_tax_excl`,`total_paid`,`date_add`,`date_upd`,`invoice_date`,`total_paid`,`carrier_tax_rate`,`module`  from `" . _DB_PREFIX_ . "orders` where `id_order`=" . $id_order. "");
                    $order_details   = Db::getInstance()->executeS("SELECT `product_id`,`reduction_percent`,`product_attribute_id`,`id_order_detail`,`product_quantity`,`product_name`,`reduction_amount_tax_incl`,`original_product_price`,`product_price`,`unit_price_tax_excl`,`reduction_amount_tax_excl`  from `" . _DB_PREFIX_ . "order_detail` where `id_order`=" .$id_order. "");       
                    Db::getInstance()->execute("INSERT INTO `" . _DB_PREFIX_ . "erp_order_merge`(`erp_order_id`,`prst_order_id`,`erp_order_name`,`presta_module`)VALUES(0," .$id_order. ",'Need to Update','None')");
                    $carrier          = new ErpCarrier();
                    $customer         = new ErpCustomer();
                    $address          = new ErpCustomerAddress();
                    $id_customer      = $customer->check_specific_customer($userId, $client, $order['id_customer']);
                    Db::getInstance()->execute("UPDATE `"._DB_PREFIX_."erp_address_merge` SET `is_synch`=0 where id_customer = " .$order['id_customer']. "");
                    $delivery_address = $address->check_specific_address($userId, $client, $order['id_address_delivery'],$order['id_customer']);
                    $carrier_data   = $carrier->check_specific_carrier($shipping_price['id_carrier'], $userId, $client);                    
                    $invoice_address  = $address->check_specific_address($userId, $client, $order['id_address_invoice'],$order['id_customer']);
                    $currency         = new ErpCurrency();
                    $currency_id         = $currency->check_specific_currency($order['id_currency'], $userId, $client);   
                    $shop             = new ErpShop(); 
                    $erp_shop_id      = $shop->check_specific_shop($order['id_shop'], $userId, $client);
                    $order_data=array(
                        'partner_id'            =>  new xmlrpcval($id_customer, "int"),
                        'partner_invoice_id'    =>  new xmlrpcval($invoice_address, "string"),
                        'partner_shipping_id'   =>  new xmlrpcval($delivery_address, "string"),
                        'pricelist_id'          =>  new xmlrpcval($currency_id, "string"),
                        'presta_order_id'       =>  new xmlrpcval($id_order, "string"),
                        'ecommerce_order_id'    =>  new xmlrpcval($id_order, "string"),
                        'ecommerce_order_ref'   =>  new xmlrpcval($order['reference'], "string"),
                        'date_add'              =>  new xmlrpcval($order['date_add'], "string"),
                        'date_upd'              =>  new xmlrpcval($order['date_upd'], "string"),
                        'presta_total'          =>  new xmlrpcval($order['total_paid'], "string"),
                        'presta_module'         =>  new xmlrpcval($order['module'], "string"),
                        'carrier_id'            =>  new xmlrpcval($carrier_data['erp_id'], "string"),
                        'ecommerce_channel'     =>  new xmlrpcval('prestashop', "string"),
                        'shop_id'               =>  new xmlrpcval($erp_shop_id, "string"),
                    );
                    $context = array(
                        'ecommerce' => new xmlrpcval('prestashop', "string"),
                        'presta_order_id' => new xmlrpcval($id_order, "int"),
                        'ecommerce_order_id' => new xmlrpcval($id_order, "string"),
                    );

                    //~ if(Configuration::getGlobalValue("ErpMultiShop") == 1){
                        //~ include_once _PS_MODULE_DIR_.'/prestaerpmultishop/ErpShop.php';
                        //~ $shop             = new ErpShop();
                        //~ $erp_shop_id      = $shop->check_specific_shop($order['id_shop'], $userId, $client);
                        //~ $order_data['shop_id']=new xmlrpcval($erp_shop_id, "string");
                    //~ }  

                    $msg1         = new xmlrpcmsg('execute');
                    $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                    $msg1->addParam(new xmlrpcval($userId, "int"));
                    $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                    $msg1->addParam(new xmlrpcval("wk.skeleton", "string"));
                    $msg1->addParam(new xmlrpcval("create_order", "string"));
                    $msg1->addParam(new xmlrpcval($order_data, "struct"));
                    $msg1->addParam(new xmlrpcval($context, "struct")); 
                    $response  = $client->send($msg1);
                    if (!$response->faultCode()){
                        $response_array=$response->value()->me['struct'];                        
                        $status  = $response_array['status'];
                        $status_message   = $response_array['status_message'];

                        if ($status == true){
                            $erp_order_id   = $response_array['order_id']->scalarval();
                            $erp_order_name  = $response_array['order_name']->scalarval();

                            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_order_merge` SET `erp_order_id`=".$erp_order_id.",`erp_order_name`='".$erp_order_name."',`presta_module`='".$order['module']."'  where `prst_order_id`=" .$id_order. "");

                            $product     = new ErpProductCombination();
                            $tax         = new ErpTaxes();
                            
                            foreach ($order_details as $details) {
                                $erp_product_id    = $product->check_specific_product($details['product_id'],$details['product_attribute_id'],$userId, $client);          
                                $total_price   = $details['unit_price_tax_excl'];           
                                $reduction_rate_tax_excl='0';
                                $tax_id = Db::getInstance()->getRow("SELECT `id_tax`  from `" . _DB_PREFIX_ . "order_detail_tax` where `id_order_detail`=" . $details['id_order_detail'] . "");
                                if ($tax_id['id_tax'] > 0) {
                                    $erp_tax_id     = $tax->check_specific_tax($tax_id['id_tax'], $client, $userId);            
                                }
                                else
                                    $erp_tax_id = -1;
                                $line_array_product=array(
                                    'order_id'          =>  new xmlrpcval($erp_order_id, "int"),
                                    'type'              =>  new xmlrpcval('Product', "string"),
                                    'product_id'        =>  new xmlrpcval($erp_product_id, "int"),
                                    'price_unit'        =>  new xmlrpcval($total_price, "string"),
                                    'product_uom_qty'   =>  new xmlrpcval($details['product_quantity'], "string"),
                                    'name'              =>  new xmlrpcval(str_replace('+', ' ',urlencode($details['product_name'])), "string"),
                                    'discount'          =>  new xmlrpcval($reduction_rate_tax_excl, "string"),
                                    'tax_id'            =>  new xmlrpcval($erp_tax_id, "string"),
                                    'ecommerce_channel'             =>  new xmlrpcval('prestashop', "string"),
                                );
                                $msg1         = new xmlrpcmsg('execute');
                                $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                                $msg1->addParam(new xmlrpcval($userId, "int"));
                                $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                                $msg1->addParam(new xmlrpcval("wk.skeleton", "string"));
                                $msg1->addParam(new xmlrpcval("create_sale_order_line", "string"));
                                $msg1->addParam(new xmlrpcval($line_array_product, "struct"));     
                                $response1  = $client->send($msg1);
                                if (!$response1->faultCode()){
                                    $response1_array=$response1->value()->me['struct'];
                                    $order_line_status     = $response1_array['status'];
                                    $order_line_status_message   = $response1_array['status_message'];
                                }
                            }
                            
                            if ((float)$order['total_shipping_tax_excl']!=(float)$order['total_shipping_tax_incl']){        
                                $check_tax = Db::getInstance()->getRow("SELECT `erp_tax_id` from `"._DB_PREFIX_."erp_tax_merge` where `rate`=".$order['carrier_tax_rate']."");
                                if ($check_tax)
                                    $ship_tax_id=$check_tax['erp_tax_id'];
                            }
                            else
                                $ship_tax_id=-1;
                            
                            $line_array_carrier=array(
                                'order_id'           =>  new xmlrpcval($erp_order_id, "int"),
                                'type'               =>  new xmlrpcval('Shipping', "string"),
                                'price_unit'         =>  new xmlrpcval($shipping_price['shipping_cost_tax_excl'], "string"),
                                'product_uom_qty'    =>  new xmlrpcval('1', "string"),
                                'name'               =>  new xmlrpcval(str_replace('+',' ',urlencode($carrier_data['name'])), "string"),
                                'tax_id'             =>  new xmlrpcval($ship_tax_id, "string"),
                                'ecommerce_channel'  =>  new xmlrpcval('prestashop', "string"),
                                );
                            $context = array(
                                'ecommerce'  => new xmlrpcval('prestashop', "string"),
                                'type'       =>  new xmlrpcval('shipping', "string"),
                                'carrier_id' => new xmlrpcval($carrier_data['erp_id'], "int"),
                            );
                            $msg1         = new xmlrpcmsg('execute');
                            $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                            $msg1->addParam(new xmlrpcval($userId, "int"));
                            $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                            $msg1->addParam(new xmlrpcval("wk.skeleton", "string"));
                            $msg1->addParam(new xmlrpcval("create_order_shipping_and_voucher_line", "string"));
                            $msg1->addParam(new xmlrpcval($line_array_carrier, "struct"));     
                            $msg1->addParam(new xmlrpcval($context, "struct")); 
                            $response2  = $client->send($msg1);
                            if (!$response2->faultCode()){
                                $response2_array=$response2->value()->me['struct'];
                                $order_line_status_message   = $response2_array['status_message'];
                            }
                            $voucher_price = $order['total_discounts_tax_incl'];
                            if ($voucher_price > 0) {
                                $i=$i+1;                    
                                $line_array_voucher =array(
                                    'order_id'          =>  new xmlrpcval($erp_order_id, "int"),
                                    'type'              =>  new xmlrpcval('Voucher', "string"),
                                    'price_unit'        =>  new xmlrpcval('-'.$voucher_price, "string"),
                                    'product_uom_qty'   =>  new xmlrpcval('1', "string"),
                                    'name'              =>  new xmlrpcval('voucher', "string"),
                                    'ecommerce_channel'             =>  new xmlrpcval('prestashop', "string")
                                );
                                $context = array(
                                    'ecommerce' => new xmlrpcval('prestashop', "string"),
                                    'type'      =>  new xmlrpcval('voucher', "string"),
                                );

                                $msg1         = new xmlrpcmsg('execute');
                                $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                                $msg1->addParam(new xmlrpcval($userId, "int"));
                                $msg1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                                $msg1->addParam(new xmlrpcval("wk.skeleton", "string"));
                                $msg1->addParam(new xmlrpcval("create_order_shipping_and_voucher_line", "string"));
                                $msg1->addParam(new xmlrpcval($line_array_voucher, "struct"));   
                                $msg1->addParam(new xmlrpcval($context, "struct"));
                                $response3  = $client->send($msg1);
                                if (!$response3->faultCode()){
                                    $response1_array=$response3->value()->me['struct'];
                                    $order_line_status     = $response3_array['status'];
                                    $order_line_status_message   = $response3_array['status_message'];
                                }
                            }                            
                            $data = array(      
                                'order_id'  =>$erp_order_id,                
                                'client'    =>$client,              
                                'userId'    =>$userId
                            );
                            if ((int)(round($erp_total))==(int)(round((float)($ps_total)))){                       
                                if (Configuration::getGlobalValue('ErpConfirmOrder')==True){              
                                    $check_order = $this->set_order_confirm($data);
                                    if (($check_order['status']==True) &&(Configuration::getGlobalValue('ErpAutoInvoice'))){
                                        $generate_invoice = $this->auto_gen_invoice($data);
                                    }           
                                }                   
                            }                                         
                            if (($order['current_state'] == 2) || ($order['current_state'] == 12) ){
                                $order_merge_data = Db::getInstance()->getRow("SELECT * from `"._DB_PREFIX_."erp_order_merge` where `prst_order_id`=".$id_order."");
                                if ($order_merge_data['erp_invoice_id']==0){
                                    $generate_invoice = $this->auto_gen_invoice($data);
                                    $erp_inv_id = $generate_invoice['erp_invoice_id'];
                                }
                                else
                                    $erp_inv_id = $order_merge_data['erp_invoice_id'];

                                $payment_data = array(      
                                    'erp_customer_id'   =>$id_customer,
                                    'presta_reference'  =>$order['reference'],
                                    'presta_amount'     =>$order['total_paid_tax_incl'],
                                    'erp_invoice_id'    =>$erp_inv_id,
                                    'order_id'          =>$erp_order_id,
                                    'client'            =>$client,
                                    'userId'            =>$userId,
                                    'module'            =>$order['module']
                                    );
                                $this->set_order_paid($payment_data);           
                            }
                        }                            
                        else{
                            Db::getInstance()->delete("erp_order_merge","prst_order_id=".$id_order." and erp_order_name='Need to Update'");
                            $log = new pob_log();
                            $log->logMessage(__FILE__,__LINE__,$response->raw_data,'CRITICAL: Error in creating SO: ');
                        }
                    }
                    else{
                        Db::getInstance()->delete("erp_order_merge","prst_order_id=".$id_order." and erp_order_name='Need to Update'");
                        $log = new pob_log();
                        $log->logMessage(__FILE__,__LINE__,$response->raw_data,'CRITICAL:Error in creating SO: ');
                    } 
                }
                else{
                    $log = new pob_log();
                    $log->logMessage(__FILE__,__LINE__,$id_order,'CRITICAL: Connection Error'); 
                }
            }   
        }
    }

    
    public function set_order_confirm($data){
        $log = new pob_log();   
        if (!$data['userId']){
            $response = $this->get_erp_connection();
            $data['userId'] = $response['user_id'];
            $data['client'] = $response['client'];          
        }
        if ($data['userId']){
            $userId     = $data['userId'];
            $order_id   = $data['order_id'];
            $client     = $data['client'];
            $context = array(
                'ecommerce' => new xmlrpcval('prestashop', "string")
            );            
            $msg = new xmlrpcmsg('execute');
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
            $msg->addParam(new xmlrpcval($userId, "int"));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
            $msg->addParam(new xmlrpcval("wk.skeleton", "string")); 
            $msg->addParam(new xmlrpcval("confirm_odoo_order", "string")); 
            $msg->addParam(new xmlrpcval($order_id, "int"));
            $msg->addParam(new xmlrpcval($context, "struct")); 
            $resp = $client->send($msg);
            if ($resp->faultCode()){
                $log->logMessage(__FILE__,__LINE__,$resp->raw_data,'CRITICAL: Error in Confirming Order');
                return array(                               
                    'status' =>False
                );
            }
            else{
                $log->logMessage(__FILE__,__LINE__,'Order is Confirmed on Openerp.','INFO');
                return array(                               
                    'status' =>True
                );
            }
        }
    }

    public function auto_gen_invoice($data){
        $log = new pob_log();   
        if (!$data['userId']){
            $response = $this->get_erp_connection();
            $data['userId']  = $response['user_id'];
            $data['client']  = $response['client'];
            
        }
        if ($data['userId']){
            $userId     = $data['userId'];
            $order_id   = $data['order_id'];
            $client     = $data['client'];
            $context = array(
                'ecommerce' => new xmlrpcval('prestashop', "string")
            );

            $msg = new xmlrpcmsg('execute');
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
            $msg->addParam(new xmlrpcval($userId, "int"));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
            $msg->addParam(new xmlrpcval("wk.skeleton", "string"));
            $msg->addParam(new xmlrpcval("create_order_invoice", "string"));
            $msg->addParam(new xmlrpcval($order_id, "int"));
            $msg->addParam(new xmlrpcval($context, "struct"));
            $resp = $client->send($msg);
            if (!$resp->faultCode()){   
                $erp_invoice_id  = $resp->value()->me['int'];
                $log->logMessage(__FILE__,__LINE__,'Invoice is Generated on Openerp.','INFO');
                return array(
                    'erp_invoice_id' =>$erp_invoice_id,             
                    'status'         =>True
                    );
            }
            else{           
                $log->logMessage(__FILE__,__LINE__,$resp->raw_data,'CRITICAL: Error in creating Invoice: ');
                return array(                               
                    'status'         =>False
                );
            }
        }
    }       

    public function hookActionObjectCategoryUpdateAfter($params){
        $id_category = Tools::getValue('id_category');
        if ($id_category) 
            Db::getInstance()->update('erp_category_merge',array('is_synch'=>0),'prestashop_category_id='.$id_category);
        if (Configuration::getGlobalValue("ErpMultiLang")){
            Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_category_merge` SET `translate_state`='to_translate', `color`='DarkOrange'  WHERE `prestashop_category_id`=".$id_category."");
        }
    }

    public function hookActionObjectProductUpdateAfter($params){
        $id_product = Tools::getValue('id_product');
        if ($id_product){ 
            Db::getInstance()->update('erp_product_merge',array('is_synch'=>0),'prestashop_product_id='.$id_product);
            Db::getInstance()->update('erp_product_template_merge',array('is_synch'=>0),'presta_product_id='.$id_product);
            if (Configuration::getGlobalValue("ErpMultiLang")){
                Db::getInstance()->execute("UPDATE  `" . _DB_PREFIX_ . "erp_product_template_merge` SET `translate_state`='to_translate', `color`='DarkOrange'  WHERE `presta_product_id`=".$id_product."");
            }
        }
    }

    public function hookActionValidateOrder($params){
        $id_cart = Tools::getValue('id_cart');
        if ($id_cart){
            $order_data = Db::getInstance()->getRow("SELECT `id_order` from `"._DB_PREFIX_."orders` where `id_cart`=".$id_cart."");
            $id_order=$order_data['id_order'];
            $this->hookDisplayOrderConfirmation($params,$id_order);
        }
    }

    public function set_order_paid($data){
        $log = new pob_log();
        if (!$data['userId']){
            $response = $this->get_erp_connection();
            $data['userId'] = $response['user_id'];
            $data['client'] = $response['client'];
        }
        if ($data['userId']){
            $userId  = $data['userId'];
            $client  = $data['client'];

            $payment = new ErpPaymentMethod();
            $erp_payment_id = $payment->check_specific_payment_method($data['module'],$userId,$client);

            $order_payment_arr  = array(
                'journal_id'    =>new xmlrpcval($erp_payment_id,"int"),
                'order_id'      =>new xmlrpcval($data['order_id'],"int"),
                );
            $context = array(
                'ecommerce' => new xmlrpcval('prestashop', "string")
            );

            $order_payment = new xmlrpcmsg('execute');
            $order_payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"),"string"));            
            $order_payment->addParam(new xmlrpcval($userId, "int"));    
            $order_payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));       
            $order_payment->addParam(new xmlrpcval("wk.skeleton", "string"));
            $order_payment->addParam(new xmlrpcval("set_order_paid", "string"));
            $order_payment->addParam(new xmlrpcval($order_payment_arr, "struct"));  
            $order_payment->addParam(new xmlrpcval($context, "struct"));    
            $order_pay_resp = $client->send($order_payment);
            if ($order_pay_resp->faultCode()){              
                $log->logMessage(__FILE__,__LINE__,$order_pay_resp->raw_data,'CRITICAL: Error in making invoice Paid: ');
                return False;
            }
            else{
                $log->logMessage(__FILE__,__LINE__,'Order is Set to Paid.','INFO');
            }
            return True;
        }
        else
            return False;
    }

    public function hookActionPaymentConfirmation($params){
        $log = new pob_log();            
        if((Configuration::getGlobalValue('ErpAutoStatusUpdate')==True)){
            $id_order = Tools::getValue('id_order');
            $id_order_state = Tools::getValue('id_order_state');
            
            if($id_order_state == 2 || $id_order_state == 12){      
                $order_details = Db::getInstance()->getRow("SELECT *  from `"._DB_PREFIX_."erp_order_merge` where `prst_order_id`=".$id_order."");
                $customer = Db::getInstance()->getRow("SELECT *  from `"._DB_PREFIX_."orders` where `id_order`=".$id_order."");
                $erp_customer = Db::getInstance()->getRow("SELECT *  from `"._DB_PREFIX_."erp_customer_merge` where `prestashop_customer_id`=".$customer['id_customer']."");

                $connect = $this->get_erp_connection();
                if ($connect['status']==true){
                    $client =   $connect['client'];
                    $userId =   $connect['user_id'];
                    $payment = new ErpPaymentMethod();
                    $erp_payment_id = $payment->check_specific_payment_method($customer['module'],$userId,$client);
                    $order_payment_arr = array(
                        'journal_id'    =>new xmlrpcval($erp_payment_id,"int"),
                        'order_id'      =>new xmlrpcval($order_details['erp_order_id'],"int"),
                    );
                    $context = array(
                        'ecommerce' => new xmlrpcval('prestashop', "string")
                        );
                    
                    $order_payment = new xmlrpcmsg('execute');
                    $order_payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                    $order_payment->addParam(new xmlrpcval($userId, "int"));
                    $order_payment->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                    $order_payment->addParam(new xmlrpcval("wk.skeleton", "string"));
                    $order_payment->addParam(new xmlrpcval("set_order_paid", "string"));
                    $order_payment->addParam(new xmlrpcval($order_payment_arr, "struct"));
                    $order_payment->addParam(new xmlrpcval($context, "struct"));
                    $order_pay_resp = $client->send($order_payment);            
                    if ($order_pay_resp->faultCode())
                        $log->logMessage(__FILE__,__LINE__, $order_pay_resp->raw_data, 'CRITICAL: Error in making Invoice Paid: '); 
                    else
                        $log->logMessage(__FILE__,__LINE__, 'Order is Set to Paid.', 'Info');
                }
            }
        }
    }

}

?>
