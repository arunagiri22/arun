<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminErpTaxesController extends ModuleAdminController{
    
    public function __construct(){
        $this->bootstrap = true;
        $this->table               = 'erp_tax_merge';
        $this->className           = 'ErpTaxes';
        $this->lang                = false;
        $this->add                 = true;
        $this->edit                = true;
        $this->delete              = true;
        $this->view                = true;
        $this->imageType           = 'jpg';
        // $this->list_no_link        = true;
        $this->context             = Context::getContext();
        $this->addRowAction('delete');
        $this->addRowAction('view');

        $this->fields_list         = array(
            'id' => array(
                'title' => 'Id',
                'align' => 'center',
                'width' => 'auto',
            ),
            'erp_tax_id' => array(
                'title' => 'Erp Tax Id',
                'align' => 'center',
               'width' => 'auto',
            ),
            'prestashop_tax_id' => array(
                'title' => 'Prestashop Tax Id',
                'align' => 'center',
                'width' => 'auto',
            ),
            'rate' => array(
                'title' => 'Rate',
                'align' => 'center',
                'width' => 'auto',
            ),
            'created_by' => array(
                'title' => 'Created By',
                'align' => 'center',
                'width' => 'auto',
            ),
            'created_on' => array(
                'title' => 'Created on',
                'align' => 'center',
                'width' => 'auto',
            ),
            'is_synch' => array(
                'title' => $this->l('Is Synchronised'),
                'align' => 'center',
                'type' => 'bool',
                'width' => 'auto',
                'icon' => array(
                1 => array(
                    'src' => 'enabled-2.gif',
                    'alt' => $this->l('Synchronized')
                ),
                0 => array(
                    'src' => 'disabled.gif',
                    'alt' => $this->l('Need Synchronization')
                ),
                ),
            )
        );

        $this->bulk_actions        = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $this->identifier          = 'id';
        parent::__construct();
        $index               = count($this->_conf)+1;
        $this->_conf[$index] = $this->l('All Tax(es) successfully exported to Odoo.');
    }

    public function postProcess() {	
        if (!$this->loadObject(true))
            return;          
        if (Tools::getValue('synchronize')) {
            $log = new pob_log();
            if (!class_exists('xmlrpc_client'))
                include_once 'xmlrpc.inc';
            $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/object");
            $sock   = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/common");
            $msg    = new xmlrpcmsg('login');
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
            $resp = $sock->send($msg);
            if ($resp->faultCode()) {
                $error_message  = $resp->faultString();             
                $log->logMessage(__FILE__,__LINE__,'Connection Error : Invalid Odoo Information.',"ERROR");
                $this->errors[] = Tools::displayError($this->l('Connection Error : Invalid Odoo Information.' . $error_message));
            } else {
                $userId = $resp->value()->scalarval();
                if ($userId <= 0){                  
                    $log->logMessage(__FILE__,__LINE__,'Invalid UserName or Password. Please check the Odoo configuration.',"ERROR");
                    $this->errors[] = Tools::displayError($this->l('Invalid UserName or Password. Please check the Odoo configuration.'));}
                else {
                    $presta_user = $this->context->employee->firstname . ' ' . $this->context->employee->lastname . ' (' . $this->context->employee->email . ')';
                    $tax    = new ErpTaxes();
                    $check       = $tax->check_all_taxes($client, $userId, $presta_user);
                    $index       = count($this->_conf);
                    if ($check['is_error'] == 0 && $check['value'] == 1) {
                        Tools::redirectAdmin(self::$currentIndex . '&conf=' . $index . '&token=' . $this->token);                                     
                        $log->logMessage(__FILE__,__LINE__,'All Tax(s) successfully exported to Odoo.','INFO');
                    } elseif ($check['is_error'] == 0 && $check['value'] == 0) {
                        Tools::redirectAdmin(self::$currentIndex . '&conf=' . $index . '&token=' . $this->token);
                    } else {
                        $this->errors[] = Tools::displayError($this->l('Error in Prestashop`s Id : ' . $check['ids']));                                     
                        $log->logMessage(__FILE__,__LINE__,'Error in Prestashop`s Id : ' . $check['ids'].'/n'.'Error Message(s):' . $check['error_message'],"CRITICAL");
                    }
                }
            }
        } else
            return parent::postProcess();               
    }
	
    public function processDelete(){        
        $record_id = Tools::getValue('id');
        if ($record_id){
            Db::getInstance()->delete('erp_tax_merge','id='.$record_id);
        }       
    }

    public function initToolbar(){          
        $this->page_header_toolbar_btn['refresh'] = array(
            'href' => self::$currentIndex.'&synchronize=1&token=' . $this->token . '',
            'desc' => $this->l('Synchronize')
        );

        $this->page_header_toolbar_btn['manual_map'] = array(
                'href' => self::$currentIndex.'&add'.$this->table.'&token='.$this->token.'&manualmap=1',
                'desc' => $this->l('Manual Mapping'),
                'imgclass' => 'new'
            );
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
        $this->toolbar_btn['wk_hide'] = array(
                                    'desc' => $this->l('')
                                );
    }

    public function initPageHeaderToolbar(){
        parent::initPageHeaderToolbar();
        $this->context->smarty->clearAssign('help_link');   
    }
    
    public function setMedia(){
        parent::setMedia();
        $this->addJS(_MODULE_DIR_.'prestaerp/views/js/pob_js.js');
    }

	public function processBulkDelete(){
		$result = true;
		if (is_array($this->boxes) && !empty($this->boxes)){
			foreach ($this->boxes as $id_data) {
				Db::getInstance()->delete('erp_tax_merge', 'id = ' . (int) $id_data);
			}
			return $result;
		}
		else
			$this->errors[] = Tools::displayError('You must select at least one element to delete.');       
    }	
    public function renderView(){
        $id = Tools::getValue('id');
        $id_lang = $this->context->language->id;
        if ($id)
        {
            $obj_ps_erp_tax = new ErpTaxes();
            $tax_tpl = $obj_ps_erp_tax->tax_details($id);           
            $this->context->smarty->assign('tax_tpl', $tax_tpl);
            $this->context->smarty->assign('modules_dir', _MODULE_DIR_);
        }
        return parent::renderView();
    }
    
    public function renderForm(){   
        $log = new pob_log();
        if (!class_exists('xmlrpc_client'))
            include_once 'xmlrpc.inc';
        $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/object");
        $sock   = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/common");
        $msg    = new xmlrpcmsg('login');
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
        $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
        $resp = $sock->send($msg);
        if ($resp->faultCode()) {
            $error_message  = $resp->faultString();         
            $log->logMessage(__FILE__,__LINE__,'Connection Error : Invalid Odoo Information.',"CRITICAL");
            $this->errors[] = Tools::displayError($this->l('Connection Error : Invalid Odoo Information.' . $error_message));
        }else{
            $userId  = $resp->value()->scalarval();
            $option = array(  new xmlrpcval('purchase', 'string'),
                            );
            $key = array(new xmlrpcval(
                        array(  new xmlrpcval('type_tax_use' , "string"), 
                                new xmlrpcval('not in',"string"),
                                new xmlrpcval($option,"array")),"array"),
                );

            $msg_ser = new xmlrpcmsg('execute');
            $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
            $msg_ser->addParam(new xmlrpcval($userId, "int"));
            $msg_ser->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
            $msg_ser->addParam(new xmlrpcval("account.tax", "string"));
            $msg_ser->addParam(new xmlrpcval("search", "string"));
            $msg_ser->addParam(new xmlrpcval($key, "array"));
            $resp0 = $client->send($msg_ser);
            if ($resp0->faultCode()) {
                $error_message  = $resp0->faultString();                
                $log->logMessage(__FILE__,__LINE__,$error_message,"CRITICAL");
                $this->errors[] = Tools::displayError($this->l('Error : ' . $error_message));
            } else {
                $val      = $resp0->value()->me['array'];
                $key1     = array(
                    new xmlrpcval('id', 'integer'),
                    new xmlrpcval('name', 'string'),
                    new xmlrpcval('amount', 'string')
                );
                $msg_ser1 = new xmlrpcmsg('execute');
                $msg_ser1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), "string"));
                $msg_ser1->addParam(new xmlrpcval($userId, "int"));
                $msg_ser1->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), "string"));
                $msg_ser1->addParam(new xmlrpcval("account.tax", "string"));
                $msg_ser1->addParam(new xmlrpcval("read", "string"));
                $msg_ser1->addParam(new xmlrpcval($val, "array"));
                $msg_ser1->addParam(new xmlrpcval($key1, "array"));
                $resp1 = $client->send($msg_ser1);
                if ($resp1->faultCode()) {
                    $error_message  = $resp1->faultString();                    
                    $log->logMessage(__FILE__,__LINE__,$error_message,"CRITICAL");
                    $this->errors[] = Tools::displayError($this->l('Error : ' . $error_message));
                } else {
                    $value_array = $resp1->value()->scalarval();
                    $count       = count($value_array);
                    $arr = array();
                    for ($x = 0; $x < $count; $x++) {
                        $arr[$x]['id']   = $value_array[$x]->me['struct']['id']->me['int'];
                        $arr[$x]['name'] = $value_array[$x]->me['struct']['name']->me['string'] . ' (' . ($value_array[$x]->me['struct']['amount']->me['double']*100) . '%)';
                    }
                    $this->fields_form = array(
                        'legend' => array(
                            'title' => $this->l('Manual Tax Mapping'),
                            'image' => '../modules/prestaerp/views/img/ErpProducts.png'
                        ),
                        'input' => array(
                            array(
                                'type' => 'select',
                                'label' => $this->l('Odoo Tax'),
                                'name' => 'erpname',
                                'lang' => true,
                                'width' => 13,
                                'options' => array(
                                    'query' => $arr,
                                    'id' => 'id',
                                    'name' => 'name'
                                )
                            ),
                            array(
                                'type' => 'select',
                                'label' => $this->l('Prestashop Tax'),
                                'name' => 'prestaname',
                                'lang' => true,
                                'width' => 1000,                                
                                'options' => array(
                                    'query' => Db::getInstance()->executeS("SELECT * from `" . _DB_PREFIX_ . "tax` where active=1 and deleted = 0"),
                                    'id' => 'id_tax',
                                    'name' => 'rate'
                                )
                            )
                        ),
                        'submit' => array(
                            'title' => $this->l('   Save   '),
                            'class' => 'button'
                        )
                    );
                }
            }
        }
        return parent::renderForm();
    }

    public function processSave(){
        $erp_tax_id        = Tools::getValue('erpname');
        $prestashop_tax_id = Tools::getValue('prestaname');
        $e_count           = Db::getInstance()->executeS("SELECT * from `" . _DB_PREFIX_ . "erp_tax_merge` where `erp_tax_id`=" . $erp_tax_id);
        $p_count           = Db::getInstance()->executeS("SELECT * from `" . _DB_PREFIX_ . "erp_tax_merge` where `prestashop_tax_id`=" . $prestashop_tax_id);
        if (count($e_count) > 0) {
            $this->errors[] = Tools::displayError($this->l('This Odoo Id has been already mapped.'));
            $this->display  = 'add';
        }
        if (count($p_count) > 0) {
            $this->errors[] = Tools::displayError($this->l('This prestaShop Id has been already mapped.'));
            $this->display  = 'add';
        } 
        if ((count($p_count) <= 0) && (count($e_count) <= 0)){
            $presta_user = $this->context->employee->firstname . ' ' . $this->context->employee->lastname . ' (' . $this->context->employee->email . ')';
            $tax         = new ErpTaxes();
            $tax->addto_tax_merge($erp_tax_id, $prestashop_tax_id, $presta_user);
            Tools::redirectAdmin(self::$currentIndex . '&conf=' . 3 . '&token=' . $this->token);
        }
    }
}
?>