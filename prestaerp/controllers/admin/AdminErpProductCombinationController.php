<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminErpProductCombinationController extends ModuleAdminController{

    public function __construct(){
        $this->bootstrap = true;
        $this->table               = 'erp_product_merge';
        $this->className           = 'ErpProductCombination';
        $this->lang                = false;
        $this->add                 = true;
        $this->edit                = true;
        $this->delete              = true;
        $this->view                = true;
        $this->imageType           = 'jpg';
        // $this->list_no_link        = true;
        $this->context             = Context::getContext();
        $this->addRowAction('delete');
        $this->addRowAction('view');


        $this->fields_list         = array(
            'id' => array(
                'title' => 'Id',
                'align' => 'text-center',
                'width' => 'auto',
            ),
            'erp_template_id' => array(
                'title' => 'Erp Product Template Id',
                'width' => 'auto',
                'align' => 'text-center'
            ),
            'erp_product_id' => array(
                'title' => 'Erp Product Id',
                'width' => 'auto',
                'align' => 'text-center'
            ),
            'prestashop_product_id' => array(
                'title' => 'Prestashop Product Id',
                'align' => 'text-center',
                'width' => 'auto',
            ),
            'prestashop_product_attribute_id' => array(
                'title' => 'Prestashop Attribute Id',
                'align' => 'text-center',
               'width' => 'auto',
            ),
            'created_by' => array(
                'title' => 'Created By',
                'align' => 'text-center',
                'width' => 'auto',
            ),
            'created_on' => array(
                'title' => 'Created on',
                'align' => 'text-center',
                'width' => 'auto',
            ),
          
            'is_synch' => array(
                'title' => $this->l('Is Synchronised'),
                'align' => 'text-center',
                'type' => 'bool',
                'width' => 'auto',
                'icon' => array(
                1 => array(
                    'src' => 'enabled-2.gif',
                    'alt' => $this->l('Synchronized')
                ),
                0 => array(
                    'src' => 'disabled.gif',
                    'alt' => $this->l('Need Synchronization')
                ),
                ),
            )
        );
		
        $this->bulk_actions        = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $this->identifier          = 'id';
        parent::__construct();
        $index               = count($this->_conf)+1;
        $this->_conf[$index] = $this->l('All Product Combination(s) successfully exported to Odoo.');
    }

    public function postProcess() {	
        if (!$this->loadObject(true))
            return; 
        $log = new pob_log();
        if (Tools::getValue('synchronize')) {
            if (!class_exists('xmlrpc_client'))
                include_once 'xmlrpc.inc';
            $client = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/object");
            $sock   = new xmlrpc_client(Configuration::getGlobalValue("ErpUrl") . ":" . Configuration::getGlobalValue("ErpPort") . "/xmlrpc/common");
            $msg    = new xmlrpcmsg('login');
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpDatabase"), 'string'));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpUsername"), 'string'));
            $msg->addParam(new xmlrpcval(Configuration::getGlobalValue("ErpPassword"), 'string'));
            $resp = $sock->send($msg);
            if ($resp->faultCode()) {
                $error_message  = $resp->faultString();             
                $log->logMessage(__FILE__,__LINE__,'Connection Error : Invalid Odoo Information.',"ERROR");
                $this->errors[] = Tools::displayError($this->l('Connection Error : Invalid Odoo Information.' . $error_message));
            } else {
                $userId = $resp->value()->scalarval();
                if ($userId <= 0){                                  
                    $log->logMessage(__FILE__,__LINE__,'Invalid UserName or Password. Please check the Odoo configuration.',"ERROR");
                    $this->errors[] = Tools::displayError($this->l('Invalid UserName or Password. Please check the Odoo configuration.'));}
                else {
                    $presta_user = $this->context->employee->firstname . ' ' . $this->context->employee->lastname . ' (' . $this->context->employee->email . ')';
                    $combination    = new ErpProductCombination();
                    $check       = $combination->check_all_products($userId, $client, $presta_user);
                    $index       = count($this->_conf);
                    if ($check['is_error'] == 0 && $check['value'] == 1) {
                        Tools::redirectAdmin(self::$currentIndex . '&conf=' . $index . '&token=' . $this->token);
                        $log->logMessage(__FILE__,__LINE__,'All Product(s) successfully exported to Odoo.','INFO');
                    } elseif ($check['is_error'] == 0 && $check['value'] == 0) {
                        $this->displayInformation($this->l('No new Product(s) found.'));
                    } else {
                        if ($check['ids'])
                            $this->errors[] = Tools::displayError($this->l('Error in Prestashop`s Id : ' . $check['ids']));
                        $this->errors[] = Tools::displayError($this->l('Error Message(s):' . $check['error_message']));                                     
                        $log->logMessage(__FILE__,__LINE__,'Error in Prestashop`s Id : ' . $check['ids'].'/n'.'Error Message(s): ' . $check['error_message'],"CRITICAL");
                    }
                }
            }
        } else
            return parent::postProcess();              
    }
	
	
    public function processDelete(){        
        $record_id = Tools::getValue('id');
        if ($record_id){
            Db::getInstance()->delete('erp_product_merge','id='.$record_id);
        }       
    }

	public function initToolbar(){          
        $this->page_header_toolbar_btn['refresh'] = array(
            'href' => self::$currentIndex.'&synchronize=1&token=' . $this->token . '',
            'desc' => $this->l('Synchronize')
        );
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
        $this->toolbar_btn['wk_hide'] = array(
                                    'desc' => $this->l('')
                                );
    }
    public function renderView(){
        $id = Tools::getValue('id');
        $id_lang = $this->context->language->id;
        if ($id)
        {
            $obj_ps_erp_product = new ErpProductCombination();
            $product_tpl = $obj_ps_erp_product->product_details($id);           
            $this->context->smarty->assign('product_tpl', $product_tpl);
            $this->context->smarty->assign('modules_dir', _MODULE_DIR_);
        }
        return parent::renderView();
    }

    public function initPageHeaderToolbar(){
        parent::initPageHeaderToolbar();
        $this->context->smarty->clearAssign('help_link');   
    }
    
    public function setMedia(){
        parent::setMedia();
        $this->addJS(_MODULE_DIR_.'prestaerp/views/js/pob_js.js');
    }

	public function processBulkDelete(){
		$result = true;
		if (is_array($this->boxes) && !empty($this->boxes)){
			foreach ($this->boxes as $id_data) {
				Db::getInstance()->delete('erp_product_merge', 'id = ' . (int) $id_data);
			}
			return $result;
		}
		else
			$this->errors[] = Tools::displayError('You must select at least one element to delete.');       
    } 

}
?>