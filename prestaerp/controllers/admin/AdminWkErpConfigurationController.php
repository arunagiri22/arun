<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminWkErpConfigurationController extends ModuleAdminController{

    public function __construct(){
        $this->bootstrap = true;
        $this->table     = 'configuration';  
        $this->context   = Context::getContext();

        $this->fields_options = array(
            'general' => array(
                'title' =>  $this->l('POB Connection Configuration'),
                'fields' => array(                  
                    'ErpUrl' => array(
                            'title' => $this->l('Url'),                            
                            'required'=>true,
                            'type' => 'text'),
                    'ErpPort' => array(
                            'title' => $this->l('Port'),
                            'required'=>true,                            
                            'type' => 'text'),
                    'ErpDatabase' => array(
                            'title' => $this->l('Database'),
                            'required'=>true,                            
                            'type' => 'text'),
                    'ErpUsername' => array(
                            'title' => $this->l('User Name'), 
                            'required'=>true,                           
                            'type' => 'text')                  
                ),
                'submit' => array('title' => $this->l('Save and Stay'))
            ),
            'general1' => array(
                'title' =>  $this->l('Odoo Order Settings'),
                'fields' => array(
                    'ErpAutoStatusUpdate' => array(
                        'title' => $this->l('Automatically Update Status from Prestashop to Odoo'),
                        'desc' => $this->l('If Enabled, When status of an order is updated in Prestashop, then the status of order will automatically be updated in Odoo.'),
                        'cast' => 'intval', 'type' => 'bool'),
                    'ErpConfirmOrder' => array(
                        'title' => $this->l('Automatically Confirm Orders'),
                        'desc' => $this->l('If enabled, orders created from Prestashop end will automatically be created in confirm State in odoo'),
                        'cast' => 'intval',
                        'type' => 'bool'),                    
                    'ErpAutoInvoice' => array(
                        'title' => $this->l('Automatically Generate Invoice'),
                        'desc' => $this->l('If Enabled, Invoice will be automatically created in Odoo for exported orders'),
                        'validation' => 'isBool',
                        'cast' => 'intval',
                        'type' => 'bool'
                        ),
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
            'general2' => array(
                'title' =>  $this->l('Back Up Options'),
                'fields' => array(
                    'ErpDropTables' => array(
                        'title' => $this->l('Drop Database Tables during Update'),
                        'desc' => $this->l('Enable this If you wish to drop all mapping tables during module update'),
                        'cast' => 'intval', 'type' => 'bool'),                    
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
            'general3' => array(
                'title' =>  $this->l('Optional Features'),
                'fields' => array(
                    'ErpMultiLang' => array(
                        'title' => $this->l('Multi-Language'),
                        'desc' => $this->l('Before enabling this feature, make sure that you have installed the Multi-Language extension on Odoo end also'),
                        'cast' => 'intval', 'type' => 'bool'),
                    'ErpMultiShop' => array(
                        'title' => $this->l('Multi-Shop'),
                        'desc' => $this->l('Before enabling this feature, make sure that you have installed the Multi-Shop extension on Odoo end also'),
                        'cast' => 'intval',
                        'type' => 'bool'),
                ),
                'submit' => array('title' => $this->l('Save'))
            ), 
            'ErpResetDb' => array(
                'title' =>  $this->l('Reset POB mapping Tables'),
                'image' => '../img/admin/exchangesrate.gif',
                'description' => $this->l('Using this Option will delete all data in POB tables, Please make sure you are really want to do this, or you can ask for help from POB`s support Team.'),
                'submit' => array(
                    'title' => $this->l('Reset All POB Tables Now'),
                    'name' => 'ErpResetDb'
                )
            )           
        );
        $type = 'text';
        if ($this->context->employee->id_profile!=='1')
            $type= 'hidden'; 

        $this->fields_options['general']['fields']['ErpPassword'] = array(
                            'title' => $this->l('Password'),
                            'required'=>true,                            
                            'type' => $type);

        parent::__construct();
        $index               = count($this->_conf)+1;
        $this->_conf[$index] = $this->l('Data in all POB Tables are successfully deleted.');
    }

    public function setMedia(){
        parent::setMedia();
        $this->addJS(_MODULE_DIR_.'prestaerp/views/js/pob_js.js');
    }

    public function initPageHeaderToolbar(){
        parent::initPageHeaderToolbar();
        $this->context->smarty->clearAssign('help_link');   
    }
    

 
    public function initProcess(){
        if (Tools::isSubmit('ErpResetDb')){
            $conf_obj = new WkErpConfiguration();
            $conf_obj->deletePOBTables();
        }
    }


    public function postProcess(){
        $flag1=$flag2=$flag3=$flag4=True;
        $conf_obj = new WkErpConfiguration();    
        if (Tools::getIsset('ErpUrl') && $conf_obj->AllowChanges()){ 
            $msg = '';           
            $flag1 = $conf_obj->UpdateConfig();
            if ($flag1){
                $test_conn = $conf_obj->TestConnection();               
            }
            else{
                $msg = 'All Fields are mandatory';
            }
            if(Configuration::getGlobalValue('ErpMultiShop'))
                $flag2 = $conf_obj->CheckMultiShop();
                if (!$flag2){
                    $msg = $msg. "Sorry, 'POB : Multi-Shop Extension' not found on this PrestaShop`s server. Please contact our support team for further details" ;
                   
                }
            if(Configuration::getGlobalValue('ErpMultiLang')){
                $flag3 = $conf_obj->CheckMultiLanguage();  
                if (!$flag3)
                    $msg = $msg. "Sorry, 'POB : Multi-Language Extension' not found on this PrestaShop`s server. Please contact our support team for further details" ;         
                }
            if(Configuration::getGlobalValue('ErpAutoInvoice'))   
                $flag4 = $conf_obj->CheckOrderInvoice();
                if (!$flag4)
                     $msg = $msg. "Sorry, You cannot set Automatic Invoicing to 'True' if Automatic Order Confirmation is 'False'" ;
            if($flag1 and $flag2 and $flag3 and $flag4 and $test_conn['status']){
                $message = $test_conn['message'];
                $this->displayInformation($this->l($message));
                $this->displayInformation($this->l($msg));
                // Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
            }
            else{
                if (!$test_conn['status']){
                    $msg = $test_conn['message'];
                }
                $this->errors[] = Tools::displayError($this->l($msg));
            }
        }     
        parent::processSave();
        
    }
    

}
