<?php
/*
* 2007-2012 PrestaShop
* NOTICE OF LICENSE
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
* DISCLAIMER
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*  @author Webkul Sogtware Pvt. Ltd <www.webkul.com>
*  @copyright  2009-2015 Webkul Software Pvt. Ltd.
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminErpOrdersController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table               = 'erp_order_merge';
        $this->className           = 'ErpOrders';
        $this->lang                = false;
        $this->add                 = true;
        $this->edit                = true;
        $this->delete              = true;
        $this->view                = true;
        // $this->list_no_link        = true;
        $this->imageType           = 'jpg';
        $this->context             = Context::getContext();
        $this->addRowAction('delete');
        $this->addRowAction('view');

        $this->fields_list         = array(
            'id' => array(
                'title' => 'Id',
                'align' => 'center',
                'width' => 'auto',
            ),
            'prst_order_id' => array(
                'title' => 'Prestashop Order Id',
                'align' => 'center',
                'width' => 'auto',
            ),
            'erp_order_id' => array(
                'title' => 'Erp Order Id',
                'align' => 'center',
                'width' => 'auto',
            ),
            'erp_order_name' => array(
                'title' => 'Erp Order Reference',
                'align' => 'center',
                'width' => 'auto',
            ),
            'created_on' => array(
                'title' => 'Created on',
                'align' => 'center',
                'width' => 'auto',
            ),
        );
        $this->bulk_actions        = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $this->identifier          = 'id';
        parent::__construct();
    }	
	
    public function processDelete(){        
        $record_id = Tools::getValue('id');
        if ($record_id){
            Db::getInstance()->delete('erp_order_merge','id='.$record_id);
        }       
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
        $this->context->smarty->clearAssign('help_link');   
    }

	public function initToolbar(){
        parent::initToolbar();
        
        unset($this->toolbar_btn['new']);
        $this->toolbar_btn['wk_hide'] = array(
                                    'desc' => $this->l('')
                                );        
    }

	public function processBulkDelete(){
		$result = true;
		if (is_array($this->boxes) && !empty($this->boxes)){
			foreach ($this->boxes as $id_data) {
				Db::getInstance()->delete('erp_order_merge', 'id = ' . (int) $id_data);
			}
			return $result;
		}
		else
			$this->errors[] = Tools::displayError('You must select at least one element to delete.');
       
    }
    public function renderView(){
        $id = Tools::getValue('id');
        $id_lang = $this->context->language->id;
        if ($id)
        {
            $obj_ps_erp_order = new ErpOrders();
            $order_tpl = $obj_ps_erp_order->order_details($id);           
            $this->context->smarty->assign('order_tpl', $order_tpl);
            $this->context->smarty->assign('modules_dir', _MODULE_DIR_);
        }
        return parent::renderView();
    }

}
?>